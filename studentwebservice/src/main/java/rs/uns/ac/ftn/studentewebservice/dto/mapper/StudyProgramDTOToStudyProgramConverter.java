package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.StudyProgramDTO;
import rs.uns.ac.ftn.studentewebservice.entity.StudyProgram;
import rs.uns.ac.ftn.studentewebservice.entity.TypeOfStudy;
import rs.uns.ac.ftn.studentewebservice.service.TypeOfStudyService;

@Component
public class StudyProgramDTOToStudyProgramConverter implements Converter<StudyProgramDTO, StudyProgram> {

    @Autowired
    TypeOfStudyService typeOfStudyService;

    @Override
    public StudyProgram convert(StudyProgramDTO studyProgramDTO) {
        StudyProgram studyProgram = new StudyProgram();
        studyProgram.setId(studyProgramDTO.getId());
        studyProgram.setName(studyProgramDTO.getName());
        studyProgram.setDeleted(studyProgramDTO.isDeleted());
        TypeOfStudy typeOfStudy = typeOfStudyService.findById(studyProgramDTO.getTypeOfStudyDTO().getId());
        if (typeOfStudy != null) {
            studyProgram.setTypeOfStudy(typeOfStudy);
        } else {
            //TODO izazovi izuzetak
        }
        return studyProgram;
    }
}
