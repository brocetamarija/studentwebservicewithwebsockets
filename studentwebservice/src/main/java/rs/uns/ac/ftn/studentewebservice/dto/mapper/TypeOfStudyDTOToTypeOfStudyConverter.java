package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.TypeOfStudyDTO;
import rs.uns.ac.ftn.studentewebservice.entity.TypeOfStudy;

@Component
public class TypeOfStudyDTOToTypeOfStudyConverter implements Converter<TypeOfStudyDTO, TypeOfStudy> {

    @Override
    public TypeOfStudy convert(TypeOfStudyDTO typeOfStudyDTO) {
        TypeOfStudy typeOfStudy = new TypeOfStudy();
        typeOfStudy.setId(typeOfStudyDTO.getId());
        typeOfStudy.setName(typeOfStudyDTO.getName());
        typeOfStudy.setDeleted(typeOfStudyDTO.isDeleted());
        return typeOfStudy;
    }
}
