package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.ExamPeriod;
import rs.uns.ac.ftn.studentewebservice.repository.ExamPeriodRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.BadRequestException;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ExamPeriodService {

    @Autowired
    ExamPeriodRepository examPeriodRepository;

    public List<ExamPeriod> findAll() {
        return examPeriodRepository.findAll();
    }

    public Page<ExamPeriod> findAll(Pageable pageable) {
        return examPeriodRepository.findAll(pageable);
    }


    public ExamPeriod findById(Long id) {
        return examPeriodRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Exam period not found!", "Exam period with given id: " + id + " is not found!" ));
    }

    public ExamPeriod save(ExamPeriod examPeriod) {
        return examPeriodRepository.save(examPeriod);
    }

    public ExamPeriod edit(ExamPeriod ep, Long id){
        ExamPeriod examPeriod = findById(id);
        examPeriod = examPeriodRepository.save(ep);
        return examPeriod;
    }

    public void delete(Long id) {
        ExamPeriod examPeriod = findById(id);
        if (examPeriod.getExamRealizations().isEmpty()) {
            examPeriod.setDeleted(true);
            examPeriodRepository.save(examPeriod);
        } else {
            throw new BadRequestException("Exam period cannot be deleted!",
                    "Exam period cannot be deleted because there is exam realizations for this exam period!");
        }

    }

    /**
     * Lista ispitnih rokova koji su u toku ili ce se tek odrzati
     *
     * Iz liste svih ispitnih rokova filtriraju se rokovi gde je danasnji datum pre krajnjeg datuma roka
     * @return examPeriods Povratna vrednost je lista filtriranih ispitnih rokova
     * */
    public List<ExamPeriod> findAllFuture() {
        List<ExamPeriod> examPeriods = examPeriodRepository.findAll();
        Date today = new Date();
        examPeriods = examPeriods.stream().filter(examPeriod -> today.before(examPeriod.getEndDate())).collect(Collectors.toList());

        return examPeriods;
    }
}
