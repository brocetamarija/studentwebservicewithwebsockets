package rs.uns.ac.ftn.studentewebservice.entity;


import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Where(clause = "deleted = false")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "streetName", nullable = false, length = 30)
    private String streetName;

    @Column(name = "streetNumber", nullable = false)
    private int streetNumber;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @ManyToOne
    @JoinColumn(name = "placeId", referencedColumnName = "id")
    private Place place;

    @OneToMany(mappedBy = "address", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<User> users = new HashSet<>();

    public Address() {
    }

    public Address(String streetName, int streetNumber, boolean deleted, Place place, Set<User> users) {
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.deleted = deleted;
        this.place = place;
        this.users = users;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Address a = (Address) o;
        return Objects.equals(id, a.id);
    }

    public void add(User user) {
        if (user.getAddress() != null) {
            user.getAddress().getUsers().remove(user);
        }
        user.setAddress(this);
        getUsers().add(user);
    }

    public void remove(User user) {
        user.setAddress(null);
        getUsers().remove(user);
    }
}
