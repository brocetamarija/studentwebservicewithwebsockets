package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.Document;
import rs.uns.ac.ftn.studentewebservice.repository.DocumentRepository;
import rs.uns.ac.ftn.studentewebservice.util.DateFormat;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class DocumentService {

    @Autowired
    DocumentRepository documentRepository;

    @Autowired
    FileService fileService;

    @Autowired
    StudentService studentService;

    @Autowired
    DateFormat dateFormat;

    public Page<Document> findAll(Pageable pageable) {
        return documentRepository.findAll(pageable);
    }

    public List<Document> findByStudentId(Long id) {
        return documentRepository.findByStudent_Id(id);
    }

    public Page<Document> findByStudentIdPage(Long id, Pageable pageable) {
        return documentRepository.findByStudent_Id(id, pageable);
    }

    public Document findById(Long id) {
        return documentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Document Not Found!", "Document with given id: " + id + " is not found!"));
    }

    public Document save(Document document) {
        return documentRepository.save(document);
    }

    public Document edit(Document d, Long id){
        Document document = findById(id);
        document = documentRepository.save(d);
        return document;
    }

    public Document delete(Long id) {
        Document document = findById(id);

        document.setDeleted(true);
        documentRepository.save(document);

        return document;
    }

    public void deleteByStudentId(Long id) {
        List<Document> documents = findByStudentId(id);
        for (Document d : documents) {
            d.setDeleted(true);
            documentRepository.save(d);
        }
    }

    /**
     * Dodavanje dokumenta
     * Dodavanje dokumenta se realizuje tako sto se iz fajla izvuku informacije o putanji i datumu kad je fajl dodat
     *
     * @param document Parametar je dokument koji je preuzet sa klijentske strane
     * @return d Povratna vrednost je dokument koji je sacuvan
     */
    public Document add(Document document) {
        Document d = new Document();
        Path path = Paths.get(fileService.getResourceFilePath(fileService.getDATA_DIR_PATH()).getAbsolutePath() + File.separator + document.getFilePath());
        try {
            d.setDate(dateFormat.convertFileTime(Files.getLastModifiedTime(path)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        d.setFilePath(document.getFilePath());
        d.setType(document.getType());
        d.setStudent(document.getStudent());

        return documentRepository.save(d);
    }

    /**
     * Konvertovanje datuma dobijenog iz fajla u format koji nam odgovara za prikaz i cuvanje u bazi
     *
     * @param fileTime Parametar koji se dobija iz dokumenta
     * @return date Konvertovani datum
     */
    /*private Date convertFileTime(FileTime fileTime) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;
        try {
            date = dateFormat.parse(dateFormat.format(fileTime.toMillis()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }*/

}
