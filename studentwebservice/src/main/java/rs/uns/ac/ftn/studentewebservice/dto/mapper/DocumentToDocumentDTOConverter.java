package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.DocumentDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Document;

import java.util.ArrayList;
import java.util.List;

@Component
public class DocumentToDocumentDTOConverter implements Converter<Document, DocumentDTO> {

    @Autowired
    StudentToStudentDTOConverter toStudentDTOConverter;

    @Override
    public DocumentDTO convert(Document document) {
        DocumentDTO documentDTO = new DocumentDTO();
        documentDTO.setId(document.getId());
        documentDTO.setType(document.getType());
        documentDTO.setFilePath(document.getFilePath());
        documentDTO.setDate(document.getDate());
        documentDTO.setDeleted(document.isDeleted());
        documentDTO.setStudentDTO(toStudentDTOConverter.convert(document.getStudent()));
        return documentDTO;
    }

    public List<DocumentDTO> convert(List<Document> documents){
        List<DocumentDTO> retVal = new ArrayList<>();
        for(Document d:documents){
            retVal.add(convert(d));
        }
        return retVal;
    }
}
