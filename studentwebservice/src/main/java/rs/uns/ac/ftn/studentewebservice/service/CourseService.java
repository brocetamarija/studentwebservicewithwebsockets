package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.Course;
import rs.uns.ac.ftn.studentewebservice.entity.Enrollment;
import rs.uns.ac.ftn.studentewebservice.entity.Exam;
import rs.uns.ac.ftn.studentewebservice.repository.CourseRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.BadRequestException;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CourseService {

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    TeachingService teachingService;

    @Autowired
    EnrollmentService enrollmentService;

    @Autowired
    ExamService examService;

    public Page<Course> findAll(Pageable pageable) {
        return courseRepository.findAll(pageable);
    }

    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    public Course findById(Long id) {
        return courseRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Course Not Found!", "Course with given id: " + id + " is not found!"));
    }

    public List<Course> findByStudyProgramId(Long id) {
        return courseRepository.findByStudyProgram_Id(id);
    }

    public Course save(Course course) {
        return courseRepository.save(course);
    }

    public Course edit(Course c, Long id){
        Course course = findById(id);

        course = courseRepository.save(c);
        return course;
    }

    public void delete(Long id) {
        Course course = findById(id);

        if (course.getExamRealizations().isEmpty() && course.getEnrollments().isEmpty()) {
            course.setDeleted(true);
            courseRepository.save(course);
            teachingService.deleteByCourseId(id);
        } else {
            throw new BadRequestException("Cannot be deleted!", "Course cannot be deleted because there is exam realizations or enrollments for this course!");
        }
    }

    public void deleteByStudyProgramId(Long id) {
        List<Course> courses = findByStudyProgramId(id);
        for (Course c : courses) {
            c.setDeleted(true);
            courseRepository.save(c);
        }
    }

    public Page<Course> findByStudyProgramPageable(Long id, Pageable pageable) {
        return courseRepository.findByStudyProgram_Id(id, pageable);
    }

    public List<Course> findByStudyProgramAndYearOfStudy(Long id, int year) {
        return courseRepository.findByStudyProgram_IdAndYearOfStudy(id, year);
    }

    /**
     * Pronalazenje predmeta koje student slusa a nije polozio
     *
     * @param studentID Parametar po kome se pronalazi student
     * @return List Vraca listu predmeta
     */
    public List<Course> unfinishedCourses(Long studentID) {
        List<Exam> exams = examService.findByStudentId(studentID);
        List<Enrollment> enrollments = enrollmentService.findByStudentId(studentID);
        List<Course> courses =
                enrollments.stream()
                        .map(e -> e.getCourse())
                        .collect(Collectors.toList());
        List<Course> passedCourses =
                exams.stream()
                        .filter(e -> e.getFinalGrade() >= 6)
                        .map(e -> e.getExamRealization().getCourse())
                        .collect(Collectors.toList());

        courses.removeIf(c -> passedCourses.contains(c));

        return courses;
    }

}
