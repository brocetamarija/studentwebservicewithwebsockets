package rs.uns.ac.ftn.studentewebservice.dto;

import java.util.Date;

public class DocumentDTO {

    private Long id;
    private String type;
    private String filePath;
    private Date date;
    private boolean deleted;
    private StudentDTO studentDTO;

    public DocumentDTO() {}

    public DocumentDTO(Long id, String type, String filePath, Date date, boolean deleted, StudentDTO studentDTO) {
        this.id = id;
        this.type = type;
        this.filePath = filePath;
        this.date = date;
        this.deleted = deleted;
        this.studentDTO = studentDTO;
    }

    /*public DocumentDTO(Document document){
        this(document.getId(),
                document.getType(),
                document.getFilePath(),
                document.isDeleted(),
                new StudentDTO(document.getStudent()));
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public StudentDTO getStudentDTO() {
        return studentDTO;
    }

    public void setStudentDTO(StudentDTO studentDTO) {
        this.studentDTO = studentDTO;
    }
}
