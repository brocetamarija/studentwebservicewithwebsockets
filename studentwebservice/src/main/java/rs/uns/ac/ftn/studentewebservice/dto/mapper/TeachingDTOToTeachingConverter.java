package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.TeachingDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Course;
import rs.uns.ac.ftn.studentewebservice.entity.Teacher;
import rs.uns.ac.ftn.studentewebservice.entity.Teaching;
import rs.uns.ac.ftn.studentewebservice.service.CourseService;
import rs.uns.ac.ftn.studentewebservice.service.TeacherService;

@Component
public class TeachingDTOToTeachingConverter implements Converter<TeachingDTO, Teaching> {

    @Autowired
    TeacherService teacherService;

    @Autowired
    CourseService courseService;

    @Override
    public Teaching convert(TeachingDTO teachingDTO) {
        Teaching teaching = new Teaching();
        teaching.setId(teachingDTO.getId());
        teaching.setDeleted(teachingDTO.isDeleted());
        Teacher teacher = teacherService.findById(teachingDTO.getTeacherDTO().getId());
        if(teacher != null){
            teaching.setTeacher(teacher);
        }else{
            //TODO izazovi izuzetak
        }

        Course course = courseService.findById(teachingDTO.getCourseDTO().getId());
        if(course != null){
            teaching.setCourse(course);
        }else{
            //TODO izazovi izuzetak
        }
        return teaching;
    }
}
