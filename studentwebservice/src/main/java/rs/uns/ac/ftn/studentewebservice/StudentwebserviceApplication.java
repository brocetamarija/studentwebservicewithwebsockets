package rs.uns.ac.ftn.studentewebservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentwebserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentwebserviceApplication.class, args);
	}
}
