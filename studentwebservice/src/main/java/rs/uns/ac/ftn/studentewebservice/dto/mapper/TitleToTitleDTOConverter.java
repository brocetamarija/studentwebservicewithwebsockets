package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.TitleDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Title;

import java.util.ArrayList;
import java.util.List;

@Component
public class TitleToTitleDTOConverter implements Converter<Title, TitleDTO> {


    @Override
    public TitleDTO convert(Title title) {
        TitleDTO titleDTO = new TitleDTO();
        titleDTO.setId(title.getId());
        titleDTO.setName(title.getName());
        titleDTO.setDeleted(title.isDeleted());
        return titleDTO;
    }

    public List<TitleDTO> convert(List<Title> titles){
        List<TitleDTO> retVal = new ArrayList<>();
        for(Title t:titles){
            retVal.add(convert(t));
        }
        return retVal;
    }
}
