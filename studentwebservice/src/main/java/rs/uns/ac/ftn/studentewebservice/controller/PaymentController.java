package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.PaymentDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.PaymentDTOToPaymentConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.PaymentToPaymentDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.Payment;
import rs.uns.ac.ftn.studentewebservice.service.PaymentService;

import java.util.List;

@RestController
@RequestMapping(value = "api/payments")
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @Autowired
    PaymentToPaymentDTOConverter toPaymentDTOConverter;

    @Autowired
    PaymentDTOToPaymentConverter toPaymentConverter;


    @GetMapping(value = "/{id}")
    public ResponseEntity<PaymentDTO> findById(@PathVariable("id") Long id){
        return new ResponseEntity<>(toPaymentDTOConverter.convert(paymentService.findById(id)), HttpStatus.OK);
    }

    @GetMapping(value = "/student/{id}")
    public ResponseEntity<List<PaymentDTO>> findByStudentId(@PathVariable("id") Long id, Pageable pageable){
        Page<Payment> payments = paymentService.findByStudentId(id, pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages", Integer.toString(payments.getTotalPages()));
        headers.add("access-control-expose-headers", "totalPages");
        return new ResponseEntity<>(toPaymentDTOConverter.convert(payments.getContent()), headers, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<PaymentDTO> create(@RequestBody PaymentDTO paymentDTO){
        Payment payment = paymentService.create(toPaymentConverter.convert(paymentDTO));
        return new ResponseEntity<>(toPaymentDTOConverter.convert(payment), HttpStatus.CREATED);
    }

    @PostMapping(value = "/exam-registration-payment")
    public ResponseEntity<PaymentDTO> addPaymentForExamRegistration(@RequestBody PaymentDTO paymentDTO){
        Payment payment = paymentService.addPaymentForExamRegistration(toPaymentConverter.convert(paymentDTO));
        return new ResponseEntity<>(toPaymentDTOConverter.convert(payment), HttpStatus.OK);
    }

}
