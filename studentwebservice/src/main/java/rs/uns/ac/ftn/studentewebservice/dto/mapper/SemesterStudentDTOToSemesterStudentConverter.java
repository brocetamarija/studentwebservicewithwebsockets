package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.SemesterStudentDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Semester;
import rs.uns.ac.ftn.studentewebservice.entity.SemesterStudent;
import rs.uns.ac.ftn.studentewebservice.entity.Student;
import rs.uns.ac.ftn.studentewebservice.service.SemesterService;
import rs.uns.ac.ftn.studentewebservice.service.StudentService;

@Component
public class SemesterStudentDTOToSemesterStudentConverter implements Converter<SemesterStudentDTO, SemesterStudent> {
    @Autowired
    StudentService studentService;

    @Autowired
    SemesterService semesterService;

    @Override
    public SemesterStudent convert(SemesterStudentDTO semesterStudentDTO) {
        SemesterStudent semesterStudent = new SemesterStudent();
        semesterStudent.setId(semesterStudentDTO.getId());
        semesterStudent.setCertified(semesterStudentDTO.isCertified());
        semesterStudent.setSemesterCertificationPrice(semesterStudentDTO.getSemesterCertificationPrice());
        semesterStudent.setDeleted(semesterStudentDTO.isDeleted());
        Student student = studentService.findById(semesterStudentDTO.getStudentDTO().getId());
        if(student != null){
            semesterStudent.setStudent(student);
        }else{
            //TODO Izazovi izuzetak
        }
        Semester semester = semesterService.findById(semesterStudentDTO.getSemesterDTO().getId());
        if(semester != null){
            semesterStudent.setSemester(semester);
        }else{
            //TODO izazovi izuzetak
        }
        return semesterStudent;
    }
}
