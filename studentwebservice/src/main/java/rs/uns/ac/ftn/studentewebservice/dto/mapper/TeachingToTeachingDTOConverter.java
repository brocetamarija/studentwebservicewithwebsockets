package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.TeachingDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Teaching;

import java.util.ArrayList;
import java.util.List;

@Component
public class TeachingToTeachingDTOConverter implements Converter<Teaching, TeachingDTO> {

    @Autowired
    TeacherToTeacherDTOConverter toTeacherDTOConverter;

    @Autowired
    CourseToCourseDTOConverter toCourseDTOConverter;

    @Override
    public TeachingDTO convert(Teaching teaching) {
        TeachingDTO teachingDTO = new TeachingDTO();
        teachingDTO.setId(teaching.getId());
        teachingDTO.setDeleted(teaching.isDeleted());
        teachingDTO.setTeacherDTO(toTeacherDTOConverter.convert(teaching.getTeacher()));
        teachingDTO.setCourseDTO(toCourseDTOConverter.convert(teaching.getCourse()));
        return teachingDTO;
    }

    public List<TeachingDTO> convert(List<Teaching> teachings){
        List<TeachingDTO> retVal = new ArrayList<>();
        for(Teaching t:teachings){
            retVal.add(convert(t));
        }
        return retVal;
    }
}
