package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.StudentDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.StudentDTOToStudentConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.StudentToStudentDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.Student;
import rs.uns.ac.ftn.studentewebservice.service.StudentService;

import java.util.List;

@RestController
@RequestMapping(value = "api/students")
public class StudentController {

    @Autowired
    StudentService studentService;

    @Autowired
    StudentToStudentDTOConverter toStudentDTOConverter;

    @Autowired
    StudentDTOToStudentConverter toStudentConverter;

    @GetMapping
    public ResponseEntity<List<StudentDTO>> findAllPageable(Pageable pageable){
        Page<Student> students = studentService.findAll(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(students.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");
        return new ResponseEntity<>(toStudentDTOConverter.convert(students.getContent()), headers, HttpStatus.OK);
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<StudentDTO>> findAll(){
        return new ResponseEntity<>(toStudentDTOConverter.convert(studentService.findAll()), HttpStatus.OK);
    }

    @GetMapping(value = "/study-program/{studyProgramID}/study-type/{studyTypeID}")
    public ResponseEntity<List<StudentDTO>> findByStudyProgramAndStudyType(@PathVariable("studyProgramID") Long studyProgramID,
                                                                           @PathVariable("studyTypeID") Long studyTypeID){
        return new ResponseEntity<>(toStudentDTOConverter.convert(studentService.findByStudyProgramAndStudyType(studyProgramID,studyTypeID)),
                HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<StudentDTO> findById(@PathVariable("id") Long id){
        return new ResponseEntity<>(toStudentDTOConverter.convert(studentService.findById(id)), HttpStatus.OK);
    }

    @GetMapping(value = "/by-username/{username}")
    public ResponseEntity<StudentDTO> findByUsername(@PathVariable("username") String username){
        return new ResponseEntity<>(toStudentDTOConverter.convert(studentService.findByUsername(username)), HttpStatus.OK);
    }

    @GetMapping(value = "/study-program/{id}")
    public ResponseEntity<List<StudentDTO>> findByStudyProgram(@PathVariable("id") Long id, Pageable pageable){
        Page<Student> students = studentService.findByStudyProgram(id, pageable);

        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages", Integer.toString(students.getTotalPages()));
        headers.add("access-control-expose-headers", "totalPages");

        return new ResponseEntity<>(toStudentDTOConverter.convert(students.getContent()),headers, HttpStatus.OK);
    }

    @GetMapping(value = "/by-registered-exam/{examRealizationID}")
    public ResponseEntity<List<StudentDTO>> findStudentsWhoRegisterExam(@PathVariable("examRealizationID") Long examRealizationID){
        return new ResponseEntity<>(toStudentDTOConverter.convert(studentService.studentsWhoRegisterExam(examRealizationID)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<StudentDTO> create(@RequestBody StudentDTO studentDTO){
        Student student = studentService.add(toStudentConverter.convert(studentDTO));
        return new ResponseEntity<>(toStudentDTOConverter.convert(student), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<StudentDTO> update(@RequestBody StudentDTO studentDTO, @PathVariable("id") Long id){
        Student student = studentService.edit(toStudentConverter.convert(studentDTO), id);
        return new ResponseEntity<>(toStudentDTOConverter.convert(student), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        studentService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
