package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.Address;
import rs.uns.ac.ftn.studentewebservice.entity.Authority;
import rs.uns.ac.ftn.studentewebservice.entity.User;
import rs.uns.ac.ftn.studentewebservice.entity.UserAuthority;
import rs.uns.ac.ftn.studentewebservice.repository.UserRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.BadRequestException;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;


@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AddressService addressService;

    @Autowired
    AuthorityService authorityService;

    @Autowired
    UserAuthorityService userAuthorityService;

    @Autowired
    PasswordEncoder passwordEncoder;


    public Page<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    public User findById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("User Not Found!", "User with given id: " + id + " is not found!"));
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public void delete(Long id) {
        User user = findById(id);
        if (user.getStudent() != null && user.getTeacher() != null) {
            throw new BadRequestException("Cannot be deleted!", "User cannot be deleted!");
        } else {
            user.setDeleted(true);
            userRepository.save(user);
        }
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public Page<User> findByUserAuthority(String authority, Pageable pageable) {
        return userRepository.findByUserAuthorities_Authority_Name(authority, pageable);
    }

    public Page<User> getAdmins(Pageable pageable) {
        return findByUserAuthority("ROLE_ADMIN", pageable);
    }

    public User add(User u) {
        Authority authority = authorityService.findByName("ROLE_ADMIN");
        User user = new User();
        user.setId(u.getId());
        user.setFirstName(u.getFirstName());
        user.setLastName(u.getLastName());
        user.setUsername(u.getEmail());
        user.setPassword(passwordEncoder.encode("admin"));
        user.setEmail(u.getEmail());
        user.setDeleted(false);
        UserAuthority userAuthority = new UserAuthority();
        userAuthority.setUser(user);
        userAuthority.setAuthority(authority);
        user.getUserAuthorities().add(userAuthority);
        user.setType(authority.getName());
        Address a = addressService.save(u.getAddress());
        user.setAddress(a);
        userRepository.save(user);

        return user;
    }

    public User edit(User u, Long id) {
        User user = findById(id);
        Address a = u.getAddress();
        addressService.save(a);
        user.setAddress(a);
        user = userRepository.save(u);
        return user;

    }

    public User addUserForStudentOrTeacher(User user, Authority authority) {
        user.setUsername(user.getEmail());
        user.setPassword(passwordEncoder.encode("ftn"));
        user.setDeleted(false);
        UserAuthority userAuthority = new UserAuthority();
        userAuthority.setAuthority(authority);
        userAuthority.setUser(user);
        user.getUserAuthorities().add(userAuthority);
        user.setType(authority.getName());
        Address a = addressService.save(user.getAddress());
        user.setAddress(a);
        return userRepository.save(user);
    }


}
