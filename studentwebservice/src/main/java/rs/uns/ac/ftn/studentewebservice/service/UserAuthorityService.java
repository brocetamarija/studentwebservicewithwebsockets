package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.UserAuthority;
import rs.uns.ac.ftn.studentewebservice.repository.UserAuthorityRepository;

@Service
public class UserAuthorityService {

    @Autowired
    UserAuthorityRepository userAuthorityRepository;

    public UserAuthority save(UserAuthority userAuthority) {
        return userAuthorityRepository.save(userAuthority);
    }
}
