package rs.uns.ac.ftn.studentewebservice.dto;

import java.util.Date;

public class ExamPeriodDTO {

    private Long id;
    private String name;
    private Date startDate;
    private Date endDate;
    private boolean deleted;

    public ExamPeriodDTO() {}

    public ExamPeriodDTO(Long id, String name, Date startDate, Date endDate, boolean deleted) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.deleted = deleted;
    }

    /*public ExamPeriodDTO(ExamPeriod examPeriod){
        this(examPeriod.getId(),
                examPeriod.getName(),
                examPeriod.getStartDate(),
                examPeriod.getEndDate(),
                examPeriod.isDeleted());
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
