package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.TeacherTitleDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.TeacherTitleDTOToTeacherTitleConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.TeacherTitleToTeacherTitleDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.TeacherTitle;
import rs.uns.ac.ftn.studentewebservice.service.TeacherTitleService;

import java.util.List;

@RestController
@RequestMapping(value = "api/teacher-titles")
public class TeacherTitleController {

    @Autowired
    TeacherTitleService teacherTitleService;

    @Autowired
    TeacherTitleToTeacherTitleDTOConverter toTeacherTitleDTOConverter;

    @Autowired
    TeacherTitleDTOToTeacherTitleConverter toTeacherTitleConverter;


    /*@GetMapping
    public ResponseEntity<List<TeacherTitleDTO>> findAll(Pageable pageable){
        Page<TeacherTitle> teacherTitles = teacherTitleService.findAll(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(teacherTitles.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");

        return new ResponseEntity<>(toTeacherTitleDTOConverter.convert(teacherTitles.getContent()), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<TeacherTitleDTO> findById(@PathVariable("id") Long id){
        return new ResponseEntity<>(toTeacherTitleDTOConverter.convert(teacherTitleService.findById(id)), HttpStatus.OK);
    }*/

    @GetMapping(value = "/teacher/{id}")
    public ResponseEntity<List<TeacherTitleDTO>> findByTeacherId(@PathVariable("id") Long id){
        return new ResponseEntity<>(toTeacherTitleDTOConverter.convert(teacherTitleService.findByTeacherId(id)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TeacherTitleDTO> create(@RequestBody TeacherTitleDTO teacherTitleDTO){
        TeacherTitle teacherTitle = teacherTitleService.save(toTeacherTitleConverter.convert(teacherTitleDTO));
        return new ResponseEntity<>(toTeacherTitleDTOConverter.convert(teacherTitle), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<TeacherTitleDTO> update(@RequestBody TeacherTitleDTO teacherTitleDTO, @PathVariable("id") Long id){
        TeacherTitle teacherTitle = teacherTitleService.edit(toTeacherTitleConverter.convert(teacherTitleDTO),id);
        return new ResponseEntity<>(toTeacherTitleDTOConverter.convert(teacherTitle), HttpStatus.OK);
    }

}
