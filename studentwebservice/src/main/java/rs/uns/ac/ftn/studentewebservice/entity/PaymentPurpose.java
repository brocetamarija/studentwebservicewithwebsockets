package rs.uns.ac.ftn.studentewebservice.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Where(clause = "deleted = false")
public class PaymentPurpose {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, length = 30)
    private String name;

    @Column(name = "deleted")
    private boolean deleted;

    @OneToMany(mappedBy = "paymentPurpose", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Payment> payments = new HashSet<>();

    public PaymentPurpose() {
    }

    public PaymentPurpose(String name, boolean deleted, Set<Payment> payments) {
        this.name = name;
        this.deleted = deleted;
        this.payments = payments;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Set<Payment> getPayments() {
        return payments;
    }

    public void setPayments(Set<Payment> payments) {
        this.payments = payments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PaymentPurpose pp = (PaymentPurpose) o;
        return Objects.equals(id, pp.id);
    }

    public void add(Payment payment) {
        if (payment.getPaymentPurpose() != null) {
            payment.getPaymentPurpose().getPayments().remove(payment);
        }
        payment.setPaymentPurpose(this);
        getPayments().add(payment);
    }

    public void remove(Payment payment) {
        payment.setPaymentPurpose(null);
        getPayments().remove(payment);
    }
}
