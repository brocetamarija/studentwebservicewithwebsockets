package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.TeacherTitleDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Teacher;
import rs.uns.ac.ftn.studentewebservice.entity.TeacherTitle;
import rs.uns.ac.ftn.studentewebservice.entity.Title;
import rs.uns.ac.ftn.studentewebservice.service.TeacherService;
import rs.uns.ac.ftn.studentewebservice.service.TitleService;

@Component
public class TeacherTitleDTOToTeacherTitleConverter implements Converter<TeacherTitleDTO, TeacherTitle> {

    @Autowired
    TitleService titleService;

    @Autowired
    TeacherService teacherService;

    @Override
    public TeacherTitle convert(TeacherTitleDTO teacherTitleDTO) {
        TeacherTitle teacherTitle = new TeacherTitle();
        teacherTitle.setId(teacherTitleDTO.getId());
        teacherTitle.setStartDate(teacherTitleDTO.getStartDate());
        teacherTitle.setEndDate(teacherTitleDTO.getEndDate());
        teacherTitle.setDeleted(teacherTitleDTO.isDeleted());
        Teacher teacher = teacherService.findById(teacherTitleDTO.getTeacherDTO().getId());
        if(teacher != null){
            teacherTitle.setTeacher(teacher);
        }else{
            //TODO izazovi izuzetak
        }

        Title title = titleService.findById(teacherTitleDTO.getTitleDTO().getId());
        if(title != null){
            teacherTitle.setTitle(title);
        }else{
            //TODO izazovi izuzetak
        }
        return teacherTitle;
    }
}
