package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.Title;
import rs.uns.ac.ftn.studentewebservice.repository.TitleRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.BadRequestException;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.List;

@Service
public class TitleService {

    @Autowired
    TitleRepository titleRepository;

    public Page<Title> findAll(Pageable pageable) {
        return titleRepository.findAll(pageable);
    }

    public List<Title> findAll() {
        return titleRepository.findAll();
    }

    public Title findById(Long id) {
        return titleRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Title Not Found!", "Title with given id: " + id + " is not found!"));
    }

    public Title save(Title title) {
        return titleRepository.save(title);
    }

    public void delete(Long id) {
        Title title = findById(id);
        if (title.getTeacherTitles().isEmpty()) {
            title.setDeleted(true);
            titleRepository.save(title);
        } else {
            throw new BadRequestException("Cannot delete title!", "Title cannot be deleted because there is teachers who have this title!");
        }

    }
}
