package rs.uns.ac.ftn.studentewebservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.uns.ac.ftn.studentewebservice.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Account findByStudent_Id(Long id);
}
