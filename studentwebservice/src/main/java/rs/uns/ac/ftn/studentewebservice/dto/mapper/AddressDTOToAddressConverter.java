package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.AddressDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Address;
import rs.uns.ac.ftn.studentewebservice.entity.Place;
import rs.uns.ac.ftn.studentewebservice.service.PlaceService;

@Component
public class AddressDTOToAddressConverter implements Converter<AddressDTO, Address> {

    @Autowired
    PlaceService placeService;

    @Override
    public Address convert(AddressDTO addressDTO) {
        Address address = new Address();
        address.setId(addressDTO.getId());
        address.setStreetName(addressDTO.getStreetName());
        address.setStreetNumber(addressDTO.getStreetNumber());
        address.setDeleted(addressDTO.isDeleted());
        Place place = placeService.findById(addressDTO.getPlaceDTO().getId());
        if(place != null){
            address.setPlace(place);
        }else{
            //TODO izazovi izuzetak
        }
        return address;
    }
}
