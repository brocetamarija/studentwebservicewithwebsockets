package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.dto.NotificationDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Exam;
import rs.uns.ac.ftn.studentewebservice.entity.ExamRealization;
import rs.uns.ac.ftn.studentewebservice.entity.Student;
import rs.uns.ac.ftn.studentewebservice.repository.ExamRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ExamService {

    @Autowired
    ExamRepository examRepository;

    @Autowired
    StudentService studentService;

    @Autowired
    ExamRealizationService examRealizationService;

    @Autowired
    NotificationService notificationService;

    public Page<Exam> findAll(Pageable pageable) {
        return examRepository.findAll(pageable);
    }

    public Exam findById(Long id) {
        return examRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Exam Not Found!", "Exam with given id: " + id + " is not found"));
    }

    public List<Exam> findByStudentId(Long id) {
        return examRepository.findByStudent_Id(id);
    }

    public Exam save(Exam exam) {
        return examRepository.save(exam);
    }

    public Exam update(Long id, Exam e) {
        Exam exam = findById(id);
        notificationService.notify("exam-results", new NotificationDTO("Objavljeni su rezultati iz ".concat(e.getExamRealization().getCourse().getName()), e.getExamRealization().getId().toString()));
        exam = save(e);
        return exam;
    }

    public void delete(Long id) {
        Exam exam = findById(id);

        exam.setDeleted(true);
        examRepository.save(exam);
    }

    /**
     * Brisanje ispiti vezanih za studenta, u slucaju da se brise student
     *
     * @param studentID Parametar po kome se pronalaze ispiti
     */
    public void deleteByStudentId(Long studentID) {
        List<Exam> exams = findByStudentId(studentID);
        for (Exam e : exams) {
            delete(e.getId());
        }
    }

    /**
     * Polozeni ispiti
     * Pronalazi ispite po student id kojima je ocena veca ili jednaka 6
     *
     * @param studentID Parametar po kome se pronalaze ispiti za studenta
     * @return List<Exam> Vraca listu ispita
     */
    public List<Exam> passedExams(Long studentID) {
        List<Exam> exams = findByStudentId(studentID);
        exams =
                exams.stream()
                        .filter(exam -> exam.getFinalGrade() >= 6)
                        .collect(Collectors.toList());
        return exams;
    }


    /**
     * Ukupan broj ESPB bodova polozenih ispita
     * Racuna ukupan broj ESPB bodova za odredjenog studenta
     *
     * @param studentID Parametar po kome se pronalazi ukupan broj ESPB za studenta
     * @return int Vraca sumu ESPB bodova
     */
    public int totalECTS(Long studentID) {
        List<Exam> exams = passedExams(studentID);
        int totalPoints =
                exams.stream()
                        .mapToInt(exam -> exam.getExamRealization().getCourse().getECTSPoints())
                        .sum();
        return totalPoints;
    }


    /**
     * Prosecna ocena
     * Racuna prosecnu ocenu za odredjenog studenta
     *
     * @param studentID Parametar po kome se pronalazi prosecna ocena za studenta
     * @return double Vraca prosecnu ocenu
     */
    public double averageGrade(Long studentID) {
        List<Exam> exams = passedExams(studentID);
        double avgGrade =
                exams.stream()
                        .mapToDouble(exam -> exam.getFinalGrade())
                        .average().orElse(0);
        return avgGrade;
    }


    /**
     * Rezultati prijavljenih ispita
     * Pronalazi ispite kojima je ocena veca od 0 za odredjenog studenta u odredjjenom ispitnom roku
     *
     * @param studentID    Parametar po kome se pronalaze ispiti za studenta
     * @param examPeriodID Parametar po kome se pronalaze ispiti u ispitnom roku
     */
    public List<Exam> examResultsByStudentAndExamPeriod(Long studentID, Long examPeriodID) {
        List<Exam> exams = examRepository.findByStudent_IdAndExamRealization_ExamPeriod_Id(studentID, examPeriodID);
        exams =
                exams.stream()
                        .filter(e -> e.getFinalGrade() > 0)
                        .collect(Collectors.toList());
        return exams;
    }

    /**
     * Prijava ispita
     * Prijava ispita se realizuje tako sto se dodaje ispit koji je vezan za odredjenog studenta, i svi bodovi se postavljaju na 0
     */
    public Exam examRegistration(Exam e) {
        Exam exam = new Exam();
        Student student = studentService.findById(e.getStudent().getId());
        ExamRealization examRealization = examRealizationService.findById(e.getExamRealization().getId());
        exam.setStudent(student);
        exam.setExamRealization(examRealization);
        exam.setLabPoints(0);
        exam.setExamPoints(0);
        exam.setTotalPoints(0);
        exam.setFinalGrade(0);

        return examRepository.save(exam);
    }

    /**
     * Prijavljeni ispiti
     * Pronalazi ispite kojima je ocena 0 za odredjenog studenta u odredjenom ispitnom roku
     *
     * @param studentID    Parametar po kome se pronalaze ispiti za studenta
     * @param examPeriodID Parametar po kome se pronalaze ispiti u ispitnom roku
     */
    public List<Exam> registeredExams(Long studentID, Long examPeriodID) {
        List<Exam> exams = examRepository.findByStudent_IdAndExamRealization_ExamPeriod_Id(studentID, examPeriodID);
        return exams
                .stream()
                .filter(e -> e.getFinalGrade() == 0)
                .collect(Collectors.toList());
    }

    /**
     * Odjava ispita
     * Pronalazi se ispit po id i brise se
     *
     * @param id Parametar po kome se ispit pronalazi
     * @return e Povratna vrednost je ispit koji je obrisan
     */
    public Exam cancelExamRegistration(Long id) {
        Exam e = findById(id);
        if (e != null) {
            delete(id);
        } else {
            //TODO izazovi izuzetak
        }
        return e;
    }

    /**
     * Lista ispita za koje je potrebno uneti rezultate
     *
     * @param id Parametar po kome se pronalaze ispiti
     * @return list Povratna vrednost je lista ispita koji su pronadjeni
     */
    public List<Exam> examsForEnteringResults(Long id) {
        List<Exam> exams = examRepository.findByExamRealization_Id(id);
        return exams
                .stream()
                .filter(e -> e.getFinalGrade() == 0)
                .collect(Collectors.toList());
    }

    /**
     * Racunanje konacnog broja bodova
     * Sabiraju se bodovi iz predispitnih obaveza i bodovi sa zavrsnog ispita
     *
     * @param labPoints  Bodovi iz predispitnih obaveza
     * @param examPoints Bodovi sa zavrsnog ispita
     * @return int Konacan broj bodova
     */
    public int countTotalPoints(int labPoints, int examPoints) {
        int totalPoints = labPoints + examPoints;
        return totalPoints;
    }

    /**
     * Racunanje ocene
     * Na osnovu konacnog broja bodova dolazimo do konacne ocene
     *
     * @param totalPoints Konacan broj bodova
     * @return int Konacna ocena
     */
    public int countGrade(int totalPoints) {
        int grade;
        if (totalPoints >= 91) {
            grade = 10;
        } else if (totalPoints >= 81) {
            grade = 9;
        } else if (totalPoints >= 71) {
            grade = 8;
        } else if (totalPoints >= 61) {
            grade = 7;
        } else if (totalPoints >= 51) {
            grade = 6;
        } else {
            grade = 5;
        }

        return grade;
    }

    /**
     * Rezultati ispita za profesora
     * Lista rezultata ispita za izabrani ispit
     *
     * @param id Id koji se odnosi na realizaciju ispita na osnovu kojeg se pronalaze rezultati
     * @return list Povratna vrednost je lista rezultata
     */
    public List<Exam> examResultsForTeacher(Long id) {
        List<Exam> exams = examRepository.findByExamRealization_Id(id);
        return exams
                .stream()
                .filter(e -> e.getFinalGrade() > 0)
                .collect(Collectors.toList());
    }


    public List<Exam> findByExamRealizationId(Long examRealizationID) {
        return examRepository.findByExamRealization_Id(examRealizationID);
    }
}
