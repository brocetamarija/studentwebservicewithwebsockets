package rs.uns.ac.ftn.studentewebservice.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.Address;
import rs.uns.ac.ftn.studentewebservice.repository.AddressRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.BadRequestException;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.List;

@Service
public class AddressService {

    @Autowired
    AddressRepository addressRepository;


    public List<Address> findAll() {
        return addressRepository.findAll();
    }

    public Page<Address> findAll(Pageable pageable) {
        return addressRepository.findAll(pageable);
    }

    public List<Address> findByPlaceId(Long id) {
        return addressRepository.findByPlace_Id(id);
    }

    public Address findById(Long id) {
        return addressRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Address Not Found!", "Address with given id: " + id + " is not found!"));
    }

    public Address save(Address address) {
        return addressRepository.save(address);
    }

    public Address edit(Address a, Long id){
        Address address = findById(id);

        address = addressRepository.save(a);
        return address;
    }

    public void delete(Long id) {
        Address address = findById(id);

        if (address.getUsers().isEmpty()) {
            address.setDeleted(true);
            addressRepository.save(address);
        } else {
            throw new BadRequestException("Cannot be deleted!", "Address cannot be deleted because student is connected to it!");
        }
    }

    public void deleteByPlaceId(Long id) {
        List<Address> addresses = findByPlaceId(id);
        for (Address a : addresses) {
            a.setDeleted(true);
            addressRepository.save(a);
        }
    }

    public Address add(Address a) {
        Address address = new Address();
        address.setStreetName(a.getStreetName());
        address.setStreetNumber(a.getStreetNumber());
        address.setDeleted(false);
        address.setPlace(a.getPlace());
        return addressRepository.save(address);
    }

}
