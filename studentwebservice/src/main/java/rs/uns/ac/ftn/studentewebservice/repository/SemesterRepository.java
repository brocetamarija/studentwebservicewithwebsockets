package rs.uns.ac.ftn.studentewebservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.uns.ac.ftn.studentewebservice.entity.Semester;

public interface SemesterRepository extends JpaRepository<Semester, Long> {
    Semester findFirstByOrderByIdDesc();
}
