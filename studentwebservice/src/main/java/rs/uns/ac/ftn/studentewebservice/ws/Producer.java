package rs.uns.ac.ftn.studentewebservice.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.NotificationDTO;

@Component
public class Producer {

    @Autowired
    private SimpMessagingTemplate template;

    public void notify(String topic, NotificationDTO notification) {
        this.template.convertAndSend("/topic/" + topic, notification);
        return;
    }

}
