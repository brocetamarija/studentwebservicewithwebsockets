package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.ExamPeriodDTO;
import rs.uns.ac.ftn.studentewebservice.entity.ExamPeriod;

import java.util.ArrayList;
import java.util.List;

@Component
public class ExamPeriodToExamPeriodDTOConverter implements Converter<ExamPeriod, ExamPeriodDTO> {


    @Override
    public ExamPeriodDTO convert(ExamPeriod examPeriod) {
        ExamPeriodDTO examPeriodDTO = new ExamPeriodDTO();
        examPeriodDTO.setId(examPeriod.getId());
        examPeriodDTO.setName(examPeriod.getName());
        examPeriodDTO.setStartDate(examPeriod.getStartDate());
        examPeriodDTO.setEndDate(examPeriod.getEndDate());
        examPeriodDTO.setDeleted(examPeriod.isDeleted());
        return examPeriodDTO;
    }

    public List<ExamPeriodDTO> convert(List<ExamPeriod> examPeriods){
        List<ExamPeriodDTO> retVal = new ArrayList<>();
        for(ExamPeriod ep:examPeriods){
            retVal.add(convert(ep));
        }
        return retVal;
    }
}
