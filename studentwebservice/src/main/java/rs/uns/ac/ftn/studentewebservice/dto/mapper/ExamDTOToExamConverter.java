package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.ExamDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Exam;
import rs.uns.ac.ftn.studentewebservice.entity.ExamRealization;
import rs.uns.ac.ftn.studentewebservice.entity.Student;
import rs.uns.ac.ftn.studentewebservice.service.ExamRealizationService;
import rs.uns.ac.ftn.studentewebservice.service.StudentService;

@Component
public class ExamDTOToExamConverter implements Converter<ExamDTO, Exam> {

    @Autowired
    StudentService studentService;

    @Autowired
    ExamRealizationService examRealizationService;

    @Override
    public Exam convert(ExamDTO examDTO) {
        Exam exam = new Exam();
        exam.setId(examDTO.getId());
        exam.setLabPoints(examDTO.getLabPoints());
        exam.setExamPoints(examDTO.getExamPoints());
        exam.setTotalPoints(examDTO.getTotalPoints());
        exam.setFinalGrade(examDTO.getFinalGrade());
        exam.setDeleted(examDTO.isDeleted());
        Student student = studentService.findById(examDTO.getStudentDTO().getId());
        if(student != null){
            exam.setStudent(student);
        }else{
            //TODO izazovi izuzetak
        }

        ExamRealization examRealization = examRealizationService.findById(examDTO.getExamRealizationDTO().getId());
        if(examRealization != null){
            exam.setExamRealization(examRealization);
        }else{
            //TODO izazovi izuzetak
        }
        return exam;
    }
}
