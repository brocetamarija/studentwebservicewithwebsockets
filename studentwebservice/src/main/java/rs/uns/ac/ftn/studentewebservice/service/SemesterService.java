package rs.uns.ac.ftn.studentewebservice.service;

import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.Semester;
import rs.uns.ac.ftn.studentewebservice.repository.SemesterRepository;
import rs.uns.ac.ftn.studentewebservice.scheduler.NotificationSemesterCertificationScheduler;
import rs.uns.ac.ftn.studentewebservice.util.DateFormat;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.List;

@Service
public class SemesterService {

    @Autowired
    SemesterRepository semesterRepository;

    @Autowired
    NotificationService notificationService;

    @Autowired
    StudentService studentService;

    @Autowired
    DateFormat dateFormat;

    @Autowired
    NotificationSemesterCertificationScheduler scheduler;

    public List<Semester> findAll() {
        return semesterRepository.findAll();
    }

    public Semester findById(Long id) {
        return semesterRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Semester Not Found!",
                "Semester with given id: " + id + " is not found!"));
    }

    public Semester save(Semester semester) throws SchedulerException {
        scheduler.scheduleJob(semester.getEndDate());
        return semesterRepository.save(semester);
    }

    public Semester edit(Semester s, Long id){
        Semester semester = findById(id);
        semester = semesterRepository.save(s);
        return semester;
    }

    public void delete(Long id) {
        Semester semester = findById(id);
        semester.setDeleted(true);
         semesterRepository.save(semester);
    }

    public Semester findLast() {
        Semester semester = semesterRepository.findFirstByOrderByIdDesc();
        return semester;
    }
}
