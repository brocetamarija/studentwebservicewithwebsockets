package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.PaymentPurposeDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.PaymentPurposeDTOToPaymentPurposeConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.PaymentPurposeToPaymentPurposeDTOConverter;
import rs.uns.ac.ftn.studentewebservice.service.PaymentPurposeService;

import java.util.List;

@RestController
@RequestMapping(value = "api/payment-purposes")
public class PaymentPurposeController {

    @Autowired
    PaymentPurposeService paymentPurposeService;

    @Autowired
    PaymentPurposeToPaymentPurposeDTOConverter toPaymentPurposeDTOConverter;

    @Autowired
    PaymentPurposeDTOToPaymentPurposeConverter toPaymentPurposeConverter;

    @GetMapping
    public ResponseEntity<List<PaymentPurposeDTO>> findAll(){
        return new ResponseEntity<>(toPaymentPurposeDTOConverter.convert(paymentPurposeService.findAll()), HttpStatus.OK);
    }

    @GetMapping(value = "/{name}")
    public ResponseEntity<PaymentPurposeDTO> findByName(@PathVariable("name") String name){
        return new ResponseEntity<>(toPaymentPurposeDTOConverter.convert(paymentPurposeService.findByName(name)), HttpStatus.OK);
    }

}
