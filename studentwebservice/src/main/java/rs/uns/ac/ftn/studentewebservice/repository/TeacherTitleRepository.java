package rs.uns.ac.ftn.studentewebservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.uns.ac.ftn.studentewebservice.entity.TeacherTitle;

import java.util.List;

public interface TeacherTitleRepository extends JpaRepository<TeacherTitle, Long> {
    List<TeacherTitle> findByTeacher_Id(Long id);
}
