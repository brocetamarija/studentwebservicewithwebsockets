package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.ExamDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.ExamDTOToExamConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.ExamToExamDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.Exam;
import rs.uns.ac.ftn.studentewebservice.service.ExamService;

import java.util.List;

@RestController
@RequestMapping(value = "api/exams")
public class ExamController {

    @Autowired
    ExamService examService;

    @Autowired
    ExamToExamDTOConverter toExamDTOConverter;

    @Autowired
    ExamDTOToExamConverter toExamConverter;


    @GetMapping(value = "/{id}")
    public ResponseEntity<ExamDTO> findById(@PathVariable("id") Long id){
        return new ResponseEntity<>(toExamDTOConverter.convert(examService.findById(id)), HttpStatus.OK);
    }

    @GetMapping(value = "/passed-exams/{id}")
    public ResponseEntity<List<ExamDTO>> passedExams(@PathVariable("id") Long id){
        return new ResponseEntity<>(toExamDTOConverter.convert(examService.passedExams(id)), HttpStatus.OK);
    }

    @GetMapping(value = "/total-ects-points/{id}")
    public ResponseEntity<Integer> totalEctsPoints(@PathVariable("id") Long id){
        return new ResponseEntity<>(examService.totalECTS(id), HttpStatus.OK);
    }

    @GetMapping(value = "/average-grade/{id}")
    public ResponseEntity<Double> averageGrade(@PathVariable("id") Long id){
        double averageGrade = examService.averageGrade(id);
        return new ResponseEntity<>(averageGrade, HttpStatus.OK);
    }


    @GetMapping(value = "/student-exam-results/{studentID}/{examPeriodID}")
    public ResponseEntity<List<ExamDTO>> examResultsForStudent(@PathVariable("studentID") Long studentID, @PathVariable("examPeriodID") Long examPeriodID){
        List<Exam> exams = examService.examResultsByStudentAndExamPeriod(studentID,examPeriodID);
        return new ResponseEntity<>(toExamDTOConverter.convert(exams), HttpStatus.OK);
    }

    @GetMapping(value = "/registered-exams/student/{studentID}/exam-period/{examPeriodID}")
    public ResponseEntity<List<ExamDTO>> registeredExams(@PathVariable("studentID") Long studentID, @PathVariable("examPeriodID") Long examPeriodID){
        List<Exam> exams = examService.registeredExams(studentID, examPeriodID);
        return new ResponseEntity<>(toExamDTOConverter.convert(exams), HttpStatus.OK);
    }

    @GetMapping(value = "/exam-enter-results/{id}")
    public ResponseEntity<List<ExamDTO>> examsForEnteringResults(@PathVariable("id") Long id){
        List<Exam> exams = examService.examsForEnteringResults(id);
        return new ResponseEntity<>(toExamDTOConverter.convert(exams), HttpStatus.OK);
    }

    @GetMapping(value = "/count-total-points")
    public ResponseEntity<Integer> countTotalPoints(@RequestParam("lab-points") int labPoints, @RequestParam("exam-points") int examPoints){
        int totalPoints = examService.countTotalPoints(labPoints, examPoints);
        System.out.println(totalPoints);
        return new ResponseEntity<>(totalPoints, HttpStatus.OK);
    }

    @GetMapping(value = "/count-grade")
    public ResponseEntity<Integer> getCountGrade(@RequestParam("total-points") int totalPoints){
        int grade = examService.countGrade(totalPoints);
        return new ResponseEntity<>(grade, HttpStatus.OK);
    }

    @GetMapping(value = "/teacher-exam-results/{id}")
    public ResponseEntity<List<ExamDTO>> examResultsForTeacher(@PathVariable("id") Long id){
        List<Exam> exams = examService.examResultsForTeacher(id);
        return new ResponseEntity<>(toExamDTOConverter.convert(exams), HttpStatus.OK);
    }


    @PostMapping(value = "/exam-registration")
    public ResponseEntity<ExamDTO> examRegistration(@RequestBody ExamDTO examDTO){
        Exam exam = examService.examRegistration(toExamConverter.convert(examDTO));
        return new ResponseEntity<>(toExamDTOConverter.convert(exam), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<ExamDTO> update(@RequestBody ExamDTO examDTO, @PathVariable("id") Long id){
        Exam exam = examService.update(id, toExamConverter.convert(examDTO));
        return new ResponseEntity<>(toExamDTOConverter.convert(exam), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        examService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(value = "/cancel-exam-registration/{id}")
    public ResponseEntity<ExamDTO> cancelExamRegistration(@PathVariable("id") Long id){
        Exam exam = examService.cancelExamRegistration(id);
        return new ResponseEntity<>(toExamDTOConverter.convert(exam), HttpStatus.OK);
    }
}
