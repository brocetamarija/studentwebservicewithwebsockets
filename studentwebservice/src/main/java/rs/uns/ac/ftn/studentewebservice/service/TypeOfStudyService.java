package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.TypeOfStudy;
import rs.uns.ac.ftn.studentewebservice.repository.TypeOfStudyRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.BadRequestException;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.List;

@Service
public class TypeOfStudyService {

    @Autowired
    TypeOfStudyRepository typeOfStudyRepository;

    public List<TypeOfStudy> findAll() {
        return typeOfStudyRepository.findAll();
    }

    public Page<TypeOfStudy> findAll(Pageable pageable) {
        return typeOfStudyRepository.findAll(pageable);
    }

    public TypeOfStudy findById(Long id) {
        return typeOfStudyRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Type of study Not Found!",
                        "Type of study with given id: " + id + " is not found!"));
    }

    public TypeOfStudy save(TypeOfStudy typeOfStudy) {
        return typeOfStudyRepository.save(typeOfStudy);
    }

    public TypeOfStudy edit(TypeOfStudy ts, Long id){
        TypeOfStudy typeOfStudy = findById(id);
        typeOfStudy = typeOfStudyRepository.save(ts);
        return typeOfStudy;
    }

    public void delete(Long id) {
        TypeOfStudy typeOfStudy = findById(id);
        if (typeOfStudy.getStudyPrograms().isEmpty()) {
            typeOfStudy.setDeleted(true);
            typeOfStudyRepository.save(typeOfStudy);
        } else {
            throw new BadRequestException("Cannot be deleted!",
                    "Type of study cannot be deleted because there is study programs with this study type!");
        }

    }
}
