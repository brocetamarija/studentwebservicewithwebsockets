package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.PaymentPurposeDTO;
import rs.uns.ac.ftn.studentewebservice.entity.PaymentPurpose;

@Component
public class PaymentPurposeDTOToPaymentPurposeConverter implements Converter<PaymentPurposeDTO, PaymentPurpose> {


    @Override
    public PaymentPurpose convert(PaymentPurposeDTO paymentPurposeDTO) {
        PaymentPurpose paymentPurpose = new PaymentPurpose();
        paymentPurpose.setId(paymentPurposeDTO.getId());
        paymentPurpose.setName(paymentPurposeDTO.getName());
        paymentPurpose.setDeleted(paymentPurpose.isDeleted());
        return paymentPurpose;
    }
}
