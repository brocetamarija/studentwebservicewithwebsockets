package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.StudyProgram;
import rs.uns.ac.ftn.studentewebservice.repository.StudyProgramRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.BadRequestException;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.List;

@Service
public class StudyProgramService {

    @Autowired
    StudyProgramRepository studyProgramRepository;

    @Autowired
    CourseService courseService;

    public Page<StudyProgram> findAll(Pageable pageable) {
        return studyProgramRepository.findAll(pageable);
    }

    public List<StudyProgram> findAll() {
        return studyProgramRepository.findAll();
    }

    public StudyProgram findById(Long id) {
        return studyProgramRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Study program Not Found!", "Study program with given id: " + id + " is not found!"));
    }

    public StudyProgram save(StudyProgram studyProgram) {
        return studyProgramRepository.save(studyProgram);
    }

    public StudyProgram edit(StudyProgram sp, Long id){
        StudyProgram studyProgram = findById(id);
        studyProgram = studyProgramRepository.save(sp);
        return studyProgram;
    }

    public void delete(Long id) {
        StudyProgram studyProgram = findById(id);
        if (studyProgram.getStudents().isEmpty()) {
            studyProgram.setDeleted(true);
            studyProgramRepository.save(studyProgram);
            courseService.deleteByStudyProgramId(id);
        }else{
            throw new BadRequestException("Cannot be deleted!",
                    "Study program cannot be deleted because there is students who are enrolled on that study program!");
        }

    }

    public Page<StudyProgram> findByStudyTypePageable(Long id, Pageable pageable) {
        return studyProgramRepository.findByTypeOfStudy_Id(id, pageable);
    }

    public List<StudyProgram> findByStudyType(Long id){
        return studyProgramRepository.findByTypeOfStudy_Id(id);
    }
}
