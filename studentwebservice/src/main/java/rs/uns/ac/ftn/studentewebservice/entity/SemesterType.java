package rs.uns.ac.ftn.studentewebservice.entity;

public enum SemesterType {

    Zimski,
    Letnji
}
