package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.ExamRealizationDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Course;
import rs.uns.ac.ftn.studentewebservice.entity.ExamPeriod;
import rs.uns.ac.ftn.studentewebservice.entity.ExamRealization;
import rs.uns.ac.ftn.studentewebservice.entity.Teacher;
import rs.uns.ac.ftn.studentewebservice.service.CourseService;
import rs.uns.ac.ftn.studentewebservice.service.ExamPeriodService;
import rs.uns.ac.ftn.studentewebservice.service.TeacherService;

@Component
public class ExamRealizationDTOToExamRealizationConverter implements Converter<ExamRealizationDTO, ExamRealization> {

    @Autowired
    TeacherService teacherService;

    @Autowired
    CourseService courseService;

    @Autowired
    ExamPeriodService examPeriodService;


    @Override
    public ExamRealization convert(ExamRealizationDTO examRealizationDTO) {
        ExamRealization examRealization = new ExamRealization();
        examRealization.setId(examRealizationDTO.getId());
        examRealization.setClassroom(examRealizationDTO.getClassroom());
        examRealization.setDate(examRealizationDTO.getDate());
        examRealization.setExamPrice(examRealizationDTO.getExamPrice());
        examRealization.setDeleted(examRealizationDTO.isDeleted());
        Teacher teacher = teacherService.findById(examRealizationDTO.getTeacherDTO().getId());
        if(teacher != null){
            examRealization.setTeacher(teacher);
        }else{
            //TODO izazovi izuzetak
        }

        Course course = courseService.findById(examRealizationDTO.getCourseDTO().getId());
        if(course != null){
            examRealization.setCourse(course);
        }else{
            //TODO izazovi izuzetak
        }

        ExamPeriod examPeriod = examPeriodService.findById(examRealizationDTO.getExamPeriodDTO().getId());
        if(examPeriod != null){
            examRealization.setExamPeriod(examPeriod);
        }else{
            //TODO izazovi izuzetak
        }
        return examRealization;
    }
}
