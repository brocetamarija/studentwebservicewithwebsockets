package rs.uns.ac.ftn.studentewebservice.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.PaymentPurpose;
import rs.uns.ac.ftn.studentewebservice.repository.PaymentPurposeRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.BadRequestException;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.List;

@Service
public class PaymentPurposeService {

    @Autowired
    PaymentPurposeRepository paymentPurposeRepository;

    public List<PaymentPurpose> findAll() {
        return paymentPurposeRepository.findAll();
    }

    public PaymentPurpose findById(Long id) {
        return paymentPurposeRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Payment purpose Not Found!", "Payment purpose with given id: " + id + " is not found!"));
    }

    public PaymentPurpose save(PaymentPurpose paymentPurpose) {
        return paymentPurposeRepository.save(paymentPurpose);
    }

    public void delete(Long id) {
        PaymentPurpose paymentPurpose = findById(id);

        if (paymentPurpose.getPayments().isEmpty()) {
            paymentPurpose.setDeleted(true);
            paymentPurposeRepository.save(paymentPurpose);
        } else {
            throw new BadRequestException("Cannot be deleted!", "Payment purpose cannot be deleted because there is payments for this payment purpose!");
        }

    }

    public PaymentPurpose findByName(String name) {
        PaymentPurpose paymentPurpose = paymentPurposeRepository.findByName(name);
        if(paymentPurpose == null){
            throw new EntityNotFoundException("Payment purpose Not Found!", "Payment purpose with given name: " + name + " is not found!");
        }
        return paymentPurpose;
    }
}
