package rs.uns.ac.ftn.studentewebservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.uns.ac.ftn.studentewebservice.entity.Teaching;

import java.util.List;

public interface TeachingRepository extends JpaRepository<Teaching, Long> {

    List<Teaching> findByTeacher_Id(Long placeId);

    List<Teaching> findByCourse_Id(Long id);

    List<Teaching> findCourseByTeacher_Id(Long id);

    Page<Teaching> findByTeacher_Id(Long id, Pageable pageable);
}
