package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.*;
import rs.uns.ac.ftn.studentewebservice.repository.StudentRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    UserService userService;

    @Autowired
    EnrollmentService enrollmentService;

    @Autowired
    PaymentService paymentService;

    @Autowired
    DocumentService documentService;

    @Autowired
    ExamService examService;

    @Autowired
    AccountService accountService;

    @Autowired
    AuthorityService authorityService;

    @Autowired
    StudyProgramService studyProgramService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AddressService addressService;

    public Page<Student> findAll(Pageable pageable) {
        return studentRepository.findAll(pageable);
    }

    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    public Student findById(Long id) {
        return studentRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Student not found",
                        "Student with given id: " + id + " is not found!"));
    }

    public Student save(Student student) {
        return studentRepository.save(student);
    }

    public Student edit(Student s, Long id) {
        Student student = findById(id);
        User user = userService.edit(s.getUser(), s.getUser().getId());
        student.setUser(user);
        student = studentRepository.save(s);
        return student;
    }

    public void delete(Long id){
        Student student = findById(id);
        if (student != null) {
            student.setDeleted(true);
            student.getUser().setDeleted(true);
            studentRepository.save(student);
            enrollmentService.deleteByStudentId(id);
            paymentService.deleteByStudentId(id);
            documentService.deleteByStudentId(id);
            examService.deleteByStudentId(id);
            accountService.deleteByStudent(id);
        } else {
            throw new EntityNotFoundException("Student not found",
                    "Student with given id: " + id + " is not found!");
        }
    }

    public Student findByUsername(String username) {
        return studentRepository.findByUser_Username(username);
    }

    public Student add(Student s) {
        Authority authority = authorityService.findByName("ROLE_STUDENT");
        Student student = new Student();
        student.setId(s.getId());
        student.setCardNumber(s.getCardNumber());
        student.setBirthDate(s.getBirthDate());
        student.setPhoneNumber(s.getPhoneNumber());
        student.setYearOfStudy(s.getYearOfStudy());
        student.setDeleted(false);
        User user = userService.addUserForStudentOrTeacher(s.getUser(), authority);
        student.setUser(user);
        StudyProgram sp = studyProgramService.findById(s.getStudyProgram().getId());
        if (sp != null) {
            student.setStudyProgram(sp);
        } else {
            throw new EntityNotFoundException("Study program Not Found!",
                    "Study program with given id: " + s.getStudyProgram().getId() + " is not found!");
        }

        studentRepository.save(student);
        return student;
    }


    public Page<Student> findByStudyProgram(Long id, Pageable pageable) {
        return studentRepository.findByStudyProgram_Id(id, pageable);
    }

    public List<Student> studentsWhoRegisterExam(Long examRealizationID) {
        List<Exam> exams = examService.findByExamRealizationId(examRealizationID);
        List<Student> students = new ArrayList<>();
        Date today = new Date();
        exams
                .stream()
                .filter(e -> today.after(e.getExamRealization().getExamPeriod().getStartDate()) &&
                        today.before(e.getExamRealization().getExamPeriod().getEndDate()))
                .forEach(e -> students.add(e.getStudent()));

        return students;
    }

    public List<Student> findByStudyProgramAndStudyType(Long studyProgramID, Long studyTypeID){
        return studentRepository.findByStudyProgram_IdAndStudyProgram_TypeOfStudy_Id(studyProgramID, studyTypeID);
    }
}

