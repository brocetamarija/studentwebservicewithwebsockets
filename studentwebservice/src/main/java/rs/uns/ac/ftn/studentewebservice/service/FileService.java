package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@Service
public class FileService {

    @Value("${dataDir}")
    private String DATA_DIR_PATH;


    @Autowired
    DocumentService documentService;

    public static byte[] readBytesFromFile(String filePath) {
        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;
        try {
            File file = new File(filePath);
            bytesArray = new byte[(int) file.length()];

            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return bytesArray;
    }

    public File getResourceFilePath(String path) {
        URL url = this.getClass().getClassLoader().getResource(path);
        File file = null;
        try {
            file = new File(url.toURI());
        } catch (URISyntaxException e) {
            file = new File(url.getPath());
        }
        return file;
    }

    public byte[] download(String filename) throws UnsupportedEncodingException {
        String decoded = URLDecoder.decode(filename, "UTF-8");
        String path = getResourceFilePath(DATA_DIR_PATH).getAbsolutePath() + "\\" + decoded;

        File file = null;
        try {
            file = new File(path);
        } catch (Exception e) {
            e.printStackTrace();
        }

        byte[] bFile = readBytesFromFile(file.toString());

        return bFile;
    }


    public String saveUploadedFile(MultipartFile file) throws IOException {

        String retVal = null;
        if (!file.isEmpty()) {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(getResourceFilePath(DATA_DIR_PATH).getAbsolutePath() + File.separator + file.getOriginalFilename());
            Files.write(path, bytes);
            retVal = file.getOriginalFilename();
        }

        return retVal;

    }


    public String removeFile(String filename) {
        String path = getResourceFilePath(DATA_DIR_PATH).getAbsolutePath() + "\\" + filename;

        File file = new File(path);

        if (file.delete()) {
            return "Success";
        } else {
            return "Failed";
        }
    }

    public String getDATA_DIR_PATH() {
        return DATA_DIR_PATH;
    }

}
