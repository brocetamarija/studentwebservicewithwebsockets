package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.Authority;
import rs.uns.ac.ftn.studentewebservice.entity.Teacher;
import rs.uns.ac.ftn.studentewebservice.entity.User;
import rs.uns.ac.ftn.studentewebservice.repository.TeacherRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.List;

@Service
public class TeacherService {

    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    TeacherTitleService teacherTitleService;

    @Autowired
    TeachingService teachingService;

    @Autowired
    UserService userService;

    @Autowired
    AuthorityService authorityService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AddressService addressService;

    public Page<Teacher> findAll(Pageable pageable) {
        return teacherRepository.findAll(pageable);
    }

    public List<Teacher> findAll() {
        return teacherRepository.findAll();
    }

    public Teacher findById(Long id) {
        return teacherRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Teacher Not Found!", "Teacher with given id: " + id + " is not found!"));
    }

    public Teacher save(Teacher teacher) {
        return teacherRepository.save(teacher);
    }

    public Teacher edit(Teacher t, Long id) {
        Teacher teacher = findById(id);
        User user = userService.edit(t.getUser(), t.getUser().getId());
        teacher.setUser(user);
        teacher = teacherRepository.save(t);
        return teacher;

    }

    public void delete(Long id) {
        Teacher teacher = findById(id);
        teacher.setDeleted(true);
        teacher.getUser().setDeleted(true);
        teacherRepository.save(teacher);
        teacherTitleService.deleteByTeacherId(id);
        teachingService.deleteByTeacherId(id);
    }

    public Teacher findByUsername(String username) {
        Teacher t = teacherRepository.findByUser_Username(username);
        if(t == null){
            throw new EntityNotFoundException("Teacher Not Found!", "Teacher with given username: " + username + " is not found!");
        }
        return t;
    }

    public Teacher add(Teacher t) {
        Authority authority = authorityService.findByName("ROLE_TEACHER");
        Teacher teacher = new Teacher();
        teacher.setId(t.getId());
        teacher.setTeacherCode(t.getTeacherCode());
        teacher.setDeleted(false);
        User user = userService.addUserForStudentOrTeacher(t.getUser(), authority);
        teacher.setUser(user);

        return teacherRepository.save(teacher);
    }


}
