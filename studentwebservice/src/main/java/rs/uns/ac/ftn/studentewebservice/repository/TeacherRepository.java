package rs.uns.ac.ftn.studentewebservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.uns.ac.ftn.studentewebservice.entity.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {


    Teacher findByUser_Username(String username);
}
