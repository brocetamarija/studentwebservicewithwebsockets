package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.TypeOfStudyDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.TypeOfStudyDTOToTypeOfStudyConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.TypeOfStudyToTypeOfStudyDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.TypeOfStudy;
import rs.uns.ac.ftn.studentewebservice.service.TypeOfStudyService;

import java.util.List;

@RestController
@RequestMapping(value = "api/study-types")
public class TypeOfStudyController {

    @Autowired
    TypeOfStudyService typeOfStudyService;

    @Autowired
    TypeOfStudyToTypeOfStudyDTOConverter toTypeOfStudyDTOConverter;

    @Autowired
    TypeOfStudyDTOToTypeOfStudyConverter toTypeOfStudyConverter;

    @GetMapping
    public ResponseEntity<List<TypeOfStudyDTO>> findAll(){
        return new ResponseEntity<>(toTypeOfStudyDTOConverter.convert(typeOfStudyService.findAll()), HttpStatus.OK);
    }

    @GetMapping(value = "/pageable")
    public ResponseEntity<List<TypeOfStudyDTO>> findAll(Pageable pageable){
        Page<TypeOfStudy> typeOfStudies = typeOfStudyService.findAll(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages", Integer.toString(typeOfStudies.getTotalPages()));
        headers.add("access-control-expose-headers", "totalPages");

        return new ResponseEntity<>(toTypeOfStudyDTOConverter.convert(typeOfStudies.getContent()), headers, HttpStatus.OK);
    }


    @GetMapping(value = "/{id}")
    public ResponseEntity<TypeOfStudyDTO> findById(@PathVariable("id") Long id){
        return new ResponseEntity<>(toTypeOfStudyDTOConverter.convert(typeOfStudyService.findById(id)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TypeOfStudyDTO> create(@RequestBody TypeOfStudyDTO typeOfStudyDTO){
        TypeOfStudy typeOfStudy = typeOfStudyService.save(toTypeOfStudyConverter.convert(typeOfStudyDTO));
        return new ResponseEntity<>(toTypeOfStudyDTOConverter.convert(typeOfStudy), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<TypeOfStudyDTO> update(@RequestBody TypeOfStudyDTO typeOfStudyDTO, @PathVariable("id") Long id){
        TypeOfStudy typeOfStudy = typeOfStudyService.edit(toTypeOfStudyConverter.convert(typeOfStudyDTO),id);
        return new ResponseEntity<>(toTypeOfStudyDTOConverter.convert(typeOfStudy), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        typeOfStudyService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
