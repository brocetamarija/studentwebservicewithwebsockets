package rs.uns.ac.ftn.studentewebservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.uns.ac.ftn.studentewebservice.entity.Enrollment;

import java.util.List;

public interface EnrollmentRepository extends JpaRepository<Enrollment, Long> {
    List<Enrollment> findByStudent_Id(Long id);

    Page<Enrollment> findByStudent_Id(long id, Pageable pageable);
}
