package rs.uns.ac.ftn.studentewebservice.dto;

public class CourseDTO {

    private Long id;
    private String code;
    private String name;
    private int ECTSPoints;
    private int numberOfClass;
    private int yearOfStudy;
    private boolean deleted;
    private StudyProgramDTO studyProgramDTO;

    public CourseDTO() {}

    public CourseDTO(Long id, String code, String name, int ECTSPoints, int numberOfClass, int yearOfStudy, boolean deleted, StudyProgramDTO studyProgramDTO) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.ECTSPoints = ECTSPoints;
        this.numberOfClass = numberOfClass;
        this.yearOfStudy = yearOfStudy;
        this.deleted = deleted;
        this.studyProgramDTO = studyProgramDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getECTSPoints() {
        return ECTSPoints;
    }

    public void setECTSPoints(int ECTSPoints) {
        this.ECTSPoints = ECTSPoints;
    }

    public int getNumberOfClass() {
        return numberOfClass;
    }

    public void setNumberOfClass(int numberOfClass) {
        this.numberOfClass = numberOfClass;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public StudyProgramDTO getStudyProgramDTO() {
        return studyProgramDTO;
    }

    public void setStudyProgramDTO(StudyProgramDTO studyProgramDTO) {
        this.studyProgramDTO = studyProgramDTO;
    }

}
