package rs.uns.ac.ftn.studentewebservice.dto;

import java.util.Date;

public class EnrollmentDTO {

    private Long id;
    private Date startDate;
    private Date endDate;
    private boolean deleted;
    private StudentDTO studentDTO;
    private CourseDTO courseDTO;

    public EnrollmentDTO() {}

    public EnrollmentDTO(Long id, Date startDate, Date endDate, boolean deleted, StudentDTO studentDTO, CourseDTO courseDTO) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.deleted = deleted;
        this.studentDTO = studentDTO;
        this.courseDTO = courseDTO;
    }

    /*public EnrollmentDTO(Enrollment enrollment){
        this(enrollment.getId(),
                enrollment.getStartDate(),
                enrollment.getEndDate(),
                enrollment.isDeleted(),
                new StudentDTO(enrollment.getStudent()),
                new CourseDTO(enrollment.getCourse()));
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public StudentDTO getStudentDTO() {
        return studentDTO;
    }

    public void setStudentDTO(StudentDTO studentDTO) {
        this.studentDTO = studentDTO;
    }

    public CourseDTO getCourseDTO() {
        return courseDTO;
    }

    public void setCourseDTO(CourseDTO courseDTO) {
        this.courseDTO = courseDTO;
    }
}
