package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.DocumentDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.DocumentDTOToDocumentConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.DocumentToDocumentDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.Document;
import rs.uns.ac.ftn.studentewebservice.service.DocumentService;

import java.util.List;

@RestController
@RequestMapping(value = "api/documents")
public class DocumentController {

    @Autowired
    DocumentService documentService;

    @Autowired
    DocumentToDocumentDTOConverter toDocumentDTOConverter;

    @Autowired
    DocumentDTOToDocumentConverter toDocumentConverter;

    @GetMapping
    public ResponseEntity<List<DocumentDTO>> findAll(Pageable pageable){
        Page<Document> documents = documentService.findAll(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(documents.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");

        return new ResponseEntity<>(toDocumentDTOConverter.convert(documents.getContent()),headers, HttpStatus.OK);
    }

    @GetMapping(value = "/student/{id}")
    public ResponseEntity<List<DocumentDTO>> findByStudentId(@PathVariable("id") Long id, Pageable pageable){
        Page<Document> documents = documentService.findByStudentIdPage(id, pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages", Integer.toString(documents.getTotalPages()));
        headers.add("access-control-expose-headers", "totalPages");

        return new ResponseEntity<>(toDocumentDTOConverter.convert(documents.getContent()), headers, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<DocumentDTO> create(@RequestBody DocumentDTO documentDTO){
        Document document = documentService.add(toDocumentConverter.convert(documentDTO));

        return new ResponseEntity<>(toDocumentDTOConverter.convert(document), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<DocumentDTO> update(@RequestBody DocumentDTO documentDTO, @PathVariable("id") Long id){
        Document document = documentService.edit(toDocumentConverter.convert(documentDTO),id);
        return new ResponseEntity<>(toDocumentDTOConverter.convert(document),HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<DocumentDTO> delete(@PathVariable("id") Long id){
        Document document = documentService.delete(id);
        return new ResponseEntity<>(toDocumentDTOConverter.convert(document),HttpStatus.OK);
    }
}
