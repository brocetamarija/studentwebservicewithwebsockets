package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.ExamPeriodDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.ExamPeriodDTOToExamPeriodConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.ExamPeriodToExamPeriodDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.ExamPeriod;
import rs.uns.ac.ftn.studentewebservice.service.ExamPeriodService;

import java.util.List;

@RestController
@RequestMapping(value = "api/exam-periods")
public class ExamPeriodController {

    @Autowired
    ExamPeriodService examPeriodService;

    @Autowired
    ExamPeriodToExamPeriodDTOConverter toExamPeriodDTOConverter;

    @Autowired
    ExamPeriodDTOToExamPeriodConverter toExamPeriodConverter;

    @GetMapping
    public ResponseEntity<List<ExamPeriodDTO>> findAll(){
        return new ResponseEntity<>(toExamPeriodDTOConverter.convert(examPeriodService.findAll()), HttpStatus.OK);
    }

    @GetMapping(value = "/pageable")
    public ResponseEntity<List<ExamPeriodDTO>> findAll(Pageable pageable){
        Page<ExamPeriod> examPeriods = examPeriodService.findAll(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages", Integer.toString(examPeriods.getTotalPages()));
        headers.add("access-control-expose-headers", "totalPages");

        return new ResponseEntity<>(toExamPeriodDTOConverter.convert(examPeriods.getContent()), headers, HttpStatus.OK);

    }

    @GetMapping(value = "/future")
    public ResponseEntity<List<ExamPeriodDTO>> findAllFuture(){
        return new ResponseEntity<>(toExamPeriodDTOConverter.convert(examPeriodService.findAllFuture()), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ExamPeriodDTO> create(@RequestBody ExamPeriodDTO examPeriodDTO){
        ExamPeriod examPeriod = examPeriodService.save(toExamPeriodConverter.convert(examPeriodDTO));
        return new ResponseEntity<>(toExamPeriodDTOConverter.convert(examPeriod), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<ExamPeriodDTO> update(@RequestBody ExamPeriodDTO examPeriodDTO, @PathVariable("id") Long id){
        ExamPeriod examPeriod = examPeriodService.edit(toExamPeriodConverter.convert(examPeriodDTO),id);
        return new ResponseEntity<>(toExamPeriodDTOConverter.convert(examPeriod), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        examPeriodService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
