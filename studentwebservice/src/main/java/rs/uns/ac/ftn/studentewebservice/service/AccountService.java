package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.Account;
import rs.uns.ac.ftn.studentewebservice.repository.AccountRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.BadRequestException;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;


@Service
public class AccountService {

    @Autowired
    AccountRepository accountRepository;

    public Page<Account> findAll(Pageable pageable) {
        return accountRepository.findAll(pageable);
    }

    public Account findById(Long id) {
        return accountRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Account not found", "Account with given id: " + id + " is not found!"));
    }

    public Account save(Account account) {
        return accountRepository.save(account);
    }

    public Account edit(Account a, Long id) {
        Account account = findById(id);

        account = accountRepository.save(a);
        return account;
    }

    public void delete(Long id) {
        Account account = findById(id);

        if (account.getStudent().isDeleted() == true) {
            account.setDeleted(true);
            accountRepository.save(account);
        } else {
            throw new BadRequestException("Cannot be deleted!", "Account cannot be deleted because student is connected to it!");
        }

    }

    public Account findByStudentId(Long id) {
        Account account = accountRepository.findByStudent_Id(id);
        if (account == null) {
            throw new EntityNotFoundException("Account not found", "Account with given student id: " + id + " is not found!");
        }
        return account;
    }

    public void deleteByStudent(Long id) {
        Account account = findByStudentId(id);
        account.setDeleted(true);
        accountRepository.save(account);
    }

    public Account create(Account a) {
        Account account = new Account();
        account.setGiroAccountNumber(a.getGiroAccountNumber());
        account.setPersonalReferenceNumber(a.getPersonalReferenceNumber());
        account.setModelNumber(a.getModelNumber());
        account.setStudent(a.getStudent());
        account.setCurrentAccountBalance(0);

        return accountRepository.save(account);
    }

    /**
     * Izmena trenutnog stanja racuna / Dodavanje na stanje
     * Pronalazi se racun po id i trenutno stanje racuna se uvecava za odredjenu vrednost
     *
     * @param a      Racun kojem se menja trenutno stanje
     * @param id     Id po kome se pronalazi racun
     * @param amount Vrednost za koju se uvecava stanje racuna
     * @return account Povratna vrednost je racun sa izmenjenim stanjem racuna
     */
    public Account updateCurrentAccountBalanceAdd(Account a, Long id, double amount) {
        Account account = findById(id);
        double newBalance = a.getCurrentAccountBalance() + amount;
        account.setCurrentAccountBalance(newBalance);

        return accountRepository.save(account);
    }

    /**
     * Izmena trenutnog stanja racuna / Oduzimanje sa stanja
     * Pronalazi se racun po id i trenutno stanje racuna se smanjuje za odredjenu vrednost
     *
     * @param a      Racun kojem se menja trenutno stanje
     * @param id     Id po kome se pronalazi racun
     * @param amount Vrednost za koju se smanjuje stanje racuna
     * @return account Povratna vrednost je racun sa izmenjenim stanjem racuna
     */
    public Account updateCurrentAccountBalanceSubstract(Account a, Long id, double amount) {
        Account account = findById(id);
        double newBalance = a.getCurrentAccountBalance() - amount;
        account.setCurrentAccountBalance(newBalance);

        return accountRepository.save(account);
    }


}
