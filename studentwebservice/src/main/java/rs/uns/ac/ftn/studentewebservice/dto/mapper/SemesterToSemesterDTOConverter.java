package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.SemesterDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Semester;

import java.util.ArrayList;
import java.util.List;

@Component
public class SemesterToSemesterDTOConverter implements Converter<Semester, SemesterDTO> {


    @Override
    public SemesterDTO convert(Semester semester) {
        SemesterDTO semesterDTO = new SemesterDTO();
        semesterDTO.setId(semester.getId());
        semesterDTO.setStartDate(semester.getStartDate());
        semesterDTO.setEndDate(semester.getEndDate());
        semesterDTO.setSemesterType(semester.getSemesterType());
        semesterDTO.setDeleted(semester.isDeleted());
        return semesterDTO;
    }

    public List<SemesterDTO> convert(List<Semester> semesters){
        List<SemesterDTO> retVal = new ArrayList<>();
        for(Semester s:semesters){
            retVal.add(convert(s));
        }
        return retVal;
    }
}
