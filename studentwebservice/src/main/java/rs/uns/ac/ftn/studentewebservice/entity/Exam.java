package rs.uns.ac.ftn.studentewebservice.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Where(clause = "deleted = false")
public class Exam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "labPoints", nullable = false)
    private int labPoints;

    @Column(name = "examPoints", nullable = false)
    private int examPoints;

    @Column(name = "totalPoints", nullable = false)
    private int totalPoints;

    @Column(name = "finalGrade", nullable = false)
    private int finalGrade;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @ManyToOne
    @JoinColumn(name = "studentId", referencedColumnName = "id")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "examRealizationId", referencedColumnName = "id")
    private ExamRealization examRealization;

    public Exam() {
    }

    public Exam(int labPoints, int examPoints, int totalPoints, int finalGrade, boolean deleted, Student student,
                ExamRealization examRealization) {
        this.labPoints = labPoints;
        this.examPoints = examPoints;
        this.totalPoints = totalPoints;
        this.finalGrade = finalGrade;
        this.deleted = deleted;
        this.student = student;
        this.examRealization = examRealization;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getLabPoints() {
        return labPoints;
    }

    public void setLabPoints(int labPoints) {
        this.labPoints = labPoints;
    }

    public int getExamPoints() {
        return examPoints;
    }

    public void setExamPoints(int examPoints) {
        this.examPoints = examPoints;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public int getFinalGrade() {
        return finalGrade;
    }

    public void setFinalGrade(int finalGrade) {
        this.finalGrade = finalGrade;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public ExamRealization getExamRealization() {
        return examRealization;
    }

    public void setExamRealization(ExamRealization examRealization) {
        this.examRealization = examRealization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Exam e = (Exam) o;
        return Objects.equals(id, e.id);
    }

}
