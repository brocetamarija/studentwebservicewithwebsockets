package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.RestController;
import rs.uns.ac.ftn.studentewebservice.dto.NotificationDTO;
import rs.uns.ac.ftn.studentewebservice.service.NotificationService;
import rs.uns.ac.ftn.studentewebservice.ws.Producer;

@RestController
public class NotificationController {

    @Autowired
    Producer producer;

    @Autowired
    NotificationService notificationService;

    @MessageMapping("/ws/{topic}")
    @SendTo("/topic/{topic}")
    public void send(
            @DestinationVariable("topic") String topic, NotificationDTO notificationDTO)
            throws Exception {
        notificationService.notify(topic, notificationDTO);
    }

}
