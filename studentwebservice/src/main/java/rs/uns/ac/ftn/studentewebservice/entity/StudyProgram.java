package rs.uns.ac.ftn.studentewebservice.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Where(clause = "deleted = false")
public class StudyProgram {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, length = 70)
    private String name;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @ManyToOne
    @JoinColumn(name = "typeOfStudyId", referencedColumnName = "id")
    private TypeOfStudy typeOfStudy;

    @OneToMany(mappedBy = "studyProgram", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Student> students = new HashSet<>();

    @OneToMany(mappedBy = "studyProgram", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Course> courses = new HashSet<>();

    public StudyProgram() {
    }

    public StudyProgram(String name, boolean deleted, TypeOfStudy typeOfStudy, Set<Student> students, Set<Course> courses) {
        this.name = name;
        this.deleted = deleted;
        this.typeOfStudy = typeOfStudy;
        this.students = students;
        this.courses = courses;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TypeOfStudy getTypeOfStudy() {
        return typeOfStudy;
    }

    public void setTypeOfStudy(TypeOfStudy typeOfStudy) {
        this.typeOfStudy = typeOfStudy;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StudyProgram sp = (StudyProgram) o;
        return Objects.equals(id, sp.id);
    }

    public void add(Student student) {
        if (student.getStudyProgram() != null) {
            student.getStudyProgram().getStudents().remove(student);
        }
        student.setStudyProgram(this);
        getStudents().add(student);
    }

    public void remove(Student student) {
        student.setStudyProgram(null);
        getStudents().remove(student);
    }

    public void add(Course course) {
        if (course.getStudyProgram() != null) {
            course.getStudyProgram().getCourses().remove(course);
        }
        course.setStudyProgram(this);
        getCourses().add(course);
    }

    public void remove(Course course) {
        course.setStudyProgram(null);
        getCourses().remove(course);
    }
}
