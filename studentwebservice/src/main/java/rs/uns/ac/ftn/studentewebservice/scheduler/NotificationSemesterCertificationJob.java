package rs.uns.ac.ftn.studentewebservice.scheduler;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.NotificationDTO;
import rs.uns.ac.ftn.studentewebservice.service.NotificationService;

import java.util.Date;

@Component
public class NotificationSemesterCertificationJob implements Job {


    private NotificationService notificationService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        String date = jobDataMap.getString("endDate");
        notificationService = (NotificationService) jobDataMap.get("notificationService");
        notificationService.notify("note",
                new NotificationDTO("Overa semestra je u toku, molimo Vas da overite semestar do ".concat(date), ""));
    }

}
