package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.UserDTO;
import rs.uns.ac.ftn.studentewebservice.entity.User;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserToUserDTOConverter implements Converter<User, UserDTO> {


    @Autowired
    AddressToAddressDTOConverter toAddressDTOConverter;

    @Override
    public UserDTO convert(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setUsername(user.getUsername());
        userDTO.setPassword(user.getPassword());
        userDTO.setEmail(user.getEmail());
        userDTO.setType(user.getType());
        userDTO.setDeleted(user.isDeleted());
        userDTO.setAddressDTO(toAddressDTOConverter.convert(user.getAddress()));
        return userDTO;
    }

    public List<UserDTO> convert(List<User> users){
        List<UserDTO> retVal = new ArrayList<>();
        for(User u:users){
            retVal.add(convert(u));
        }
        return retVal;
    }
}
