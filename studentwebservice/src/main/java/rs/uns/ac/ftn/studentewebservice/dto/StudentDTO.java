package rs.uns.ac.ftn.studentewebservice.dto;

import java.util.Date;

public class StudentDTO {

    private Long id;
    private String cardNumber;
    private String phoneNumber;
    private Date birthDate;
    private int yearOfStudy;
    private boolean deleted;
    private UserDTO userDTO;
    private StudyProgramDTO studyProgramDTO;


    public StudentDTO() {}

    public StudentDTO(Long id, String cardNumber, String phoneNumber, Date birthDate, int yearOfStudy, boolean deleted, UserDTO userDTO, StudyProgramDTO studyProgramDTO) {
        this.id = id;
        this.cardNumber = cardNumber;
        this.phoneNumber = phoneNumber;
        this.birthDate = birthDate;
        this.yearOfStudy = yearOfStudy;
        this.deleted = deleted;
        this.userDTO = userDTO;
        this.studyProgramDTO = studyProgramDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public StudyProgramDTO getStudyProgramDTO() {
        return studyProgramDTO;
    }

    public void setStudyProgramDTO(StudyProgramDTO studyProgramDTO) {
        this.studyProgramDTO = studyProgramDTO;
    }

}
