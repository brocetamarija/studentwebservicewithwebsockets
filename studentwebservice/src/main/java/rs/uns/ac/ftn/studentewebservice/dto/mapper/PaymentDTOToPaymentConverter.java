package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.PaymentDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Payment;
import rs.uns.ac.ftn.studentewebservice.entity.PaymentPurpose;
import rs.uns.ac.ftn.studentewebservice.entity.Student;
import rs.uns.ac.ftn.studentewebservice.service.PaymentPurposeService;
import rs.uns.ac.ftn.studentewebservice.service.StudentService;

@Component
public class PaymentDTOToPaymentConverter implements Converter<PaymentDTO, Payment> {

    @Autowired
    StudentService studentService;

    @Autowired
    PaymentPurposeService paymentPurposeService;

    @Override
    public Payment convert(PaymentDTO paymentDTO) {
        Payment payment = new Payment();
        payment.setId(paymentDTO.getId());
        payment.setAmount(paymentDTO.getAmount());
        payment.setDate(paymentDTO.getDate());
        payment.setDeleted(paymentDTO.isDeleted());
        payment.setPaymentType(paymentDTO.getPaymentType());
        Student student = studentService.findById(paymentDTO.getStudentDTO().getId());
        if(student != null){
            payment.setStudent(student);
        }else{
            //TODO izazovi izuzetak
        }
        PaymentPurpose paymentPurpose = paymentPurposeService.findById(paymentDTO.getPaymentPurposeDTO().getId());
        if(payment != null){
            payment.setPaymentPurpose(paymentPurpose);
        }else{
            //TODO izazovi izuzetak
        }

        return payment;
    }
}
