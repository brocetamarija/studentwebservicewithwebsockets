package rs.uns.ac.ftn.studentewebservice.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Where(clause = "deleted = false")
public class TypeOfStudy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @OneToMany(mappedBy = "typeOfStudy", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<StudyProgram> studyPrograms = new HashSet<>();

    public TypeOfStudy() {
    }

    public TypeOfStudy(String name, boolean deleted, Set<StudyProgram> studyPrograms) {
        this.name = name;
        this.deleted = deleted;
        this.studyPrograms = studyPrograms;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Set<StudyProgram> getStudyPrograms() {
        return studyPrograms;
    }

    public void setStudyPrograms(Set<StudyProgram> studyPrograms) {
        this.studyPrograms = studyPrograms;
    }
}
