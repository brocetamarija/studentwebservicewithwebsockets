package rs.uns.ac.ftn.studentewebservice.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Where(clause = "deleted = false")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "giroAccountNumber", nullable = false, length = 30)
    private String giroAccountNumber;

    @Column(name = "modelNumber", nullable = false)
    private int modelNumber;

    @Column(name = "personalReferenceNumber", nullable = false, length = 30)
    private String personalReferenceNumber;

    @Column(name = "currentAccountBalance", nullable = false)
    private double currentAccountBalance;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;


    @OneToOne
    @JoinColumn(name = "studentId", referencedColumnName = "id")
    private Student student;

    public Account() {
    }

    public Account(String giroAccountNumber, int modelNumber, String personalReferenceNumber, double currentAccountBalance, boolean deleted, Student student) {
        this.giroAccountNumber = giroAccountNumber;
        this.modelNumber = modelNumber;
        this.personalReferenceNumber = personalReferenceNumber;
        this.currentAccountBalance = currentAccountBalance;
        this.deleted = deleted;
        this.student = student;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGiroAccountNumber() {
        return giroAccountNumber;
    }

    public void setGiroAccountNumber(String giroAccountNumber) {
        this.giroAccountNumber = giroAccountNumber;
    }

    public int getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(int modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getPersonalReferenceNumber() {
        return personalReferenceNumber;
    }

    public void setPersonalReferenceNumber(String personalReferenceNumber) {
        this.personalReferenceNumber = personalReferenceNumber;
    }

    public double getCurrentAccountBalance() {
        return currentAccountBalance;
    }

    public void setCurrentAccountBalance(double currentAccountBalance) {
        this.currentAccountBalance = currentAccountBalance;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account a = (Account) o;
        return Objects.equals(id, a.id);
    }
}
