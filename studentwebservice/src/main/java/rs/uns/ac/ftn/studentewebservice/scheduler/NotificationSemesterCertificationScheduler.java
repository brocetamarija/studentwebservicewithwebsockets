package rs.uns.ac.ftn.studentewebservice.scheduler;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.service.NotificationService;
import rs.uns.ac.ftn.studentewebservice.util.DateFormat;

import java.util.Date;
import java.util.UUID;

@Component
public class NotificationSemesterCertificationScheduler {

    @Autowired
    NotificationService notificationService;

    @Autowired
    DateFormat dateFormat;

    public Scheduler getScheduler() throws SchedulerException {
        Scheduler scheduler = new StdSchedulerFactory().getScheduler();
        return scheduler;
    }

    public JobDetail buildJobDetail(String endDate) {
        try {
            JobDataMap jobDataMap = new JobDataMap();
            jobDataMap.put("endDate", endDate);
            jobDataMap.put("notificationService", notificationService);
            return JobBuilder.newJob(NotificationSemesterCertificationJob.class)
                    .withIdentity(UUID.randomUUID().toString(), "semester-certification-jobs")
                    .withDescription("Notify about semester certification")
                    .usingJobData(jobDataMap)
                    .storeDurably()
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Trigger buildJobTrigger(Date startDate, Date endDate) {
        return TriggerBuilder.newTrigger()
                .withIdentity(UUID.randomUUID().toString(), "semester-certification-jobs")
                .startAt(startDate)
                .withSchedule(CronScheduleBuilder.cronSchedule("0/10 * * * * ?"))
                .endAt(endDate)
                .build();
    }

    public void scheduleJob(Date date) throws SchedulerException {
        String d = dateFormat.formatDate(date);
        Date startDate = dateFormat.subtractDays(date);
        try {
            JobDetail jobDetail = buildJobDetail(d);
            Trigger trigger = buildJobTrigger(startDate, date);
            getScheduler().start();
            getScheduler().scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}
