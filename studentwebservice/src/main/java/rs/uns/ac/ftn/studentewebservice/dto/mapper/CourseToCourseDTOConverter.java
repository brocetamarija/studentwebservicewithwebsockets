package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.CourseDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Course;

import java.util.ArrayList;
import java.util.List;

@Component
public class CourseToCourseDTOConverter implements Converter<Course, CourseDTO> {

    @Autowired
    StudyProgramToStudyProgramDTOConverter toStudyProgramDTOConverter;

    @Override
    public CourseDTO convert(Course course) {
        CourseDTO courseDTO = new CourseDTO();
        courseDTO.setId(course.getId());
        courseDTO.setName(course.getName());
        courseDTO.setCode(course.getCode());
        courseDTO.setECTSPoints(course.getECTSPoints());
        courseDTO.setNumberOfClass(course.getNumberOfClass());
        courseDTO.setYearOfStudy(course.getYearOfStudy());
        courseDTO.setDeleted(course.isDeleted());
        courseDTO.setStudyProgramDTO(toStudyProgramDTOConverter.convert(course.getStudyProgram()));
        return courseDTO;
    }

    public List<CourseDTO> convert(List<Course> courses){
        List<CourseDTO> retVal = new ArrayList<>();
        for(Course c:courses){
            retVal.add(convert(c));
        }
        return retVal;
    }
}
