package rs.uns.ac.ftn.studentewebservice.controller;

import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.SemesterDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.SemesterDTOToSemesterConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.SemesterToSemesterDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.Semester;
import rs.uns.ac.ftn.studentewebservice.service.SemesterService;

import java.util.List;

@RestController
@RequestMapping(value = "api/semesters")
public class SemesterController {

    @Autowired
    SemesterService semesterService;

    @Autowired
    SemesterDTOToSemesterConverter toSemesterConverter;

    @Autowired
    SemesterToSemesterDTOConverter toSemesterDTOConverter;

    @GetMapping
    public ResponseEntity<List<SemesterDTO>> getAll(){
        return new ResponseEntity<>(toSemesterDTOConverter.convert(semesterService.findAll()), HttpStatus.OK);
    }

    @GetMapping(value = "/last")
    public ResponseEntity<SemesterDTO> getLast(){
        Semester semester = semesterService.findLast();
        try {
            return new ResponseEntity<>(toSemesterDTOConverter.convert(semester), HttpStatus.OK);
        }catch(NullPointerException e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<SemesterDTO> addSemester(@RequestBody SemesterDTO semesterDTO) throws SchedulerException {
        Semester semester = semesterService.save(toSemesterConverter.convert(semesterDTO));
        return new ResponseEntity<>(toSemesterDTOConverter.convert(semester), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<SemesterDTO> update(@RequestBody SemesterDTO semesterDTO, @PathVariable("id") Long id){
        Semester semester = semesterService.edit(toSemesterConverter.convert(semesterDTO),id);
        return new ResponseEntity<>(toSemesterDTOConverter.convert(semester), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        semesterService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
