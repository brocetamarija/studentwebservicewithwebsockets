package rs.uns.ac.ftn.studentewebservice.dto;

public class FilepathDTO {

    private String path;

    public FilepathDTO(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
