package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.PaymentPurposeDTO;
import rs.uns.ac.ftn.studentewebservice.entity.PaymentPurpose;

import java.util.ArrayList;
import java.util.List;

@Component
public class PaymentPurposeToPaymentPurposeDTOConverter implements Converter<PaymentPurpose, PaymentPurposeDTO> {


    @Override
    public PaymentPurposeDTO convert(PaymentPurpose paymentPurpose) {
        PaymentPurposeDTO paymentPurposeDTO = new PaymentPurposeDTO();
        paymentPurposeDTO.setId(paymentPurpose.getId());
        paymentPurposeDTO.setName(paymentPurpose.getName());
        paymentPurposeDTO.setDeleted(paymentPurpose.isDeleted());
        return paymentPurposeDTO;
    }

    public List<PaymentPurposeDTO> convert(List<PaymentPurpose> purposes){
        List<PaymentPurposeDTO> retVal = new ArrayList<>();
        for(PaymentPurpose pp:purposes){
            retVal.add(convert(pp));
        }
        return retVal;
    }
}
