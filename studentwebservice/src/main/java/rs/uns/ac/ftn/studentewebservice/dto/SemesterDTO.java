package rs.uns.ac.ftn.studentewebservice.dto;

import rs.uns.ac.ftn.studentewebservice.entity.SemesterType;

import java.util.Date;

public class SemesterDTO {

    private Long id;
    private Date startDate;
    private Date endDate;

    private SemesterType semesterType;
    private boolean deleted;

    public SemesterDTO() {
    }

    public SemesterDTO(Long id, Date startDate, Date endDate, SemesterType semesterType, boolean deleted) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.semesterType = semesterType;
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public SemesterType getSemesterType() {
        return semesterType;
    }

    public void setSemesterType(SemesterType semesterType) {
        this.semesterType = semesterType;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
