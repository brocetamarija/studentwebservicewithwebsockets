package rs.uns.ac.ftn.studentewebservice.util;

import org.springframework.stereotype.Component;


import java.nio.file.attribute.FileTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component
public class DateFormat {

    public String formatDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM");
        String dateString = sdf.format(date);
        return dateString;
    }

    public Date subtractDays(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -14);
        Date dateMinus14days = cal.getTime();
        return dateMinus14days;
    }

    /**
     * Konvertovanje datuma dobijenog iz fajla u format koji nam odgovara za prikaz i cuvanje u bazi
     *
     * @param fileTime Parametar koji se dobija iz dokumenta
     * @return date Konvertovani datum
     */
    public Date convertFileTime(FileTime fileTime) {
        java.text.DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;
        try {
            date = dateFormat.parse(dateFormat.format(fileTime.toMillis()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

}
