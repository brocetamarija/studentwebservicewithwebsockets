package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.AccountDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Account;

import java.util.ArrayList;
import java.util.List;

@Component
public class AccountToAccountDTOConverter implements Converter<Account, AccountDTO> {

    @Autowired
    StudentToStudentDTOConverter toStudentDTOConverter;

    @Override
    public AccountDTO convert(Account account) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setId(account.getId());
        accountDTO.setGiroAccountNumber(account.getGiroAccountNumber());
        accountDTO.setModelNumber(account.getModelNumber());
        accountDTO.setPersonalReferenceNumber(account.getPersonalReferenceNumber());
        accountDTO.setCurrentAccountBalance(account.getCurrentAccountBalance());
        accountDTO.setDeleted(account.isDeleted());
        accountDTO.setStudentDTO(toStudentDTOConverter.convert(account.getStudent()));
        return accountDTO;
    }

    public List<AccountDTO> convert(List<Account> accounts){
        List<AccountDTO> retVal = new ArrayList<>();
        for(Account a:accounts){
            retVal.add(convert(a));
        }
        return retVal;
    }
}
