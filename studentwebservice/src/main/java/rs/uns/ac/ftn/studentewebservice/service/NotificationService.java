package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.dto.NotificationDTO;
import rs.uns.ac.ftn.studentewebservice.ws.Producer;

@Service
public class NotificationService {

    @Autowired
    Producer producer;

    public void notify(String topic, NotificationDTO notificationDTO) {
        producer.notify(topic, notificationDTO);
    }
}
