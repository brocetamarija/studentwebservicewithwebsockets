package rs.uns.ac.ftn.studentewebservice.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Where(clause = "deleted = false")
public class SemesterStudent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "isCertified")
    private boolean isCertified;

    @Column(name = "semesterCertificationPrice")
    private double semesterCertificationPrice;

    @Column(name = "deleted")
    private boolean deleted;

    @ManyToOne
    @JoinColumn(name = "studentId", referencedColumnName = "id")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "semesterId", referencedColumnName = "id")
    private Semester semester;

    public SemesterStudent() {
    }

    public SemesterStudent(boolean isCertified, double semesterCertificationPrice, boolean deleted, Student student, Semester semester) {
        this.isCertified = isCertified;
        this.semesterCertificationPrice = semesterCertificationPrice;
        this.deleted = deleted;
        this.student = student;
        this.semester = semester;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isCertified() {
        return isCertified;
    }

    public void setCertified(boolean certified) {
        isCertified = certified;
    }

    public double getSemesterCertificationPrice() {
        return semesterCertificationPrice;
    }

    public void setSemesterCertificationPrice(double semesterCertificationPrice) {
        this.semesterCertificationPrice = semesterCertificationPrice;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }
}
