package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.ExamRealizationDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.ExamRealizationDTOToExamRealizationConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.ExamRealizationToExamRealizationDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.ExamRealization;
import rs.uns.ac.ftn.studentewebservice.service.ExamRealizationService;


import java.util.List;

@RestController
@RequestMapping(value = "api/exam-realizations")
public class ExamRealizationController {

    @Autowired
    ExamRealizationService examRealizationService;

    @Autowired
    ExamRealizationToExamRealizationDTOConverter toExamRealizationDTOConverter;

    @Autowired
    ExamRealizationDTOToExamRealizationConverter toExamRealizationConverter;

    @GetMapping(value = "/{id}")
    public ResponseEntity<ExamRealizationDTO> findById(@PathVariable("id") Long id){
        return new ResponseEntity<>(toExamRealizationDTOConverter.convert(examRealizationService.findById(id)), HttpStatus.OK);
    }

    @GetMapping(value = "study-program/{studyProgramId}/exam-period/{examPeriodId}")
    public ResponseEntity<List<ExamRealizationDTO>> findByStudyProgramAndExamPeriod(@PathVariable("studyProgramId") Long studyProgramId, @PathVariable("examPeriodId") Long examPeriodId, Pageable pageable){
        Page<ExamRealization> examRealizations = examRealizationService.findByStudyProgramAndExamPeriod(studyProgramId, examPeriodId, pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages", Integer.toString(examRealizations.getTotalPages()));
        headers.add("access-control-expose-headers", "totalPages");

        return new ResponseEntity<>(toExamRealizationDTOConverter.convert(examRealizations.getContent()), headers, HttpStatus.OK);
    }

    @GetMapping(value = "student/{studentId}/exam-period/{examPeriodId}")
    public ResponseEntity<List<ExamRealizationDTO>> getExamRealizationsForRegistration(@PathVariable("studentId") Long studentID, @PathVariable("examPeriodId") Long examPeriodID){
        return new ResponseEntity<>(toExamRealizationDTOConverter.convert(examRealizationService.examRealizationsForRegistration(studentID,examPeriodID)), HttpStatus.OK);
    }

    @GetMapping(value = "/teacher/{teacherId}/exam-period/{examPeriodId}")
    public ResponseEntity<List<ExamRealizationDTO>> getExamRealizationsTeacher(@PathVariable("teacherId") Long teacherID, @PathVariable("examPeriodId") Long examPeriodID){
        return new ResponseEntity<>(toExamRealizationDTOConverter.convert(examRealizationService.examRealizationsTeacher(teacherID, examPeriodID)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ExamRealizationDTO> create(@RequestBody ExamRealizationDTO examRealizationDTO){
        ExamRealization examRealization = examRealizationService.save(toExamRealizationConverter.convert(examRealizationDTO));
        return new ResponseEntity<>(toExamRealizationDTOConverter.convert(examRealization), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<ExamRealizationDTO> update(@RequestBody ExamRealizationDTO examRealizationDTO, @PathVariable("id") Long id){
        ExamRealization examRealization = examRealizationService.edit(toExamRealizationConverter.convert(examRealizationDTO), id);
        return new ResponseEntity<>(toExamRealizationDTOConverter.convert(examRealization), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        examRealizationService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
