package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.TitleDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.TitleDTOToTitleConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.TitleToTitleDTOConverter;
import rs.uns.ac.ftn.studentewebservice.service.TitleService;

import java.util.List;

@RestController
@RequestMapping(value = "api/titles")
public class TitleController {

    @Autowired
    TitleService titleService;

    @Autowired
    TitleToTitleDTOConverter toTitleDTOConverter;

    @Autowired
    TitleDTOToTitleConverter toTitleConverter;

    @GetMapping
    public ResponseEntity<List<TitleDTO>> findAll(){
        return new ResponseEntity<>(toTitleDTOConverter.convert(titleService.findAll()), HttpStatus.OK);
    }

}
