package rs.uns.ac.ftn.studentewebservice.dto;

public class TeachingDTO {

    private Long id;
    private boolean deleted;
    private TeacherDTO teacherDTO;
    private CourseDTO courseDTO;

    public TeachingDTO() {
    }

    public TeachingDTO(Long id, boolean deleted, TeacherDTO teacherDTO, CourseDTO courseDTO) {
        this.id = id;
        this.deleted = deleted;
        this.teacherDTO = teacherDTO;
        this.courseDTO = courseDTO;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public TeacherDTO getTeacherDTO() {
        return teacherDTO;
    }

    public void setTeacherDTO(TeacherDTO teacherDTO) {
        this.teacherDTO = teacherDTO;
    }

    public CourseDTO getCourseDTO() {
        return courseDTO;
    }

    public void setCourseDTO(CourseDTO courseDTO) {
        this.courseDTO = courseDTO;
    }
}
