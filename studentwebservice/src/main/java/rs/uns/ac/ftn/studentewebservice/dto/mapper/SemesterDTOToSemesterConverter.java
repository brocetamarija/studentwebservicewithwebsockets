package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.SemesterDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Semester;

@Component
public class SemesterDTOToSemesterConverter implements Converter<SemesterDTO, Semester> {


    @Override
    public Semester convert(SemesterDTO semesterDTO) {
        Semester semester = new Semester();
        semester.setId(semesterDTO.getId());
        semester.setStartDate(semesterDTO.getStartDate());
        semester.setEndDate(semesterDTO.getEndDate());
        semester.setSemesterType(semesterDTO.getSemesterType());
        semester.setDeleted(semesterDTO.isDeleted());
        return semester;
    }


}
