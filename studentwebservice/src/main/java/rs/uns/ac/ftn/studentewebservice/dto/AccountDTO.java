package rs.uns.ac.ftn.studentewebservice.dto;

public class AccountDTO {

    private Long id;
    private String giroAccountNumber;
    private int modelNumber;
    private String personalReferenceNumber;
    private double currentAccountBalance;
    private boolean deleted;
    private StudentDTO studentDTO;

    public AccountDTO() {
    }


    public AccountDTO(Long id, String giroAccountNumber, int modelNumber, String personalReferenceNumber, double currentAccountBalance, boolean deleted, StudentDTO studentDTO) {
        this.id = id;
        this.giroAccountNumber = giroAccountNumber;
        this.modelNumber = modelNumber;
        this.personalReferenceNumber = personalReferenceNumber;
        this.currentAccountBalance = currentAccountBalance;
        this.deleted = deleted;
        this.studentDTO = studentDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGiroAccountNumber() {
        return giroAccountNumber;
    }

    public void setGiroAccountNumber(String giroAccountNumber) {
        this.giroAccountNumber = giroAccountNumber;
    }

    public int getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(int modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getPersonalReferenceNumber() {
        return personalReferenceNumber;
    }

    public void setPersonalReferenceNumber(String personalReferenceNumber) {
        this.personalReferenceNumber = personalReferenceNumber;
    }

    public double getCurrentAccountBalance() {
        return currentAccountBalance;
    }

    public void setCurrentAccountBalance(double currentAccountBalance) {
        this.currentAccountBalance = currentAccountBalance;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public StudentDTO getStudentDTO() {
        return studentDTO;
    }

    public void setStudentDTO(StudentDTO studentDTO) {
        this.studentDTO = studentDTO;
    }
}
