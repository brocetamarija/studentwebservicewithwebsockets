package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.StudentDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Component
public class StudentToStudentDTOConverter implements Converter<Student, StudentDTO> {

    @Autowired
    UserToUserDTOConverter toUserDTOConverter;

    @Autowired
    StudyProgramToStudyProgramDTOConverter toStudyProgramDTOConverter;

    @Autowired
    AccountToAccountDTOConverter toAccountDTOConverter;

    @Override
    public StudentDTO convert(Student student) {
        StudentDTO studentDTO = new StudentDTO();
        studentDTO.setId(student.getId());
        studentDTO.setCardNumber(student.getCardNumber());
        studentDTO.setPhoneNumber(student.getPhoneNumber());
        studentDTO.setBirthDate(student.getBirthDate());
        studentDTO.setYearOfStudy(student.getYearOfStudy());
        studentDTO.setDeleted(student.isDeleted());
        studentDTO.setUserDTO(toUserDTOConverter.convert(student.getUser()));
        studentDTO.setStudyProgramDTO(toStudyProgramDTOConverter.convert(student.getStudyProgram()));
        return studentDTO;
    }

    public List<StudentDTO> convert(List<Student> students){
        List<StudentDTO> retVal = new ArrayList<>();
        for(Student s:students){
            retVal.add(convert(s));
        }
        return retVal;
    }
}
