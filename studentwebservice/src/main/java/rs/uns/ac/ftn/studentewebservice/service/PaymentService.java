package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.Payment;
import rs.uns.ac.ftn.studentewebservice.entity.PaymentPurpose;
import rs.uns.ac.ftn.studentewebservice.entity.PaymentType;
import rs.uns.ac.ftn.studentewebservice.entity.Student;
import rs.uns.ac.ftn.studentewebservice.repository.PaymentRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.Date;
import java.util.List;

@Service
public class PaymentService {

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    StudentService studentService;

    @Autowired
    PaymentPurposeService paymentPurposeService;

    public Page<Payment> findAll(Pageable pageable) {
        return paymentRepository.findAll(pageable);
    }

    public List<Payment> findByStudentId(Long id) {
        return paymentRepository.findByStudent_Id(id);
    }

    public Payment findById(Long id) {
        return paymentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Payment Not Found!", "Payment with given id: " + id + " is not found!"));
    }

    public Payment save(Payment payment) {
        return paymentRepository.save(payment);
    }

    public void delete(Long id) {
        Payment payment = findById(id);

        payment.setDeleted(true);
        paymentRepository.save(payment);
    }

    public void deleteByStudentId(Long id) {
        List<Payment> payments = findByStudentId(id);
        for (Payment p : payments) {
            delete(p.getId());
        }
    }

    public Page<Payment> findByStudentId(Long id, Pageable pageable) {
        return paymentRepository.findByStudent_Id(id, pageable);
    }

    public Payment create(Payment p) {
        Payment payment = new Payment();
        Date date = new Date();
        payment.setDate(date);
        payment.setAmount(p.getAmount());
        payment.setPaymentType(PaymentType.PAYMENT_IN);
        payment.setPaymentPurpose(p.getPaymentPurpose());
        payment.setStudent(p.getStudent());

        return paymentRepository.save(payment);
    }

    public Payment addPaymentForExamRegistration(Payment p) {
        Student student = studentService.findById(p.getStudent().getId());
        PaymentPurpose paymentPurpose = paymentPurposeService.findByName(p.getPaymentPurpose().getName());
        Payment payment = new Payment();
        Date date = new Date();
        payment.setDate(date);
        payment.setStudent(student);
        payment.setPaymentPurpose(paymentPurpose);
        payment.setPaymentType(PaymentType.PAYMENT_OUT);
        payment.setAmount(p.getAmount());

        return paymentRepository.save(payment);
    }


}
