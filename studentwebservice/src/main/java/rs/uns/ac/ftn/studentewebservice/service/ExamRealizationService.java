package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.Course;
import rs.uns.ac.ftn.studentewebservice.entity.ExamRealization;
import rs.uns.ac.ftn.studentewebservice.repository.ExamRealizationRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.ArrayList;
import java.util.List;

@Service
public class ExamRealizationService {

    @Autowired
    ExamRealizationRepository examRealizationRepository;

    @Autowired
    CourseService courseService;

    public Page<ExamRealization> findAll(Pageable pageable) {
        return examRealizationRepository.findAll(pageable);
    }

    public ExamRealization findById(Long id) {
        return examRealizationRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Exam realization Not found!", "Exam realization with given id: " + id + " is not found!"));
    }

    public ExamRealization save(ExamRealization examRealization) {
        return examRealizationRepository.save(examRealization);
    }

    public ExamRealization edit(ExamRealization er, Long id){
        ExamRealization examRealization = findById(id);
        examRealization = examRealizationRepository.save(er);
        return examRealization;
    }

    public void delete(Long id) {
        ExamRealization examRealization = findById(id);

        examRealization.setDeleted(true);
        examRealizationRepository.save(examRealization);

    }

    /**
     * Lista ispita za odredjeni studijski program u odredjenom roku
     *
     * @param studyProgramId Id studijskog programa za koji se pronalaze ispiti
     * @param examPeriodId   Id ispitnog roka u kom se odrzavaju ispiti
     * @return list Lista pronadjenih ispita
     */
    public Page<ExamRealization> findByStudyProgramAndExamPeriod(Long studyProgramId, Long examPeriodId, Pageable pageable) {
        return examRealizationRepository.findByCourse_StudyProgram_IdAndExamPeriod_Id(studyProgramId, examPeriodId, pageable);
    }


    /**
     * Lista ispita za prijavu
     * Pronalazi se lista predmeta koje student slusa a nije polozio,
     * nakon toga se pronalaze realizacije ispita za svaki predmet u odredjenom ispitnom roku
     * i za svaki ispit se proverava da li je vec prijavljen, ukoliko nije dodaje se u listu realizacija ispita
     *
     * @param studentID    Id studenta za koga se pronalaze ispiti za prijavu
     * @param examPeriodID Id ispitnog roka u kom ce se prijavljivati ispiti
     * @return list Lista realizacija ispita za prijavu
     */
    public List<ExamRealization> examRealizationsForRegistration(Long studentID, Long examPeriodID) {
        List<Course> courses = courseService.unfinishedCourses(studentID);
        List<ExamRealization> examRealizations = new ArrayList<>();
        courses
                .stream()
                .forEach(c -> examRealizationRepository.findByCourse_IdAndExamPeriod_Id(c.getId(), examPeriodID).
                        forEach(e -> {
                            if (e.getExams().size() == 0) {
                                examRealizations.add(e);
                            }
                        }));

        return examRealizations;
    }


    /**
     * Lista buducih ispita ispita
     * Lista ispita za profesora koji tek treba da se odrze
     *
     * @param teacherID    Parametar na osnovu kojeg se pronalaze ispiti vezanni za profesora
     * @param examPeriodID Parametar na osnovu kojeg se pronalaze ispit vezani za ispitni rok
     */
    public List<ExamRealization> examRealizationsTeacher(Long teacherID, Long examPeriodID) {
        return examRealizationRepository.findByTeacher_IdAndExamPeriod_Id(teacherID, examPeriodID);
    }

}
