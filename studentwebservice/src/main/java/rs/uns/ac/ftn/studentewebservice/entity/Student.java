package rs.uns.ac.ftn.studentewebservice.entity;


import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Where(clause = "deleted = false")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "cardNumber", nullable = false, length = 15)
    private String cardNumber;

    @Column(name = "phoneNumber", nullable = false, length = 15)
    private String phoneNumber;

    @Temporal(TemporalType.DATE)
    @Column(name = "birthDate", nullable = false)
    private Date birthDate;

    @Column(name = "year_of_study")
    private int yearOfStudy;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @OneToOne
    @JoinColumn(name = "userId", referencedColumnName = "id")
    private User user;

    @OneToOne(mappedBy = "student")
    private Account account;

    @ManyToOne
    @JoinColumn(name = "studyProgramId", referencedColumnName = "id")
    private StudyProgram studyProgram;

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Document> documents = new HashSet<>();

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Payment> payments = new HashSet<>();

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Enrollment> enrollments = new HashSet<>();

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Exam> exams = new HashSet<>();

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<SemesterStudent> semesterStudents = new HashSet<>();

    //getters, setters, constructors

    public Student() {
    }

    public Student(String cardNumber, String phoneNumber, Date birthDate, int yearOfStudy, boolean deleted, User user, Account account, StudyProgram studyProgram, Set<Document> documents, Set<Payment> payments, Set<Enrollment> enrollments, Set<Exam> exams, Set<SemesterStudent> semesterStudents) {
        this.cardNumber = cardNumber;
        this.phoneNumber = phoneNumber;
        this.birthDate = birthDate;
        this.yearOfStudy = yearOfStudy;
        this.deleted = deleted;
        this.user = user;
        this.account = account;
        this.studyProgram = studyProgram;
        this.documents = documents;
        this.payments = payments;
        this.enrollments = enrollments;
        this.exams = exams;
        this.semesterStudents = semesterStudents;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public StudyProgram getStudyProgram() {
        return studyProgram;
    }

    public void setStudyProgram(StudyProgram studyProgram) {
        this.studyProgram = studyProgram;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Set<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(Set<Document> documents) {
        this.documents = documents;
    }

    public Set<Payment> getPayments() {
        return payments;
    }

    public void setPayments(Set<Payment> payments) {
        this.payments = payments;
    }

    public Set<Enrollment> getEnrollments() {
        return enrollments;
    }

    public void setEnrollments(Set<Enrollment> enrollments) {
        this.enrollments = enrollments;
    }

    public Set<Exam> getExams() {
        return exams;
    }

    public void setExams(Set<Exam> exams) {
        this.exams = exams;
    }

    public Set<SemesterStudent> getSemesterStudents() {
        return semesterStudents;
    }

    public void setSemesterStudents(Set<SemesterStudent> semesterStudents) {
        this.semesterStudents = semesterStudents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Student s = (Student) o;
        return Objects.equals(id, s.id);
    }

    public void add(Document document) {
        if (document.getStudent() != null) {
            document.getStudent().getDocuments().remove(document);
        }
        document.setStudent(this);
        getDocuments().add(document);
    }

    public void remove(Document document) {
        document.setStudent(null);
        getDocuments().remove(document);
    }

    public void add(Payment payment) {
        if (payment.getStudent() != null) {
            payment.getStudent().getPayments().remove(payment);
        }
        payment.setStudent(this);
        getPayments().add(payment);
    }

    public void remove(Payment payment) {
        payment.setStudent(null);
        getPayments().remove(payment);
    }

    public void add(Enrollment enrollment) {
        if (enrollment.getStudent() != null) {
            enrollment.getStudent().getEnrollments().remove(enrollment);
        }
        enrollment.setStudent(this);
        getEnrollments().add(enrollment);
    }

    public void remove(Enrollment enrollment) {
        enrollment.setStudent(null);
        getEnrollments().remove(enrollment);
    }

    public void add(Exam exam) {
        if (exam.getStudent() != null) {
            exam.getStudent().getExams().remove(exam);
        }
        exam.setStudent(this);
        getExams().add(exam);
    }

    public void remove(Exam exam) {
        exam.setStudent(null);
        getExams().remove(exam);
    }
}
