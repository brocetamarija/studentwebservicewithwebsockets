package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.AccountDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Account;
import rs.uns.ac.ftn.studentewebservice.entity.Student;
import rs.uns.ac.ftn.studentewebservice.service.StudentService;

@Component
public class AccountDTOToAccountConverter implements Converter<AccountDTO, Account> {

    @Autowired
    StudentService studentService;

    @Override
    public Account convert(AccountDTO accountDTO) {
        Account account = new Account();
        account.setId(accountDTO.getId());
        account.setGiroAccountNumber(accountDTO.getGiroAccountNumber());
        account.setModelNumber(accountDTO.getModelNumber());
        account.setPersonalReferenceNumber(accountDTO.getPersonalReferenceNumber());
        account.setCurrentAccountBalance(accountDTO.getCurrentAccountBalance());
        account.setDeleted(accountDTO.isDeleted());
        Student student = studentService.findById(accountDTO.getStudentDTO().getId());
        if(student != null){
            account.setStudent(student);
        }else{
            //TODO izazovi izuzetak
        }

        return account;
    }
}
