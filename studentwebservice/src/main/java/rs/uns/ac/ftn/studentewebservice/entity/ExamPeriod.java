package rs.uns.ac.ftn.studentewebservice.entity;


import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Where(clause = "deleted = false")
public class ExamPeriod {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, length = 40)
    private String name;

    @Temporal(TemporalType.DATE)
    @Column(name = "startDate", nullable = false)
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "endDate", nullable = false)
    private Date endDate;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @OneToMany(mappedBy = "examPeriod", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<ExamRealization> examRealizations = new HashSet<>();

    public ExamPeriod() {
    }

    public ExamPeriod(String name, Date startDate, Date endDate, boolean deleted, Set<ExamRealization> examRealizations) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.deleted = deleted;
        this.examRealizations = examRealizations;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Set<ExamRealization> getExamRealizations() {
        return examRealizations;
    }

    public void setExamRealizations(Set<ExamRealization> examRealizations) {
        this.examRealizations = examRealizations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ExamPeriod ep = (ExamPeriod) o;
        return Objects.equals(id, ep.id);
    }

    public void add(ExamRealization examRealization) {
        if (examRealization.getExamPeriod() != null) {
            examRealization.getExamPeriod().getExamRealizations().remove(examRealization);
        }
        examRealization.setExamPeriod(this);
        getExamRealizations().add(examRealization);
    }

    public void remove(ExamRealization examRealization) {
        examRealization.setExamPeriod(null);
        getExamRealizations().remove(examRealization);
    }
}
