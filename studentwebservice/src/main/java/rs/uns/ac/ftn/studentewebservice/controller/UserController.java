package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.LoginDTO;
import rs.uns.ac.ftn.studentewebservice.dto.TokenDTO;
import rs.uns.ac.ftn.studentewebservice.dto.UserDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.UserDTOToUserConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.UserToUserDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.User;
import rs.uns.ac.ftn.studentewebservice.security.TokenHelper;
import rs.uns.ac.ftn.studentewebservice.service.UserDetailsServiceImpl;
import rs.uns.ac.ftn.studentewebservice.service.UserService;

import java.util.List;

@RestController
@RequestMapping(value = "api/users")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserToUserDTOConverter toUserDTOConverter;

    @Autowired
    UserDTOToUserConverter toUserConverter;

    @Autowired
    TokenHelper tokenHelper;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @GetMapping(value = "/by-username/{username}")
    public ResponseEntity<UserDTO> getByUsername(@PathVariable("username") String username){
        return new ResponseEntity<>(toUserDTOConverter.convert(userService.findByUsername(username)), HttpStatus.OK);
    }

    @GetMapping(value = "/admins")
    public ResponseEntity<List<UserDTO>> getAdmins(Pageable pageable){
        Page<User> users = userService.getAdmins(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(users.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");
        System.out.println(users.getTotalPages());
        return new ResponseEntity<>(toUserDTOConverter.convert(users.getContent()), headers, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UserDTO> create(@RequestBody UserDTO userDTO){
        User user = userService.add(toUserConverter.convert(userDTO));
        return new ResponseEntity<>(toUserDTOConverter.convert(user), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<UserDTO> update(@RequestBody UserDTO userDTO, @PathVariable("id") Long id){
        User user = userService.edit(toUserConverter.convert(userDTO), id);
        return new ResponseEntity<>(toUserDTOConverter.convert(user), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        userService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping(value = "/login")
    public ResponseEntity<TokenDTO> login(@RequestBody LoginDTO loginDTO) {
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    loginDTO.getUsername(), loginDTO.getPassword());
            Authentication authentication = authenticationManager.authenticate(token);
            UserDetails details = userDetailsService.loadUserByUsername(loginDTO.getUsername());
            return new ResponseEntity<>(new TokenDTO(tokenHelper.generateToken(details)), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(new TokenDTO(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/change-password")
    public ResponseEntity<?> changePassword(@RequestBody PasswordChanger passwordChanger){
        userDetailsService.changePassword(passwordChanger.oldPassword, passwordChanger.newPassword);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    static class PasswordChanger {
        public String oldPassword;
        public String newPassword;
    }


}
