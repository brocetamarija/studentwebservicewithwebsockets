package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.TeacherDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Teacher;

import java.util.ArrayList;
import java.util.List;

@Component
public class TeacherToTeacherDTOConverter implements Converter<Teacher, TeacherDTO> {

    @Autowired
    UserToUserDTOConverter toUserDTOConverter;

    @Override
    public TeacherDTO convert(Teacher teacher) {
        TeacherDTO teacherDTO = new TeacherDTO();
        teacherDTO.setId(teacher.getId());
        teacherDTO.setTeacherCode(teacher.getTeacherCode());
        teacherDTO.setDeleted(teacher.isDeleted());
        teacherDTO.setUserDTO(toUserDTOConverter.convert(teacher.getUser()));
        return teacherDTO;
    }

    public List<TeacherDTO> convert(List<Teacher> teachers){
        List<TeacherDTO> retVal = new ArrayList<>();
        for(Teacher t:teachers){
            retVal.add(convert(t));
        }
        return retVal;
    }
}
