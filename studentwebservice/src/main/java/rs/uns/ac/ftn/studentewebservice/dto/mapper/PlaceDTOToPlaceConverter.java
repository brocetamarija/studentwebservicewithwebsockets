package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.PlaceDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Place;

@Component
public class PlaceDTOToPlaceConverter implements Converter<PlaceDTO, Place> {


    @Override
    public Place convert(PlaceDTO placeDTO) {
        Place place = new Place();
        place.setId(placeDTO.getId());
        place.setName(placeDTO.getName());
        place.setZipCode(placeDTO.getZipCode());
        place.setDeleted(placeDTO.isDeleted());
        return place;
    }
}
