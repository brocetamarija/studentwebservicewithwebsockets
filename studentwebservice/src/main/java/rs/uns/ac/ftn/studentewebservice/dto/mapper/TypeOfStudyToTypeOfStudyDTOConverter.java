package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.TypeOfStudyDTO;
import rs.uns.ac.ftn.studentewebservice.entity.TypeOfStudy;

import java.util.ArrayList;
import java.util.List;

@Component
public class TypeOfStudyToTypeOfStudyDTOConverter implements Converter<TypeOfStudy, TypeOfStudyDTO> {


    @Override
    public TypeOfStudyDTO convert(TypeOfStudy typeOfStudy) {
        TypeOfStudyDTO typeOfStudyDTO = new TypeOfStudyDTO();
        typeOfStudyDTO.setId(typeOfStudy.getId());
        typeOfStudyDTO.setName(typeOfStudy.getName());
        typeOfStudyDTO.setDeleted(typeOfStudy.isDeleted());
        return typeOfStudyDTO;
    }

    public List<TypeOfStudyDTO> convert(List<TypeOfStudy> typeOfStudies){
        List<TypeOfStudyDTO> retVal = new ArrayList<>();
        for(TypeOfStudy ts:typeOfStudies){
            retVal.add(convert(ts));
        }
        return retVal;
    }
}
