package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.EnrollmentDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Course;
import rs.uns.ac.ftn.studentewebservice.entity.Enrollment;
import rs.uns.ac.ftn.studentewebservice.entity.Student;
import rs.uns.ac.ftn.studentewebservice.service.CourseService;
import rs.uns.ac.ftn.studentewebservice.service.StudentService;

@Component
public class EnrollmentDTOToEnrollmentConverter implements Converter<EnrollmentDTO, Enrollment> {

    @Autowired
    StudentService studentService;

    @Autowired
    CourseService courseService;

    @Override
    public Enrollment convert(EnrollmentDTO enrollmentDTO) {
        Enrollment enrollment = new Enrollment();
        enrollment.setId(enrollmentDTO.getId());
        enrollment.setStartDate(enrollmentDTO.getStartDate());
        enrollment.setEndDate(enrollmentDTO.getEndDate());
        enrollment.setDeleted(enrollmentDTO.isDeleted());
        Student student = studentService.findById(enrollmentDTO.getStudentDTO().getId());
        if(student != null){
            enrollment.setStudent(student);
        }else{
            //TODO izazovi izuzetak
        }

        Course course = courseService.findById(enrollmentDTO.getCourseDTO().getId());
        if(course != null){
            enrollment.setCourse(course);
        }else{
            //TODO izazovi izuzetak
        }
        return enrollment;
    }
}
