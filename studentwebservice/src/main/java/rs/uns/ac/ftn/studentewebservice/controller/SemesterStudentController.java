package rs.uns.ac.ftn.studentewebservice.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.SemesterStudentDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.SemesterStudentDTOToSemesterStudentConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.SemesterStudentToSemesterStudentDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.SemesterStudent;
import rs.uns.ac.ftn.studentewebservice.service.SemesterStudentService;

@RestController
@RequestMapping(value = "api/semester-student")
public class SemesterStudentController {

    @Autowired
    SemesterStudentService semesterStudentService;

    @Autowired
    SemesterStudentDTOToSemesterStudentConverter toSemesterStudentConverter;

    @Autowired
    SemesterStudentToSemesterStudentDTOConverter toSemesterStudentDTOConverter;

    @GetMapping(value = "/check/{id}")
    public ResponseEntity<Boolean> check(@PathVariable("id") Long id){
        boolean check = semesterStudentService.checkIfSemesterAdded(id);
        return new ResponseEntity<>(check, HttpStatus.OK);
    }

    @GetMapping(value = "/is-certified/{id}")
    public ResponseEntity<SemesterStudentDTO> checkIfSemesterCertified(@PathVariable("id") Long id){
        SemesterStudent ss = semesterStudentService.getIsSemesterCertified(id);
        return new ResponseEntity<>(toSemesterStudentDTOConverter.convert(ss), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<SemesterStudentDTO> add(@RequestBody SemesterStudentDTO semesterStudentDTO){
        SemesterStudent semesterStudent = semesterStudentService.addSemesterToStudent(toSemesterStudentConverter.convert(semesterStudentDTO));
        return new ResponseEntity<>(toSemesterStudentDTOConverter.convert(semesterStudent), HttpStatus.OK);
    }

    @PutMapping(value = "/semester-certification/{id}")
    public ResponseEntity<SemesterStudentDTO> semesterCertification(@RequestBody SemesterStudentDTO semesterStudentDTO, @PathVariable("id") Long id){
        SemesterStudent semesterStudent = semesterStudentService.semesterCertification(toSemesterStudentConverter.convert(semesterStudentDTO), id);
        return new ResponseEntity<>(toSemesterStudentDTOConverter.convert(semesterStudent), HttpStatus.OK);
    }
}
