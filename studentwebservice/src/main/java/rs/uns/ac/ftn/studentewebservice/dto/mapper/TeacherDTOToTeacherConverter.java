package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.TeacherDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Teacher;

@Component
public class TeacherDTOToTeacherConverter implements Converter<TeacherDTO, Teacher> {

    @Autowired
    UserDTOToUserConverter toUserConverter;

    @Override
    public Teacher convert(TeacherDTO teacherDTO) {
        Teacher teacher = new Teacher();
        teacher.setId(teacherDTO.getId());
        teacher.setTeacherCode(teacherDTO.getTeacherCode());
        teacher.setDeleted(teacherDTO.isDeleted());
        teacher.setUser(toUserConverter.convert(teacherDTO.getUserDTO()));
        return teacher;
    }
}
