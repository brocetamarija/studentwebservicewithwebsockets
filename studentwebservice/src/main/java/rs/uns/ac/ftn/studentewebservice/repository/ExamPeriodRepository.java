package rs.uns.ac.ftn.studentewebservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.uns.ac.ftn.studentewebservice.entity.ExamPeriod;

public interface ExamPeriodRepository extends JpaRepository<ExamPeriod, Long> {
}
