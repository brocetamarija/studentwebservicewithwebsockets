package rs.uns.ac.ftn.studentewebservice.dto;

public class AddressDTO {

    private Long id;
    private String streetName;
    private int streetNumber;
    private boolean deleted;
    private PlaceDTO placeDTO;

    public AddressDTO() {
    }

    public AddressDTO(Long id, String streetName, int streetNumber, boolean deleted, PlaceDTO placeDTO) {
        this.id = id;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.deleted = deleted;
        this.placeDTO = placeDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public PlaceDTO getPlaceDTO() {
        return placeDTO;
    }

    public void setPlaceDTO(PlaceDTO placeDTO) {
        this.placeDTO = placeDTO;
    }
}
