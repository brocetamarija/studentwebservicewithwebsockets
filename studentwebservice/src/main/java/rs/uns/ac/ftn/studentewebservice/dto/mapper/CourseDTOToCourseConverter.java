package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.CourseDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Course;
import rs.uns.ac.ftn.studentewebservice.entity.StudyProgram;
import rs.uns.ac.ftn.studentewebservice.service.StudyProgramService;

@Component
public class CourseDTOToCourseConverter implements Converter<CourseDTO, Course> {

    @Autowired
    StudyProgramService studyProgramService;

    @Override
    public Course convert(CourseDTO courseDTO) {
        Course course = new Course();
        course.setId(courseDTO.getId());
        course.setName(courseDTO.getName());
        course.setCode(courseDTO.getCode());
        course.setECTSPoints(courseDTO.getECTSPoints());
        course.setNumberOfClass(courseDTO.getNumberOfClass());
        course.setYearOfStudy(courseDTO.getYearOfStudy());
        course.setDeleted(courseDTO.isDeleted());
        StudyProgram studyProgram = studyProgramService.findById(courseDTO.getStudyProgramDTO().getId());
        if(studyProgram != null){
            course.setStudyProgram(studyProgram);
        }else{
            //TODO izazovi izuzatak
        }
        return course;
    }
}
