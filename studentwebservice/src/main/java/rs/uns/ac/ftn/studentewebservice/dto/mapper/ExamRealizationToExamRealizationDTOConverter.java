package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.ExamRealizationDTO;
import rs.uns.ac.ftn.studentewebservice.entity.ExamRealization;

import java.util.ArrayList;
import java.util.List;

@Component
public class ExamRealizationToExamRealizationDTOConverter implements Converter<ExamRealization, ExamRealizationDTO> {

    @Autowired
    CourseToCourseDTOConverter toCourseDTOConverter;

    @Autowired
    TeacherToTeacherDTOConverter toTeacherDTOConverter;

    @Autowired
    ExamPeriodToExamPeriodDTOConverter toExamPeriodDTOConverter;

    @Override
    public ExamRealizationDTO convert(ExamRealization examRealization) {
        ExamRealizationDTO examRealizationDTO = new ExamRealizationDTO();
        examRealizationDTO.setId(examRealization.getId());
        examRealizationDTO.setClassroom(examRealization.getClassroom());
        examRealizationDTO.setDate(examRealization.getDate());
        examRealizationDTO.setExamPrice(examRealization.getExamPrice());
        examRealizationDTO.setDeleted(examRealization.isDeleted());
        examRealizationDTO.setCourseDTO(toCourseDTOConverter.convert(examRealization.getCourse()));
        examRealizationDTO.setTeacherDTO(toTeacherDTOConverter.convert(examRealization.getTeacher()));
        examRealizationDTO.setExamPeriodDTO(toExamPeriodDTOConverter.convert(examRealization.getExamPeriod()));
        return examRealizationDTO;
    }

    public List<ExamRealizationDTO> convert(List<ExamRealization> examRealizations){
        List<ExamRealizationDTO> retVal = new ArrayList<>();
        for(ExamRealization er:examRealizations){
            retVal.add(convert(er));
        }
        return retVal;
    }
}
