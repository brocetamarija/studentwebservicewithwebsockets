package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.EnrollmentDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Enrollment;

import java.util.ArrayList;
import java.util.List;

@Component
public class EnrollmentToEnrollmentDTOConverter implements Converter<Enrollment, EnrollmentDTO> {

    @Autowired
    CourseToCourseDTOConverter toCourseDTOConverter;

    @Autowired
    StudentToStudentDTOConverter toStudentDTOConverter;

    @Override
    public EnrollmentDTO convert(Enrollment enrollment) {
        EnrollmentDTO enrollmentDTO = new EnrollmentDTO();
        enrollmentDTO.setId(enrollment.getId());
        enrollmentDTO.setStartDate(enrollment.getStartDate());
        enrollmentDTO.setEndDate(enrollment.getEndDate());
        enrollmentDTO.setDeleted(enrollment.isDeleted());
        enrollmentDTO.setStudentDTO(toStudentDTOConverter.convert(enrollment.getStudent()));
        enrollmentDTO.setCourseDTO(toCourseDTOConverter.convert(enrollment.getCourse()));
        return enrollmentDTO;
    }

    public List<EnrollmentDTO> convert(List<Enrollment> enrollments){
        List<EnrollmentDTO> retVal = new ArrayList<>();
        for(Enrollment e:enrollments){
            retVal.add(convert(e));
        }
        return retVal;
    }
}
