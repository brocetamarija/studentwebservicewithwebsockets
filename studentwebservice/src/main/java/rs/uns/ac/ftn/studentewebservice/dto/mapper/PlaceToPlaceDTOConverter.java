package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.PlaceDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Place;

import java.util.ArrayList;
import java.util.List;

@Component
public class PlaceToPlaceDTOConverter implements Converter<Place, PlaceDTO> {


    @Override
    public PlaceDTO convert(Place place) {
        PlaceDTO placeDTO = new PlaceDTO();
        placeDTO.setId(place.getId());
        placeDTO.setName(place.getName());
        placeDTO.setZipCode(place.getZipCode());
        placeDTO.setDeleted(place.isDeleted());
        return placeDTO;
    }

    public List<PlaceDTO> convert(List<Place> places){
        List<PlaceDTO> retVal = new ArrayList<>();
        for(Place p:places){
            retVal.add(convert(p));
        }
        return retVal;
    }
}
