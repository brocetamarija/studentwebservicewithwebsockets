package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.AddressDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.AddressDTOToAddressConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.AddressToAddressDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.Address;
import rs.uns.ac.ftn.studentewebservice.service.AddressService;


@RestController
@RequestMapping(value = "api/addresses")
public class AddressController {

    @Autowired
    AddressService addressService;

    @Autowired
    AddressToAddressDTOConverter toAddressDTOConverter;

    @Autowired
    AddressDTOToAddressConverter toAddressConverter;


    @PostMapping
    public ResponseEntity<AddressDTO> create(@RequestBody AddressDTO addressDTO){
        Address address = addressService.save(toAddressConverter.convert(addressDTO));
        return new ResponseEntity<>(toAddressDTOConverter.convert(address), HttpStatus.CREATED);
    }

}
