package rs.uns.ac.ftn.studentewebservice.entity;

public enum PaymentType {

    PAYMENT_IN,
    PAYMENT_OUT
}
