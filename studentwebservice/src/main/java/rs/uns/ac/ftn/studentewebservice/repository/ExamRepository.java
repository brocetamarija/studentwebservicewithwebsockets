package rs.uns.ac.ftn.studentewebservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.uns.ac.ftn.studentewebservice.entity.Exam;

import java.util.List;

public interface ExamRepository extends JpaRepository<Exam, Long> {
    List<Exam> findByStudent_Id(Long id);

    List<Exam> findByStudent_IdAndExamRealization_ExamPeriod_Id(Long studentID, Long examPeriodID);

    List<Exam> findByExamRealization_Id(Long id);
}
