package rs.uns.ac.ftn.studentewebservice.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Where(clause = "deleted = false")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "code", nullable = false, length = 10)
    private String code;

    @Column(name = "name", nullable = false, length = 30)
    private String name;

    @Column(name = "ectsPoints", nullable = false)
    private int ECTSPoints;

    @Column(name = "numberOfClass")
    private int numberOfClass;

    @Column(name = "yearOfStudy")
    private int yearOfStudy;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @ManyToOne
    @JoinColumn(name = "studyProgramId", referencedColumnName = "id")
    private StudyProgram studyProgram;

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Enrollment> enrollments = new HashSet<>();

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Teaching> teachings = new HashSet<>();

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<ExamRealization> examRealizations = new HashSet<>();

    public Course() {
    }

    public Course(String code, String name, int ECTSPoints, int numberOfClass, int yearOfStudy, boolean deleted, StudyProgram studyProgram, Set<Enrollment> enrollments, Set<Teaching> teachings, Set<ExamRealization> examRealizations) {
        this.code = code;
        this.name = name;
        this.ECTSPoints = ECTSPoints;
        this.numberOfClass = numberOfClass;
        this.yearOfStudy = yearOfStudy;
        this.deleted = deleted;
        this.studyProgram = studyProgram;
        this.enrollments = enrollments;
        this.teachings = teachings;
        this.examRealizations = examRealizations;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getECTSPoints() {
        return ECTSPoints;
    }

    public void setECTSPoints(int ECTSPoints) {
        this.ECTSPoints = ECTSPoints;
    }

    public int getNumberOfClass() {
        return numberOfClass;
    }

    public void setNumberOfClass(int numberOfClass) {
        this.numberOfClass = numberOfClass;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public StudyProgram getStudyProgram() {
        return studyProgram;
    }

    public void setStudyProgram(StudyProgram studyProgram) {
        this.studyProgram = studyProgram;
    }

    public Set<Enrollment> getEnrollments() {
        return enrollments;
    }

    public void setEnrollments(Set<Enrollment> enrollments) {
        this.enrollments = enrollments;
    }

    public Set<Teaching> getTeachings() {
        return teachings;
    }

    public void setTeachings(Set<Teaching> teachings) {
        this.teachings = teachings;
    }

    public Set<ExamRealization> getExamRealizations() {
        return examRealizations;
    }

    public void setExamRealizations(Set<ExamRealization> examRealizations) {
        this.examRealizations = examRealizations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Course c = (Course) o;
        return Objects.equals(id, c.id);
    }

    public void add(Enrollment enrollment) {
        if (enrollment.getCourse() != null) {
            enrollment.getCourse().getEnrollments().remove(enrollment);
        }
        enrollment.setCourse(this);
        getEnrollments().add(enrollment);
    }

    public void remove(Enrollment enrollment) {
        enrollment.setCourse(null);
        getEnrollments().remove(enrollment);
    }

    public void add(Teaching teaching) {
        if (teaching.getCourse() != null) {
            teaching.getCourse().getTeachings().remove(teaching);
        }
        teaching.setCourse(this);
        getTeachings().add(teaching);
    }

    public void remove(Teaching teaching) {
        teaching.setCourse(null);
        getTeachings().remove(teaching);
    }

    public void add(ExamRealization examRealization) {
        if (examRealization.getCourse() != null) {
            examRealization.getCourse().getExamRealizations().remove(examRealization);
        }
        examRealization.setCourse(this);
        getExamRealizations().add(examRealization);
    }

    public void remove(ExamRealization examRealization) {
        examRealization.setCourse(null);
        getExamRealizations().remove(examRealization);
    }
}
