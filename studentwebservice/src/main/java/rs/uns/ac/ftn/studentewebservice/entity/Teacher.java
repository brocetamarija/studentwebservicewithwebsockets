package rs.uns.ac.ftn.studentewebservice.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Where(clause = "deleted = false")
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "teacherCode", nullable = false, length = 10)
    private String teacherCode;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @OneToOne
    @JoinColumn(name = "userId", referencedColumnName = "id")
    private User user;

    @OneToMany(mappedBy = "teacher", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Teaching> teachings = new HashSet<>();

    @OneToMany(mappedBy = "teacher", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<TeacherTitle> teacherTitles = new HashSet<>();

    public Teacher() {
    }

    public Teacher(String teacherCode, boolean deleted, User user, Set<Teaching> teachings, Set<TeacherTitle> teacherTitles) {
        this.teacherCode = teacherCode;
        this.deleted = deleted;
        this.user = user;
        this.teachings = teachings;
        this.teacherTitles = teacherTitles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeacherCode() {
        return teacherCode;
    }

    public void setTeacherCode(String teacherCode) {
        this.teacherCode = teacherCode;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Teaching> getTeachings() {
        return teachings;
    }

    public void setTeachings(Set<Teaching> teachings) {
        this.teachings = teachings;
    }

    public Set<TeacherTitle> getTeacherTitles() {
        return teacherTitles;
    }

    public void setTeacherTitles(Set<TeacherTitle> teacherTitles) {
        this.teacherTitles = teacherTitles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Teacher t = (Teacher) o;
        return Objects.equals(id, t.id);
    }

    public void add(TeacherTitle teacherTitle) {
        if (teacherTitle.getTeacher() != null) {
            teacherTitle.getTeacher().getTeacherTitles().remove(teacherTitle);
        }
        teacherTitle.setTeacher(this);
        getTeacherTitles().add(teacherTitle);
    }

    public void remove(TeacherTitle teacherTitle) {
        teacherTitle.setTitle(null);
        getTeacherTitles().remove(teacherTitle);
    }

    public void add(Teaching teaching) {
        if (teaching.getTeacher() != null) {
            teaching.getTeacher().getTeachings().remove(teaching);
        }
        teaching.setTeacher(this);
        getTeachings().add(teaching);
    }

    public void remove(Teaching teaching) {
        teaching.setTeacher(null);
        getTeachings().remove(teaching);
    }
}
