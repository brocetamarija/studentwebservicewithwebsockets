package rs.uns.ac.ftn.studentewebservice.dto;

public class NotificationDTO {

    private String message;
    private String id;

    public NotificationDTO(String message, String id) {
        this.message = message;
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public String getId() {
        return id;
    }

}
