package rs.uns.ac.ftn.studentewebservice.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Where(clause = "deleted = false")
public class Semester {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.DATE)
    @Column(name = "startDate", nullable = false)
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "endDate", nullable = false)
    private Date endDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "semesterType", nullable = false, length = 15)
    private SemesterType semesterType;

    @Column(name = "deleted")
    private boolean deleted;

    @OneToMany(mappedBy = "semester", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<SemesterStudent> semesterStudents = new HashSet<>();

    //getters, setters, constructors

    public Semester() {
    }

    public Semester(Date startDate, Date endDate, SemesterType semesterType, boolean deleted, Set<SemesterStudent> semesterStudents) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.semesterType = semesterType;
        this.deleted = deleted;
        this.semesterStudents = semesterStudents;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public SemesterType getSemesterType() {
        return semesterType;
    }

    public void setSemesterType(SemesterType semesterType) {
        this.semesterType = semesterType;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Set<SemesterStudent> getSemesterStudents() {
        return semesterStudents;
    }

    public void setSemesterStudents(Set<SemesterStudent> semesterStudents) {
        this.semesterStudents = semesterStudents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Semester s = (Semester) o;
        return Objects.equals(id, s.id);
    }
}
