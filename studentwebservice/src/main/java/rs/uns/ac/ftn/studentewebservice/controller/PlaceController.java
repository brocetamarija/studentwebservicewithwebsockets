package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.PlaceDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.PlaceDTOToPlaceConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.PlaceToPlaceDTOConverter;
import rs.uns.ac.ftn.studentewebservice.service.PlaceService;

import java.util.List;

@RestController
@RequestMapping(value = "api/places")
public class PlaceController {

    @Autowired
    PlaceService placeService;

    @Autowired
    PlaceToPlaceDTOConverter toPlaceDTOConverter;

    @Autowired
    PlaceDTOToPlaceConverter toPlaceConverter;

    @GetMapping
    public ResponseEntity<List<PlaceDTO>> findAll(){
        return new ResponseEntity<>(toPlaceDTOConverter.convert(placeService.findAll()), HttpStatus.OK);
    }

}
