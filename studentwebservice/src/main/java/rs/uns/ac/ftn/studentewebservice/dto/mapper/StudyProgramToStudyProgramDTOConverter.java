package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.StudyProgramDTO;
import rs.uns.ac.ftn.studentewebservice.entity.StudyProgram;

import java.util.ArrayList;
import java.util.List;

@Component
public class StudyProgramToStudyProgramDTOConverter implements Converter<StudyProgram, StudyProgramDTO> {

    @Autowired
    TypeOfStudyToTypeOfStudyDTOConverter toTypeOfStudyDTOConverter;

    @Override
    public StudyProgramDTO convert(StudyProgram studyProgram) {
        StudyProgramDTO studyProgramDTO = new StudyProgramDTO();
        studyProgramDTO.setId(studyProgram.getId());
        studyProgramDTO.setName(studyProgram.getName());
        studyProgramDTO.setTypeOfStudyDTO(toTypeOfStudyDTOConverter.convert(studyProgram.getTypeOfStudy()));
        studyProgramDTO.setDeleted(studyProgram.isDeleted());
        return studyProgramDTO;
    }

    public List<StudyProgramDTO> convert(List<StudyProgram> studyPrograms){
        List<StudyProgramDTO> retVal = new ArrayList<>();
        for(StudyProgram sp:studyPrograms){
            retVal.add(convert(sp));
        }
        return retVal;
    }
}
