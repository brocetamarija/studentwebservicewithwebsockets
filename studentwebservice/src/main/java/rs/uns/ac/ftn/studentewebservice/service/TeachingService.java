package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.Teaching;
import rs.uns.ac.ftn.studentewebservice.repository.TeachingRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.List;

@Service
public class TeachingService {

    @Autowired
    TeachingRepository teachingRepository;

    public Page<Teaching> findAll(Pageable pageable) {
        return teachingRepository.findAll(pageable);
    }

    public Teaching findById(Long id) {
        return teachingRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Teaching Not Found!", "Teaching with given id: " + id + " is not found!"));
    }

    public List<Teaching> findByTeacherId(Long id) {
        return teachingRepository.findByTeacher_Id(id);
    }

    public List<Teaching> findByCourseId(Long id) {
        return teachingRepository.findByCourse_Id(id);
    }

    public Teaching save(Teaching teaching) {
        return teachingRepository.save(teaching);
    }

    public void delete(Long id) {
        Teaching teaching = findById(id);
        teaching.setDeleted(true);
        teachingRepository.save(teaching);
    }

    public void deleteByTeacherId(Long id) {
        List<Teaching> teachings = findByTeacherId(id);
        for (Teaching t : teachings) {
            delete(t.getId());
        }
    }

    public void deleteByCourseId(Long id) {
        List<Teaching> teachings = findByCourseId(id);
        for (Teaching t : teachings) {
            delete(t.getId());
        }
    }

    public Page<Teaching> findByTeacherId(Long id, Pageable pageable) {
        return teachingRepository.findByTeacher_Id(id, pageable);
    }

}
