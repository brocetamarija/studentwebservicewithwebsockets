package rs.uns.ac.ftn.studentewebservice.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Where(clause = "deleted = false")
public class Title {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @OneToMany(mappedBy = "title", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<TeacherTitle> teacherTitles = new HashSet<>();

    public Title() {
    }

    public Title(String name, boolean deleted, Set<TeacherTitle> teacherTitles) {
        this.name = name;
        this.deleted = deleted;
        this.teacherTitles = teacherTitles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Set<TeacherTitle> getTeacherTitles() {
        return teacherTitles;
    }

    public void setTeacherTitles(Set<TeacherTitle> teacherTitles) {
        this.teacherTitles = teacherTitles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Title t = (Title) o;
        return Objects.equals(id, t.id);
    }

    public void add(TeacherTitle teacherTitle) {
        if (teacherTitle.getTitle() != null) {
            teacherTitle.getTitle().getTeacherTitles().remove(teacherTitle);
        }
        teacherTitle.setTitle(this);
        getTeacherTitles().add(teacherTitle);
    }

    public void remove(TeacherTitle teacherTitle) {
        teacherTitle.setTitle(null);
        getTeacherTitles().remove(teacherTitle);
    }
}
