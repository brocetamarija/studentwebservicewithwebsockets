package rs.uns.ac.ftn.studentewebservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.uns.ac.ftn.studentewebservice.entity.Course;

import java.util.List;

public interface CourseRepository extends JpaRepository<Course, Long> {
    List<Course> findByStudyProgram_Id(Long id);

    Page<Course> findByStudyProgram_Id(Long id, Pageable pageable);

    List<Course> findByStudyProgram_IdAndYearOfStudy(Long id, int year);
}
