package rs.uns.ac.ftn.studentewebservice.dto;


public class StudyProgramDTO {

    private Long id;
    private String name;
    private TypeOfStudyDTO typeOfStudyDTO;
    private boolean deleted;

    public StudyProgramDTO() {
    }

    public StudyProgramDTO(Long id, String name, TypeOfStudyDTO typeOfStudyDTO, boolean deleted) {
        this.id = id;
        this.name = name;
        this.typeOfStudyDTO = typeOfStudyDTO;
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TypeOfStudyDTO getTypeOfStudyDTO() {
        return typeOfStudyDTO;
    }

    public void setTypeOfStudyDTO(TypeOfStudyDTO typeOfStudyDTO) {
        this.typeOfStudyDTO = typeOfStudyDTO;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
