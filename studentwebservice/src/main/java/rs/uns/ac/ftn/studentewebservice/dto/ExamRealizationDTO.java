package rs.uns.ac.ftn.studentewebservice.dto;

import java.util.Date;

public class ExamRealizationDTO {

    private Long id;
    private Date date;
    private String classroom;
    private double examPrice;
    private boolean deleted;
    private CourseDTO courseDTO;
    private TeacherDTO teacherDTO;
    private ExamPeriodDTO examPeriodDTO;

    public ExamRealizationDTO() {}

    public ExamRealizationDTO(Long id, Date date, String classroom, double examPrice, boolean deleted, CourseDTO courseDTO, TeacherDTO teacherDTO, ExamPeriodDTO examPeriodDTO) {
        this.id = id;
        this.date = date;
        this.classroom = classroom;
        this.examPrice = examPrice;
        this.deleted = deleted;
        this.courseDTO = courseDTO;
        this.teacherDTO = teacherDTO;
        this.examPeriodDTO = examPeriodDTO;
    }

   /*public ExamRealizationDTO(ExamRealization examRealization){
        this(examRealization.getId(),
                examRealization.getDate(),
                examRealization.getClassroom(),
                examRealization.isDeleted(),
                new CourseDTO(examRealization.getCourse()),
                new TeacherDTO(examRealization.getTeacher()),
                new ExamPeriodDTO(examRealization.getExamPeriod()));
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public double getExamPrice() {
        return examPrice;
    }

    public void setExamPrice(double examPrice) {
        this.examPrice = examPrice;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public CourseDTO getCourseDTO() {
        return courseDTO;
    }

    public void setCourseDTO(CourseDTO courseDTO) {
        this.courseDTO = courseDTO;
    }

    public TeacherDTO getTeacherDTO() {
        return teacherDTO;
    }

    public void setTeacherDTO(TeacherDTO teacherDTO) {
        this.teacherDTO = teacherDTO;
    }

    public ExamPeriodDTO getExamPeriodDTO() {
        return examPeriodDTO;
    }

    public void setExamPeriodDTO(ExamPeriodDTO examPeriodDTO) {
        this.examPeriodDTO = examPeriodDTO;
    }
}
