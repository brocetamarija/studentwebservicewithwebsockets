package rs.uns.ac.ftn.studentewebservice.dto;

public class PaymentPurposeDTO {

    private Long id;
    private String name;
    private boolean deleted;

    public PaymentPurposeDTO() {
    }

    public PaymentPurposeDTO(Long id, String name, boolean deleted) {
        this.id = id;
        this.name = name;
        this.deleted = deleted;
    }

   /* public PaymentPurposeDTO(PaymentPurpose paymentPurpose){
        this(paymentPurpose.getId(),
                paymentPurpose.getName(),
                paymentPurpose.isDeleted());
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
