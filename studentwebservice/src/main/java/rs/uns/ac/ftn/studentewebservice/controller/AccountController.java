package rs.uns.ac.ftn.studentewebservice.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.AccountDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.AccountDTOToAccountConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.AccountToAccountDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.Account;
import rs.uns.ac.ftn.studentewebservice.service.AccountService;

@RestController
@RequestMapping(value = "api/accounts")
public class AccountController {

    @Autowired
    AccountService accountService;

    @Autowired
    AccountToAccountDTOConverter toAccountDTOConverter;

    @Autowired
    AccountDTOToAccountConverter toAccountConverter;

    @GetMapping(value = "/student/{id}")
    public ResponseEntity<AccountDTO> findByStudentId(@PathVariable("id") Long id){
        Account account = accountService.findByStudentId(id);
        if(account == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(toAccountDTOConverter.convert(account), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<AccountDTO> create(@RequestBody AccountDTO accountDTO){
        Account account = accountService.create(toAccountConverter.convert(accountDTO));

        return new ResponseEntity<>(toAccountDTOConverter.convert(account), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<AccountDTO> update(@RequestBody AccountDTO accountDTO, @PathVariable("id") Long id){
        Account account = accountService.edit(toAccountConverter.convert(accountDTO), id);
        return new ResponseEntity<>(toAccountDTOConverter.convert(account), HttpStatus.OK);
    }

    @PutMapping(value = "/update-balance-add/{id}")
    public ResponseEntity<AccountDTO> updateCurrentAccountBalanceAdd(@RequestBody AccountDTO accountDTO,  @PathVariable("id") Long id, @RequestParam("amount") double amount){
        Account account = accountService.updateCurrentAccountBalanceAdd(toAccountConverter.convert(accountDTO), id, amount);
        return new ResponseEntity<>(toAccountDTOConverter.convert(account), HttpStatus.OK);
    }

    @PutMapping(value = "/update-balance-subtract/{id}")
    public ResponseEntity<AccountDTO> updateCurrentAccountBalanceSubtract(@RequestBody AccountDTO accountDTO,  @PathVariable("id") Long id, @RequestParam("amount") double amount){
        Account account = accountService.updateCurrentAccountBalanceSubstract(toAccountConverter.convert(accountDTO), id, amount);
        return new ResponseEntity<>(toAccountDTOConverter.convert(account), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        accountService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
