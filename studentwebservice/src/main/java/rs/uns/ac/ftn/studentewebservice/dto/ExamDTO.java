package rs.uns.ac.ftn.studentewebservice.dto;

public class ExamDTO {

    private Long id;
    private int labPoints;
    private int examPoints;
    private int totalPoints;
    private int finalGrade;
    private boolean deleted;
    private StudentDTO studentDTO;
    private ExamRealizationDTO examRealizationDTO;

    public ExamDTO() {}

    public ExamDTO(Long id, int labPoints, int examPoints, int totalPoints, int finalGrade, boolean deleted, StudentDTO studentDTO, ExamRealizationDTO examRealizationDTO) {
        this.id = id;
        this.labPoints = labPoints;
        this.examPoints = examPoints;
        this.totalPoints = totalPoints;
        this.finalGrade = finalGrade;
        this.deleted = deleted;
        this.studentDTO = studentDTO;
        this.examRealizationDTO = examRealizationDTO;
    }

    /*public ExamDTO(Exam exam){
        this(exam.getId(),
                exam.getLabPoints(),
                exam.getExamPoints(),
                exam.getTotalPoints(),
                exam.getFinalGrade(),
                exam.isDeleted(),
                new StudentDTO(exam.getStudent()),
                new ExamRealizationDTO(exam.getExamRealization()));
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getLabPoints() {
        return labPoints;
    }

    public void setLabPoints(int labPoints) {
        this.labPoints = labPoints;
    }

    public int getExamPoints() {
        return examPoints;
    }

    public void setExamPoints(int examPoints) {
        this.examPoints = examPoints;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public int getFinalGrade() {
        return finalGrade;
    }

    public void setFinalGrade(int finalGrade) {
        this.finalGrade = finalGrade;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public StudentDTO getStudentDTO() {
        return studentDTO;
    }

    public void setStudentDTO(StudentDTO studentDTO) {
        this.studentDTO = studentDTO;
    }

    public ExamRealizationDTO getExamRealizationDTO() {
        return examRealizationDTO;
    }

    public void setExamRealizationDTO(ExamRealizationDTO examRealizationDTO) {
        this.examRealizationDTO = examRealizationDTO;
    }
}
