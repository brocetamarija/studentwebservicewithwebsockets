package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.CourseDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.CourseDTOToCourseConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.CourseToCourseDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.Course;
import rs.uns.ac.ftn.studentewebservice.service.CourseService;

import java.util.List;

@RestController
@RequestMapping(value = "api/courses")
public class CourseController {

    @Autowired
    CourseService courseService;

    @Autowired
    CourseToCourseDTOConverter toCourseDTOConverter;

    @Autowired
    CourseDTOToCourseConverter toCourseConverter;

    @GetMapping
    public ResponseEntity<List<CourseDTO>> findAll(){
        return new ResponseEntity<>(toCourseDTOConverter.convert(courseService.findAll()),HttpStatus.OK);
    }

    @GetMapping(value = "/pageable")
    public ResponseEntity<List<CourseDTO>> findAll(Pageable pageable){
        Page<Course> courses = courseService.findAll(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(courses.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");

        return new ResponseEntity<>(toCourseDTOConverter.convert(courses.getContent()), headers, HttpStatus.OK);
    }

    @GetMapping(value = "/course/{id}")
    public ResponseEntity<List<CourseDTO>> getByStudyProgram(@PathVariable("id") Long id, Pageable pageable){
        Page<Course> courses = courseService.findByStudyProgramPageable(id, pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages", Integer.toString(courses.getTotalPages()));
        headers.add("access-control-expose-headers", "totalPages");

        return new ResponseEntity<>(toCourseDTOConverter.convert(courses.getContent()), headers, HttpStatus.OK);
    }

    @GetMapping(value = "/study-program/{studyProgramId}/year/{yearId}")
    public ResponseEntity<List<CourseDTO>> findByStudyProgramAndYearOfStudy(@PathVariable("studyProgramId") Long studyProgramId, @PathVariable("yearId")int yearId){
        return new ResponseEntity<>(toCourseDTOConverter.convert(courseService.findByStudyProgramAndYearOfStudy(studyProgramId,yearId)), HttpStatus.OK);
    }

    @GetMapping(value = "/study-program/{id}")
    public ResponseEntity<List<CourseDTO>> findByStudyProgram(@PathVariable("id") Long id){
        return new ResponseEntity<>(toCourseDTOConverter.convert(courseService.findByStudyProgramId(id)), HttpStatus.OK);
    }


    @GetMapping(value = "/unfinished-courses/{id}")
    public ResponseEntity<List<CourseDTO>> failedExams(@PathVariable("id") Long id){
        return new ResponseEntity<>(toCourseDTOConverter.convert(courseService.unfinishedCourses(id)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<CourseDTO> create(@RequestBody CourseDTO courseDTO){
        Course course = courseService.save(toCourseConverter.convert(courseDTO));
        return new ResponseEntity<>(toCourseDTOConverter.convert(course), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<CourseDTO> update(@RequestBody CourseDTO courseDTO, @PathVariable("id") Long id){
        Course course = courseService.edit(toCourseConverter.convert(courseDTO),id);
        return new ResponseEntity<>(toCourseDTOConverter.convert(course), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        courseService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
