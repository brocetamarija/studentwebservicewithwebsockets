package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.ExamDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Exam;

import java.util.ArrayList;
import java.util.List;

@Component
public class ExamToExamDTOConverter implements Converter<Exam, ExamDTO> {

    @Autowired
    StudentToStudentDTOConverter toStudentDTOConverter;

    @Autowired
    ExamRealizationToExamRealizationDTOConverter toExamRealizationDTOConverter;


    @Override
    public ExamDTO convert(Exam exam) {
        ExamDTO examDTO = new ExamDTO();
        examDTO.setId(exam.getId());
        examDTO.setLabPoints(exam.getLabPoints());
        examDTO.setExamPoints(exam.getExamPoints());
        examDTO.setTotalPoints(exam.getTotalPoints());
        examDTO.setFinalGrade(exam.getFinalGrade());
        examDTO.setDeleted(exam.isDeleted());
        examDTO.setStudentDTO(toStudentDTOConverter.convert(exam.getStudent()));
        examDTO.setExamRealizationDTO(toExamRealizationDTOConverter.convert(exam.getExamRealization()));
        return examDTO;
    }

    public List<ExamDTO> convert(List<Exam> exams){
        List<ExamDTO> retVal = new ArrayList<>();
        for(Exam e:exams){
            retVal.add(convert(e));
        }
        return retVal;
    }
}
