package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.SemesterStudentDTO;
import rs.uns.ac.ftn.studentewebservice.entity.SemesterStudent;

import java.util.ArrayList;
import java.util.List;

@Component
public class SemesterStudentToSemesterStudentDTOConverter implements Converter<SemesterStudent, SemesterStudentDTO> {

    @Autowired
    StudentToStudentDTOConverter toStudentDTOConverter;

    @Autowired
    SemesterToSemesterDTOConverter toSemesterDTOConverter;

    @Override
    public SemesterStudentDTO convert(SemesterStudent semesterStudent) {
        SemesterStudentDTO semesterStudentDTO = new SemesterStudentDTO();
        semesterStudentDTO.setId(semesterStudent.getId());
        semesterStudentDTO.setCertified(semesterStudent.isCertified());
        semesterStudentDTO.setSemesterCertificationPrice(semesterStudent.getSemesterCertificationPrice());
        semesterStudentDTO.setDeleted(semesterStudent.isDeleted());
        semesterStudentDTO.setStudentDTO(toStudentDTOConverter.convert(semesterStudent.getStudent()));
        semesterStudentDTO.setSemesterDTO(toSemesterDTOConverter.convert(semesterStudent.getSemester()));
        return semesterStudentDTO;
    }

    public List<SemesterStudentDTO> convert(List<SemesterStudent> semesterStudents){
        List<SemesterStudentDTO> retVal = new ArrayList<>();
        for(SemesterStudent ss:semesterStudents){
            retVal.add(convert(ss));
        }
        return retVal;
    }


}
