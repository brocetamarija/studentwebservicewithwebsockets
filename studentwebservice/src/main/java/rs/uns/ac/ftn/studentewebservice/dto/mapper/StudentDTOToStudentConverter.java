package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.StudentDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Student;
import rs.uns.ac.ftn.studentewebservice.entity.StudyProgram;
import rs.uns.ac.ftn.studentewebservice.service.StudyProgramService;

@Component
public class StudentDTOToStudentConverter implements Converter<StudentDTO, Student> {

    @Autowired
    StudyProgramService studyProgramService;

    @Autowired
    UserDTOToUserConverter toUserConverter;

    @Autowired
    AccountDTOToAccountConverter toAccountConverter;

    @Override
    public Student convert(StudentDTO studentDTO) {
        Student student = new Student();
        student.setId(studentDTO.getId());
        student.setCardNumber(studentDTO.getCardNumber());
        student.setPhoneNumber(studentDTO.getPhoneNumber());
        student.setBirthDate(studentDTO.getBirthDate());
        student.setYearOfStudy(studentDTO.getYearOfStudy());
        student.setDeleted(studentDTO.isDeleted());
        student.setUser(toUserConverter.convert(studentDTO.getUserDTO()));
        StudyProgram studyProgram = studyProgramService.findById(studentDTO.getStudyProgramDTO().getId());
        if(studyProgram != null){
            student.setStudyProgram(studyProgram);
        }else{
            //TODO izazovi izuzetak
        }
        return student;
    }
}
