package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.StudyProgramDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.StudyProgramDTOToStudyProgramConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.StudyProgramToStudyProgramDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.StudyProgram;
import rs.uns.ac.ftn.studentewebservice.service.StudyProgramService;

import java.util.List;

@RestController
@RequestMapping(value = "api/study-programs")
public class StudyProgramController {

    @Autowired
    StudyProgramService studyProgramService;

    @Autowired
    StudyProgramToStudyProgramDTOConverter toStudyProgramDTOConverter;

    @Autowired
    StudyProgramDTOToStudyProgramConverter toStudyProgramConverter;

    @GetMapping
    public ResponseEntity<List<StudyProgramDTO>> findAll(){
        return new ResponseEntity<>(toStudyProgramDTOConverter.convert(studyProgramService.findAll()), HttpStatus.OK);
    }

    @GetMapping(value = "/pageable")
    public ResponseEntity<List<StudyProgramDTO>> findAll(Pageable pageable){
        Page<StudyProgram> studyPrograms = studyProgramService.findAll(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(studyPrograms.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");

        return new ResponseEntity<>(toStudyProgramDTOConverter.convert(studyPrograms.getContent()),headers, HttpStatus.OK);
    }

    @GetMapping(value = "/study-type-pageable/{id}")
    public ResponseEntity<List<StudyProgramDTO>> findByStudyType(@PathVariable("id") Long id, Pageable pageable){
        Page<StudyProgram> studyPrograms = studyProgramService.findByStudyTypePageable(id, pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages", Integer.toString(studyPrograms.getTotalPages()));
        headers.add("access-control-expose-headers", "totalPages");

        return new ResponseEntity<>(toStudyProgramDTOConverter.convert(studyPrograms.getContent()),headers, HttpStatus.OK);
    }

    @GetMapping(value="/study-type/{id}")
    public ResponseEntity<List<StudyProgramDTO>> findByStudyType(@PathVariable("id") Long id){
        return new ResponseEntity<>(toStudyProgramDTOConverter.convert(studyProgramService.findByStudyType(id)), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<StudyProgramDTO> findById(@PathVariable("id") Long id){
        return new ResponseEntity<>(toStudyProgramDTOConverter.convert(studyProgramService.findById(id)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<StudyProgramDTO> create(@RequestBody StudyProgramDTO studyProgramDTO){
        StudyProgram studyProgram = studyProgramService.save(toStudyProgramConverter.convert(studyProgramDTO));
        return new ResponseEntity<>(toStudyProgramDTOConverter.convert(studyProgram), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<StudyProgramDTO> update(@RequestBody StudyProgramDTO studyProgramDTO, @PathVariable("id") Long id){
        StudyProgram studyProgram = studyProgramService.edit(toStudyProgramConverter.convert(studyProgramDTO), id);
        return new ResponseEntity<>(toStudyProgramDTOConverter.convert(studyProgram), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        studyProgramService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
