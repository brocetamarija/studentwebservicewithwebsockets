package rs.uns.ac.ftn.studentewebservice.entity;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Where(clause = "deleted = false")
public class Place {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, length = 20)
    private String name;

    @Column(name = "zipCode", nullable = false)
    private int zipCode;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @OneToMany(mappedBy = "place", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Address> addresses = new HashSet<>();

    public Place() {
    }

    public Place(String name, int zipCode, boolean deleted, Set<Address> addresses) {
        this.name = name;
        this.zipCode = zipCode;
        this.deleted = deleted;
        this.addresses = addresses;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Place p = (Place) o;
        return Objects.equals(id, p.id);
    }

    public void add(Address address) {
        if (address.getPlace() != null) {
            address.getPlace().getAddresses().remove(address);
        }
        address.setPlace(this);
        getAddresses().add(address);
    }

    public void remove(Address address) {
        address.setPlace(null);
        getAddresses().remove(address);
    }

}
