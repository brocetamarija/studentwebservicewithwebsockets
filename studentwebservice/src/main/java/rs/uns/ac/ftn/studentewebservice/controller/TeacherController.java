package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.TeacherDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.TeacherDTOToTeacherConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.TeacherToTeacherDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.Teacher;
import rs.uns.ac.ftn.studentewebservice.service.TeacherService;

import java.util.List;

@RestController
@RequestMapping(value = "api/teachers")
public class TeacherController {

    @Autowired
    TeacherService teacherService;

    @Autowired
    TeacherToTeacherDTOConverter toTeacherDTOConverter;

    @Autowired
    TeacherDTOToTeacherConverter toTeacherConverter;

    @GetMapping
    public ResponseEntity<List<TeacherDTO>> findAll(){
        return new ResponseEntity<>(toTeacherDTOConverter.convert(teacherService.findAll()), HttpStatus.OK);
    }

    @GetMapping(value = "/pageable")
    public ResponseEntity<List<TeacherDTO>> findAll(Pageable pageable){
        Page<Teacher> teachers = teacherService.findAll(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(teachers.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");

        return new ResponseEntity<>(toTeacherDTOConverter.convert(teachers.getContent()), headers, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<TeacherDTO> findById(@PathVariable("id") Long id){
        return new ResponseEntity<>(toTeacherDTOConverter.convert(teacherService.findById(id)), HttpStatus.OK);
    }

    @GetMapping(value = "/by-username/{username}")
    public ResponseEntity<TeacherDTO> findByUsername(@PathVariable("username") String username){
        return new ResponseEntity<>(toTeacherDTOConverter.convert(teacherService.findByUsername(username)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TeacherDTO> create(@RequestBody TeacherDTO teacherDTO){
        Teacher teacher = teacherService.add(toTeacherConverter.convert(teacherDTO));
        return new ResponseEntity<>(toTeacherDTOConverter.convert(teacher), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<TeacherDTO> update(@RequestBody TeacherDTO teacherDTO, @PathVariable("id") Long id){
        Teacher teacher = teacherService.edit(toTeacherConverter.convert(teacherDTO), id);
        return new ResponseEntity<>(toTeacherDTOConverter.convert(teacher), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        teacherService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
