package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.DocumentDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Document;
import rs.uns.ac.ftn.studentewebservice.entity.Student;
import rs.uns.ac.ftn.studentewebservice.service.StudentService;

@Component
public class DocumentDTOToDocumentConverter implements Converter<DocumentDTO, Document> {

    @Autowired
    StudentService studentService;

    @Override
    public Document convert(DocumentDTO documentDTO) {
        Document document = new Document();
        document.setId(documentDTO.getId());
        document.setType(documentDTO.getType());
        document.setDate(documentDTO.getDate());
        document.setFilePath(documentDTO.getFilePath());
        Student student = studentService.findById(documentDTO.getStudentDTO().getId());
        if(student != null){
            document.setStudent(student);
        }else{
            //TODO izazovi izuzetak
        }
        return document;
    }
}
