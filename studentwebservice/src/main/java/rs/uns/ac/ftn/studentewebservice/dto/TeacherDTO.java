package rs.uns.ac.ftn.studentewebservice.dto;

public class TeacherDTO {

    private Long id;
    private String teacherCode;
    private boolean deleted;
    private UserDTO userDTO;

    public TeacherDTO() {
    }

    public TeacherDTO(Long id, String teacherCode, boolean deleted, UserDTO userDTO) {
        this.id = id;
        this.teacherCode = teacherCode;
        this.deleted = deleted;
        this.userDTO = userDTO;
    }

    /*public TeacherDTO(Teacher teacher){
        this(teacher.getId(),
                teacher.getTeacherCode(),
                teacher.isDeleted(),
                new UserDTO(teacher.getUser()));
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeacherCode() {
        return teacherCode;
    }

    public void setTeacherCode(String teacherCode) {
        this.teacherCode = teacherCode;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}
