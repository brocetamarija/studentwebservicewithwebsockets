package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.UserDTO;
import rs.uns.ac.ftn.studentewebservice.entity.User;
import rs.uns.ac.ftn.studentewebservice.service.AddressService;

@Component
public class UserDTOToUserConverter implements Converter<UserDTO, User> {

    @Autowired
    AddressService addressService;

    @Autowired
    AddressDTOToAddressConverter toAddressConverter;

    @Override
    public User convert(UserDTO userDTO) {
        User user = new User();
        user.setId(userDTO.getId());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        user.setEmail(userDTO.getEmail());
        user.setType(userDTO.getType());
        user.setDeleted(userDTO.isDeleted());
        /*Address address = addressService.findById(userDTO.getAddressDTO().getId());
        if(address != null){
            user.setAddress(address);
        }else{
            //TODO izazovi izuzetak
        }*/
        user.setAddress(toAddressConverter.convert(userDTO.getAddressDTO()));
        return user;
    }
}
