package rs.uns.ac.ftn.studentewebservice.entity;


import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Where(clause = "deleted = false")
public class ExamRealization {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date", nullable = false)
    private Date date;

    @Column(name = "classroom", nullable = false, length = 10)
    private String classroom;

    @Column(name = "examPrice", nullable = false)
    private double examPrice;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @ManyToOne
    @JoinColumn(name = "courseId", referencedColumnName = "id")
    private Course course;

    @ManyToOne
    @JoinColumn(name = "teacherId", referencedColumnName = "id")
    private Teacher teacher;

    @ManyToOne
    @JoinColumn(name = "examPeriodId", referencedColumnName = "id")
    private ExamPeriod examPeriod;

    @OneToMany(mappedBy = "examRealization", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Exam> exams = new HashSet<>();

    public ExamRealization() {
    }

    public ExamRealization(Date date, String classroom, double examPrice, boolean deleted, Course course, Teacher teacher, ExamPeriod examPeriod, Set<Exam> exams) {
        this.date = date;
        this.classroom = classroom;
        this.examPrice = examPrice;
        this.deleted = deleted;
        this.course = course;
        this.teacher = teacher;
        this.examPeriod = examPeriod;
        this.exams = exams;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public double getExamPrice() {
        return examPrice;
    }

    public void setExamPrice(double examPrice) {
        this.examPrice = examPrice;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public ExamPeriod getExamPeriod() {
        return examPeriod;
    }

    public void setExamPeriod(ExamPeriod examPeriod) {
        this.examPeriod = examPeriod;
    }

    public Set<Exam> getExams() {
        return exams;
    }

    public void setExams(Set<Exam> exams) {
        this.exams = exams;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ExamRealization er = (ExamRealization) o;
        return Objects.equals(id, er.id);
    }

    public void add(Exam exam) {
        if (exam.getExamRealization() != null) {
            exam.getExamRealization().getExams().remove(exam);
        }
        exam.setExamRealization(this);
        getExams().add(exam);
    }

    public void remove(Exam exam) {
        exam.setExamRealization(null);
        getExams().remove(exam);
    }
}
