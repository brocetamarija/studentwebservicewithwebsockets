package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.TeacherTitle;
import rs.uns.ac.ftn.studentewebservice.repository.TeacherTitleRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;


import java.util.List;

@Service
public class TeacherTitleService {

    @Autowired
    TeacherTitleRepository teacherTitleRepository;

    public Page<TeacherTitle> findAll(Pageable pageable) {
        return teacherTitleRepository.findAll(pageable);
    }

    public TeacherTitle findById(Long id) {
        return teacherTitleRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Teacher title Not Found!", "Teacher title with given id: " + id + " is not found!"));
    }

    public List<TeacherTitle> findByTeacherId(Long id) {
        return teacherTitleRepository.findByTeacher_Id(id);
    }

    public TeacherTitle save(TeacherTitle teacherTitle) {
        return teacherTitleRepository.save(teacherTitle);
    }

    public TeacherTitle edit(TeacherTitle tt, Long id){
        TeacherTitle teacherTitle = findById(id);
        teacherTitle = teacherTitleRepository.save(tt);
        return teacherTitle;
    }

    public void delete(Long id) {
        TeacherTitle teacherTitle = findById(id);

        teacherTitle.setDeleted(true);
        teacherTitleRepository.save(teacherTitle);
    }

    public void deleteByTeacherId(Long id) {
        List<TeacherTitle> teacherTitles = findByTeacherId(id);
        for (TeacherTitle tt : teacherTitles) {
            tt.setDeleted(true);
            teacherTitleRepository.save(tt);
        }
    }


}
