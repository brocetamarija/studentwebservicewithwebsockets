package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import rs.uns.ac.ftn.studentewebservice.dto.FilepathDTO;
import rs.uns.ac.ftn.studentewebservice.service.FileService;


import java.io.IOException;
import java.io.UnsupportedEncodingException;


@RestController
@RequestMapping(value = "api/files")
public class FileController {

    @Autowired
    FileService fileService;

    @GetMapping(value = "/download/{filename:.+}")
    public ResponseEntity<byte[]> download(@PathVariable("filename") String filename) throws UnsupportedEncodingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setContentDisposition(ContentDisposition.parse("attachment; filename=" + filename));
        headers.add("filename",filename);

        byte[] bFile = fileService.download(filename);

        return new ResponseEntity<>(bFile, headers, HttpStatus.OK);
    }

    @PostMapping(value = "/upload")
    public ResponseEntity<FilepathDTO> upload(@RequestParam MultipartFile file) throws IOException {
        String path = fileService.saveUploadedFile(file);

        return new ResponseEntity<>(new FilepathDTO(path), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{filename:.+}")
    public ResponseEntity<String> delete(@PathVariable("filename") String filename){
        String f = fileService.removeFile(filename);
        return new ResponseEntity<>(f, HttpStatus.OK);
    }
}
