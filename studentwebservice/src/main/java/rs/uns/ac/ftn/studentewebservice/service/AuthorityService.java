package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.Authority;
import rs.uns.ac.ftn.studentewebservice.repository.AuthorityRepository;

@Service
public class AuthorityService {

    @Autowired
    AuthorityRepository authorityRepository;

    public Authority findByName(String name) {
        return authorityRepository.findByName(name);
    }

}
