package rs.uns.ac.ftn.studentewebservice.dto;

import java.util.Date;

public class TeacherTitleDTO {

    private Long id;
    private Date startDate;
    private Date endDate;
    private boolean deleted;
    private TitleDTO titleDTO;
    private TeacherDTO teacherDTO;

    public TeacherTitleDTO() {
    }

    public TeacherTitleDTO(Long id, Date startDate, Date endDate, boolean deleted, TitleDTO titleDTO, TeacherDTO teacherDTO) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.deleted = deleted;
        this.titleDTO = titleDTO;
        this.teacherDTO = teacherDTO;
    }

    /*public TeacherTitleDTO(TeacherTitle teacherTitle){
        this(teacherTitle.getId(),
                teacherTitle.getStartDate(),
                teacherTitle.getEndDate(),
                teacherTitle.isDeleted(),
                new TitleDTO(teacherTitle.getTitle()),
                new TeacherDTO(teacherTitle.getTeacher()));
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public TitleDTO getTitleDTO() {
        return titleDTO;
    }

    public void setTitleDTO(TitleDTO titleDTO) {
        this.titleDTO = titleDTO;
    }

    public TeacherDTO getTeacherDTO() {
        return teacherDTO;
    }

    public void setTeacherDTO(TeacherDTO teacherDTO) {
        this.teacherDTO = teacherDTO;
    }
}
