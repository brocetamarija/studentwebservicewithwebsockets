package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.TitleDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Title;

@Component
public class TitleDTOToTitleConverter implements Converter<TitleDTO, Title> {


    @Override
    public Title convert(TitleDTO titleDTO) {
        Title title = new Title();
        title.setId(titleDTO.getId());
        title.setName(titleDTO.getName());
        title.setDeleted(titleDTO.isDeleted());
        return title;
    }
}
