package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.PaymentDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Payment;

import java.util.ArrayList;
import java.util.List;

@Component
public class PaymentToPaymentDTOConverter implements Converter<Payment, PaymentDTO> {

    @Autowired
    StudentToStudentDTOConverter toStudentDTOConverter;

    @Autowired
    PaymentPurposeToPaymentPurposeDTOConverter toPaymentPurposeDTOConverter;

    @Override
    public PaymentDTO convert(Payment payment) {
        PaymentDTO paymentDTO = new PaymentDTO();
        paymentDTO.setId(payment.getId());
        paymentDTO.setDate(payment.getDate());
        paymentDTO.setPaymentType(payment.getPaymentType());
        paymentDTO.setAmount(payment.getAmount());
        paymentDTO.setDeleted(payment.isDeleted());
        paymentDTO.setStudentDTO(toStudentDTOConverter.convert(payment.getStudent()));
        paymentDTO.setPaymentPurposeDTO(toPaymentPurposeDTOConverter.convert(payment.getPaymentPurpose()));
        return paymentDTO;
    }

    public List<PaymentDTO> convert(List<Payment> payments){
        List<PaymentDTO> retVal = new ArrayList<>();
        for(Payment p:payments){
            retVal.add(convert(p));
        }
        return retVal;
    }
}
