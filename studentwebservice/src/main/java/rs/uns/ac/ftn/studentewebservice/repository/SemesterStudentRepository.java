package rs.uns.ac.ftn.studentewebservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.uns.ac.ftn.studentewebservice.entity.SemesterStudent;

public interface SemesterStudentRepository extends JpaRepository<SemesterStudent, Long> {
    SemesterStudent findByStudent_Id(Long studentID);

    SemesterStudent findFirstByStudent_IdOrderByIdDesc(Long studentID);
}
