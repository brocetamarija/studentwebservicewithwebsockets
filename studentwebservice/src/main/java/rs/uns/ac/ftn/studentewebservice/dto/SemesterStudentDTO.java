package rs.uns.ac.ftn.studentewebservice.dto;

public class SemesterStudentDTO {

    private Long id;
    private boolean isCertified;
    private double semesterCertificationPrice;
    private boolean deleted;
    private StudentDTO studentDTO;
    private SemesterDTO semesterDTO;

    public SemesterStudentDTO() {
    }

    public SemesterStudentDTO(Long id, boolean isCertified, double semesterCertificationPrice, boolean deleted, StudentDTO studentDTO, SemesterDTO semesterDTO) {
        this.id = id;
        this.isCertified = isCertified;
        this.semesterCertificationPrice = semesterCertificationPrice;
        this.deleted = deleted;
        this.studentDTO = studentDTO;
        this.semesterDTO = semesterDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isCertified() {
        return isCertified;
    }

    public void setCertified(boolean certified) {
        isCertified = certified;
    }

    public double getSemesterCertificationPrice() {
        return semesterCertificationPrice;
    }

    public void setSemesterCertificationPrice(double semesterCertificationPrice) {
        this.semesterCertificationPrice = semesterCertificationPrice;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public StudentDTO getStudentDTO() {
        return studentDTO;
    }

    public void setStudentDTO(StudentDTO studentDTO) {
        this.studentDTO = studentDTO;
    }

    public SemesterDTO getSemesterDTO() {
        return semesterDTO;
    }

    public void setSemesterDTO(SemesterDTO semesterDTO) {
        this.semesterDTO = semesterDTO;
    }
}
