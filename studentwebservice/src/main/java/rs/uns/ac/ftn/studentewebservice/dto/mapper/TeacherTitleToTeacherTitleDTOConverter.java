package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.TeacherTitleDTO;
import rs.uns.ac.ftn.studentewebservice.entity.TeacherTitle;

import java.util.ArrayList;
import java.util.List;

@Component
public class TeacherTitleToTeacherTitleDTOConverter implements Converter<TeacherTitle, TeacherTitleDTO> {

    @Autowired
    TeacherToTeacherDTOConverter toTeacherDTOConverter;

    @Autowired
    TitleToTitleDTOConverter toTitleDTOConverter;

    @Override
    public TeacherTitleDTO convert(TeacherTitle teacherTitle) {
        TeacherTitleDTO teacherTitleDTO = new TeacherTitleDTO();
        teacherTitleDTO.setId(teacherTitle.getId());
        teacherTitleDTO.setStartDate(teacherTitle.getStartDate());
        teacherTitleDTO.setEndDate(teacherTitle.getEndDate());
        teacherTitleDTO.setDeleted(teacherTitle.isDeleted());
        teacherTitleDTO.setTeacherDTO(toTeacherDTOConverter.convert(teacherTitle.getTeacher()));
        teacherTitleDTO.setTitleDTO(toTitleDTOConverter.convert(teacherTitle.getTitle()));
        return teacherTitleDTO;
    }

    public List<TeacherTitleDTO> convert(List<TeacherTitle> teacherTitles) {
        List<TeacherTitleDTO> retVal = new ArrayList<>();
        for (TeacherTitle tt : teacherTitles) {
            retVal.add(convert(tt));
        }
        return retVal;
    }
}
