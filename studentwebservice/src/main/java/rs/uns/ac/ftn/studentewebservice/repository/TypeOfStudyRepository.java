package rs.uns.ac.ftn.studentewebservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.uns.ac.ftn.studentewebservice.entity.TypeOfStudy;

public interface TypeOfStudyRepository extends JpaRepository<TypeOfStudy, Long> {
}
