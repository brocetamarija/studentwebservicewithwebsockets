package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.Enrollment;
import rs.uns.ac.ftn.studentewebservice.repository.EnrollmentRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.List;

@Service
public class EnrollmentService {

    @Autowired
    EnrollmentRepository enrollmentRepository;

    @Autowired
    CourseService courseService;

    public Page<Enrollment> findAll(Pageable pageable) {
        return enrollmentRepository.findAll(pageable);
    }

    public Enrollment findById(Long id) {
        return enrollmentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Enrollment Not Found!", "Enrollment with given id: " + id + " is not found!"));
    }

    public List<Enrollment> findByStudentId(Long id) {
        return enrollmentRepository.findByStudent_Id(id);
    }

    public Enrollment save(Enrollment enrollment) {
        return enrollmentRepository.save(enrollment);
    }

    public Enrollment edit(Enrollment e, Long id){
        Enrollment enrollment = findById(id);
        enrollment = enrollmentRepository.save(e);
        return enrollment;
    }

    public void delete(Long id) {
        Enrollment enrollment = findById(id);

        enrollment.setDeleted(true);
        enrollmentRepository.save(enrollment);
    }

    public void deleteByStudentId(Long id) {
        List<Enrollment> enrollments = findByStudentId(id);
        for (Enrollment e : enrollments) {
            e.setDeleted(true);
            enrollmentRepository.save(e);
        }
    }

    public Page<Enrollment> findByStudentId(long id, Pageable pageable) {
        return enrollmentRepository.findByStudent_Id(id, pageable);
    }

}
