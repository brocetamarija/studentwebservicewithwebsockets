package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.EnrollmentDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.EnrollmentDTOToEnrollmentConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.EnrollmentToEnrollmentDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.Enrollment;
import rs.uns.ac.ftn.studentewebservice.service.EnrollmentService;

import java.util.List;

@RestController
@RequestMapping(value = "api/enrollments")
public class EnrollmentController {

    @Autowired
    EnrollmentService enrollmentService;

    @Autowired
    EnrollmentToEnrollmentDTOConverter toEnrollmentDTOConverter;

    @Autowired
    EnrollmentDTOToEnrollmentConverter toEnrollmentConverter;

    @GetMapping(value = "/student/{id}")
    public ResponseEntity<List<EnrollmentDTO>> findByStudentId(@PathVariable("id") Long id, Pageable pageable){
        Page<Enrollment> enrollments = enrollmentService.findByStudentId(id, pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages", Integer.toString(enrollments.getTotalPages()));
        headers.add("access-control-expose-headers", "totalPages");

        return new ResponseEntity<>(toEnrollmentDTOConverter.convert(enrollments.getContent()), headers, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<EnrollmentDTO> create(@RequestBody EnrollmentDTO enrollmentDTO){
        Enrollment enrollment = enrollmentService.save(toEnrollmentConverter.convert(enrollmentDTO));
        return new ResponseEntity<>(toEnrollmentDTOConverter.convert(enrollment), HttpStatus.CREATED);
    }


    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        enrollmentService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
