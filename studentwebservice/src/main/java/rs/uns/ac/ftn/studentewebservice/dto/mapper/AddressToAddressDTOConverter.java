package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.AddressDTO;
import rs.uns.ac.ftn.studentewebservice.entity.Address;

import java.util.ArrayList;
import java.util.List;

@Component
public class AddressToAddressDTOConverter implements Converter<Address, AddressDTO> {

    @Autowired
    PlaceToPlaceDTOConverter toPlaceDTOConverter;

    @Override
    public AddressDTO convert(Address address) {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setId(address.getId());
        addressDTO.setStreetName(address.getStreetName());
        addressDTO.setStreetNumber(address.getStreetNumber());
        addressDTO.setDeleted(address.isDeleted());
        addressDTO.setPlaceDTO(toPlaceDTOConverter.convert(address.getPlace()));
        return addressDTO;
    }

    public List<AddressDTO> convert(List<Address> addresses) {
        List<AddressDTO> retVal = new ArrayList<>();
        for (Address a:addresses) {
            retVal.add(convert(a));
        }
        return  retVal;
    }
}
