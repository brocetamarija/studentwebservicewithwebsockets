package rs.uns.ac.ftn.studentewebservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.uns.ac.ftn.studentewebservice.entity.PaymentPurpose;

public interface PaymentPurposeRepository extends JpaRepository<PaymentPurpose, Long> {
    PaymentPurpose findByName(String name);
}
