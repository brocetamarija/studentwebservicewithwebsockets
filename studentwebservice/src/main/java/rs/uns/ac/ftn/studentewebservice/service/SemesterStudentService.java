package rs.uns.ac.ftn.studentewebservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.uns.ac.ftn.studentewebservice.entity.Course;
import rs.uns.ac.ftn.studentewebservice.entity.Semester;
import rs.uns.ac.ftn.studentewebservice.entity.SemesterStudent;
import rs.uns.ac.ftn.studentewebservice.entity.Student;
import rs.uns.ac.ftn.studentewebservice.repository.SemesterStudentRepository;
import rs.uns.ac.ftn.studentewebservice.exceptions.EntityNotFoundException;

import java.util.List;

@Service
public class SemesterStudentService {

    @Autowired
    SemesterStudentRepository semesterStudentRepository;

    @Autowired
    CourseService courseService;

    @Autowired
    StudentService studentService;

    @Autowired
    SemesterService semesterService;

    public SemesterStudent save(SemesterStudent semesterStudent) {
        return semesterStudentRepository.save(semesterStudent);
    }

    public SemesterStudent findByStudentId(Long studentID) {
        SemesterStudent ss = semesterStudentRepository.findByStudent_Id(studentID);
        return ss;
    }

    /**
     * Dodavanje semestra koji student pohadja
     *
     * Pronalazi se student po ID-u i u zavisnosti od toga koja je student godina ili da li je polozio sve ispite iz prethodne godine odredjuje se cena overe semestra
     * @param semesterStudent Podaci koji se preuzimaju sa klijentske strane
     * @return ss Povratna vrednost je SemesterStudent koji je sacuvan
     * */
    public SemesterStudent addSemesterToStudent(SemesterStudent semesterStudent) {
        SemesterStudent ss = new SemesterStudent();

        ss.setId(semesterStudent.getId());
        ss.setDeleted(false);
        ss.setCertified(false);
        ss.setStudent(semesterStudent.getStudent());
        ss.setSemester(semesterStudent.getSemester());
        Student student = studentService.findById(semesterStudent.getStudent().getId());
        if (student.getYearOfStudy() == 1) {
            ss.setSemesterCertificationPrice(2000);
        } else {
            List<Course> courses = courseService.unfinishedCourses(student.getId());
            if (courses.size() == 0) {
                ss.setSemesterCertificationPrice(1000);
            } else {
                ss.setSemesterCertificationPrice(2000);
            }
        }

        return save(ss);
    }


    /**
     * Overa semestra
     *
     * @param semesterStudent Podaci koji se preuzimaju sa klijentske strane
     * @param studentID ID po kome se pronalazi entitet koji se menja
     * @return ss Entitet koji je izmenjen
     * */
    public SemesterStudent semesterCertification(SemesterStudent semesterStudent, Long studentID) {
        SemesterStudent ss = findByStudentId(studentID);
        if(ss == null){

        }
        ss.setCertified(true);
        ss = save(semesterStudent);
        return ss;
    }

    /**
     * Provera da li je studentu dodat semestar koji pohadja
     *
     * Pronaci poslednji dodati entitet po ID-u studenta, ukoliko ne postoji vratiti false,
     * ukoliko postoji a poslednji dodat semestar se ne podudara sa poslednjim semestrom vezanim za studenta opet vratiti false,
     * u suprotnom vratiti true
     * @param studentID ID studenta po kome se vrsi pretrazivanje
     * */
    public boolean checkIfSemesterAdded(Long studentID) {
        SemesterStudent ss = semesterStudentRepository.findFirstByStudent_IdOrderByIdDesc(studentID);
        Semester semester = semesterService.findLast();

        if (ss == null) {
            return false;
        } else {
            if(semester != ss.getSemester()){
                return false;
            }

            return true;
        }
    }

    /**
     * Provera da li je student overio semestar
     *
     * Pronalazi se poslednji semestar vezan za studenta
     * @param studentID Parametar po kome se pronalazi entitet
     * */
    public SemesterStudent getIsSemesterCertified(Long studentID) {
        SemesterStudent ss = semesterStudentRepository.findFirstByStudent_IdOrderByIdDesc(studentID);
        if(ss == null){
            throw new EntityNotFoundException("SemesterStudent Not Found!", "Semester for student with given student id: " + studentID + " is not found!");
        }
        return ss;
    }
}
