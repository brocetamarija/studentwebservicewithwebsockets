package rs.uns.ac.ftn.studentewebservice.dto;

import rs.uns.ac.ftn.studentewebservice.entity.Place;

public class PlaceDTO {

    private Long id;
    private String name;
    private int zipCode;
    private boolean deleted;

    public PlaceDTO() {}

    public PlaceDTO(Long id, String name, int zipCode, boolean deleted) {
        this.id = id;
        this.name = name;
        this.zipCode = zipCode;
        this.deleted = deleted;
    }

    public PlaceDTO(Place place){
        this(place.getId(),
                place.getName(),
                place.getZipCode(),
                place.isDeleted());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
