package rs.uns.ac.ftn.studentewebservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.uns.ac.ftn.studentewebservice.entity.StudyProgram;

import java.util.List;

public interface StudyProgramRepository extends JpaRepository<StudyProgram, Long> {
    Page<StudyProgram> findByTypeOfStudy_Id(Long id, Pageable pageable);

    List<StudyProgram> findByTypeOfStudy_Id(Long id);
}
