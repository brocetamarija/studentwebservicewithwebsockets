package rs.uns.ac.ftn.studentewebservice.dto.mapper;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import rs.uns.ac.ftn.studentewebservice.dto.ExamPeriodDTO;
import rs.uns.ac.ftn.studentewebservice.entity.ExamPeriod;

@Component
public class ExamPeriodDTOToExamPeriodConverter implements Converter<ExamPeriodDTO, ExamPeriod> {


    @Override
    public ExamPeriod convert(ExamPeriodDTO examPeriodDTO) {
        ExamPeriod examPeriod = new ExamPeriod();
        examPeriod.setId(examPeriodDTO.getId());
        examPeriod.setName(examPeriodDTO.getName());
        examPeriod.setStartDate(examPeriodDTO.getStartDate());
        examPeriod.setEndDate(examPeriodDTO.getEndDate());
        examPeriod.setDeleted(examPeriodDTO.isDeleted());
        return examPeriod;
    }
}
