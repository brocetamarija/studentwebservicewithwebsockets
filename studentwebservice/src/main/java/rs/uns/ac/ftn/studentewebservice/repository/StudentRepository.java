package rs.uns.ac.ftn.studentewebservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.uns.ac.ftn.studentewebservice.entity.Student;

import java.util.List;


public interface StudentRepository extends JpaRepository<Student, Long> {
    Student findByUser_Username(String username);
    Page<Student> findByStudyProgram_Id(Long id, Pageable pageable);
    List<Student> findByStudyProgram_IdAndStudyProgram_TypeOfStudy_Id(Long studyProgramID, Long studyTypeID);
}
