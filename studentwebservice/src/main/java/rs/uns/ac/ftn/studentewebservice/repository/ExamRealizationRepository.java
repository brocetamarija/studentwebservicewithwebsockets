package rs.uns.ac.ftn.studentewebservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.uns.ac.ftn.studentewebservice.entity.ExamRealization;

import java.util.List;

public interface ExamRealizationRepository extends JpaRepository<ExamRealization, Long> {

    Page<ExamRealization> findByCourse_StudyProgram_IdAndExamPeriod_Id(Long studyProgramId, Long examPeriodId, Pageable pageable);

    List<ExamRealization> findByCourse_IdAndExamPeriod_Id(Long courseID, Long examPeriodID);

    List<ExamRealization> findByTeacher_IdAndExamPeriod_Id(Long teacherID, Long examPeriodID);
}
