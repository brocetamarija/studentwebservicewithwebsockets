package rs.uns.ac.ftn.studentewebservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.uns.ac.ftn.studentewebservice.dto.TeachingDTO;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.TeachingDTOToTeachingConverter;
import rs.uns.ac.ftn.studentewebservice.dto.mapper.TeachingToTeachingDTOConverter;
import rs.uns.ac.ftn.studentewebservice.entity.Teaching;
import rs.uns.ac.ftn.studentewebservice.service.TeachingService;

import java.util.List;

@RestController
@RequestMapping(value = "api/teachings")
public class TeachingController {

    @Autowired
    TeachingService teachingService;

    @Autowired
    TeachingToTeachingDTOConverter toTeachingDTOConverter;

    @Autowired
    TeachingDTOToTeachingConverter toTeachingConverter;


    @GetMapping(value = "/teacher/{id}")
    public ResponseEntity<List<TeachingDTO>> findByTeacher(@PathVariable("id") Long id, Pageable pageable){
        Page<Teaching> teachings = teachingService.findByTeacherId(id, pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages", Integer.toString(teachings.getTotalPages()));
        headers.add("access-control-expose-headers", "totalPages");
        return new ResponseEntity<>(toTeachingDTOConverter.convert(teachings.getContent()), headers, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TeachingDTO> create(@RequestBody TeachingDTO teachingDTO){
        Teaching teaching = teachingService.save(toTeachingConverter.convert(teachingDTO));
        return new ResponseEntity<>(toTeachingDTOConverter.convert(teaching), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id){
        teachingService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
