package rs.uns.ac.ftn.studentewebservice.dto;

import rs.uns.ac.ftn.studentewebservice.entity.PaymentType;

import java.util.Date;

public class PaymentDTO {

    private Long id;
    private double amount;
    private Date date;
    private boolean deleted;
    private PaymentType paymentType;
    private PaymentPurposeDTO paymentPurposeDTO;
    private StudentDTO studentDTO;

    public PaymentDTO() {}

    public PaymentDTO(Long id, double amount, Date date, boolean deleted, PaymentType paymentType, PaymentPurposeDTO paymentPurposeDTO, StudentDTO studentDTO) {
        this.id = id;
        this.amount = amount;
        this.date = date;
        this.deleted = deleted;
        this.paymentType = paymentType;
        this.paymentPurposeDTO = paymentPurposeDTO;
        this.studentDTO = studentDTO;
    }

    /*public PaymentDTO(Payment payment){
        this(payment.getId(),
                payment.getAmount(),
                payment.getDate(),
                payment.isDeleted(),
                payment.getPaymentType(),
                new PaymentPurposeDTO(payment.getPaymentPurpose()),
                new StudentDTO(payment.getStudent()));
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public PaymentPurposeDTO getPaymentPurposeDTO() {
        return paymentPurposeDTO;
    }

    public void setPaymentPurposeDTO(PaymentPurposeDTO paymentPurposeDTO) {
        this.paymentPurposeDTO = paymentPurposeDTO;
    }

    public StudentDTO getStudentDTO() {
        return studentDTO;
    }

    public void setStudentDTO(StudentDTO studentDTO) {
        this.studentDTO = studentDTO;
    }
}
