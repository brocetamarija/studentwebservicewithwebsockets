INSERT INTO authority(name)VALUES('ROLE_ADMIN')
INSERT INTO authority(name)VALUES('ROLE_STUDENT')
INSERT INTO authority(name)VALUES('ROLE_TEACHER')

INSERT INTO place(name, zip_code, deleted)VALUES('Novi Sad', 21000, false);
INSERT INTO place(name, zip_code, deleted)VALUES('Beograd', 11000, false);
INSERT INTO place(name, zip_code, deleted)VALUES('Subotica', 24000, false);

INSERT INTO address(street_name,street_number,deleted,place_id)VALUES('Adresa 1', 10, false, 1);
INSERT INTO address(street_name,street_number,deleted,place_id)VALUES('Adresa 2', 20, false, 1);
INSERT INTO address(street_name,street_number,deleted,place_id)VALUES('Adresa 3', 30, false, 1);

INSERT INTO address(street_name,street_number,deleted,place_id)VALUES('Adresa 1', 10, false, 2);
INSERT INTO address(street_name,street_number,deleted,place_id)VALUES('Adresa 2', 20, false, 2);
INSERT INTO address(street_name,street_number,deleted,place_id)VALUES('Adresa 3', 30, false, 2);

INSERT INTO address(street_name,street_number,deleted,place_id)VALUES('Adresa 1', 10, false, 3);
INSERT INTO address(street_name,street_number,deleted,place_id)VALUES('Adresa 2', 20, false, 3);
INSERT INTO address(street_name,street_number,deleted,place_id)VALUES('Adresa 3', 30, false, 3);

INSERT INTO user(first_name,last_name,username,password,email,type,deleted,address_id)VALUES('Pera', 'Perić', 'pera', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'pera@email.com', 'ROLE_ADMIN', false, 1);
INSERT INTO user(first_name,last_name,username,password,email,type,deleted,address_id)VALUES('Marko', 'Marković', 'mare', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'mare@email.com', 'ROLE_ADMIN', false, 2);
INSERT INTO user(first_name,last_name,username,password,email,type,deleted,address_id)VALUES('Zika', 'Zikic', 'zika', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'zika@email.com', 'ROLE_ADMIN', false, 3);

INSERT INTO user(first_name,last_name,username,password,email,type,deleted,address_id)VALUES('Marija', 'Broćeta', 'mara', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'mara@email.com', 'ROLE_STUDENT', false, 4);
INSERT INTO user(first_name,last_name,username,password,email,type,deleted,address_id)VALUES('Dimitrije', 'Mijatovic', 'dimi', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'dimi@email.com', 'ROLE_STUDENT', false, 5);
INSERT INTO user(first_name,last_name,username,password,email,type,deleted,address_id)VALUES('Marko', 'Topić', 'maki', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'maki@email.com', 'ROLE_STUDENT', false, 6);

INSERT INTO user(first_name,last_name,username,password,email,type,deleted,address_id)VALUES('Siniša', 'Nikolić', 'sinisa', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'sinisa@email.com', 'ROLE_TEACHER', false, 7);
INSERT INTO user(first_name,last_name,username,password,email,type,deleted,address_id)VALUES('Miroslav', 'Zarić', 'miki', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'miki@email.com', 'ROLE_TEACHER', false, 8);
INSERT INTO user(first_name,last_name,username,password,email,type,deleted,address_id)VALUES('Milan', 'Segedinac', 'milan', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'milan@email.com', 'ROLE_TEACHER', false, 9);

INSERT INTO user_authority(user_id, authority_id)VALUES(1,1);
INSERT INTO user_authority(user_id, authority_id)VALUES(2,1);
INSERT INTO user_authority(user_id, authority_id)VALUES(3,1);

INSERT INTO user_authority(user_id, authority_id)VALUES(4,2);
INSERT INTO user_authority(user_id, authority_id)VALUES(5,2);
INSERT INTO user_authority(user_id, authority_id)VALUES(6,2);

INSERT INTO user_authority(user_id, authority_id)VALUES(7,3);
INSERT INTO user_authority(user_id, authority_id)VALUES(8,3);
INSERT INTO user_authority(user_id, authority_id)VALUES(9,3);

INSERT INTO type_of_study(name, deleted)VALUES('Osnovne akademske studije', false);
INSERT INTO type_of_study(name, deleted)VALUES('Osnovne strukovne studije', false);
INSERT INTO type_of_study(name, deleted)VALUES('Master akademske studije', false);
INSERT INTO type_of_study(name, deleted)VALUES('Specijalističke akademske studije', false);
INSERT INTO type_of_study(name, deleted)VALUES('Specijalističke strukovne studije', false);
INSERT INTO type_of_study(name, deleted)VALUES('Doktorske akademske studije', false);

INSERT INTO study_program(name, deleted, type_of_study_id)VALUES('Softverske i informacione tehnologije', false, 2);
INSERT INTO study_program(name, deleted, type_of_study_id)VALUES('Softversko inzenjerstvo i informacione tehnologije', false, 1);
INSERT INTO study_program(name, deleted, type_of_study_id)VALUES('Racunarstvo i automatika', false, 1);

INSERT INTO student(card_number,phone_number,birth_date, year_of_study, deleted,user_id,study_program_id)VALUES('SF-37-2016','064123456','1991-04-30', 1, false,4,1);
INSERT INTO student(card_number,phone_number,birth_date, year_of_study, deleted,user_id,study_program_id)VALUES('SF-32-2016','064123456','1997-06-20', 1, false,5,1);
INSERT INTO student(card_number,phone_number,birth_date, year_of_study, deleted,user_id,study_program_id)VALUES('SF-30-2016','064123456','1997-03-30', 1, false,6,1);

INSERT INTO account(giro_account_number, model_number, personal_reference_number, current_account_balance, deleted, student_id)VALUES('888-111111-23', 97, '12-12345-1-1234-1234', 0, false,1);
INSERT INTO account(giro_account_number, model_number, personal_reference_number, current_account_balance, deleted, student_id)VALUES('123-111111-23', 97, '12-12345-1-1234-4321', 0, false,2);
INSERT INTO account(giro_account_number, model_number, personal_reference_number, current_account_balance, deleted, student_id)VALUES('123-1234567-23', 97, '12-12345-1-1234-3412', 0, false,3);

INSERT INTO teacher(teacher_code, deleted, user_id)VALUES('T123', false, 7);
INSERT INTO teacher(teacher_code, deleted, user_id)VALUES('T456', false, 8);
INSERT INTO teacher(teacher_code, deleted, user_id)VALUES('T789', false, 9);

INSERT INTO course(code, name, ects_points, number_of_class, year_of_study, deleted, study_program_id)VALUES('PR1', 'Predmet 1', 7, 40, 1, false, 1);
INSERT INTO course(code, name, ects_points, number_of_class, year_of_study,deleted, study_program_id)VALUES('PR2', 'Predmet 2', 5, 32, 1, false, 1);
INSERT INTO course(code, name, ects_points, number_of_class, year_of_study,deleted, study_program_id)VALUES('PR3', 'Predmet 3', 6, 36, 1, false, 1);

INSERT INTO course(code, name, ects_points, number_of_class, year_of_study, deleted, study_program_id)VALUES('PR4', 'Predmet 4', 6, 36, 1, false, 2);
INSERT INTO course(code, name, ects_points, number_of_class, year_of_study, deleted, study_program_id)VALUES('PR5', 'Predmet 5', 4, 30, 1, false, 2);
INSERT INTO course(code, name, ects_points, number_of_class, year_of_study, deleted, study_program_id)VALUES('PR6', 'Predmet 6', 7, 40, 1, false, 2);

INSERT INTO course(code, name, ects_points, number_of_class, year_of_study, deleted, study_program_id)VALUES('PR7', 'Predmet 7', 5, 32, 1, false, 3);
INSERT INTO course(code, name, ects_points, number_of_class, year_of_study, deleted, study_program_id)VALUES('PR8', 'Predmet 8', 6, 36, 1, false, 3);
INSERT INTO course(code, name, ects_points, number_of_class, year_of_study, deleted, study_program_id)VALUES('PR9', 'Predmet 9', 7, 40, 1, false, 3);

INSERT INTO course(code, name, ects_points, number_of_class, year_of_study, deleted, study_program_id)VALUES('PR10', 'Predmet 10', 7, 40, 1, false, 1);
INSERT INTO course(code, name, ects_points, number_of_class, year_of_study, deleted, study_program_id)VALUES('PR11', 'Predmet 11', 6, 38, 1, false, 1);
INSERT INTO course(code, name, ects_points, number_of_class, year_of_study, deleted, study_program_id)VALUES('PR12', 'Predmet 12', 8, 42, 1, false, 1);

INSERT INTO payment_purpose(name, deleted)VALUES('UPIS', false);
INSERT INTO payment_purpose(name, deleted)VALUES('PRIJAVA_ISPITA', false);
INSERT INTO payment_purpose(name, deleted)VALUES('ODJAVA_ISPITA', false);
INSERT INTO payment_purpose(name, deleted)VALUES('OVERA_SEMESTRA', false);

INSERT INTO payment(amount, payment_type, date, deleted, payment_purpose_id, student_id)VALUES(6700, 'PAYMENT_IN', '2017-07-06', false, 1, 1);
INSERT INTO payment(amount, payment_type, date, deleted, payment_purpose_id, student_id)VALUES(6700, 'PAYMENT_OUT', '2017-07-07', false, 1, 1);
INSERT INTO payment(amount, payment_type, date, deleted, payment_purpose_id, student_id)VALUES(1000, 'PAYMENT_IN', '2017-12-20', false, 2, 1);
INSERT INTO payment(amount, payment_type, date, deleted, payment_purpose_id, student_id)VALUES(1000, 'PAYMENT_OUT', '2018-01-10', false, 2, 1);

INSERT INTO payment(amount, payment_type, date, deleted, payment_purpose_id, student_id)VALUES(6700, 'PAYMENT_IN', '2017-07-06', false, 1, 2);
INSERT INTO payment(amount, payment_type, date, deleted, payment_purpose_id, student_id)VALUES(6700, 'PAYMENT_OUT', '2017-07-07', false, 1, 2);
INSERT INTO payment(amount, payment_type, date, deleted, payment_purpose_id, student_id)VALUES(1000, 'PAYMENT_IN', '2017-12-20', false, 2, 2);
INSERT INTO payment(amount, payment_type, date, deleted, payment_purpose_id, student_id)VALUES(1000, 'PAYMENT_OUT', '2017-01-10', false, 2, 2);

INSERT INTO payment(amount, payment_type, date, deleted, payment_purpose_id, student_id)VALUES(6700, 'PAYMENT_IN', '2017-07-06', false, 1, 3);
INSERT INTO payment(amount, payment_type, date, deleted, payment_purpose_id, student_id)VALUES(6700, 'PAYMENT_OUT', '2017-07-07', false, 1, 3);
INSERT INTO payment(amount, payment_type, date, deleted, payment_purpose_id, student_id)VALUES(1000, 'PAYMENT_IN', '2017-12-20', false, 2, 3);
INSERT INTO payment(amount, payment_type, date, deleted, payment_purpose_id, student_id)VALUES(1000, 'PAYMENT_OUT', '2017-01-10', false, 2, 3);

INSERT INTO document(type, date, file_path, deleted, student_id)VALUES('SV Obrazac', '2017-07-06', 'test.pdf', false, 1);
INSERT INTO document(type, date, file_path, deleted, student_id)VALUES('SV Obrazac', '2017-07-06', 'CVBezSlike.pdf', false, 2);
INSERT INTO document(type, date, file_path, deleted, student_id)VALUES('SV Obrazac', '2017-07-06', 'test.pdf', false, 3);

INSERT INTO title(name, deleted)VALUES('Docent', false);
INSERT INTO title(name, deleted)VALUES('Vanredni profesor', false);
INSERT INTO title(name, deleted)VALUES('Asistent', false);
INSERT INTO title(name, deleted)VALUES('Redovni profesor', false);

INSERT INTO teacher_title(start_date, end_date, deleted, teacher_id, title_id)VALUES('2013-08-20', '2019-07-20',false, 1, 1);
INSERT INTO teacher_title(start_date, end_date, deleted, teacher_id, title_id)VALUES('2019-07-20', null,false, 1, 2);

INSERT INTO teacher_title(start_date, end_date, deleted, teacher_id, title_id)VALUES('2012-05-16', '2019-07-20',false, 2, 2);
INSERT INTO teacher_title(start_date, end_date, deleted, teacher_id, title_id)VALUES('2019-07-20', null,false, 2, 4);

INSERT INTO teacher_title(start_date, end_date, deleted, teacher_id, title_id)VALUES('2014-07-16', '2019-07-20',false, 3, 1);
INSERT INTO teacher_title(start_date, end_date, deleted, teacher_id, title_id)VALUES('2019-07-20', null,false, 3, 2);

INSERT INTO teaching(deleted, teacher_id, course_id)VALUES(false, 1, 1);
INSERT INTO teaching(deleted, teacher_id, course_id)VALUES(false, 1, 2);
INSERT INTO teaching(deleted, teacher_id, course_id)VALUES(false, 1, 3);
INSERT INTO teaching(deleted, teacher_id, course_id)VALUES(false, 1, 10);

INSERT INTO teaching(deleted, teacher_id, course_id)VALUES(false, 2, 4);
INSERT INTO teaching(deleted, teacher_id, course_id)VALUES(false, 2, 5);
INSERT INTO teaching(deleted, teacher_id, course_id)VALUES(false, 2, 6);
INSERT INTO teaching(deleted, teacher_id, course_id)VALUES(false, 2, 11);

INSERT INTO teaching(deleted, teacher_id, course_id)VALUES(false, 3, 7);
INSERT INTO teaching(deleted, teacher_id, course_id)VALUES(false, 3, 8);
INSERT INTO teaching(deleted, teacher_id, course_id)VALUES(false, 3, 9);
INSERT INTO teaching(deleted, teacher_id, course_id)VALUES(false, 3, 12);

INSERT INTO enrollment(start_date, end_date, deleted, student_id, course_id)VALUES('2017-10-10', '2018-01-20', false, 1, 1);
INSERT INTO enrollment(start_date, end_date, deleted, student_id, course_id)VALUES('2017-10-10', '2018-01-20', false, 1, 2);
INSERT INTO enrollment(start_date, end_date, deleted, student_id, course_id)VALUES('2018-10-10', '2019-01-20', false, 1, 3);
INSERT INTO enrollment(start_date, end_date, deleted, student_id, course_id)VALUES('2018-10-10', '2019-01-20', false, 1, 10);
INSERT INTO enrollment(start_date, end_date, deleted, student_id, course_id)VALUES('2018-10-10', '2019-01-20', false, 1, 11);
INSERT INTO enrollment(start_date, end_date, deleted, student_id, course_id)VALUES('2018-10-10', '2019-01-20', false, 1, 12);

INSERT INTO enrollment(start_date, end_date, deleted, student_id, course_id)VALUES('2017-10-10', '2018-01-20', false, 2, 4);
INSERT INTO enrollment(start_date, end_date, deleted, student_id, course_id)VALUES('2017-10-10', '2018-01-20', false, 2, 5);
INSERT INTO enrollment(start_date, end_date, deleted, student_id, course_id)VALUES('2018-10-10', '2019-01-20', false, 2, 6);

INSERT INTO enrollment(start_date, end_date, deleted, student_id, course_id)VALUES('2017-10-10', '2018-01-20', false, 3, 7);
INSERT INTO enrollment(start_date, end_date, deleted, student_id, course_id)VALUES('2017-10-10', '2018-01-20', false, 3, 8);
INSERT INTO enrollment(start_date, end_date, deleted, student_id, course_id)VALUES('2018-10-10', '2019-01-20', false, 3, 9);

INSERT INTO exam_period(name, start_date, end_date, deleted)VALUES('Januarski rok','2020-01-21','2020-02-04',false);
INSERT INTO exam_period(name, start_date, end_date, deleted)VALUES('Februarski rok','2020-02-04','2020-02-14',false);
INSERT INTO exam_period(name, start_date, end_date, deleted)VALUES('Aprilski rok','2020-04-08','2020-04-13',false);
INSERT INTO exam_period(name, start_date, end_date, deleted)VALUES('Junski rok','2020-06-13','2020-06-25',false);
INSERT INTO exam_period(name, start_date, end_date, deleted)VALUES('Julski rok','2020-06-26','2020-07-20',false);
INSERT INTO exam_period(name, start_date, end_date, deleted)VALUES('Avgustovski rok','2020-08-26','2020-08-31',false);
INSERT INTO exam_period(name, start_date, end_date, deleted)VALUES('Septembarski rok','2020-09-02','2020-09-14',false);
INSERT INTO exam_period(name, start_date, end_date, deleted)VALUES('Oktobarski rok','2020-09-16','2020-09-29',false);
INSERT INTO exam_period(name, start_date, end_date, deleted)VALUES('Oktobarski produzeni rok','2019-11-16','2019-11-29',false);

INSERT INTO exam_realization(date, classroom, exam_price, deleted, course_id, teacher_id, exam_period_id)VALUES('2019-01-22 12:00', 'JUG-02', 200, false, 1, 1, 1);
INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2020-01-24 13:00', 'JUG-03', 200, false, 2, 1, 1);
INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2020-01-26 14:00', '109', 200, false, 3, 1, 1);
INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2020-01-30 16:00', '310', 200, false, 11, 2, 1);
INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2020-02-02 18:00', 'JUG-02', 200, false, 12, 3, 1);


INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-09-10 12:00', 'JUG-02', 200, false, 1, 1, 7);
INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-09-08 13:00', 'JUG-03', 200, false, 2, 1, 7);
INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-09-04 14:00', '109', 200, false, 3, 1, 7);

INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-01-28 12:00', 'JUG-02', 200, false, 4, 2, 1);
INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-01-30 13:00', 'JUG-03', 200, false, 5, 2, 1);
INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-02-02 14:00', '109', 200, false, 6, 2, 1);

INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-08-28 12:00', '309', 200, false, 4, 2, 6);
INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-08-30 13:00', 'JUG-02', 200, false, 5, 2, 6);
INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-08-26 14:00', '111', 200, false, 6, 2, 6);

INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-01-23 12:00', '311', 200, false, 7, 3, 1);
INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-01-25 13:00', '209', 200, false, 8, 3, 1);
INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-02-01 14:00', '111', 200, false, 9, 3, 1);

INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-08-27 12:00', 'JUG-02', 200, false, 7, 3, 6);
INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-08-26 13:00', 'JUG-03', 200, false, 8, 3, 6);
INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-08-30 14:00', '109', 200, false, 9, 3, 6);

INSERT INTO exam_realization(date, classroom, exam_price,deleted, course_id, teacher_id, exam_period_id)VALUES('2019-11-17 14:00', '109', 200, false, 12, 3, 9);

INSERT INTO exam(lab_points, exam_points, total_points, final_grade, deleted, student_id, exam_realization_id)VALUES(35,40,75,8,false,1,1);
INSERT INTO exam(lab_points, exam_points, total_points, final_grade, deleted, student_id, exam_realization_id)VALUES(0,0,0,0,false,1,21);

INSERT INTO exam(lab_points, exam_points, total_points, final_grade, deleted, student_id, exam_realization_id)VALUES(45,40,85,9,false,2,7);
INSERT INTO exam(lab_points, exam_points, total_points, final_grade, deleted, student_id, exam_realization_id)VALUES(0,0,0,0,false,2,21);

INSERT INTO exam(lab_points, exam_points, total_points, final_grade, deleted, student_id, exam_realization_id)VALUES(50,45,95,10,false,3,13);
INSERT INTO exam(lab_points, exam_points, total_points, final_grade, deleted, student_id, exam_realization_id)VALUES(0,0,0,0,false,3,21);
