import { Component, OnInit, Input } from "@angular/core";
import { AccountService } from "../services/Account/account.service";
import { Account } from "../shared/model/Account";
import { Student } from "../shared/model/Student";
import { StudentService } from "../services/Student/student.service";

@Component({
  selector: "app-accounts",
  templateUrl: "./accounts.component.html",
  styleUrls: ["./accounts.component.css"]
})
export class AccountsComponent implements OnInit {
  @Input() studentID: number;
  account: Account = {} as Account;
  isAccFound: boolean = false;
  addForm: boolean = false;
  addAccount: boolean = true;
  student: Student = {} as Student;

  constructor(
    private accountService: AccountService,
    private studentService: StudentService
  ) {}

  ngOnInit() {
    this.getAccountByStudent(this.studentID);
    this.getStudent(this.studentID);
  }

  getAccountByStudent(id: number) {
    this.accountService.getByStudent(id).subscribe(
      data => {
        this.account = data;
        this.isAccFound = true;
      },
      err => {
        if (err.status == 404) {
          this.isAccFound = false;
        }
      }
    );
  }

  openAddForm() {
    this.addForm = true;
    this.account.studentDTO = {} as Student;
  }

  closeAddForm(event) {
    this.addForm = event;
    this.account = {} as Account;
  }

  openEditForm() {
    let clone = this.account;
    this.account = JSON.parse(JSON.stringify(clone));
    this.addForm = true;
    this.addAccount = false;
  }

  getStudent(id: number) {
    this.studentService.getByID(id).subscribe(data => {
      this.student = data;
    });
  }

  onAdd(event) {
    this.isAccFound = event;
    this.getAccountByStudent(this.studentID);
  }
}
