import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Account } from "src/app/shared/model/Account";
import { AccountService } from "src/app/services/Account/account.service";
import { Student } from "src/app/shared/model/Student";

@Component({
  selector: "app-accounts-add-edit",
  templateUrl: "./accounts-add-edit.component.html",
  styleUrls: ["./accounts-add-edit.component.css"]
})
export class AccountsAddEditComponent implements OnInit {
  @Input() account: Account = {} as Account;
  @Input() addAccount: boolean = true;
  @Input() student: Student = {} as Student;
  @Output() close = new EventEmitter();
  @Output() added = new EventEmitter();
  addForm: boolean;
  isAdded: boolean;

  constructor(private accountService: AccountService) {}

  ngOnInit() {}

  closeAddForm() {
    this.addForm = false;
    this.close.emit(this.addForm);
  }

  submit() {
    if (this.addAccount) {
      this.account.studentDTO = this.student;
      this.accountService.create(this.account).subscribe(data => {
        //this.newStudent.emit(data);
        this.isAdded = true;
        this.added.emit(this.isAdded);
        this.closeAddForm();
      });
    } else {
      this.accountService.update(this.account).subscribe(data => {
        //this.editStudent.emit(data);
        this.closeAddForm();
      });
    }
  }
}
