import { Component, OnInit } from "@angular/core";
import { Course } from "../shared/model/Course";
import { CourseService } from "../services/Course/course.service";
import { ActivatedRoute } from "@angular/router";
import { StudyProgram } from "../shared/model/StudyProgram";
import { StudyProgramService } from "../services/StudyProgram/study-program.service";

@Component({
  selector: "app-courses",
  templateUrl: "./courses.component.html",
  styleUrls: ["./courses.component.css"]
})
export class CoursesComponent implements OnInit {
  courses: Course[];
  totalPages: number[];
  studyProgramID: number;
  activePage: number = 0;
  addCourse: boolean = true;
  addForm: boolean = false;
  course: Course = {} as Course;
  courseID: number;
  studyProgram: StudyProgram = {} as StudyProgram;
  param: string;
  pageNum: number = 0;
  size: number = 3;

  constructor(
    private courseService: CourseService,
    private route: ActivatedRoute,
    private studyProgramService: StudyProgramService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studyProgramID = params["id"];
    });

    this.getCoursesByStudyProgram(this.studyProgramID, this.pageNum, this.size);
    this.getStudyProgram(this.studyProgramID);
  }

  getCoursesByStudyProgram(id: number, pageNum: number, size: number) {
    this.courseService
      .getCoursesByStudyProgramPageable(id, pageNum, size)
      .subscribe(data => {
        this.courses = data.body;
        this.totalPages = Array(Number.parseInt(data.headers.get("totalPages")))
          .fill(0)
          .map((x, i) => i);
      });
  }

  nextPage(event) {
    let pageNum = event.target.name;
    document
      .getElementById(this.activePage.toString())
      .classList.remove("active");
    this.activePage = pageNum;

    this.getCoursesByStudyProgram(this.studyProgramID, pageNum, this.size);
    document.getElementById(this.activePage.toString()).classList.add("active");
  }

  openAddForm() {
    this.addCourse = true;
    this.addForm = true;
    this.course.studyProgramDTO = {} as StudyProgram;
  }

  addNewRow(event) {
    if (this.courses.length < 3) {
      this.courses.push(event);
    } else {
      this.getCoursesByStudyProgram(
        this.studyProgramID,
        this.pageNum,
        this.size
      );
    }
  }

  closeAddForm(event) {
    this.addForm = event;
    this.course = {} as Course;
  }

  onEditRow(event) {
    this.course = event;
    let index = this.courses.findIndex(item => item.id == this.course.id);
    this.courses[index] = this.course;
  }

  onDeleteRow(event) {
    this.courseID = event;
    this.courseService.delete(this.courseID).subscribe(data => {
      this.courses = this.courses.filter(x => x.id !== this.courseID);
      this.getCoursesByStudyProgram(
        this.studyProgramID,
        this.pageNum,
        this.size
      );
    });
  }

  openEdit(event) {
    this.courseID = event;
    let clone = this.courses.find(x => x.id === this.courseID);
    this.course = JSON.parse(JSON.stringify(clone));
    this.addForm = true;
    this.addCourse = false;
  }

  getStudyProgram(id: number) {
    this.studyProgramService.getOne(id).subscribe(data => {
      this.studyProgram = data;
    });
  }
}
