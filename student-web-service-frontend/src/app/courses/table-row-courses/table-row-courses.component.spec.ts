import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowCoursesComponent } from './table-row-courses.component';

describe('TableRowCoursesComponent', () => {
  let component: TableRowCoursesComponent;
  let fixture: ComponentFixture<TableRowCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
