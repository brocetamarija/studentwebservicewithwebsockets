import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Course } from "src/app/shared/model/Course";

@Component({
  selector: "tr[app-table-row-courses]",
  templateUrl: "./table-row-courses.component.html",
  styleUrls: ["./table-row-courses.component.css"]
})
export class TableRowCoursesComponent implements OnInit {
  @Input() course: Course = {} as Course;
  @Input() i: number;
  @Input() param: string;
  @Output() deleteRow = new EventEmitter();
  @Output() editRow = new EventEmitter();
  id: number;
  constructor() {}

  ngOnInit() {}

  delete(event) {
    this.id = +event.target.value;
    this.deleteRow.emit(this.id);
  }

  edit(event) {
    this.id = +event.target.value;
    this.editRow.emit(this.id);
  }
}
