import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { StudyProgram } from "src/app/shared/model/StudyProgram";
import { Course } from "src/app/shared/model/Course";
import { CourseService } from "src/app/services/Course/course.service";

@Component({
  selector: "app-course-add-edit",
  templateUrl: "./course-add-edit.component.html",
  styleUrls: ["./course-add-edit.component.css"]
})
export class CourseAddEditComponent implements OnInit {
  @Output() close = new EventEmitter();
  @Output() newCourse = new EventEmitter<Course>();
  @Output() editCourse = new EventEmitter<Course>();
  @Input() course: Course = {} as Course;
  @Input() addCourse: boolean = true;
  @Input() studyProgram: StudyProgram = {} as StudyProgram;
  addForm: boolean;
  studyPrograms: StudyProgram[];

  constructor(private courseService: CourseService) {}

  ngOnInit() {}

  closeAddForm() {
    this.addForm = false;
    this.close.emit(this.addForm);
  }

  submit() {
    if (this.addCourse) {
      this.course.studyProgramDTO = this.studyProgram;

      this.courseService.create(this.course).subscribe(data => {
        this.newCourse.emit(data);
        this.closeAddForm();
      });
    } else {
      this.courseService.update(this.course).subscribe(data => {
        this.editCourse.emit(data);
        this.closeAddForm();
      });
    }
  }
}
