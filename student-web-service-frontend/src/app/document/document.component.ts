import { Component, OnInit } from "@angular/core";
import { DocumentService } from "../services/Document/document.service";
import { Document } from "../shared/model/Document";
import { ActivatedRoute } from "@angular/router";
import { Student } from "../shared/model/Student";
import { StudentService } from "../services/Student/student.service";
import { FileService } from "../services/File/file.service";

@Component({
  selector: "app-document",
  templateUrl: "./document.component.html",
  styleUrls: ["./document.component.css"]
})
export class DocumentComponent implements OnInit {
  documents: Document[];
  totalPages: number[];
  activePage: number = 0;
  studentID: number;
  addForm: boolean = false;
  document: Document = {} as Document;
  student: Student = {} as Student;
  documentID: number;
  pageNum: number = 0;
  size: number = 3;
  isAdmin: boolean;

  constructor(
    private documentService: DocumentService,
    private studentService: StudentService,
    private fileService: FileService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studentID = params["id"];
    });

    this.getDocumentsByStudent(this.studentID, this.pageNum, this.size);

    this.getStudent(this.studentID);
  }

  openAddForm() {
    this.addForm = true;
    this.document.studentDTO = {} as Student;
  }

  closeAddForm(event) {
    this.addForm = event;
    this.document = {} as Document;
  }

  addNewRow(event) {
    if (this.documents.length < 3) {
      this.documents.push(event);
    } else {
      this.getDocumentsByStudent(this.studentID, this.pageNum, this.size);
    }
    
  }

  onDeleteRow(event) {
    this.documentID = event;
    this.documentService.delete(this.documentID).subscribe(data => {
      this.documents = this.documents.filter(x => x.id !== this.documentID);
      this.fileService.delete(data.filePath).subscribe(data => {});
    });
  }

  getStudent(id: number) {
    this.studentService.getByID(id).subscribe(data => {
      this.student = data;
    });
  }

  getDocumentsByStudent(studentID: number, pageNum: number, size: number) {
    this.documentService
      .getByStudent(studentID, pageNum, size)
      .subscribe(data => {
        this.documents = data.body;
        this.totalPages = Array(Number.parseInt(data.headers.get("totalPages")))
          .fill(0)
          .map((x, i) => i);
      });
  }

  nextPage(event) {
    let pageNum = event.target.name;
    document
      .getElementById(this.activePage.toString())
      .classList.remove("active");
    this.activePage = pageNum;

    this.getDocumentsByStudent(this.studentID, pageNum, this.size);
    document.getElementById(this.activePage.toString()).classList.add("active");
  }

  check(event) {
    this.isAdmin = event;
  }
}
