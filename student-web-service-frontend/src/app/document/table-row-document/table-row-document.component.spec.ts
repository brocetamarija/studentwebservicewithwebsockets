import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowDocumentComponent } from './table-row-document.component';

describe('TableRowDocumentComponent', () => {
  let component: TableRowDocumentComponent;
  let fixture: ComponentFixture<TableRowDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
