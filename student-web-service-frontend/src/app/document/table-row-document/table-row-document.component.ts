import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Document } from "src/app/shared/model/Document";
import { FileService } from "src/app/services/File/file.service";
import * as fileSaver from "file-saver";

@Component({
  selector: "tr[app-table-row-document]",
  templateUrl: "./table-row-document.component.html",
  styleUrls: ["./table-row-document.component.css"]
})
export class TableRowDocumentComponent implements OnInit {
  @Input() document: Document = {} as Document;
  @Input() i: number;
  @Input() isAdmin: boolean;
  @Output() deleteRow = new EventEmitter();
  @Output() editRow = new EventEmitter();

  id: number;
  filename: string;

  constructor(private fileService: FileService) {}

  ngOnInit() {}

  delete(event) {
    this.id = +event.target.value;
    this.deleteRow.emit(this.id);
  }

  download(event) {
    this.filename = event.target.value;
    this.fileService.download(this.filename).subscribe(data => {
      const filename = data.headers.get("filename");
      this.saveFile(data.body, filename);
    });
  }

  saveFile(data: any, filename?: string) {
    const blob = new Blob([data], { type: "application/pdf" });
    fileSaver.saveAs(blob, filename);
  }
}
