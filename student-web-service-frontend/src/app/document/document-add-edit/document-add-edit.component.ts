import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { FileService } from "src/app/services/File/file.service";
import { Student } from "src/app/shared/model/Student";
import { Document } from "src/app/shared/model/Document";
import { DocumentService } from "src/app/services/Document/document.service";

@Component({
  selector: "app-document-add-edit",
  templateUrl: "./document-add-edit.component.html",
  styleUrls: ["./document-add-edit.component.css"]
})
export class DocumentAddEditComponent implements OnInit {
  @Output() close = new EventEmitter();
  @Output() newDocument = new EventEmitter<Document>();
  @Input() document: Document = {} as Document;
  @Input() student: Student = {} as Student;
  addForm: boolean;
  file: File;
  isFileSelected: boolean = false;
  isEmptyFile: boolean = false;

  constructor(
    private fileService: FileService,
    private documentService: DocumentService
  ) {}

  ngOnInit() {}

  closeAddForm() {
    this.addForm = false;
    this.close.emit(this.addForm);
  }

  selectFile(event) {
    this.file = event.target.files.item(0);
    if (this.file.size == 0) {
      this.isEmptyFile = true;
    }
    this.isFileSelected = true;
  }

  splitString(input: string) {
    return input.split(".")[0];
  }

  submit() {
    this.fileService.uploadDocument(this.file).subscribe(data => {
      this.document.type = this.splitString(this.file.name);
      this.document.filePath = data.path;
      this.document.studentDTO = this.student;
      this.documentService.create(this.document).subscribe(data => {
        this.newDocument.emit(data);
        this.closeAddForm();
      });
    });
  }
}
