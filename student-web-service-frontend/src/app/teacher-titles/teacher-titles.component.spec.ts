import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherTitlesComponent } from './teacher-titles.component';

describe('TeacherTitlesComponent', () => {
  let component: TeacherTitlesComponent;
  let fixture: ComponentFixture<TeacherTitlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherTitlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherTitlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
