import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowTeacherTitlesComponent } from './table-row-teacher-titles.component';

describe('TableRowTeacherTitlesComponent', () => {
  let component: TableRowTeacherTitlesComponent;
  let fixture: ComponentFixture<TableRowTeacherTitlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowTeacherTitlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowTeacherTitlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
