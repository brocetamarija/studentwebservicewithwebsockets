import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { TeacherTitle } from "src/app/shared/model/TeacherTitle";

@Component({
  selector: "tr[app-table-row-teacher-titles]",
  templateUrl: "./table-row-teacher-titles.component.html",
  styleUrls: ["./table-row-teacher-titles.component.css"]
})
export class TableRowTeacherTitlesComponent implements OnInit {
  @Input() teacherTitle: TeacherTitle = {} as TeacherTitle;
  @Input() i: number;
  @Input() param: string;
  @Output() editRow = new EventEmitter();
  id: number;
  constructor() {}

  ngOnInit() {}

  edit(event) {
    this.id = +event.target.value;
    this.editRow.emit(this.id);
  }
}
