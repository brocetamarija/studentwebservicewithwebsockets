import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { TeacherTitle } from "src/app/shared/model/TeacherTitle";
import { Title } from "src/app/shared/model/Title";
import { TeacherTitleService } from "src/app/services/TeacherTitle/teacher-title.service";
import { TitleService } from "src/app/services/Title/title.service";
import { Teacher } from "src/app/shared/model/Teacher";

@Component({
  selector: "app-teacher-title-add-edit",
  templateUrl: "./teacher-title-add-edit.component.html",
  styleUrls: ["./teacher-title-add-edit.component.css"]
})
export class TeacherTitleAddEditComponent implements OnInit {
  @Output() close = new EventEmitter();
  @Output() newTeacherTitle = new EventEmitter<TeacherTitle>();
  @Output() editTeacherTitle = new EventEmitter<TeacherTitle>();
  @Input() teacherTitle: TeacherTitle = {} as TeacherTitle;
  @Input() addTeacherTitle: boolean = true;
  @Input() teacher: Teacher = {} as Teacher;
  titles: Title[];
  addForm: boolean;
  titleID: number;

  constructor(
    private teacherTitleService: TeacherTitleService,
    private titleService: TitleService
  ) {}

  ngOnInit() {
    this.titleService.getTitles().subscribe(data => {
      this.titles = data;
    });
  }

  closeAddForm() {
    this.addForm = false;
    this.close.emit(this.addForm);
  }

  submit() {
    if (this.addTeacherTitle) {
      this.teacherTitle.titleDTO = this.titles.find(
        x => x.id == this.teacherTitle.titleDTO.id
      );
      this.teacherTitle.teacherDTO = this.teacher;
      this.teacherTitleService.create(this.teacherTitle).subscribe(data => {
        this.newTeacherTitle.emit(data);
        this.closeAddForm();
      });
    } else {
      this.teacherTitleService.update(this.teacherTitle).subscribe(data => {
        this.editTeacherTitle.emit(data);
        this.closeAddForm();
      });
    }
  }
}
