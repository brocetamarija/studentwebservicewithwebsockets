import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherTitleAddEditComponent } from './teacher-title-add-edit.component';

describe('TeacherTitleAddEditComponent', () => {
  let component: TeacherTitleAddEditComponent;
  let fixture: ComponentFixture<TeacherTitleAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherTitleAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherTitleAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
