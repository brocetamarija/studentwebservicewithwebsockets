import { Component, OnInit } from "@angular/core";
import { TeacherTitle } from "../shared/model/TeacherTitle";
import { TeacherTitleService } from "../services/TeacherTitle/teacher-title.service";
import { ActivatedRoute } from "@angular/router";
import { Title } from "../shared/model/Title";
import { Teacher } from "../shared/model/Teacher";
import { TeacherService } from "../services/Teacher/teacher.service";

@Component({
  selector: "app-teacher-titles",
  templateUrl: "./teacher-titles.component.html",
  styleUrls: ["./teacher-titles.component.css"]
})
export class TeacherTitlesComponent implements OnInit {
  teacherID: number;
  teacherTitles: TeacherTitle[];
  addForm: boolean = false;
  addTeacherTitle: boolean = true;
  teacherTitle: TeacherTitle = {} as TeacherTitle;
  teacherTitleID: number;
  teacher: Teacher = {} as Teacher;
  param: string;

  constructor(
    private teacherTitleService: TeacherTitleService,
    private route: ActivatedRoute,
    private teacherService: TeacherService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.teacherID = +params["id"];
    });

    this.route.params.subscribe(params => {
      this.param = params["param"];
    });

    this.teacherTitleService
      .getTitlesByTeacher(this.teacherID)
      .subscribe(data => {
        this.teacherTitles = data;
      });

    this.getTeacherByID(this.teacherID);
  }

  openAddForm() {
    this.addTeacherTitle = true;
    this.addForm = true;
    this.teacherTitle.titleDTO = {} as Title;
    this.teacherTitle.teacherDTO = {} as Teacher;
  }

  closeAddForm(event) {
    this.addForm = event;
    this.teacherTitle = {} as TeacherTitle;
  }

  addNewRow(event) {
    if (this.teacherTitles.length < 3) {
      this.teacherTitles.push(event);
    } else {
      //this.getTeachers();
    }
  }

  openEdit(event) {
    this.teacherTitleID = event;
    let clone = this.teacherTitles.find(x => x.id === this.teacherTitleID);
    this.teacherTitle = JSON.parse(JSON.stringify(clone));
    this.addForm = true;
    this.addTeacherTitle = false;
  }

  onEditRow(event) {
    this.teacherTitle = event;
    let index = this.teacherTitles.findIndex(
      item => item.id == this.teacherTitle.id
    );
    this.teacherTitles[index] = this.teacherTitle;
  }

  getTeacherByID(id: number) {
    this.teacherService.getByID(id).subscribe(data => {
      this.teacher = data;
    });
  }
}
