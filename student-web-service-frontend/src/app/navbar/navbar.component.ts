import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { AuthenticationService } from "../services/Login/Authentication/authentication.service";
import { UserService } from "../services/User/user.service";
import { User } from "../shared/model/User";
import PasswordChanger from "../shared/model/PasswordChanger";
import { StudentService } from "../services/Student/student.service";
import { TeacherService } from "../services/Teacher/teacher.service";
import { Student } from "../shared/model/Student";
import { Teacher } from "../shared/model/Teacher";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"]
})
export class NavbarComponent implements OnInit {
  loggedRole;
  logged;
  isAdmin: boolean = false;
  isStudent: boolean = false;
  isTeacher: boolean = false;
  user: User = {} as User;
  student: Student = {} as Student;
  teacher: Teacher = {} as Teacher;
  activePass: boolean = false;
  passwordChanger: PasswordChanger = {} as PasswordChanger;
  @Output() emitIsAdmin = new EventEmitter();

  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private studentService: StudentService,
    private teacherService: TeacherService
  ) {}

  ngOnInit() {
    this.logged = this.authenticationService.getCurrentUser();
    this.loggedRole = this.logged.roles[0];
    if (this.loggedRole == "ROLE_STUDENT") {
      this.studentService
        .getByUsername(this.logged.username)
        .subscribe(data => {
          this.student = data;
          this.isStudent = true;
        });
    } else if (this.loggedRole == "ROLE_TEACHER") {
      this.teacherService
        .getByUsername(this.logged.username)
        .subscribe(data => {
          this.teacher = data;
          this.isTeacher = true;
        });
    } else {
      this.userService.getByUsername(this.logged.username).subscribe(data => {
        this.user = data;
        this.isAdmin = true;
        this.emitIsAdmin.emit(this.isAdmin);
      });
    }
  }

  openPassword() {
    this.activePass = true;
  }

  closePassword() {
    this.activePass = false;
    this.passwordChanger = {} as PasswordChanger;
  }

  logout() {
    this.authenticationService.logout();
  }

  submitPasswordChange() {
    this.userService.changePassword(this.passwordChanger).subscribe(data => {});
    this.closePassword();
  }
}
