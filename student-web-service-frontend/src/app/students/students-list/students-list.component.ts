import { Component, OnInit } from '@angular/core';
import { TypeOfStudyService } from 'src/app/services/TypeOfStudy/type-of-study.service';
import { TypeOfStudy } from 'src/app/shared/model/TypeOfStudy';
import { StudyProgramService } from 'src/app/services/StudyProgram/study-program.service';
import { StudyProgram } from 'src/app/shared/model/StudyProgram';
import { StudentService } from 'src/app/services/Student/student.service';
import { Student } from 'src/app/shared/model/Student';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.css']
})
export class StudentsListComponent implements OnInit {
  studyTypes: TypeOfStudy[];
  studyTypeID: number = 1;
  studyPrograms: StudyProgram[];
  students: Student[];
  studyProgramID: number = 1;
  isAdded: boolean;

  constructor(private studyTypeService: TypeOfStudyService, private studyProgramService: StudyProgramService, private studentService:StudentService) { }

  ngOnInit() {
    this.getStudyTypes();
    this.getStudyProgramsByStudyType(this.studyTypeID);
    this.getStudentsByStudyProgramAndStudyType(this.studyProgramID,this.studyTypeID);
  }

  getStudyTypes(){
    this.studyTypeService.getAll().subscribe(data => {
      this.studyTypes = data;
    });
  }

  getStudyProgramsByStudyType(id: number){
    this.studyProgramService.getByStudyType(id).subscribe(data => {
      this.studyPrograms = data;
    });
  }

  getStudentsByStudyProgramAndStudyType(studyProgramID: number, studyTypeID: number){
    this.studentService.getByStudyProgramAndStudyType(studyProgramID, studyTypeID).subscribe(data => {
      this.students = data;
    });
  }

  onChangeStudyType(event){
    this.studyTypeID = event.target.value;
    this.getStudyProgramsByStudyType(this.studyTypeID);
    console.log(this.studyTypeID);
    console.log(this.studyProgramID);
  }

  onChangeStudyProgram(event){
    this.studyProgramID = event.target.value;
    this.getStudentsByStudyProgramAndStudyType(this.studyProgramID,this.studyTypeID);
    console.log(this.studyTypeID);
    console.log(this.studyProgramID);
  }

  check(event) {
    this.isAdded = event;
  }
}
