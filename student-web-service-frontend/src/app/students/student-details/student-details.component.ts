import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { StudentService } from "src/app/services/Student/student.service";
import { Student } from "src/app/shared/model/Student";
import { Place } from "src/app/shared/model/Place";
import { StudyProgram } from "src/app/shared/model/StudyProgram";
import { PlaceService } from "src/app/services/Place/place.service";
import { StudyProgramService } from "src/app/services/StudyProgram/study-program.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AccountService } from "src/app/services/Account/account.service";
import { Account } from "src/app/shared/model/Account";

@Component({
  selector: "app-student-details",
  templateUrl: "./student-details.component.html",
  styleUrls: ["./student-details.component.css"]
})
export class StudentDetailsComponent implements OnInit {
  @Input() addStudent: boolean = true;
  studentID: number;
  student: Student = {} as Student;
  addForm: boolean;

  constructor(
    private studentService: StudentService,
    private route: ActivatedRoute,
    private router: Router,
    private accountService: AccountService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studentID = params["id"];
    });

    this.getStudentByID(this.studentID);

    //this.getAccountByStudent(this.studentID);
  }

  getStudentByID(id: number) {
    this.studentService.getByID(id).subscribe(data => {
      this.student = data;
    });
  }

  delete() {
    this.studentService.delete(this.studentID).subscribe(data => {
      this.router.navigate([
        "/students",
        this.student.studyProgramDTO.id
      ]);
    });
  }

  edit() {
    let clone = this.student;
    this.student = JSON.parse(JSON.stringify(clone));
    this.addForm = true;
    this.addStudent = false;
  }

  closeAddForm(event) {
    this.addForm = event;
  }

  getPayments() {
    this.router.navigate(["/payments", this.studentID]);
  }

  getDocuments() {
    this.router.navigate(["/documents", this.studentID]);
  }

  getEnrollments() {
    this.router.navigate(["/enrollments", this.studentID]);
  }

  /*getAccountByStudent(id: number) {
    this.accountService.getByStudent(id).subscribe(
      data => {
        this.account = data;
        this.isAccFound = true;
      },
      err => {
        if (err.status == 404) {
          this.isAccFound = false;
        }
      }
    );
  }*/
}
