import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowStudentsComponent } from './table-row-students.component';

describe('TableRowStudentsComponent', () => {
  let component: TableRowStudentsComponent;
  let fixture: ComponentFixture<TableRowStudentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowStudentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowStudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
