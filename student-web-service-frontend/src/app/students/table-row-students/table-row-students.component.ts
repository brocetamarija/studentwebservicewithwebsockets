import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Student } from "src/app/shared/model/Student";
import { Router } from "@angular/router";
import { SemesterStudentService } from "src/app/services/SemesterStudent/semester-student.service";
import { StudentService } from "src/app/services/Student/student.service";
import { SemesterStudent } from "src/app/shared/model/SemesterStudent";
import { SemesterService } from "src/app/services/Semester/semester.service";
import { Semester } from "src/app/shared/model/Semester";

@Component({
  selector: "tr[app-table-row-students]",
  templateUrl: "./table-row-students.component.html",
  styleUrls: ["./table-row-students.component.css"]
})
export class TableRowStudentsComponent implements OnInit {
  @Input() student: Student = {} as Student;
  @Input() i: number;
  @Input() param: string;
  @Output() isAddedEmit = new EventEmitter();
  id: number;
  isAdded: boolean;
  studentS: Student = {} as Student;
  semesterStudent: SemesterStudent = {} as SemesterStudent;
  semester: Semester = {} as Semester;

  constructor(
    private router: Router,
    private semesterStudentService: SemesterStudentService,
    private studentService: StudentService,
    private semesterService: SemesterService
  ) {}

  ngOnInit() {
    this.checkIfSemesterAdded(this.student.id);
    this.getLast();
  }

  getDetails(event) {
    this.id = +event.target.value;
    this.router.navigate(["/student-details", this.id]);
  }

  addSemester(event) {
    this.id = +event.target.value;
    this.semesterStudent.studentDTO.id = this.id;
    this.semesterStudent.semesterDTO = this.semester;
    this.semesterStudentService
      .addSemesterToStudent(this.semesterStudent)
      .subscribe(data => {
        this.checkIfSemesterAdded(this.student.id);
      });
  }

  checkIfSemesterAdded(id: number) {
    this.semesterStudentService.checkIfSemesterAdded(id).subscribe(data => {
      this.isAdded = data;
      this.semesterStudent.studentDTO = {} as Student;
      this.semesterStudent.semesterDTO = {} as Semester;
      this.isAddedEmit.emit(this.isAdded);
    });
  }


  getStudent(id: number) {
    this.studentService.getByID(id).subscribe(data => {
      this.studentS = data;
    });
  }

  getLast() {
    this.semesterService.getLast().subscribe(
      data => {
        this.semester = data;
      },
      err => {
        if (err.status == 404) {
          this.isAdded = true;
          this.isAddedEmit.emit(this.isAdded);
        }
      }
    );
  }
}
