import { Component, OnInit } from "@angular/core";
import { StudentService } from "../services/Student/student.service";
import { Student } from "../shared/model/Student";
import { User } from "../shared/model/User";
import { Place } from "../shared/model/Place";
import { Address } from "../shared/model/Address";
import { StudyProgram } from "../shared/model/StudyProgram";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-students",
  templateUrl: "./students.component.html",
  styleUrls: ["./students.component.css"]
})
export class StudentsComponent implements OnInit {
  students: Student[];
  totalPages: number[];
  activePage: number = 0;
  addStudent: boolean = true;
  addForm: boolean = false;
  student: Student = {} as Student;
  studyProgramID: number;
  param: string;
  pageNum: number = 0;
  size: number = 3;
  isAdded: boolean;

  constructor(
    private studentService: StudentService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.param = params["param"];
    });

    this.route.params.subscribe(params => {
      this.studyProgramID = params["id"];
    });

    this.getStudentsByStudyProgram(
      this.studyProgramID,
      this.pageNum,
      this.size
    );
  }

  getStudentsByStudyProgram(id: number, pageNum: number, size: number) {
    this.studentService.getByStudyProgram(id, pageNum, size).subscribe(data => {
      this.students = data.body;
      this.totalPages = Array(Number.parseInt(data.headers.get("totalPages")))
        .fill(0)
        .map((x, i) => i);
    });
  }

  nextPage(event) {
    let pageNum = event.target.name;
    document
      .getElementById(this.activePage.toString())
      .classList.remove("active");
    this.activePage = pageNum;
    this.getStudentsByStudyProgram(this.studyProgramID, pageNum, this.size);
    document.getElementById(this.activePage.toString()).classList.add("active");
  }

  openAddForm() {
    this.addStudent = true;
    this.addForm = true;
    this.student.userDTO = {} as User;
    this.student.userDTO.addressDTO = {} as Address;
    this.student.userDTO.addressDTO.placeDTO = {} as Place;
    this.student.studyProgramDTO = {} as StudyProgram;
  }

  closeAddForm(event) {
    this.addForm = event;
    this.student = {} as Student;
  }

  addNewRow(event) {
    if (this.students.length < 3) {
      this.students.push(event);
    } else {
      this.getStudentsByStudyProgram(
        this.studyProgramID,
        this.pageNum,
        this.size
      );
    }
  }

  check(event) {
    this.isAdded = event;
  }
}
