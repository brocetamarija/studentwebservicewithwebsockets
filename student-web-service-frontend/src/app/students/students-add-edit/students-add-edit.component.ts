import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { Place } from "src/app/shared/model/Place";
import { Student } from "src/app/shared/model/Student";
import { StudentService } from "src/app/services/Student/student.service";
import { PlaceService } from "src/app/services/Place/place.service";
import { StudyProgramService } from "src/app/services/StudyProgram/study-program.service";
import { StudyProgram } from "src/app/shared/model/StudyProgram";

@Component({
  selector: "app-students-add-edit",
  templateUrl: "./students-add-edit.component.html",
  styleUrls: ["./students-add-edit.component.css"]
})
export class StudentsAddEditComponent implements OnInit {
  @Output() close = new EventEmitter();
  @Output() newStudent = new EventEmitter<Student>();
  @Output() editStudent = new EventEmitter<Student>();
  @Input() student: Student = {} as Student;
  @Input() addStudent: boolean = true;
  places: Place[];
  studyPrograms: StudyProgram[];
  addForm: boolean;
  placeID: number;

  constructor(
    private studentService: StudentService,
    private placeService: PlaceService,
    private studyProgramService: StudyProgramService
  ) {}

  ngOnInit() {
    this.placeService.getAll().subscribe(data => {
      this.places = data;
    });

    this.studyProgramService.getAll().subscribe(data => {
      this.studyPrograms = data;
    });
  }

  closeAddForm() {
    this.addForm = false;
    this.close.emit(this.addForm);
  }

  submit() {
    if (this.addStudent) {
      this.student.userDTO.addressDTO.placeDTO = this.places.find(
        x => x.id == this.student.userDTO.addressDTO.placeDTO.id
      );
      this.student.studyProgramDTO = this.studyPrograms.find(
        x => x.id == this.student.studyProgramDTO.id
      );

      this.studentService.create(this.student).subscribe(data => {
        this.newStudent.emit(data);
        this.closeAddForm();
      });
    } else {
      this.studentService.update(this.student).subscribe(data => {
        this.closeAddForm();
      });
    }
  }
}
