import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsAddEditComponent } from './students-add-edit.component';

describe('StudentsAddEditComponent', () => {
  let component: StudentsAddEditComponent;
  let fixture: ComponentFixture<StudentsAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
