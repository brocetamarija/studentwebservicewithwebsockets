import { SemesterType } from "./SemesterType";

export interface Semester {
  id: number;
  startDate: Date;
  endDate: Date;
  semesterType: SemesterType;
  deleted: boolean;
}
