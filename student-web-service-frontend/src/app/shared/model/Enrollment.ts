import { Student } from "./Student";
import { Course } from "./Course";

export interface Enrollment {
  id: number;
  startDate: Date;
  endDate: Date;
  deleted: boolean;
  studentDTO: Student;
  courseDTO: Course;
}
