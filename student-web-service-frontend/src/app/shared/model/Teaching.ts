import { Teacher } from "./Teacher";
import { Course } from "./Course";

export interface Teaching {
  id: number;
  deleted: boolean;
  teacherDTO: Teacher;
  courseDTO: Course;
}
