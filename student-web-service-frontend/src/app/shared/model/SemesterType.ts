export enum SemesterType {
  Letnji = "Letnji",
  Zimski = "Zimski"
}

export namespace SemesterType {
  export function values() {
    return Object.keys(SemesterType).filter(
      type => isNaN(<any>type) && type !== "values"
    );
  }
}
