export interface TypeOfStudy {
  id: number;
  name: string;
  deleted: boolean;
}
