export interface PaymentPurpose {
  id: number;
  name: string;
  deleted: boolean;
}
