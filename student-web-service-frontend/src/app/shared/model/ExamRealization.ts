import { Course } from "./Course";
import { ExamPeriod } from "./ExamPeriod";
import { Teacher } from "./Teacher";

export interface ExamRealization {
  id: number;
  date: Date;
  classroom: string;
  examPrice: number;
  deleted: boolean;
  courseDTO: Course;
  teacherDTO: Teacher;
  examPeriodDTO: ExamPeriod;
}
