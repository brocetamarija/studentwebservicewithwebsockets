import { User } from "./User";

export interface Teacher {
  id: number;
  teacherCode: string;
  deleted: boolean;
  userDTO: User;
}
