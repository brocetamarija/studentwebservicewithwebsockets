export interface Title {
  id: number;
  name: string;
  deleted: boolean;
}
