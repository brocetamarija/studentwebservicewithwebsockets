import { Place } from './Place';

export interface Address{
    id: number;
    streetName: string;
    streetNumber: number;
    deleted: boolean;
    placeDTO: Place;
}