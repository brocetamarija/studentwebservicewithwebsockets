export interface Place {
  id: number;
  name: string;
  zipCode: number;
  deleted: boolean;
}
