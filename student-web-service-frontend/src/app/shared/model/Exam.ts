import { Student } from "./Student";
import { ExamRealization } from "./ExamRealization";

export interface Exam {
  id: number;
  labPoints: number;
  examPoints: number;
  totalPoints: number;
  finalGrade: number;
  deleted: boolean;
  studentDTO: Student;
  examRealizationDTO: ExamRealization;
}
