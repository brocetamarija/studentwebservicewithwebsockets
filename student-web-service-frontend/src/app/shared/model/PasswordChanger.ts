export default interface PasswordChanger {
  oldPassword: string;
  newPassword: string;
}
