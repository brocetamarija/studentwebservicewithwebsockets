import { StudyProgram } from "./StudyProgram";

export interface Course {
  id: number;
  code: string;
  name: string;
  ECTSPoints: number;
  numberOfClass: number;
  yearOfStudy: number;
  deleted: boolean;
  studyProgramDTO: StudyProgram;
}
