export interface ExamPeriod {
  id: number;
  name: string;
  startDate: Date;
  endDate: Date;
  deleted: boolean;
}
