import { Title } from "./Title";
import { Teacher } from "./Teacher";

export interface TeacherTitle {
  id: number;
  startDate: Date;
  endDate: Date;
  deleted: boolean;
  titleDTO: Title;
  teacherDTO: Teacher;
}
