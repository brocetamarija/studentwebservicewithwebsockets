export interface UploadModel {
  filename: string;
  date: Date;
  file: File;
}
