import { TypeOfStudy } from "./TypeOfStudy";

export interface StudyProgram {
  id: number;
  name: string;
  deleted: boolean;
  typeOfStudyDTO: TypeOfStudy;
}
