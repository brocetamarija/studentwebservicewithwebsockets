import { Address } from "./Address";

export interface User {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  password: string;
  email: string;
  type: string;
  deleted: boolean;
  addressDTO: Address;
}
