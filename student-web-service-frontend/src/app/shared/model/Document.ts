import { Student } from "./Student";

export interface Document {
  id: number;
  type: string;
  filePath: string;
  deleted: boolean;
  studentDTO: Student;
}
