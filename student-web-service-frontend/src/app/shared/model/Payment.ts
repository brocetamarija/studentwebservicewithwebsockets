import { PaymentType } from "./PaymentType";
import { Student } from "./Student";
import { PaymentPurpose } from "./PaymentPurpose";

export interface Payment {
  id: number;
  amount: number;
  date: Date;
  deleted: boolean;
  paymentType: PaymentType;
  paymentPurposeDTO: PaymentPurpose;
  studentDTO: Student;
}
