import { Student } from "./Student";

export interface Account {
  id: number;
  giroAccountNumber: string;
  modelNumber: number;
  personalReferenceNumber: string;
  currentAccountBalance: number;
  deleted: boolean;
  studentDTO: Student;
}
