import { StudyProgram } from "./StudyProgram";
import { User } from "./User";

export interface Student {
  id: number;
  cardNumber: string;
  phoneNumber: string;
  birthDate: Date;
  yearOfStudy: number;
  deleted: boolean;
  userDTO: User;
  studyProgramDTO: StudyProgram;
}
