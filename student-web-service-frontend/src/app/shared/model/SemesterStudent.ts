import { Student } from "./Student";
import { Semester } from "./Semester";

export interface SemesterStudent {
  id: number;
  certified: boolean;
  semesterCertificationPrice: number;
  studentDTO: Student;
  semesterDTO: Semester;
  deleted: boolean;
}
