import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowStudentUnfinishedCoursesComponent } from './table-row-student-unfinished-courses.component';

describe('TableRowStudentUnfinishedCoursesComponent', () => {
  let component: TableRowStudentUnfinishedCoursesComponent;
  let fixture: ComponentFixture<TableRowStudentUnfinishedCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowStudentUnfinishedCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowStudentUnfinishedCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
