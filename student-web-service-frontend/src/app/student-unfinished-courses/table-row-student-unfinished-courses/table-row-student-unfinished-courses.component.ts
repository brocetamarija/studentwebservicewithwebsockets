import { Component, OnInit, Input } from "@angular/core";
import { Course } from "src/app/shared/model/Course";

@Component({
  selector: "tr[app-table-row-student-unfinished-courses]",
  templateUrl: "./table-row-student-unfinished-courses.component.html",
  styleUrls: ["./table-row-student-unfinished-courses.component.css"]
})
export class TableRowStudentUnfinishedCoursesComponent implements OnInit {
  @Input() course: Course = {} as Course;
  @Input() i: number;

  constructor() {}

  ngOnInit() {}
}
