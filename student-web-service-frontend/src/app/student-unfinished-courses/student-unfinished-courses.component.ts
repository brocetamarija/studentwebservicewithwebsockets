import { Component, OnInit } from "@angular/core";
import { Course } from "../shared/model/Course";
import { CourseService } from "../services/Course/course.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-student-unfinished-courses",
  templateUrl: "./student-unfinished-courses.component.html",
  styleUrls: ["./student-unfinished-courses.component.css"]
})
export class StudentUnfinishedCoursesComponent implements OnInit {
  courses: Course[];
  studentID: number;
  constructor(
    private courseService: CourseService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studentID = +params["id"];
    });

    this.getUnfinishedCourses(this.studentID);
  }

  getUnfinishedCourses(id: number) {
    this.courseService.getUnfinishedCourses(id).subscribe(data => {
      this.courses = data;
    });
  }
}
