import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentUnfinishedCoursesComponent } from './student-unfinished-courses.component';

describe('StudentUnfinishedCoursesComponent', () => {
  let component: StudentUnfinishedCoursesComponent;
  let fixture: ComponentFixture<StudentUnfinishedCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentUnfinishedCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentUnfinishedCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
