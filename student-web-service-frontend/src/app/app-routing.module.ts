import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { HomepageComponent } from "./homepage/homepage.component";
import { CanActivateService } from "./services/Login/Authentication/can-activate.service";
import { AdministratorsComponent } from "./administrators/administrators.component";
import { StudentsComponent } from "./students/students.component";
import { PaymentsComponent } from "./payments/payments.component";
import { DocumentComponent } from "./document/document.component";
import { TeachersComponent } from "./teachers/teachers.component";
import { TeacherTitlesComponent } from "./teacher-titles/teacher-titles.component";
import { StudyProgramsComponent } from "./study-programs/study-programs.component";
import { StudentDetailsComponent } from "./students/student-details/student-details.component";
import { CoursesComponent } from "./courses/courses.component";
import { TypeOfStudiesComponent } from "./type-of-studies/type-of-studies.component";
import { TeachingsComponent } from "./teachings/teachings.component";
import { EnrollmentsComponent } from "./enrollments/enrollments.component";
import { ExamRealizationsComponent } from "./exam-realizations/exam-realizations.component";
import { ExamPeriodsComponent } from "./exam-periods/exam-periods.component";
import { StudentPassedExamsComponent } from "./student-passed-exams/student-passed-exams.component";
import { StudentUnfinishedCoursesComponent } from "./student-unfinished-courses/student-unfinished-courses.component";
import { StudentExamResultsComponent } from "./student-exam-results/student-exam-results.component";
import { StudentExamRegistrationComponent } from "./student-exam-registration/student-exam-registration.component";
import { StudentCancelExamRegistrationComponent } from "./student-cancel-exam-registration/student-cancel-exam-registration.component";
import { RegisteredExamsComponent } from "./registered-exams/registered-exams.component";
import { TeacherExamRealizationsComponent } from "./teacher-exam-realizations/teacher-exam-realizations.component";
import { TeacherEnteringResultsComponent } from "./teacher-entering-results/teacher-entering-results.component";
import { TeacherExamResultsComponent } from "./teacher-exam-results/teacher-exam-results.component";
import { SemestersComponent } from "./semesters/semesters.component";
import { TeacherDetailsComponent } from './teachers/teacher-details/teacher-details.component';
import { StudentsListComponent } from './students/students-list/students-list.component';

const routes: Routes = [
  {
    path: "",
    component: LoginComponent
  },
  {
    path: "home",
    component: HomepageComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "administrators",
    component: AdministratorsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "students/:id",
    component: StudentsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "payments/:id",
    component: PaymentsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "documents/:id",
    component: DocumentComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "teachers",
    component: TeachersComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "titles/:param/:id",
    component: TeacherTitlesComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "study-programs",
    component: StudyProgramsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "student-details/:id",
    component: StudentDetailsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "courses/:id",
    component: CoursesComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "study-types",
    component: TypeOfStudiesComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "teachings/:param/:id",
    component: TeachingsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "enrollments/:id",
    component: EnrollmentsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "exam-realizations",
    component: ExamRealizationsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "exam-periods",
    component: ExamPeriodsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "passed-exams/:id",
    component: StudentPassedExamsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "unfinished-courses/:id",
    component: StudentUnfinishedCoursesComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "exam-results/:id",
    component: StudentExamResultsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "exam-registration/:id",
    component: StudentExamRegistrationComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "cancel-exam-registration/:id",
    component: StudentCancelExamRegistrationComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "registered-exams/:id",
    component: RegisteredExamsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "future-exams/:param/:id",
    component: TeacherExamRealizationsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "enter-results/:id",
    component: TeacherEnteringResultsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "teacher-exam-results/:id",
    component: TeacherExamResultsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "semesters",
    component: SemestersComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "teacher-details/:id",
    component: TeacherDetailsComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "students-list",
    component: StudentsListComponent,
    canActivate: [CanActivateService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
