import { Component, OnInit } from "@angular/core";
import { ExamService } from "../services/Exam/exam.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Exam } from "../shared/model/Exam";
import { WebSocketApiService } from "../services/WebSocket/web-socket-api.service";
import { Message } from "../shared/model/Message";

@Component({
  selector: "app-teacher-entering-results",
  templateUrl: "./teacher-entering-results.component.html",
  styleUrls: ["./teacher-entering-results.component.css"]
})
export class TeacherEnteringResultsComponent implements OnInit {
  examRealizationID: number;
  exams: Exam[];
  isDisabled: boolean = false;
  exam: Exam = {} as Exam;

  constructor(
    private examService: ExamService,
    private route: ActivatedRoute,
    private router: Router,
    private wsApiService: WebSocketApiService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.examRealizationID = +params["id"];
    });

    this.getExamsForEnteringResults(this.examRealizationID);
  }

  getExamsForEnteringResults(id: number) {
    this.examService.getExamsForEnteringResults(id).subscribe(data => {
      this.exams = data;
    });
  }

  confirm() {
    for (let e of this.exams) {
      this.exam = e;
      this.examService.update(this.exam).subscribe(data => {
        this.router.navigate(["/teacher-exam-results", this.examRealizationID]);
      });
    }
  }

  displayNum(event) {
    if (event < 0 || event > 50) {
      this.isDisabled = true;
    } else {
      this.isDisabled = false;
    }
  }
}
