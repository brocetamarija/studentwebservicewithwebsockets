import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowTeacherEnteringResultsComponent } from './table-row-teacher-entering-results.component';

describe('TableRowTeacherEnteringResultsComponent', () => {
  let component: TableRowTeacherEnteringResultsComponent;
  let fixture: ComponentFixture<TableRowTeacherEnteringResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowTeacherEnteringResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowTeacherEnteringResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
