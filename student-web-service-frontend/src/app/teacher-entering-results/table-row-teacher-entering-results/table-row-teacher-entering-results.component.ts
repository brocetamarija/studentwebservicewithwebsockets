import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Exam } from "src/app/shared/model/Exam";
import { ExamService } from "src/app/services/Exam/exam.service";

@Component({
  selector: "tr[app-table-row-teacher-entering-results]",
  templateUrl: "./table-row-teacher-entering-results.component.html",
  styleUrls: ["./table-row-teacher-entering-results.component.css"]
})
export class TableRowTeacherEnteringResultsComponent implements OnInit {
  @Input() exam: Exam = {} as Exam;
  @Input() i: number;
  @Output() points = new EventEmitter();
  editField: number;
  labPoints: number = 0;
  examPoints: number = 0;
  totalPoints: number = 0;
  grade: number = 0;
  dateToday = new Date();
  lessPoints: boolean = false;
  morePoints: boolean = false;

  constructor(private examService: ExamService) {}

  ngOnInit() {}

  changeValue(id: number, property: string, event: any) {
    this.editField = +event.target.value;
    if (property === "labPoints") {
      this.labPoints = this.editField;
    } else {
      this.examPoints = this.editField;
    }
    this.examService
      .countTotalPoints(this.labPoints, this.examPoints)
      .subscribe(data => {
        this.totalPoints = data;
        this.exam.totalPoints = this.totalPoints;
        this.examService.countGrade(this.totalPoints).subscribe(data => {
          this.grade = data;
          this.exam.finalGrade = this.grade;
        });
      });
  }

  onKeyUp(event: any) {
    let num = event.target.value;
    this.points.emit(num);
    if (num > 50) {
      this.morePoints = true;
    }
    if (num < 0) {
      this.lessPoints = true;
    }
  }

  checkDate(date) {
    if (this.dateToday < new Date(date)) {
      return true;
    }
  }

  closeAlert() {
    this.morePoints = false;
    this.lessPoints = false;
  }
}
