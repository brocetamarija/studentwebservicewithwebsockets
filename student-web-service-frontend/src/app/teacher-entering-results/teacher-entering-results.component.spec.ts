import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherEnteringResultsComponent } from './teacher-entering-results.component';

describe('TeacherEnteringResultsComponent', () => {
  let component: TeacherEnteringResultsComponent;
  let fixture: ComponentFixture<TeacherEnteringResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherEnteringResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherEnteringResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
