import { Component, OnInit } from "@angular/core";
import { ExamService } from "../services/Exam/exam.service";
import { Exam } from "../shared/model/Exam";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-teacher-exam-results",
  templateUrl: "./teacher-exam-results.component.html",
  styleUrls: ["./teacher-exam-results.component.css"]
})
export class TeacherExamResultsComponent implements OnInit {
  exams: Exam[];
  examRealizationID: number;
  constructor(
    private examService: ExamService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.examRealizationID = +params["id"];
    });

    this.getExamResults(this.examRealizationID);
  }

  getExamResults(id: number) {
    this.examService.getExamResultsForTeacher(id).subscribe(data => {
      this.exams = data;
    });
  }
}
