import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherExamResultsComponent } from './teacher-exam-results.component';

describe('TeacherExamResultsComponent', () => {
  let component: TeacherExamResultsComponent;
  let fixture: ComponentFixture<TeacherExamResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherExamResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherExamResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
