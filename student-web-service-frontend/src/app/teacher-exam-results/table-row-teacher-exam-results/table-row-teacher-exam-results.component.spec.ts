import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowTeacherExamResultsComponent } from './table-row-teacher-exam-results.component';

describe('TableRowTeacherExamResultsComponent', () => {
  let component: TableRowTeacherExamResultsComponent;
  let fixture: ComponentFixture<TableRowTeacherExamResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowTeacherExamResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowTeacherExamResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
