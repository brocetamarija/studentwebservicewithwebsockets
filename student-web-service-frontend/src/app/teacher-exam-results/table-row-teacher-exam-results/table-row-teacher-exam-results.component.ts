import { Component, OnInit, Input } from "@angular/core";
import { Exam } from "src/app/shared/model/Exam";

@Component({
  selector: "tr[app-table-row-teacher-exam-results]",
  templateUrl: "./table-row-teacher-exam-results.component.html",
  styleUrls: ["./table-row-teacher-exam-results.component.css"]
})
export class TableRowTeacherExamResultsComponent implements OnInit {
  @Input() exam: Exam = {} as Exam;
  @Input() i: number;
  constructor() {}

  ngOnInit() {}
}
