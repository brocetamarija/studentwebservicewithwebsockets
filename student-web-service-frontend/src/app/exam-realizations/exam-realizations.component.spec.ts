import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamRealizationsComponent } from './exam-realizations.component';

describe('ExamRealizationsComponent', () => {
  let component: ExamRealizationsComponent;
  let fixture: ComponentFixture<ExamRealizationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamRealizationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamRealizationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
