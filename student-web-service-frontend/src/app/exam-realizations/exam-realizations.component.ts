import { Component, OnInit } from "@angular/core";
import { StudyProgramService } from "../services/StudyProgram/study-program.service";
import { ExamPeriodService } from "../services/ExamPeriod/exam-period.service";
import { ExamRealizationService } from "../services/ExamRealization/exam-realization.service";
import { ExamRealization } from "../shared/model/ExamRealization";
import { StudyProgram } from "../shared/model/StudyProgram";
import { ExamPeriod } from "../shared/model/ExamPeriod";
import { Course } from "../shared/model/Course";
import { Teacher } from "../shared/model/Teacher";

@Component({
  selector: "app-exam-realizations",
  templateUrl: "./exam-realizations.component.html",
  styleUrls: ["./exam-realizations.component.css"]
})
export class ExamRealizationsComponent implements OnInit {
  examRealizations: ExamRealization[];
  studyPrograms: StudyProgram[];
  examPeriods: ExamPeriod[];
  studyProgramID: number = 1;
  examPeriodID: number;
  totalPages: number[];
  activePage: number = 0;
  pageNum: number = 0;
  size: number = 3;
  examRealizationID: number;
  examRealization: ExamRealization = {} as ExamRealization;
  addForm: boolean = false;
  addExamRealization: boolean = true;

  constructor(
    private examPeriodService: ExamPeriodService,
    private studyProgramService: StudyProgramService,
    private examRealizationService: ExamRealizationService
  ) {}

  ngOnInit() {
    this.examPeriodService.getExamPeriodsFuture().subscribe(data => {
      this.examPeriods = data;
      this.examPeriodID = this.examPeriods[0].id;

      this.getExamRealizations(
        this.studyProgramID,
        this.examPeriodID,
        this.pageNum,
        this.size
      );
    });

    this.studyProgramService.getAll().subscribe(data => {
      this.studyPrograms = data;
    });
  }

  getExamRealizations(
    studyProgramID: number,
    examPeriodID: number,
    pageNum: number,
    size: number
  ) {
    this.examRealizationService
      .getByStudyProgramAndExamPeriod(
        studyProgramID,
        examPeriodID,
        pageNum,
        size
      )
      .subscribe(data => {
        this.examRealizations = data.body;
        this.totalPages = Array(Number.parseInt(data.headers.get("totalPages")))
          .fill(0)
          .map((x, i) => i);
      });
  }

  nextPage(event) {
    let pageNum = event.target.name;
    document
      .getElementById(this.activePage.toString())
      .classList.remove("active");
    this.activePage = pageNum;
    this.getExamRealizations(
      this.studyProgramID,
      this.examPeriodID,
      pageNum,
      this.size
    );
    document.getElementById(this.activePage.toString()).classList.add("active");
  }

  onChangeStudyProgram(event) {
    this.studyProgramID = event;
    if (this.studyProgramID !== undefined) {
      this.getExamRealizations(
        this.studyProgramID,
        this.examPeriodID,
        this.pageNum,
        this.size
      );
    }
  }

  onChangeExamPeriod(event) {
    this.examPeriodID = event;
    if (this.examPeriodID !== undefined) {
      this.getExamRealizations(
        this.studyProgramID,
        this.examPeriodID,
        this.pageNum,
        this.size
      );
    }
  }

  openAddForm() {
    this.addExamRealization = true;
    this.addForm = true;
    this.examRealization.courseDTO = {} as Course;
    this.examRealization.teacherDTO = {} as Teacher;
    this.examRealization.examPeriodDTO = {} as ExamPeriod;
  }

  closeAddForm(event) {
    this.addForm = event;
    this.examRealization = {} as ExamRealization;
  }

  addNewRow(event) {
    if (this.examRealizations.length < 3) {
      this.examRealizations.push(event);
    } else {
      this.getExamRealizations(
        this.studyProgramID,
        this.examPeriodID,
        this.pageNum,
        this.size
      );
    }
  }

  onEditRow(event) {
    this.examRealization = event;
    let index = this.examRealizations.findIndex(
      item => item.id == this.examRealization.id
    );
    this.examRealizations[index] = this.examRealization;
  }

  onDeleteRow(event) {
    this.examRealizationID = event;
    this.examRealizationService
      .delete(this.examRealizationID)
      .subscribe(data => {
        this.examRealizations = this.examRealizations.filter(
          x => x.id !== this.examRealizationID
        );
        this.getExamRealizations(
          this.studyProgramID,
          this.examPeriodID,
          this.pageNum,
          this.size
        );
      });
  }

  openEdit(event) {
    this.examRealizationID = event;
    let clone = this.examRealizations.find(
      x => x.id === this.examRealizationID
    );
    this.examRealization = JSON.parse(JSON.stringify(clone));
    this.addForm = true;
    this.addExamRealization = false;
  }
}
