import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamRealizationAddEditComponent } from './exam-realization-add-edit.component';

describe('ExamRealizationAddEditComponent', () => {
  let component: ExamRealizationAddEditComponent;
  let fixture: ComponentFixture<ExamRealizationAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamRealizationAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamRealizationAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
