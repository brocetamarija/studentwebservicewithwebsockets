import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { CourseService } from "src/app/services/Course/course.service";
import { TeacherService } from "src/app/services/Teacher/teacher.service";
import { ExamRealizationService } from "src/app/services/ExamRealization/exam-realization.service";
import { ExamRealization } from "src/app/shared/model/ExamRealization";
import { Course } from "src/app/shared/model/Course";
import { Teacher } from "src/app/shared/model/Teacher";
import { ExamPeriodService } from "src/app/services/ExamPeriod/exam-period.service";
import { ExamPeriod } from "src/app/shared/model/ExamPeriod";
import { DatePipe } from "@angular/common";
import * as moment from "moment";

@Component({
  selector: "app-exam-realization-add-edit",
  templateUrl: "./exam-realization-add-edit.component.html",
  styleUrls: ["./exam-realization-add-edit.component.css"]
})
export class ExamRealizationAddEditComponent implements OnInit {
  @Input() examRealization: ExamRealization = {} as ExamRealization;
  @Input() addExamRealization: boolean = true;
  @Input() studyProgramID: number;
  @Output() close = new EventEmitter();
  @Output() newExamRealization = new EventEmitter<ExamRealization>();
  @Output() editExamRealization = new EventEmitter<ExamRealization>();
  courses: Course[];
  teachers: Teacher[];
  examPeriods: ExamPeriod[];
  addForm: boolean;
  startDate: string;
  endDate: string;
  date;
  time;
  dateError: boolean;

  constructor(
    private courseService: CourseService,
    private teacherService: TeacherService,
    private examPeriodService: ExamPeriodService,
    private examRealizationService: ExamRealizationService
  ) {}

  ngOnInit() {
    this.courseService
      .getCoursesByStudyProgram(this.studyProgramID)
      .subscribe(data => {
        this.courses = data;
      });

    this.teacherService.getTeachers().subscribe(data => {
      this.teachers = data;
    });

    this.examPeriodService.getExamPeriodsFuture().subscribe(data => {
      this.examPeriods = data;
    });

    this.date = this.examRealization.date;
    this.time = moment(this.examRealization.date).format("HH:mm");
  }

  closeAddForm() {
    this.addForm = false;
    this.close.emit(this.addForm);
  }

  submit() {
    if (this.addExamRealization) {
      this.examRealization.courseDTO = this.courses.find(
        x => x.id == this.examRealization.courseDTO.id
      );
      this.examRealization.teacherDTO = this.teachers.find(
        x => x.id == this.examRealization.teacherDTO.id
      );
      this.examRealization.examPeriodDTO = this.examPeriods.find(
        x => x.id == this.examRealization.examPeriodDTO.id
      );

      let dateOfExam = this.date + " " + this.time;
      dateOfExam = moment(dateOfExam).format("YYYY-MM-DD HH:mm");

      this.startDate = moment(
        this.examRealization.examPeriodDTO.startDate
      ).format("YYYY-MM-DD HH:mm");
      this.endDate = moment(this.examRealization.examPeriodDTO.endDate).format(
        "YYYY-MM-DD HH:mm"
      );

      if (this.date > this.endDate || this.date < this.startDate) {
        this.dateError = true;
        this.startDate = moment(
          this.examRealization.examPeriodDTO.startDate
        ).format("DD/MM/YYYY");
        this.endDate = moment(
          this.examRealization.examPeriodDTO.endDate
        ).format("DD/MM/YYYY");
        return;
      }

      this.examRealization.date = new Date(dateOfExam);
      this.examRealizationService
        .create(this.examRealization)
        .subscribe(data => {
          this.newExamRealization.emit(data);
          this.closeAddForm();
        });
    } else {
      this.examRealizationService
        .update(this.examRealization)
        .subscribe(data => {
          this.editExamRealization.emit(data);
          this.closeAddForm();
        });
    }
  }
}
