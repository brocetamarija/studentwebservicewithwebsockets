import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ExamRealization } from "src/app/shared/model/ExamRealization";

@Component({
  selector: "tr[app-table-row-exam-realizations]",
  templateUrl: "./table-row-exam-realizations.component.html",
  styleUrls: ["./table-row-exam-realizations.component.css"]
})
export class TableRowExamRealizationsComponent implements OnInit {
  @Input() examRealization: ExamRealization = {} as ExamRealization;
  @Input() i: number;
  @Output() deleteRow = new EventEmitter();
  @Output() editRow = new EventEmitter();
  id: number;
  constructor() {}

  ngOnInit() {}

  delete(event) {
    this.id = +event.target.value;
    this.deleteRow.emit(this.id);
  }

  edit(event) {
    this.id = +event.target.value;
    this.editRow.emit(this.id);
  }
}
