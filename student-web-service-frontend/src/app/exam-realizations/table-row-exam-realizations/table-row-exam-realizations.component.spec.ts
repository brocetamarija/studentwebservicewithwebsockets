import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowExamRealizationsComponent } from './table-row-exam-realizations.component';

describe('TableRowExamRealizationsComponent', () => {
  let component: TableRowExamRealizationsComponent;
  let fixture: ComponentFixture<TableRowExamRealizationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowExamRealizationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowExamRealizationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
