import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowSemestersComponent } from './table-row-semesters.component';

describe('TableRowSemestersComponent', () => {
  let component: TableRowSemestersComponent;
  let fixture: ComponentFixture<TableRowSemestersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowSemestersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowSemestersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
