import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Semester } from "src/app/shared/model/Semester";

@Component({
  selector: "tr[app-table-row-semesters]",
  templateUrl: "./table-row-semesters.component.html",
  styleUrls: ["./table-row-semesters.component.css"]
})
export class TableRowSemestersComponent implements OnInit {
  @Input() semester: Semester = {} as Semester;
  @Input() i: number;
  @Output() deleteRow = new EventEmitter();
  @Output() editRow = new EventEmitter();
  id: number;
  constructor() {}

  ngOnInit() {}

  delete(event) {
    this.id = +event.target.value;
    this.deleteRow.emit(this.id);
  }

  edit(event) {
    this.id = +event.target.value;
    this.editRow.emit(this.id);
  }
}
