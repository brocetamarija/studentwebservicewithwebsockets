import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Semester } from "src/app/shared/model/Semester";
import { SemesterType } from "src/app/shared/model/SemesterType";
import { SemesterService } from "src/app/services/Semester/semester.service";
import { WebSocketApiService } from "src/app/services/WebSocket/web-socket-api.service";
import { DatePipe } from "@angular/common";
import { Message } from "src/app/shared/model/Message";

@Component({
  selector: "app-add-semester",
  templateUrl: "./add-semester.component.html",
  styleUrls: ["./add-semester.component.css"]
})
export class AddSemesterComponent implements OnInit {
  @Input() semester: Semester = {} as Semester;
  @Input() addSemester: boolean;
  @Output() close = new EventEmitter();
  @Output() newSemester = new EventEmitter<Semester>();
  @Output() editSemester = new EventEmitter<Semester>();
  semesterTypes: any[];
  addForm: boolean;
  semesterType = SemesterType.Zimski;
  endDate: Date;
  constructor(
    private semesterService: SemesterService
  ) {}

  ngOnInit() {
    this.semesterTypes = SemesterType.values();
  }

  closeAddForm() {
    this.addForm = false;
    this.close.emit(this.addForm);
  }

  submit() {
    if (this.addSemester) {
      this.semester.semesterType = this.semesterType;
      this.semesterService.create(this.semester).subscribe(data => {
        this.newSemester.emit(data);
        this.closeAddForm();
      });
    } else {
      this.semesterService.update(this.semester).subscribe(data => {
        this.editSemester.emit(data);
        this.closeAddForm();
      });
    }
  }
}
