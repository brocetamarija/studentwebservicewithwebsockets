import { Component, OnInit } from "@angular/core";
import { Semester } from "../shared/model/Semester";
import { SemesterService } from "../services/Semester/semester.service";

@Component({
  selector: "app-semesters",
  templateUrl: "./semesters.component.html",
  styleUrls: ["./semesters.component.css"]
})
export class SemestersComponent implements OnInit {
  semesters: Semester[];
  addSemester: boolean = true;
  addForm: boolean = false;
  semester: Semester = {} as Semester;
  semesterID: number;
  constructor(private semesterService: SemesterService) {}

  ngOnInit() {
    this.getSemesters();
  }

  getSemesters() {
    this.semesterService.getAll().subscribe(data => {
      this.semesters = data;
    });
  }

  openAddForm() {
    this.addSemester = true;
    this.addForm = true;
  }

  closeAddForm(event) {
    this.addForm = event;
    this.semester = {} as Semester;
  }

  addNewRow(event) {
    this.semesters.push(event);
  }

  onEditRow(event) {
    this.semester = event;
    let index = this.semesters.findIndex(item => item.id == this.semester.id);
    this.semesters[index] = this.semester;
  }

  onDeleteRow(event) {
    this.semesterID = event;
    this.semesterService.delete(this.semesterID).subscribe(data => {
      this.semesters = this.semesters.filter(x => x.id !== this.semesterID);
      this.getSemesters();
    });
  }

  openEdit(event) {
    this.semesterID = event;
    let clone = this.semesters.find(x => x.id === this.semesterID);
    this.semester = JSON.parse(JSON.stringify(clone));
    this.addForm = true;
    this.addSemester = false;
  }
}
