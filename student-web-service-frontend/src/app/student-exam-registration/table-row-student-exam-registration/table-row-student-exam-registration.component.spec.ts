import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowStudentExamRegistrationComponent } from './table-row-student-exam-registration.component';

describe('TableRowStudentExamRegistrationComponent', () => {
  let component: TableRowStudentExamRegistrationComponent;
  let fixture: ComponentFixture<TableRowStudentExamRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowStudentExamRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowStudentExamRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
