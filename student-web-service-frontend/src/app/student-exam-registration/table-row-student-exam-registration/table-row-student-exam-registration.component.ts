import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ExamRealization } from "src/app/shared/model/ExamRealization";
import { ExamRealizationService } from "src/app/services/ExamRealization/exam-realization.service";

@Component({
  selector: "tr[app-table-row-student-exam-registration]",
  templateUrl: "./table-row-student-exam-registration.component.html",
  styleUrls: ["./table-row-student-exam-registration.component.css"]
})
export class TableRowStudentExamRegistrationComponent implements OnInit {
  @Input() examRealization: ExamRealization = {} as ExamRealization;
  @Input() i: number;
  @Output() checkedExamRealization = new EventEmitter();
  @Output() isChecked = new EventEmitter();
  examRealizationID: number;

  constructor(private examRealizationService: ExamRealizationService) {}

  ngOnInit() {}

  checkedOnChange(event) {
    this.isChecked.emit(event.target.checked);
    this.examRealizationID = event.target.value;

    this.examRealizationService
      .getByID(this.examRealizationID)
      .subscribe(data => {
        this.checkedExamRealization.emit(data);
      });
  }

  checkDate(date) {
    let d = new Date(date);
    d.setDate(d.getDate() - 3);
    let dateToday = new Date();
    if (dateToday > d) {
      return true;
    }
  }
}
