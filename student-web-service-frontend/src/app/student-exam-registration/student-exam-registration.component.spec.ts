import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentExamRegistrationComponent } from './student-exam-registration.component';

describe('StudentExamRegistrationComponent', () => {
  let component: StudentExamRegistrationComponent;
  let fixture: ComponentFixture<StudentExamRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentExamRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentExamRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
