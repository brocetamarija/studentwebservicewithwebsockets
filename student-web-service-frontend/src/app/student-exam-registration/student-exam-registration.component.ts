import { Component, OnInit } from "@angular/core";
import { ExamRealization } from "../shared/model/ExamRealization";
import { ExamRealizationService } from "../services/ExamRealization/exam-realization.service";
import { ActivatedRoute } from "@angular/router";
import { ExamPeriodService } from "../services/ExamPeriod/exam-period.service";
import { ExamPeriod } from "../shared/model/ExamPeriod";
import { AccountService } from "../services/Account/account.service";
import { StudentService } from "../services/Student/student.service";
import { Student } from "../shared/model/Student";
import { Exam } from "../shared/model/Exam";
import { ExamService } from "../services/Exam/exam.service";
import { PaymentService } from "../services/Payment/payment.service";
import { Payment } from "../shared/model/Payment";
import { Account } from "../shared/model/Account";
import { PaymentPurpose } from "../shared/model/PaymentPurpose";
import { PaymentPurposeService } from "../services/PaymentPurpose/payment-purpose.service";

@Component({
  selector: "app-student-exam-registration",
  templateUrl: "./student-exam-registration.component.html",
  styleUrls: ["./student-exam-registration.component.css"]
})
export class StudentExamRegistrationComponent implements OnInit {
  private readonly paymentPurposeName = "PRIJAVA_ISPITA";
  examRealizations: ExamRealization[];
  studentID: number;
  examPeriods: ExamPeriod[];
  examPeriodID: number;
  examRealization: ExamRealization = {} as ExamRealization;
  totalCost: number = 0;
  iDs: number[] = [];
  isChecked: boolean = false;
  currentBalance: number;
  isEnoughMoney: boolean = false;
  isCheckedExam: boolean = false;
  examRealizationID: number;
  student: Student = {} as Student;
  exam: Exam = {} as Exam;
  payment: Payment = {} as Payment;
  account: Account = {} as Account;
  paymentPurpose: PaymentPurpose = {} as PaymentPurpose;
  isDisabled: boolean;

  constructor(
    private examRealizationService: ExamRealizationService,
    private route: ActivatedRoute,
    private examPeriodService: ExamPeriodService,
    private accountService: AccountService,
    private studentService: StudentService,
    private examService: ExamService,
    private paymentService: PaymentService,
    private paymentPurposeService: PaymentPurposeService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studentID = +params["id"];
    });

    this.examPeriodService.getExamPeriodsFuture().subscribe(data => {
      this.examPeriods = data;
      this.examPeriodID = data[0].id;
      this.getExamRealizationForRegistration(this.studentID, this.examPeriodID);
    });

    this.getBalance(this.studentID);
    this.getStudent(this.studentID);
    this.getPaymentPurpose(this.paymentPurposeName);
  }

  getExamRealizationForRegistration(studentID: number, examPeriodID: number) {
    this.examRealizationService
      .getExamRealizationsForRegistration(studentID, examPeriodID)
      .subscribe(data => {
        this.examRealizations = data;
      });
  }

  onChange(event) {
    this.examPeriodID = event;
    this.getExamRealizationForRegistration(this.studentID, this.examPeriodID);
  }

  countTotalCost(event) {
    this.examRealization = event;
    if (this.isChecked) {
      this.iDs.push(this.examRealization.id);
      this.totalCost += this.examRealization.examPrice;
    } else {
      this.iDs.pop();
      this.totalCost -= this.examRealization.examPrice;
    }
  }

  check(event) {
    this.isChecked = event;
  }

  add() {
    if (this.iDs.length === 0) {
      this.isCheckedExam = true;
      this.isDisabled = true;
    } else if (
      this.currentBalance < this.totalCost ||
      this.currentBalance === 0
    ) {
      this.isEnoughMoney = true;
      this.isDisabled = true;
    } else {
      for (let er of this.iDs) {
        this.examRealizationID = er;

        this.examRealizationService
          .getByID(this.examRealizationID)
          .subscribe(data => {
            this.examRealization = data;
            this.exam.examRealizationDTO = this.examRealization;
            this.exam.studentDTO = this.student;
            this.examService.examRegistration(this.exam).subscribe(data => {});
          });
        this.examRealizations = this.examRealizations.filter(
          x => x.id !== this.examRealizationID
        );
      }

      this.addPayment(this.student, this.totalCost);
      this.updateBalance(this.account, this.totalCost);
    }
  }

  addPayment(student: Student, amount: number) {
    this.payment.studentDTO = student;
    this.payment.paymentPurposeDTO = this.paymentPurpose;
    this.payment.amount = amount;
    this.paymentService
      .addPaymentForExamRegistration(this.payment)
      .subscribe(data => {});
  }

  getBalance(studentID: number) {
    this.accountService.getByStudent(studentID).subscribe(data => {
      this.currentBalance = data.currentAccountBalance;
      this.account = data;
    });
  }

  getStudent(id: number) {
    this.studentService.getByID(id).subscribe(data => {
      this.student = data;
    });
  }

  updateBalance(account: Account, amount: number) {
    this.accountService
      .updateCurrentAccountBalanceSubtract(account, amount)
      .subscribe(data => {
        this.getBalance(this.studentID);
        this.totalCost = 0;
      });
  }

  getPaymentPurpose(name: string) {
    this.paymentPurposeService.getPaymentPurposeByName(name).subscribe(data => {
      this.paymentPurpose = data;
    });
  }

  closeAlert() {
    this.isEnoughMoney = false;
    this.isCheckedExam = false;
    this.isDisabled = false;
  }
}
