import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherExamRealizationsComponent } from './teacher-exam-realizations.component';

describe('TeacherExamRealizationsComponent', () => {
  let component: TeacherExamRealizationsComponent;
  let fixture: ComponentFixture<TeacherExamRealizationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherExamRealizationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherExamRealizationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
