import { Component, OnInit } from "@angular/core";
import { ExamRealizationService } from "../services/ExamRealization/exam-realization.service";
import { ActivatedRoute } from "@angular/router";
import { ExamPeriodService } from "../services/ExamPeriod/exam-period.service";
import { ExamPeriod } from "../shared/model/ExamPeriod";
import { ExamRealization } from "../shared/model/ExamRealization";

@Component({
  selector: "app-teacher-exam-realizations",
  templateUrl: "./teacher-exam-realizations.component.html",
  styleUrls: ["./teacher-exam-realizations.component.css"]
})
export class TeacherExamRealizationsComponent implements OnInit {
  teacherID: number;
  examPeriods: ExamPeriod[];
  examPeriodID: number;
  examRealizations: ExamRealization[];
  param: string;

  constructor(
    private examRealizationService: ExamRealizationService,
    private route: ActivatedRoute,
    private examPeriodService: ExamPeriodService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.param = params["param"];
    });

    this.route.params.subscribe(params => {
      this.teacherID = +params["id"];
    });

    if (this.param === "enter") {
      this.getFutureExamPeriods();
    }
    if (this.param === "results") {
      this.getAllExamPeriods();
    }
  }

  getAllExamPeriods() {
    this.examPeriodService.getExamPeriods().subscribe(data => {
      this.examPeriods = data;
      this.examPeriodID = data[0].id;
      this.getExamRealizations(this.teacherID, this.examPeriodID);
    });
  }

  getFutureExamPeriods() {
    this.examPeriodService.getExamPeriodsFuture().subscribe(data => {
      this.examPeriods = data;
      this.examPeriodID = data[0].id;
      this.getExamRealizations(this.teacherID, this.examPeriodID);
    });
  }

  getExamRealizations(teacherID, examPeriodID) {
    this.examRealizationService
      .getExamRealizationsForTeacher(teacherID, examPeriodID)
      .subscribe(data => {
        this.examRealizations = data;
      });
  }

  onChange(event) {
    this.examPeriodID = event;
    this.getExamRealizations(this.teacherID, this.examPeriodID);
  }
}
