import { Component, OnInit, Input, Output } from "@angular/core";
import { ExamRealization } from "src/app/shared/model/ExamRealization";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "tr[app-table-row-teacher-exam-realizations]",
  templateUrl: "./table-row-teacher-exam-realizations.component.html",
  styleUrls: ["./table-row-teacher-exam-realizations.component.css"]
})
export class TableRowTeacherExamRealizationsComponent implements OnInit {
  @Input() examRealization: ExamRealization = {} as ExamRealization;
  @Input() i: number;
  @Input() param: string;
  examRealizationID: number;

  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {}

  enterResults(event) {
    this.examRealizationID = event.target.value;
    this.router.navigate(["/enter-results", this.examRealizationID]);
  }

  results(event) {
    this.examRealizationID = event.target.value;
    this.router.navigate(["/teacher-exam-results", this.examRealizationID]);
  }
}
