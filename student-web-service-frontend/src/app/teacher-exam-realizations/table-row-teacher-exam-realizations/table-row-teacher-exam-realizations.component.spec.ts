import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowTeacherExamRealizationsComponent } from './table-row-teacher-exam-realizations.component';

describe('TableRowTeacherExamRealizationsComponent', () => {
  let component: TableRowTeacherExamRealizationsComponent;
  let fixture: ComponentFixture<TableRowTeacherExamRealizationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowTeacherExamRealizationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowTeacherExamRealizationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
