import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'replaceString'})
export class ReplaceString implements PipeTransform {
    transform(input: string) {
        input = input.replace(/_/g, " ");
        return input[0].toUpperCase() + input.substr(1).toLowerCase();
      }
}