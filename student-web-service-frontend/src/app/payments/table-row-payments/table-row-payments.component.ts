import { Component, OnInit, Input } from "@angular/core";
import { Payment } from "src/app/shared/model/Payment";

@Component({
  selector: "tr[app-table-row-payments]",
  templateUrl: "./table-row-payments.component.html",
  styleUrls: ["./table-row-payments.component.css"]
})
export class TableRowPaymentsComponent implements OnInit {
  @Input() payment: Payment = {} as Payment;
  @Input() i: number;
  constructor() {}

  ngOnInit() {}
}
