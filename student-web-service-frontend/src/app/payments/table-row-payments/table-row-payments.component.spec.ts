import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowPaymentsComponent } from './table-row-payments.component';

describe('TableRowPaymentsComponent', () => {
  let component: TableRowPaymentsComponent;
  let fixture: ComponentFixture<TableRowPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
