import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { Payment } from "src/app/shared/model/Payment";
import { PaymentService } from "src/app/services/Payment/payment.service";
import { PaymentPurpose } from "src/app/shared/model/PaymentPurpose";
import { PaymentPurposeService } from "src/app/services/PaymentPurpose/payment-purpose.service";
import { Student } from "src/app/shared/model/Student";
import { AccountService } from "src/app/services/Account/account.service";
import { Account } from "src/app/shared/model/Account";

@Component({
  selector: "app-payment-add-edit",
  templateUrl: "./payment-add-edit.component.html",
  styleUrls: ["./payment-add-edit.component.css"]
})
export class PaymentAddEditComponent implements OnInit {
  @Output() close = new EventEmitter();
  @Output() newPayment = new EventEmitter<Payment>();
  @Input() payment: Payment = {} as Payment;
  @Input() student: Student = {} as Student;

  addForm: boolean;
  paymentPurposes: PaymentPurpose[];

  constructor(
    private paymentService: PaymentService,
    private paymentPurposeService: PaymentPurposeService
  ) {}

  ngOnInit() {
    this.paymentPurposeService.getPaymentPurposes().subscribe(data => {
      this.paymentPurposes = data;
    });
  }

  closeAddForm() {
    this.addForm = false;
    this.close.emit(this.addForm);
  }

  submit() {
    this.payment.paymentPurposeDTO = this.paymentPurposes.find(
      x => x.id == this.payment.paymentPurposeDTO.id
    );
    this.payment.studentDTO = this.student;
    this.paymentService.create(this.payment).subscribe(data => {
      this.newPayment.emit(data);

      this.closeAddForm();
    });
  }
}
