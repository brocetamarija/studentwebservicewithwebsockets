import { Component, OnInit } from "@angular/core";
import { PaymentService } from "../services/Payment/payment.service";
import { Payment } from "../shared/model/Payment";
import { ActivatedRoute } from "@angular/router";
import { Student } from "../shared/model/Student";
import { PaymentPurpose } from "../shared/model/PaymentPurpose";
import { AccountService } from "../services/Account/account.service";
import { Account } from "../shared/model/Account";
import { StudentService } from "../services/Student/student.service";
import { SemesterStudentService } from "../services/SemesterStudent/semester-student.service";
import { SemesterStudent } from "../shared/model/SemesterStudent";
import { PaymentPurposeService } from "../services/PaymentPurpose/payment-purpose.service";

@Component({
  selector: "app-payments",
  templateUrl: "./payments.component.html",
  styleUrls: ["./payments.component.css"]
})
export class PaymentsComponent implements OnInit {
  private readonly paymentPurposeName = "OVERA_SEMESTRA";
  payments: Payment[];
  studentID: number;
  pageNum: number = 0;
  size: number = 3;
  totalPages: number[];
  activePage: number = 0;
  isAdmin: boolean;
  addForm: boolean = false;
  payment: Payment = {} as Payment;
  account: Account = {} as Account;
  student: Student = {} as Student;
  semesterStudent: SemesterStudent = {} as SemesterStudent;
  isCertified: boolean = false;
  paymentPurpose: PaymentPurpose = {} as PaymentPurpose;
  isEnoughMoney: boolean;

  constructor(
    private paymentService: PaymentService,
    private route: ActivatedRoute,
    private accountService: AccountService,
    private studentService: StudentService,
    private semesterStudentService: SemesterStudentService,
    private paymentPurposeService: PaymentPurposeService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studentID = params["id"];
    });

    this.getPaymentsByStudent(this.studentID, this.pageNum, this.size);
    this.getAccountByStudent(this.studentID);
    this.getStudent(this.studentID);
    this.getSemesterStudent(this.studentID);
    this.getPaymentPurpose(this.paymentPurposeName);
  }

  getPaymentsByStudent(id: number, pageNum: number, size: number) {
    this.paymentService
      .getPaymentsByStudent(this.studentID, pageNum, size)
      .subscribe(data => {
        this.payments = data.body;
        this.totalPages = Array(Number.parseInt(data.headers.get("totalPages")))
          .fill(0)
          .map((x, i) => i);
      });
  }

  nextPage(event) {
    let pageNum = event.target.name;
    document
      .getElementById(this.activePage.toString())
      .classList.remove("active");
    this.activePage = pageNum;
    this.getPaymentsByStudent(this.studentID, pageNum, this.size);
    document.getElementById(this.activePage.toString()).classList.add("active");
  }

  getAccountByStudent(id: number) {
    this.accountService.getByStudent(id).subscribe(data => {
      this.account = data;
    });
  }

  check(event) {
    this.isAdmin = event;
  }

  openAddForm() {
    this.addForm = true;
    this.payment.studentDTO = {} as Student;
    this.payment.paymentPurposeDTO = {} as PaymentPurpose;
  }

  closeAddForm(event) {
    this.addForm = event;
    this.payment = {} as Payment;
  }

  getStudent(id) {
    this.studentService.getByID(id).subscribe(data => {
      this.student = data;
    });
  }

  addNewRow(event) {
    if (this.payments.length < 3) {
      this.payments.push(event);
    } else {
      this.getPaymentsByStudent(this.studentID, this.pageNum, this.size);
    }
    this.updateCurrentBalanceAdd(this.account, event.amount);
  }

  getSemesterStudent(id: number) {
    this.semesterStudentService.getSemesterStudent(id).subscribe(data => {
      this.semesterStudent = data;
      if (!data.certified && this.checkDate(data.semesterDTO.endDate)) {
        this.isCertified = false;
      }
    },
    err => {
      if(err.status == 404){
        this.isCertified = true;
      }
    });
  }

  checkDate(date) {
    let d = new Date(date);
    d.setDate(d.getDate() - 14);
    let dateToday = new Date();
    if (dateToday > d) {
      return true;
    }
  }

  onClick() {
    if (this.account.currentAccountBalance >= this.semesterStudent.semesterCertificationPrice) {
      this.semesterStudentService
        .semesterCertification(this.semesterStudent)
        .subscribe(data => {
          this.isCertified = data.certified;
          this.addPayment(data.semesterCertificationPrice);
          this.updateCurrentBalanceSubstract(
            this.account,
            data.semesterCertificationPrice
          );
        });
    } else {
      this.isEnoughMoney = true;
    }
  }

  addPayment(amount: number) {
    this.payment.studentDTO = this.student;
    this.payment.paymentPurposeDTO = this.paymentPurpose;
    this.payment.amount = amount;
    this.paymentService
      .addPaymentForExamRegistration(this.payment)
      .subscribe(data => {});
  }

  getPaymentPurpose(name: string) {
    this.paymentPurposeService.getPaymentPurposeByName(name).subscribe(data => {
      this.paymentPurpose = data;
    });
  }

  closeAlert() {
    this.isEnoughMoney = false;
  }

  updateCurrentBalanceSubstract(account: Account, amount: number) {
    this.accountService
      .updateCurrentAccountBalanceSubtract(account, amount)
      .subscribe(data => {
        this.getAccountByStudent(this.studentID);
      });
  }

  updateCurrentBalanceAdd(account: Account, amount: number) {
    this.accountService
      .updateCurrentAccountBalanceAdd(account, amount)
      .subscribe(data => {
        this.getAccountByStudent(this.studentID);
      });
  }
}
