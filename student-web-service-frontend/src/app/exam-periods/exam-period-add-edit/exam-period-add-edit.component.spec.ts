import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamPeriodAddEditComponent } from './exam-period-add-edit.component';

describe('ExamPeriodAddEditComponent', () => {
  let component: ExamPeriodAddEditComponent;
  let fixture: ComponentFixture<ExamPeriodAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamPeriodAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamPeriodAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
