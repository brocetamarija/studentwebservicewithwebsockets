import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { ExamPeriod } from "src/app/shared/model/ExamPeriod";
import { ExamPeriodService } from "src/app/services/ExamPeriod/exam-period.service";
import { WebSocketApiService } from "src/app/services/WebSocket/web-socket-api.service";

@Component({
  selector: "app-exam-period-add-edit",
  templateUrl: "./exam-period-add-edit.component.html",
  styleUrls: ["./exam-period-add-edit.component.css"]
})
export class ExamPeriodAddEditComponent implements OnInit {
  @Output() close = new EventEmitter();
  @Output() newExamPeriod = new EventEmitter<ExamPeriod>();
  @Output() editExamPeriod = new EventEmitter<ExamPeriod>();
  @Input() examPeriod: ExamPeriod = {} as ExamPeriod;
  @Input() addExamPeriod: boolean = true;
  addForm: boolean;
  constructor(
    private examPeriodService: ExamPeriodService,
    private wsApi: WebSocketApiService
  ) {}

  ngOnInit() {}

  closeAddForm() {
    this.addForm = false;
    this.close.emit(this.addForm);
  }

  submit() {
    if (this.addExamPeriod) {
      this.examPeriodService.create(this.examPeriod).subscribe(data => {
        this.newExamPeriod.emit(data);
        this.closeAddForm();
      });
    } else {
      this.examPeriodService.update(this.examPeriod).subscribe(data => {
        this.editExamPeriod.emit(data);
        this.closeAddForm();
      });
    }
  }
}
