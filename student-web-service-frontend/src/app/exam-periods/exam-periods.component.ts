import { Component, OnInit } from "@angular/core";
import { ExamPeriodService } from "../services/ExamPeriod/exam-period.service";
import { ExamPeriod } from "../shared/model/ExamPeriod";

@Component({
  selector: "app-exam-periods",
  templateUrl: "./exam-periods.component.html",
  styleUrls: ["./exam-periods.component.css"]
})
export class ExamPeriodsComponent implements OnInit {
  examPeriods: ExamPeriod[];
  pageNum: number = 0;
  size: number = 3;
  totalPages: number[];
  activePage: number = 0;
  examPeriodID: number;
  examPeriod: ExamPeriod = {} as ExamPeriod;
  addForm: boolean = false;
  addExamPeriod: boolean = true;

  constructor(private examPeriodService: ExamPeriodService) {}

  ngOnInit() {
    this.getExamPeriods(this.pageNum, this.size);
  }

  getExamPeriods(pageNum: number, size: number) {
    this.examPeriodService
      .getExamPeriodsPageable(pageNum, size)
      .subscribe(data => {
        this.examPeriods = data.body;
        this.totalPages = Array(Number.parseInt(data.headers.get("totalPages")))
          .fill(0)
          .map((x, i) => i);
      });
  }

  nextPage(event) {
    let pageNum = event.target.name;
    document
      .getElementById(this.activePage.toString())
      .classList.remove("active");
    this.activePage = pageNum;
    this.getExamPeriods(pageNum, this.size);
    document.getElementById(this.activePage.toString()).classList.add("active");
  }

  openAddForm() {
    this.addExamPeriod = true;
    this.addForm = true;
  }

  closeAddForm(event) {
    this.addForm = event;
    this.examPeriod = {} as ExamPeriod;
  }

  addNewRow(event) {
    if (this.examPeriods.length < 3) {
      this.examPeriods.push(event);
    } else {
      this.getExamPeriods(this.pageNum, this.size);
    }
  }

  onEditRow(event) {
    this.examPeriod = event;
    let index = this.examPeriods.findIndex(
      item => item.id == this.examPeriod.id
    );
    this.examPeriods[index] = this.examPeriod;
  }

  onDeleteRow(event) {
    this.examPeriodID = event;
    this.examPeriodService.delete(this.examPeriodID).subscribe(data => {
      this.examPeriods = this.examPeriods.filter(
        x => x.id !== this.examPeriodID
      );
      this.getExamPeriods(this.pageNum, this.size);
    });
  }

  openEdit(event) {
    this.examPeriodID = event;
    let clone = this.examPeriods.find(x => x.id === this.examPeriodID);
    this.examPeriod = JSON.parse(JSON.stringify(clone));
    this.addForm = true;
    this.addExamPeriod = false;
  }
}
