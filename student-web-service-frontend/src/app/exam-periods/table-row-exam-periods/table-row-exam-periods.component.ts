import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ExamPeriod } from "src/app/shared/model/ExamPeriod";

@Component({
  selector: "tr[app-table-row-exam-periods]",
  templateUrl: "./table-row-exam-periods.component.html",
  styleUrls: ["./table-row-exam-periods.component.css"]
})
export class TableRowExamPeriodsComponent implements OnInit {
  @Input() examPeriod: ExamPeriod = {} as ExamPeriod;
  @Input() i: number;
  @Output() deleteRow = new EventEmitter();
  @Output() editRow = new EventEmitter();
  id: number;

  constructor() {}

  ngOnInit() {}

  delete(event) {
    this.id = +event.target.value;
    this.deleteRow.emit(this.id);
  }

  edit(event) {
    this.id = +event.target.value;
    this.editRow.emit(this.id);
  }
}
