import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowExamPeriodsComponent } from './table-row-exam-periods.component';

describe('TableRowExamPeriodsComponent', () => {
  let component: TableRowExamPeriodsComponent;
  let fixture: ComponentFixture<TableRowExamPeriodsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowExamPeriodsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowExamPeriodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
