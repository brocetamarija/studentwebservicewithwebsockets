import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowTypeOfStudiesComponent } from './table-row-type-of-studies.component';

describe('TableRowTypeOfStudiesComponent', () => {
  let component: TableRowTypeOfStudiesComponent;
  let fixture: ComponentFixture<TableRowTypeOfStudiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowTypeOfStudiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowTypeOfStudiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
