import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { TypeOfStudy } from "src/app/shared/model/TypeOfStudy";

@Component({
  selector: "tr[app-table-row-type-of-studies]",
  templateUrl: "./table-row-type-of-studies.component.html",
  styleUrls: ["./table-row-type-of-studies.component.css"]
})
export class TableRowTypeOfStudiesComponent implements OnInit {
  @Input() studyType: TypeOfStudy = {} as TypeOfStudy;
  @Input() i: number;
  @Output() deleteRow = new EventEmitter();
  @Output() editRow = new EventEmitter();
  id: number;
  constructor() {}

  ngOnInit() {}

  delete(event) {
    this.id = +event.target.value;
    this.deleteRow.emit(this.id);
  }

  edit(event) {
    this.id = +event.target.value;
    this.editRow.emit(this.id);
  }
}
