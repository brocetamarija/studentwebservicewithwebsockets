import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeOfStudiesComponent } from './type-of-studies.component';

describe('TypeOfStudiesComponent', () => {
  let component: TypeOfStudiesComponent;
  let fixture: ComponentFixture<TypeOfStudiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeOfStudiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeOfStudiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
