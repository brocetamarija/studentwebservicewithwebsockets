import { Component, OnInit } from "@angular/core";
import { TypeOfStudyService } from "../services/TypeOfStudy/type-of-study.service";
import { TypeOfStudy } from "../shared/model/TypeOfStudy";

@Component({
  selector: "app-type-of-studies",
  templateUrl: "./type-of-studies.component.html",
  styleUrls: ["./type-of-studies.component.css"]
})
export class TypeOfStudiesComponent implements OnInit {
  studyTypes: TypeOfStudy[];
  totalPages: number[];
  activePage: number = 0;
  addStudyType: boolean = true;
  addForm: boolean = false;
  studyType: TypeOfStudy = {} as TypeOfStudy;
  studyTypeID: number;
  pageNum: number = 0;
  size: number = 3;
  constructor(private typeOfStudyService: TypeOfStudyService) {}

  ngOnInit() {
    this.getStudyTypes(this.pageNum, this.size);
  }

  getStudyTypes(pageNum: number, size: number) {
    this.typeOfStudyService.getAllPageable(pageNum, size).subscribe(data => {
      this.studyTypes = data.body;
      this.totalPages = Array(Number.parseInt(data.headers.get("totalPages")))
        .fill(0)
        .map((x, i) => i);
    });
  }

  nextPage(event) {
    let pageNum = event.target.name;
    document
      .getElementById(this.activePage.toString())
      .classList.remove("active");
    this.activePage = pageNum;
    this.getStudyTypes(pageNum, this.size);
    document.getElementById(this.activePage.toString()).classList.add("active");
  }

  openAddForm() {
    this.addStudyType = true;
    this.addForm = true;
  }

  addNewRow(event) {
    if (this.studyTypes.length < 3) {
      this.studyTypes.push(event);
    } else {
      this.getStudyTypes(this.pageNum, this.size);
    }
  }

  closeAddForm(event) {
    this.addForm = event;
    this.studyType = {} as TypeOfStudy;
  }

  onEditRow(event) {
    this.studyType = event;
    let index = this.studyTypes.findIndex(item => item.id == this.studyType.id);
    this.studyTypes[index] = this.studyType;
  }

  onDeleteRow(event) {
    this.studyTypeID = event;
    this.typeOfStudyService.delete(this.studyTypeID).subscribe(data => {
      this.studyTypes = this.studyTypes.filter(x => x.id !== this.studyTypeID);
      this.getStudyTypes(this.pageNum, this.size);
    });
  }

  openEdit(event) {
    this.studyTypeID = event;
    let clone = this.studyTypes.find(x => x.id === this.studyTypeID);
    this.studyType = JSON.parse(JSON.stringify(clone));
    this.addForm = true;
    this.addStudyType = false;
  }
}
