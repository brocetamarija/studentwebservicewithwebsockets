import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { TypeOfStudy } from "src/app/shared/model/TypeOfStudy";
import { TypeOfStudyService } from "src/app/services/TypeOfStudy/type-of-study.service";

@Component({
  selector: "app-type-of-studies-add-edit",
  templateUrl: "./type-of-studies-add-edit.component.html",
  styleUrls: ["./type-of-studies-add-edit.component.css"]
})
export class TypeOfStudiesAddEditComponent implements OnInit {
  @Output() close = new EventEmitter();
  @Output() newStudyType = new EventEmitter<TypeOfStudy>();
  @Output() editStudyType = new EventEmitter<TypeOfStudy>();
  @Input() studyType: TypeOfStudy = {} as TypeOfStudy;
  @Input() addStudyType: boolean = true;
  addForm: boolean;
  constructor(private typeOfStudyService: TypeOfStudyService) {}

  ngOnInit() {}

  closeAddForm() {
    this.addForm = false;
    this.close.emit(this.addForm);
  }

  submit() {
    if (this.addStudyType) {
      this.typeOfStudyService.create(this.studyType).subscribe(data => {
        this.newStudyType.emit(data);
        this.closeAddForm();
      });
    } else {
      this.typeOfStudyService.update(this.studyType).subscribe(data => {
        this.editStudyType.emit(data);
        this.closeAddForm();
      });
    }
  }
}
