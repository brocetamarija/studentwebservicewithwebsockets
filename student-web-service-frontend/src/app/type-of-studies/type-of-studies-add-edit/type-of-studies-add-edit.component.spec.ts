import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeOfStudiesAddEditComponent } from './type-of-studies-add-edit.component';

describe('TypeOfStudiesAddEditComponent', () => {
  let component: TypeOfStudiesAddEditComponent;
  let fixture: ComponentFixture<TypeOfStudiesAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeOfStudiesAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeOfStudiesAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
