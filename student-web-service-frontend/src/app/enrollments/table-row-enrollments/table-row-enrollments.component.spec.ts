import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowEnrollmentsComponent } from './table-row-enrollments.component';

describe('TableRowEnrollmentsComponent', () => {
  let component: TableRowEnrollmentsComponent;
  let fixture: ComponentFixture<TableRowEnrollmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowEnrollmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowEnrollmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
