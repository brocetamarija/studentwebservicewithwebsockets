import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Enrollment } from "src/app/shared/model/Enrollment";

@Component({
  selector: "tr[app-table-row-enrollments]",
  templateUrl: "./table-row-enrollments.component.html",
  styleUrls: ["./table-row-enrollments.component.css"]
})
export class TableRowEnrollmentsComponent implements OnInit {
  @Input() enrollment: Enrollment = {} as Enrollment;
  @Input() i: number;
  @Output() deleteRow = new EventEmitter();
  @Output() editRow = new EventEmitter();
  id: number;
  constructor() {}

  ngOnInit() {}

  delete(event) {
    this.id = +event.target.value;
    this.deleteRow.emit(this.id);
  }
}
