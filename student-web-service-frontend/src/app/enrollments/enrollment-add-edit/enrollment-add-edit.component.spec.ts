import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrollmentAddEditComponent } from './enrollment-add-edit.component';

describe('EnrollmentAddEditComponent', () => {
  let component: EnrollmentAddEditComponent;
  let fixture: ComponentFixture<EnrollmentAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrollmentAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrollmentAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
