import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { CourseService } from "src/app/services/Course/course.service";
import { Course } from "src/app/shared/model/Course";
import { Student } from "src/app/shared/model/Student";
import { Enrollment } from "src/app/shared/model/Enrollment";
import { EnrollmentService } from "src/app/services/Enrollment/enrollment.service";

@Component({
  selector: "app-enrollment-add-edit",
  templateUrl: "./enrollment-add-edit.component.html",
  styleUrls: ["./enrollment-add-edit.component.css"]
})
export class EnrollmentAddEditComponent implements OnInit {
  @Input() student: Student = {} as Student;
  @Input() enrollment: Enrollment = {} as Enrollment;
  @Output() newEnrollment = new EventEmitter<Enrollment>();
  @Output() close = new EventEmitter();
  addForm: boolean;
  courses: Course[];
  selectedItems = [];
  constructor(
    private courseService: CourseService,
    public enrollmentService: EnrollmentService
  ) {}

  ngOnInit() {
    this.getCourseByStudyProgramAndYearOfStudy(
      this.student.studyProgramDTO.id,
      this.student.yearOfStudy
    );
  }

  getCourseByStudyProgramAndYearOfStudy(
    studyProgramId: number,
    yearId: number
  ) {
    this.courseService
      .getCourseByStudyProgramAndStudyYear(studyProgramId, yearId)
      .subscribe(data => {
        this.courses = data;
      });
  }

  closeAddForm() {
    this.addForm = false;
    this.close.emit(this.addForm);
  }

  submit() {
    this.selectedItems.forEach(element => {
      this.enrollment.courseDTO = element;
      this.enrollment.studentDTO = this.student;
      this.enrollmentService.create(this.enrollment).subscribe(data => {
        this.newEnrollment.emit(data);
        this.closeAddForm();
      });
    });
  }
}
