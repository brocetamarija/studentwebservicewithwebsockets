import { Component, OnInit } from "@angular/core";
import { EnrollmentService } from "../services/Enrollment/enrollment.service";
import { Enrollment } from "../shared/model/Enrollment";
import { ActivatedRoute } from "@angular/router";
import { StudentService } from "../services/Student/student.service";
import { Student } from "../shared/model/Student";
import { Course } from "../shared/model/Course";

@Component({
  selector: "app-enrollments",
  templateUrl: "./enrollments.component.html",
  styleUrls: ["./enrollments.component.css"]
})
export class EnrollmentsComponent implements OnInit {
  pageNum: number = 0;
  size: number = 3;
  totalPages: number[];
  activePage: number = 0;
  enrollments: Enrollment[];
  enrollmentID: number;
  studentID: number;
  student: Student = {} as Student;
  enrollment: Enrollment = {} as Enrollment;
  addForm: boolean = false;

  constructor(
    private enrollmentService: EnrollmentService,
    private route: ActivatedRoute,
    private studentService: StudentService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studentID = params["id"];
    });

    this.getEnrollments(this.studentID, this.pageNum, this.size);
    this.getStudentByID(this.studentID);
  }

  getEnrollments(id: number, pageNum: number, size: number) {
    this.enrollmentService.getEnrollments(id, pageNum, size).subscribe(data => {
      this.enrollments = data.body;
      this.totalPages = Array(Number.parseInt(data.headers.get("totalPages")))
        .fill(0)
        .map((x, i) => i);
    });
  }

  nextPage(event) {
    let pageNum = event.target.name;
    document
      .getElementById(this.activePage.toString())
      .classList.remove("active");
    this.activePage = pageNum;
    this.getEnrollments(this.studentID, pageNum, this.size);
    document.getElementById(this.activePage.toString()).classList.add("active");
  }

  getStudentByID(id: number) {
    this.studentService.getByID(id).subscribe(data => {
      this.student = data;
    });
  }

  openAddForm() {
    this.addForm = true;
    this.enrollment.courseDTO = {} as Course;
    this.enrollment.studentDTO = {} as Student;
  }

  closeAddForm(event) {
    this.addForm = event;
    this.enrollment = {} as Enrollment;
  }

  addNewRow(event) {
    if (this.enrollments.length < 3) {
      this.enrollments.push(event);
    } else {
      this.getEnrollments(this.studentID, this.pageNum, this.size);
    }
  }

  onDeleteRow(event) {
    this.enrollmentID = event;
    this.enrollmentService.delete(this.enrollmentID).subscribe(data => {
      this.enrollments = this.enrollments.filter(
        x => x.id !== this.enrollmentID
      );
      this.getEnrollments(this.studentID, this.pageNum, this.size);
    });
  }
}
