import { Component, OnInit, Input } from "@angular/core";
import { AuthenticationService } from "../services/Login/Authentication/authentication.service";
import { User } from "../shared/model/User";
import { Student } from "../shared/model/Student";
import { Teacher } from "../shared/model/Teacher";
import { StudentService } from "../services/Student/student.service";
import { TeacherService } from "../services/Teacher/teacher.service";
import { UserService } from "../services/User/user.service";
import { WebSocketApiService } from '../services/WebSocket/web-socket-api.service';

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  imgPath = "assets/images/grb00.gif";
  logged;
  loggedRole;
  isAdmin: boolean = false;
  isStudent: boolean = false;
  isTeacher: boolean = false;
  user: User = {} as User;
  student: Student = {} as Student;
  teacher: Teacher = {} as Teacher;
  message:string;
  isMessage: boolean = false;
  students: Student[];
  isResults: boolean = false;
  isCertification: boolean = false;


  constructor(
    private authenticationService: AuthenticationService,
    private studentService: StudentService,
    private teacherService: TeacherService,
    private userService: UserService,
    private wsService:WebSocketApiService
  ) {}

  ngOnInit() {
    this.logged = this.authenticationService.getCurrentUser();
    this.loggedRole = this.logged.roles[0];
    if (this.loggedRole == "ROLE_STUDENT") {
      this.studentService
        .getByUsername(this.logged.username)
        .subscribe(data => {
          this.student = data;
          this.isStudent = true;
        });
    } else if (this.loggedRole == "ROLE_TEACHER") {
      this.teacherService
        .getByUsername(this.logged.username)
        .subscribe(data => {
          this.teacher = data;
          this.isTeacher = true;
        });
    } else {
      this.userService.getByUsername(this.logged.username).subscribe(data => {
        this.user = data;
        this.isAdmin = true;
      });
    }
    this.sendMessageToAllStudents();
    this.recieveMessageResults();
  }

  sendMessageToAllStudents(){
    this.studentService.getAll().subscribe(data => {
      this.students = data;
      this.students.forEach(student => {
        if(student.userDTO.username === this.logged.username){
          this.receiveMessageCert();
        }
      });
    });
  }

  receiveMessageCert(){
    let stompClient = this.wsService.connect();
    stompClient.connect({}, frame => {
      stompClient.subscribe('/topic/note',notifications => {
         this.message = JSON.parse(notifications.body).message;
         this.isMessage = true;
         this.isCertification = true;
      });
    });
  }

  recieveMessageResults(){
    let stompClient = this.wsService.connect();
    stompClient.connect({}, frame => {
      stompClient.subscribe('/topic/exam-results',notifications => {
         let msg = JSON.parse(notifications.body).message;
         let id = +JSON.parse(notifications.body).id;
         this.studentService.getStudentWhoRegisterExam(id).subscribe(data => {
          this.students = data;
          this.students.forEach(student => {
            if(student.userDTO.username === this.logged.username){
              this.message = msg;
              this.isMessage = true;
              this.isResults = true;
            }
          });
         });   
      });
    });
  }
}
