import { Component, OnInit } from "@angular/core";
import { ExamPeriod } from "../shared/model/ExamPeriod";
import { ExamPeriodService } from "../services/ExamPeriod/exam-period.service";
import { ActivatedRoute } from "@angular/router";
import { ExamService } from "../services/Exam/exam.service";
import { Exam } from "../shared/model/Exam";
import { AccountService } from "../services/Account/account.service";
import { Account } from "../shared/model/Account";
import { Student } from "../shared/model/Student";
import { PaymentService } from "../services/Payment/payment.service";
import { PaymentPurposeService } from "../services/PaymentPurpose/payment-purpose.service";
import { Payment } from "../shared/model/Payment";
import { PaymentPurpose } from "../shared/model/PaymentPurpose";
import { StudentService } from "../services/Student/student.service";

@Component({
  selector: "app-student-cancel-exam-registration",
  templateUrl: "./student-cancel-exam-registration.component.html",
  styleUrls: ["./student-cancel-exam-registration.component.css"]
})
export class StudentCancelExamRegistrationComponent implements OnInit {
  private readonly paymentPurposeName = "ODJAVA_ISPITA";
  examPeriods: ExamPeriod[];
  examPeriodID: number;
  studentID: number;
  exams: Exam[];
  isChecked: boolean = false;
  exam: Exam = {} as Exam;
  iDs: number[] = [];
  totalCost: number = 0;
  currentBalance: number;
  account: Account = {} as Account;
  examID: number;
  payment: Payment = {} as Payment;
  paymentPurpose: PaymentPurpose = {} as PaymentPurpose;
  student: Student = {} as Student;

  constructor(
    private examPeriodService: ExamPeriodService,
    private route: ActivatedRoute,
    private examService: ExamService,
    private accountService: AccountService,
    private paymentService: PaymentService,
    private paymentPurposeService: PaymentPurposeService,
    private studentService: StudentService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studentID = +params["id"];
    });

    this.examPeriodService.getExamPeriodsFuture().subscribe(data => {
      this.examPeriods = data;
      this.examPeriodID = data[0].id;
      this.getRegisteredExams(this.studentID, this.examPeriodID);
    });

    this.getBalance(this.studentID);
    this.getStudent(this.studentID);
    this.getPaymentPurpose(this.paymentPurposeName);
  }

  getRegisteredExams(studentID: number, examPeriodID: number) {
    this.examService
      .getRegisteredExams(studentID, examPeriodID)
      .subscribe(data => {
        this.exams = data;
      });
  }

  onChange(event) {
    this.examPeriodID = event;
    this.getRegisteredExams(this.studentID, this.examPeriodID);
  }

  check(event) {
    this.isChecked = event;
  }

  countTotalCost(event) {
    this.exam = event;
    if (this.isChecked) {
      this.iDs.push(this.exam.id);
      this.totalCost += this.exam.examRealizationDTO.examPrice;
    } else {
      this.iDs.pop();
      this.totalCost -= this.exam.examRealizationDTO.examPrice;
    }
  }

  getBalance(studentID: number) {
    this.accountService.getByStudent(studentID).subscribe(data => {
      this.currentBalance = data.currentAccountBalance;
      this.account = data;
    });
  }

  updateBalance(account: Account, amount: number) {
    this.accountService
      .updateCurrentAccountBalanceAdd(account, amount)
      .subscribe(data => {
        this.getBalance(this.studentID);
        this.totalCost = 0;
      });
  }

  cancelRegistration() {
    for (let e of this.iDs) {
      this.examID = e;
      this.examService
        .cancelExamRegistration(this.examID)
        .subscribe(data => {});
      this.exams = this.exams.filter(x => x.id !== this.examID);
    }

    this.addPayment(this.student, this.totalCost);
    this.updateBalance(this.account, this.totalCost);
  }

  addPayment(student: Student, amount: number) {
    this.payment.studentDTO = student;
    this.payment.paymentPurposeDTO = this.paymentPurpose;
    this.payment.amount = amount;
    this.paymentService.create(this.payment).subscribe(data => {});
  }

  getPaymentPurpose(name: string) {
    this.paymentPurposeService.getPaymentPurposeByName(name).subscribe(data => {
      this.paymentPurpose = data;
    });
  }

  getStudent(id: number) {
    this.studentService.getByID(id).subscribe(data => {
      this.student = data;
    });
  }
}
