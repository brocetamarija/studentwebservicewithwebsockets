import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentCancelExamRegistrationComponent } from './student-cancel-exam-registration.component';

describe('StudentCancelExamRegistrationComponent', () => {
  let component: StudentCancelExamRegistrationComponent;
  let fixture: ComponentFixture<StudentCancelExamRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentCancelExamRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentCancelExamRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
