import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowStudentCancelExamRegistrationComponent } from './table-row-student-cancel-exam-registration.component';

describe('TableRowStudentCancelExamRegistrationComponent', () => {
  let component: TableRowStudentCancelExamRegistrationComponent;
  let fixture: ComponentFixture<TableRowStudentCancelExamRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowStudentCancelExamRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowStudentCancelExamRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
