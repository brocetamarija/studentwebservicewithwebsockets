import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Exam } from "src/app/shared/model/Exam";
import { ExamService } from "src/app/services/Exam/exam.service";

@Component({
  selector: "tr[app-table-row-student-cancel-exam-registration]",
  templateUrl: "./table-row-student-cancel-exam-registration.component.html",
  styleUrls: ["./table-row-student-cancel-exam-registration.component.css"]
})
export class TableRowStudentCancelExamRegistrationComponent implements OnInit {
  @Input() exam: Exam = {} as Exam;
  @Input() i: number;
  @Output() checkedExam = new EventEmitter();
  @Output() isChecked = new EventEmitter();
  examID: number;

  constructor(private examService: ExamService) {}

  ngOnInit() {}

  checkedOnChange(event) {
    this.isChecked.emit(event.target.checked);
    this.examID = event.target.value;

    this.examService.getByID(this.examID).subscribe(data => {
      this.checkedExam.emit(data);
    });
  }

  checkDate(date) {
    let d = new Date(date);
    d.setDate(d.getDate() - 3);
    let dateToday = new Date();
    if (dateToday > d) {
      return true;
    }
  }
}
