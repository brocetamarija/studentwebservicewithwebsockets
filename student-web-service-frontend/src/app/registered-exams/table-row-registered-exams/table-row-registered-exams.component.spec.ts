import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowRegisteredExamsComponent } from './table-row-registered-exams.component';

describe('TableRowRegisteredExamsComponent', () => {
  let component: TableRowRegisteredExamsComponent;
  let fixture: ComponentFixture<TableRowRegisteredExamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowRegisteredExamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowRegisteredExamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
