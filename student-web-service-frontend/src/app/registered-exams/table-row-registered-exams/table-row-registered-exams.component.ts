import { Component, OnInit, Input } from "@angular/core";
import { Exam } from "src/app/shared/model/Exam";

@Component({
  selector: "tr[app-table-row-registered-exams]",
  templateUrl: "./table-row-registered-exams.component.html",
  styleUrls: ["./table-row-registered-exams.component.css"]
})
export class TableRowRegisteredExamsComponent implements OnInit {
  @Input() exam: Exam = {} as Exam;
  @Input() i: number;
  constructor() {}

  ngOnInit() {}
}
