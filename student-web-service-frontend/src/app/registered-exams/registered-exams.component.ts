import { Component, OnInit } from "@angular/core";
import { ExamPeriodService } from "../services/ExamPeriod/exam-period.service";
import { ActivatedRoute } from "@angular/router";
import { ExamService } from "../services/Exam/exam.service";
import { ExamPeriod } from "../shared/model/ExamPeriod";
import { Exam } from "../shared/model/Exam";

@Component({
  selector: "app-registered-exams",
  templateUrl: "./registered-exams.component.html",
  styleUrls: ["./registered-exams.component.css"]
})
export class RegisteredExamsComponent implements OnInit {
  studentID: number;
  examPeriods: ExamPeriod[];
  examPeriodID: number;
  exams: Exam[];

  constructor(
    private examPeriodService: ExamPeriodService,
    private route: ActivatedRoute,
    private examService: ExamService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studentID = +params["id"];
    });

    this.examPeriodService.getExamPeriodsFuture().subscribe(data => {
      this.examPeriods = data;
      this.examPeriodID = data[0].id;
      this.getRegisteredExams(this.studentID, this.examPeriodID);
    });
  }

  getRegisteredExams(studentID: number, examPeriodID: number) {
    this.examService
      .getRegisteredExams(studentID, examPeriodID)
      .subscribe(data => {
        this.exams = data;
      });
  }

  onChange(event) {
    this.examPeriodID = event;
    this.getRegisteredExams(this.studentID, this.examPeriodID);
  }
}
