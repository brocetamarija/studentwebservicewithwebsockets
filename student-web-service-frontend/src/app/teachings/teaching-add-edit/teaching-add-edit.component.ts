import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { TeachingService } from "src/app/services/Teaching/teaching.service";
import { Teaching } from "src/app/shared/model/Teaching";
import { Course } from "src/app/shared/model/Course";
import { CourseService } from "src/app/services/Course/course.service";
import { Teacher } from "src/app/shared/model/Teacher";

@Component({
  selector: "app-teaching-add-edit",
  templateUrl: "./teaching-add-edit.component.html",
  styleUrls: ["./teaching-add-edit.component.css"]
})
export class TeachingAddEditComponent implements OnInit {
  @Output() close = new EventEmitter();
  @Output() newTeaching = new EventEmitter<Teaching>();
  @Input() teaching: Teaching = {} as Teaching;
  @Input() teacher: Teacher = {} as Teacher;
  courses: Course[];
  addForm: boolean;

  constructor(
    private teachingService: TeachingService,
    private courseService: CourseService
  ) {}

  ngOnInit() {
    this.courseService.getCourses().subscribe(data => {
      this.courses = data;
    });
  }

  closeAddForm() {
    this.addForm = false;
    this.close.emit(this.addForm);
  }

  submit() {
    this.teaching.courseDTO = this.courses.find(
      x => x.id == this.teaching.courseDTO.id
    );
    this.teaching.teacherDTO = this.teacher;
    this.teachingService.create(this.teaching).subscribe(data => {
      this.newTeaching.emit(data);
      this.closeAddForm();
    });
  }
}
