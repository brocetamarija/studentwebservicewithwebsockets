import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeachingAddEditComponent } from './teaching-add-edit.component';

describe('TeachingAddEditComponent', () => {
  let component: TeachingAddEditComponent;
  let fixture: ComponentFixture<TeachingAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeachingAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeachingAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
