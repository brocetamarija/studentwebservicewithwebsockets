import { Component, OnInit } from "@angular/core";
import { TeachingService } from "../services/Teaching/teaching.service";
import { Teaching } from "../shared/model/Teaching";
import { ActivatedRoute } from "@angular/router";
import { TeacherService } from "../services/Teacher/teacher.service";
import { Teacher } from "../shared/model/Teacher";
import { Course } from "../shared/model/Course";

@Component({
  selector: "app-teachings",
  templateUrl: "./teachings.component.html",
  styleUrls: ["./teachings.component.css"]
})
export class TeachingsComponent implements OnInit {
  teachings: Teaching[];
  teacherID: number;
  teacher: Teacher = {} as Teacher;
  addForm: boolean = false;
  teaching: Teaching = {} as Teaching;
  teachingID: number;
  totalPages: number[];
  activePage: number = 0;
  pageNum: number = 0;
  size: number = 3;
  param: string;

  constructor(
    private teachingService: TeachingService,
    private route: ActivatedRoute,
    private teacherService: TeacherService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.teacherID = params["id"];
    });

    this.route.params.subscribe(params => {
      this.param = params["param"];
    });

    this.getTeachingsByTeacher(this.teacherID, this.pageNum, this.size);
    this.getTeacher(this.teacherID);
  }

  getTeachingsByTeacher(id: number, pageNum, size) {
    this.teachingService
      .getTeachingsByTeacher(id, pageNum, size)
      .subscribe(data => {
        this.teachings = data.body;
        this.totalPages = Array(Number.parseInt(data.headers.get("totalPages")))
          .fill(0)
          .map((x, i) => i);
      });
  }

  nextPage(event) {
    let pageNum = event.target.name;
    document
      .getElementById(this.activePage.toString())
      .classList.remove("active");
    this.activePage = pageNum;
    this.getTeachingsByTeacher(this.teacherID, pageNum, this.size);
    document.getElementById(this.activePage.toString()).classList.add("active");
  }

  getTeacher(id: number) {
    this.teacherService.getByID(id).subscribe(data => {
      this.teacher = data;
    });
  }

  openAddForm() {
    this.addForm = true;
    this.teaching.teacherDTO = {} as Teacher;
    this.teaching.courseDTO = {} as Course;
  }

  closeAddForm(event) {
    this.addForm = event;
    this.teaching = {} as Teaching;
  }

  addNewRow(event) {
    if (this.teachings.length < 3) {
      this.teachings.push(event);
    } else {
      this.getTeachingsByTeacher(this.teacherID, this.pageNum, this.size);
    }
  }

  onDeleteRow(event) {
    this.teachingID = event;
    this.teachingService.delete(this.teachingID).subscribe(data => {
      this.teachings = this.teachings.filter(x => x.id !== this.teachingID);
      this.getTeachingsByTeacher(this.teacherID, this.pageNum, this.size);
    });
  }
}
