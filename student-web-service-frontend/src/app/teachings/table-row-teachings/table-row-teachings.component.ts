import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Teaching } from "src/app/shared/model/Teaching";

@Component({
  selector: "tr[app-table-row-teachings]",
  templateUrl: "./table-row-teachings.component.html",
  styleUrls: ["./table-row-teachings.component.css"]
})
export class TableRowTeachingsComponent implements OnInit {
  @Input() teaching: Teaching = {} as Teaching;
  @Input() i: number;
  @Input() param: string;
  @Output() deleteRow = new EventEmitter();
  @Output() editRow = new EventEmitter();
  id: number;
  constructor() {}

  ngOnInit() {}

  delete(event) {
    this.id = +event.target.value;
    this.deleteRow.emit(this.id);
  }
}
