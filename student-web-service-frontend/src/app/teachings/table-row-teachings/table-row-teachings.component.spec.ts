import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowTeachingsComponent } from './table-row-teachings.component';

describe('TableRowTeachingsComponent', () => {
  let component: TableRowTeachingsComponent;
  let fixture: ComponentFixture<TableRowTeachingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowTeachingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowTeachingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
