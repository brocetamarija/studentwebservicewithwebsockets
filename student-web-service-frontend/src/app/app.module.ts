import { DatePipe } from "@angular/common";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatAutocompleteModule,
  MatFormFieldModule,
  MatSelectModule,
  MatTooltipModule
} from "@angular/material";
import { MatInputModule } from "@angular/material/input";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgSelectModule } from "@ng-select/ng-select";
import "hammerjs";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { AccountsAddEditComponent } from "./accounts/accounts-add-edit/accounts-add-edit.component";
import { AccountsComponent } from "./accounts/accounts.component";
import { AdministratorsComponent } from "./administrators/administrators.component";
import { AdminsAddEditComponent } from "./administrators/admins-add-edit/admins-add-edit.component";
import { TableRowAdminsComponent } from "./administrators/table-row-admins/table-row-admins.component";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { CourseAddEditComponent } from "./courses/course-add-edit/course-add-edit.component";
import { CoursesComponent } from "./courses/courses.component";
import { TableRowCoursesComponent } from "./courses/table-row-courses/table-row-courses.component";
import { DocumentAddEditComponent } from "./document/document-add-edit/document-add-edit.component";
import { DocumentComponent } from "./document/document.component";
import { TableRowDocumentComponent } from "./document/table-row-document/table-row-document.component";
import { EnrollmentAddEditComponent } from "./enrollments/enrollment-add-edit/enrollment-add-edit.component";
import { EnrollmentsComponent } from "./enrollments/enrollments.component";
import { TableRowEnrollmentsComponent } from "./enrollments/table-row-enrollments/table-row-enrollments.component";
import { ExamPeriodAddEditComponent } from "./exam-periods/exam-period-add-edit/exam-period-add-edit.component";
import { ExamPeriodsComponent } from "./exam-periods/exam-periods.component";
import { TableRowExamPeriodsComponent } from "./exam-periods/table-row-exam-periods/table-row-exam-periods.component";
import { ExamRealizationAddEditComponent } from "./exam-realizations/exam-realization-add-edit/exam-realization-add-edit.component";
import { ExamRealizationsComponent } from "./exam-realizations/exam-realizations.component";
import { TableRowExamRealizationsComponent } from "./exam-realizations/table-row-exam-realizations/table-row-exam-realizations.component";
import { HeaderComponent } from "./header/header.component";
import { HomepageComponent } from "./homepage/homepage.component";
import { LoginComponent } from "./login/login.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { PaymentAddEditComponent } from "./payments/payment-add-edit/payment-add-edit.component";
import { PaymentsComponent } from "./payments/payments.component";
import { ReplaceString } from "./payments/replace-string.pipe";
import { TableRowPaymentsComponent } from "./payments/table-row-payments/table-row-payments.component";
import { AccountService } from "./services/Account/account.service";
import { AddressService } from "./services/Address/address.service";
import { CourseService } from "./services/Course/course.service";
import { DocumentService } from "./services/Document/document.service";
import { EnrollmentService } from "./services/Enrollment/enrollment.service";
import { ExamService } from "./services/Exam/exam.service";
import { ExamPeriodService } from "./services/ExamPeriod/exam-period.service";
import { ExamRealizationService } from "./services/ExamRealization/exam-realization.service";
import { FileService } from "./services/File/file.service";
import { AuthenticationService } from "./services/Login/Authentication/authentication.service";
import { CanActivateService } from "./services/Login/Authentication/can-activate.service";
import { JwtUtilsService } from "./services/Login/JWTUtils/jwt-utils.service";
import { TokenInterceptorService } from "./services/Login/TokenInterceptor/token-interceptor.service";
import { PaymentService } from "./services/Payment/payment.service";
import { PaymentPurposeService } from "./services/PaymentPurpose/payment-purpose.service";
import { PlaceService } from "./services/Place/place.service";
import { StudentService } from "./services/Student/student.service";
import { StudyProgramService } from "./services/StudyProgram/study-program.service";
import { TeacherService } from "./services/Teacher/teacher.service";
import { TeacherTitleService } from "./services/TeacherTitle/teacher-title.service";
import { TeachingService } from "./services/Teaching/teaching.service";
import { TitleService } from "./services/Title/title.service";
import { TypeOfStudyService } from "./services/TypeOfStudy/type-of-study.service";
import { UserService } from "./services/User/user.service";
import { StudentExamRegistrationComponent } from "./student-exam-registration/student-exam-registration.component";
import { TableRowStudentExamRegistrationComponent } from "./student-exam-registration/table-row-student-exam-registration/table-row-student-exam-registration.component";
import { StudentExamResultsComponent } from "./student-exam-results/student-exam-results.component";
import { TableRowStudentExamResultsComponent } from "./student-exam-results/table-row-student-exam-results/table-row-student-exam-results.component";
import { StudentPassedExamsComponent } from "./student-passed-exams/student-passed-exams.component";
import { TableRowStudentPassedExamsComponent } from "./student-passed-exams/table-row-student-passed-exams/table-row-student-passed-exams.component";
import { StudentUnfinishedCoursesComponent } from "./student-unfinished-courses/student-unfinished-courses.component";
import { TableRowStudentUnfinishedCoursesComponent } from "./student-unfinished-courses/table-row-student-unfinished-courses/table-row-student-unfinished-courses.component";
import { StudentDetailsComponent } from "./students/student-details/student-details.component";
import { StudentsAddEditComponent } from "./students/students-add-edit/students-add-edit.component";
import { StudentsComponent } from "./students/students.component";
import { TableRowStudentsComponent } from "./students/table-row-students/table-row-students.component";
import { StudyProgramAddEditComponent } from "./study-programs/study-program-add-edit/study-program-add-edit.component";
import { StudyProgramsComponent } from "./study-programs/study-programs.component";
import { TableRowStudyProgramsComponent } from "./study-programs/table-row-study-programs/table-row-study-programs.component";
import { TableRowTeacherTitlesComponent } from "./teacher-titles/table-row-teacher-titles/table-row-teacher-titles.component";
import { TeacherTitleAddEditComponent } from "./teacher-titles/teacher-title-add-edit/teacher-title-add-edit.component";
import { TeacherTitlesComponent } from "./teacher-titles/teacher-titles.component";
import { TableRowTeachersComponent } from "./teachers/table-row-teachers/table-row-teachers.component";
import { TeachersAddEditComponent } from "./teachers/teachers-add-edit/teachers-add-edit.component";
import { TeachersComponent } from "./teachers/teachers.component";
import { TableRowTeachingsComponent } from "./teachings/table-row-teachings/table-row-teachings.component";
import { TeachingAddEditComponent } from "./teachings/teaching-add-edit/teaching-add-edit.component";
import { TeachingsComponent } from "./teachings/teachings.component";
import { TableRowTypeOfStudiesComponent } from "./type-of-studies/table-row-type-of-studies/table-row-type-of-studies.component";
import { TypeOfStudiesAddEditComponent } from "./type-of-studies/type-of-studies-add-edit/type-of-studies-add-edit.component";
import { TypeOfStudiesComponent } from "./type-of-studies/type-of-studies.component";
import { StudentCancelExamRegistrationComponent } from "./student-cancel-exam-registration/student-cancel-exam-registration.component";
import { TableRowStudentCancelExamRegistrationComponent } from "./student-cancel-exam-registration/table-row-student-cancel-exam-registration/table-row-student-cancel-exam-registration.component";
import { RegisteredExamsComponent } from "./registered-exams/registered-exams.component";
import { TableRowRegisteredExamsComponent } from "./registered-exams/table-row-registered-exams/table-row-registered-exams.component";
import { TeacherExamRealizationsComponent } from "./teacher-exam-realizations/teacher-exam-realizations.component";
import { TableRowTeacherExamRealizationsComponent } from "./teacher-exam-realizations/table-row-teacher-exam-realizations/table-row-teacher-exam-realizations.component";
import { TeacherEnteringResultsComponent } from "./teacher-entering-results/teacher-entering-results.component";
import { TableRowTeacherEnteringResultsComponent } from "./teacher-entering-results/table-row-teacher-entering-results/table-row-teacher-entering-results.component";
import { TeacherExamResultsComponent } from "./teacher-exam-results/teacher-exam-results.component";
import { TableRowTeacherExamResultsComponent } from "./teacher-exam-results/table-row-teacher-exam-results/table-row-teacher-exam-results.component";
import { TableRowSemestersComponent } from "./semesters/table-row-semesters/table-row-semesters.component";
import { AddSemesterComponent } from "./semesters/add-semester/add-semester.component";
import { SemestersComponent } from "./semesters/semesters.component";
import { TeacherDetailsComponent } from './teachers/teacher-details/teacher-details.component';
import { StudentsListComponent } from './students/students-list/students-list.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomepageComponent,
    NavbarComponent,
    LoginComponent,
    AdministratorsComponent,
    TableRowAdminsComponent,
    AdminsAddEditComponent,
    StudentsComponent,
    TableRowStudentsComponent,
    StudentsAddEditComponent,
    PaymentsComponent,
    TableRowPaymentsComponent,
    DocumentComponent,
    TableRowDocumentComponent,
    DocumentAddEditComponent,
    TeachersComponent,
    TableRowTeachersComponent,
    TeachersAddEditComponent,
    TeacherTitlesComponent,
    TableRowTeacherTitlesComponent,
    TeacherTitleAddEditComponent,
    StudyProgramsComponent,
    TableRowStudyProgramsComponent,
    StudyProgramAddEditComponent,
    StudentDetailsComponent,
    CoursesComponent,
    TableRowCoursesComponent,
    CourseAddEditComponent,
    TypeOfStudiesComponent,
    TableRowTypeOfStudiesComponent,
    TypeOfStudiesAddEditComponent,
    TeachingsComponent,
    TableRowTeachingsComponent,
    TeachingAddEditComponent,
    EnrollmentsComponent,
    TableRowEnrollmentsComponent,
    EnrollmentAddEditComponent,
    AccountsComponent,
    AccountsAddEditComponent,
    ExamRealizationsComponent,
    TableRowExamRealizationsComponent,
    ExamRealizationAddEditComponent,
    ExamPeriodsComponent,
    TableRowExamPeriodsComponent,
    ExamPeriodAddEditComponent,
    StudentPassedExamsComponent,
    TableRowStudentPassedExamsComponent,
    StudentUnfinishedCoursesComponent,
    TableRowStudentUnfinishedCoursesComponent,
    StudentExamResultsComponent,
    TableRowStudentExamResultsComponent,
    StudentExamRegistrationComponent,
    PaymentAddEditComponent,
    TableRowStudentExamRegistrationComponent,
    ReplaceString,
    StudentCancelExamRegistrationComponent,
    TableRowStudentCancelExamRegistrationComponent,
    RegisteredExamsComponent,
    TableRowRegisteredExamsComponent,
    TeacherExamRealizationsComponent,
    TableRowTeacherExamRealizationsComponent,
    TeacherEnteringResultsComponent,
    TableRowTeacherEnteringResultsComponent,
    TeacherExamResultsComponent,
    TableRowTeacherExamResultsComponent,
    AddSemesterComponent,
    SemestersComponent,
    TableRowSemestersComponent,
    TeacherDetailsComponent,
    StudentsListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatAutocompleteModule,
    BrowserAnimationsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgSelectModule,
    MatTooltipModule
  ],
  providers: [
    AuthenticationService,
    CanActivateService,
    JwtUtilsService,
    UserService,
    StudentService,
    TeacherService,
    AddressService,
    StudyProgramService,
    PlaceService,
    DocumentService,
    PaymentService,
    CourseService,
    DatePipe,
    AccountService,
    EnrollmentService,
    ExamPeriodService,
    ExamRealizationService,
    FileService,
    TeacherTitleService,
    TeachingService,
    TitleService,
    TypeOfStudyService,
    ExamService,
    PaymentPurposeService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
