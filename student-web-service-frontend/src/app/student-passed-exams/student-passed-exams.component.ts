import { Component, OnInit } from "@angular/core";
import { Exam } from "../shared/model/Exam";
import { ExamService } from "../services/Exam/exam.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-student-passed-exams",
  templateUrl: "./student-passed-exams.component.html",
  styleUrls: ["./student-passed-exams.component.css"]
})
export class StudentPassedExamsComponent implements OnInit {
  exams: Exam[];
  studentID: number;
  totalPoints: number;
  averageGrade: number;

  constructor(
    private examService: ExamService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studentID = +params["id"];
    });

    this.getPassedExams(this.studentID);
    this.getAverageGrade(this.studentID);
    this.getTotalPoints(this.studentID);
  }

  getPassedExams(id: number) {
    this.examService.getPassedExams(id).subscribe(data => {
      this.exams = data;
    });
  }

  getTotalPoints(id: number) {
    this.examService.getTotalEctsPoints(id).subscribe(data => {
      this.totalPoints = data;
    });
  }

  getAverageGrade(id: number) {
    this.examService.getAverageGrade(id).subscribe(data => {
      this.averageGrade = data;
    });
  }
}
