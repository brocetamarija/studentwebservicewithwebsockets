import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowStudentPassedExamsComponent } from './table-row-student-passed-exams.component';

describe('TableRowStudentPassedExamsComponent', () => {
  let component: TableRowStudentPassedExamsComponent;
  let fixture: ComponentFixture<TableRowStudentPassedExamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowStudentPassedExamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowStudentPassedExamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
