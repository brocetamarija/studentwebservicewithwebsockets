import { Component, OnInit, Input } from "@angular/core";
import { Exam } from "src/app/shared/model/Exam";

@Component({
  selector: "tr[app-table-row-student-passed-exams]",
  templateUrl: "./table-row-student-passed-exams.component.html",
  styleUrls: ["./table-row-student-passed-exams.component.css"]
})
export class TableRowStudentPassedExamsComponent implements OnInit {
  @Input() exam: Exam = {} as Exam;
  @Input() i: number;
  constructor() {}

  ngOnInit() {}
}
