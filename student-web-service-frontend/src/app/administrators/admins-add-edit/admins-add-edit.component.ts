import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { PlaceService } from "src/app/services/Place/place.service";
import { Place } from "src/app/shared/model/Place";
import { User } from "src/app/shared/model/User";
import { UserService } from "src/app/services/User/user.service";

@Component({
  selector: "app-admins-add-edit",
  templateUrl: "./admins-add-edit.component.html",
  styleUrls: ["./admins-add-edit.component.css"]
})
export class AdminsAddEditComponent implements OnInit {
  @Output() close = new EventEmitter();
  @Output() newAdmin = new EventEmitter<User>();
  @Output() editAdmin = new EventEmitter<User>();
  @Input() user: User = {} as User;
  @Input() addAdmin: boolean = true;
  places: Place[];
  addForm: boolean;
  placeID: number;

  constructor(
    private placeService: PlaceService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.placeService.getAll().subscribe(data => {
      this.places = data;
    });
  }

  closeAddForm() {
    this.addForm = false;
    this.close.emit(this.addForm);
  }

  submit() {
    if (this.addAdmin) {
      this.user.addressDTO.placeDTO = this.places.find(
        x => x.id == this.user.addressDTO.placeDTO.id
      );
      this.userService.create(this.user).subscribe(data => {
        this.newAdmin.emit(data);
        this.closeAddForm();
      });
    } else {
      this.userService.update(this.user).subscribe(data => {
        this.editAdmin.emit(data);
        this.closeAddForm();
      });
    }
  }
}
