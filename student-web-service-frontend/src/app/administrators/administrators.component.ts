import { Component, OnInit } from "@angular/core";
import { UserService } from "../services/User/user.service";
import { User } from "../shared/model/User";
import { Address } from "../shared/model/Address";
import { Place } from "../shared/model/Place";

@Component({
  selector: "app-administrators",
  templateUrl: "./administrators.component.html",
  styleUrls: ["./administrators.component.css"]
})
export class AdministratorsComponent implements OnInit {
  admins: User[];
  id: number;
  addForm: boolean = false;
  totalPages: number[];
  activePage: number = 0;
  user: User = {} as User;
  addAdmin: boolean = true;
  pageNum: number = 0;
  size: number = 3;

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.getAdmins(this.pageNum, this.size);
  }

  nextPage(event) {
    let pageNum = event.target.name;
    document
      .getElementById(this.activePage.toString())
      .classList.remove("active");
    this.activePage = pageNum;
    this.getAdmins(pageNum, this.size);
    document.getElementById(this.activePage.toString()).classList.add("active");
  }

  onDeleteRow(event) {
    this.id = event;
    this.userService.delete(this.id).subscribe(data => {
      this.admins = this.admins.filter(x => x.id !== this.id);
      this.getAdmins(this.pageNum, this.size);
    });
  }

  openEdit(event) {
    this.id = event;
    let clone = this.admins.find(x => x.id === this.id);
    this.user = JSON.parse(JSON.stringify(clone));
    this.addForm = true;
    this.addAdmin = false;
  }

  onEditRow(event) {
    this.user = event;
    let index = this.admins.findIndex(item => item.id == this.user.id);
    this.admins[index] = this.user;
  }

  addNewRow(event) {
    if (this.admins.length < 3) {
      this.admins.push(event);
    } else {
      this.getAdmins(this.pageNum, this.size);
    }
  }

  openAddForm() {
    this.addAdmin = true;
    this.addForm = true;
    this.user.addressDTO = {} as Address;
    this.user.addressDTO.placeDTO = {} as Place;
  }

  closeAddForm(event) {
    this.addForm = event;
    this.user = {} as User;
  }

  getAdmins(pageNum: number, size: number) {
    this.userService.getAdmins(pageNum, size).subscribe(data => {
      this.admins = data.body;
      this.totalPages = Array(Number.parseInt(data.headers.get("totalPages")))
        .fill(0)
        .map((x, i) => i);
    });
  }
}
