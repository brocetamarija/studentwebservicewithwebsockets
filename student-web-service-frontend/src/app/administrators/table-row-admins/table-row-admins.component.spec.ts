import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowAdminsComponent } from './table-row-admins.component';

describe('TableRowAdminsComponent', () => {
  let component: TableRowAdminsComponent;
  let fixture: ComponentFixture<TableRowAdminsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowAdminsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowAdminsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
