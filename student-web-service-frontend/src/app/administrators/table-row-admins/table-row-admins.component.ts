import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { User } from "src/app/shared/model/User";

@Component({
  selector: "tr[app-table-row-admins]",
  templateUrl: "./table-row-admins.component.html",
  styleUrls: ["./table-row-admins.component.css"]
})
export class TableRowAdminsComponent implements OnInit {
  @Input() admin: User = {} as User;
  @Input() i: number;
  @Output() deleteRow = new EventEmitter();
  @Output() editRow = new EventEmitter();

  id: number;
  constructor() {}

  ngOnInit() {}

  delete(event) {
    this.id = +event.target.value;
    this.deleteRow.emit(this.id);
  }

  edit(event) {
    this.id = +event.target.value;
    this.editRow.emit(this.id);
  }
}
