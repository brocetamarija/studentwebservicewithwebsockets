import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Enrollment } from "src/app/shared/model/Enrollment";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class EnrollmentService {
  private readonly url = "api/enrollments";
  constructor(private http: HttpClient) {}

  getEnrollments(id: number, pageNumber: number, size: number): any {
    return this.http.get(
      this.url + `/student/${id}?page=${pageNumber}&size=${size}`,
      { observe: "response" }
    );
  }

  create(enrollment: Enrollment): Observable<Enrollment> {
    return this.http.post<Enrollment>(this.url, enrollment);
  }

  delete(id: number) {
    return this.http.delete(this.url + `/${id}`);
  }
}
