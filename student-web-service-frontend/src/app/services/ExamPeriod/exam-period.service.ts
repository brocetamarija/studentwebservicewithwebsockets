import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ExamPeriod } from "src/app/shared/model/ExamPeriod";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ExamPeriodService {
  private readonly url = "api/exam-periods";
  constructor(private http: HttpClient) {}

  getExamPeriods(): Observable<ExamPeriod[]> {
    return this.http.get<ExamPeriod[]>(this.url);
  }

  getExamPeriodsPageable(pageNumber: number, size: number): any {
    return this.http.get(
      this.url + `/pageable?page=${pageNumber}&size=${size}`,
      { observe: "response" }
    );
  }

  getExamPeriodsFuture(): Observable<ExamPeriod[]> {
    return this.http.get<ExamPeriod[]>(this.url + `/future`);
  }

  create(examPeriod: ExamPeriod): Observable<ExamPeriod> {
    return this.http.post<ExamPeriod>(this.url, examPeriod);
  }

  update(examPeriod: ExamPeriod): Observable<ExamPeriod> {
    return this.http.put<ExamPeriod>(
      this.url + `/${examPeriod.id}`,
      examPeriod
    );
  }

  delete(id: number) {
    return this.http.delete(this.url + `/${id}`);
  }
}
