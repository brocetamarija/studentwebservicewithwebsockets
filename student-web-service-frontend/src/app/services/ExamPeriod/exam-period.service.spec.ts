import { TestBed } from '@angular/core/testing';

import { ExamPeriodService } from './exam-period.service';

describe('ExamPeriodService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExamPeriodService = TestBed.get(ExamPeriodService);
    expect(service).toBeTruthy();
  });
});
