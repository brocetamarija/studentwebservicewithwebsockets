import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Course } from "src/app/shared/model/Course";

@Injectable({
  providedIn: "root"
})
export class CourseService {
  private readonly url = "api/courses";

  constructor(private http: HttpClient) {}

  getCoursesPageable(pageNumber: number, size: number): any {
    return this.http.get(
      this.url + `/pageable?page=${pageNumber}&size=${size}`,
      {
        observe: "response"
      }
    );
  }

  getCourses(): Observable<Course[]> {
    return this.http.get<Course[]>(this.url);
  }

  getCoursesByStudyProgramPageable(
    id: number,
    pageNumber: number,
    size: number
  ): any {
    return this.http.get(
      this.url + `/course/${id}?page=${pageNumber}&size=${size}`,
      {
        observe: "response"
      }
    );
  }

  getCoursesByStudyProgram(id: number): Observable<Course[]> {
    return this.http.get<Course[]>(this.url + `/study-program/${id}`);
  }

  getCourseByStudyProgramAndStudyYear(
    studyProgramId: number,
    yearId: number
  ): Observable<Course[]> {
    return this.http.get<Course[]>(
      this.url + `/study-program/${studyProgramId}/year/${yearId}`
    );
  }

  getUnfinishedCourses(id: number): Observable<Course[]> {
    return this.http.get<Course[]>(this.url + `/unfinished-courses/${id}`);
  }

  create(course: Course): Observable<Course> {
    return this.http.post<Course>(this.url, course);
  }

  update(course: Course): Observable<Course> {
    return this.http.put<Course>(this.url + `/${course.id}`, course);
  }

  delete(id: number) {
    return this.http.delete(this.url + `/${id}`);
  }
}
