import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { TypeOfStudy } from "src/app/shared/model/TypeOfStudy";

@Injectable({
  providedIn: "root"
})
export class TypeOfStudyService {
  private readonly url = "api/study-types";

  constructor(private http: HttpClient) {}

  getAll(): Observable<TypeOfStudy[]> {
    return this.http.get<TypeOfStudy[]>(this.url);
  }

  getAllPageable(pageNumber: number, size: number): any {
    return this.http.get(
      this.url + `/pageable?page=${pageNumber}&size=${size}`,
      {
        observe: "response"
      }
    );
  }

  getOne(id: number): Observable<TypeOfStudy> {
    return this.http.get<TypeOfStudy>(this.url + `/${id}`);
  }

  create(typeOfStudy: TypeOfStudy): Observable<TypeOfStudy> {
    return this.http.post<TypeOfStudy>(this.url, typeOfStudy);
  }

  update(typeOfStudy: TypeOfStudy): Observable<TypeOfStudy> {
    return this.http.put<TypeOfStudy>(
      this.url + `/${typeOfStudy.id}`,
      typeOfStudy
    );
  }

  delete(id: number) {
    return this.http.delete(this.url + `/${id}`);
  }
}
