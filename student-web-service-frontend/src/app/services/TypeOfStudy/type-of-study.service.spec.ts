import { TestBed } from '@angular/core/testing';

import { TypeOfStudyService } from './type-of-study.service';

describe('TypeOfStudyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypeOfStudyService = TestBed.get(TypeOfStudyService);
    expect(service).toBeTruthy();
  });
});
