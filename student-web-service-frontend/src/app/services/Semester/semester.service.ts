import { Injectable } from "@angular/core";
import { Semester } from "src/app/shared/model/Semester";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class SemesterService {
  private readonly url = "api/semesters";
  constructor(private http: HttpClient) {}

  getAll(): Observable<Semester[]> {
    return this.http.get<Semester[]>(this.url);
  }

  create(semester: Semester): Observable<Semester> {
    return this.http.post<Semester>(this.url, semester);
  }

  update(semester: Semester): Observable<Semester> {
    return this.http.put<Semester>(this.url + `/${semester.id}`, semester);
  }

  delete(id: number): any {
    return this.http.delete(this.url + `/${id}`);
  }

  getLast(): Observable<Semester> {
    return this.http.get<Semester>(this.url + `/last`);
  }
}
