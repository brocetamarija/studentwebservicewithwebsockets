import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Account } from "src/app/shared/model/Account";

@Injectable({
  providedIn: "root"
})
export class AccountService {
  private readonly url = "api/accounts";
  constructor(private http: HttpClient) {}

  getByStudent(id: number): Observable<Account> {
    return this.http.get<Account>(this.url + `/student/${id}`);
  }

  create(account: Account): Observable<Account> {
    return this.http.post<Account>(this.url, account);
  }

  update(account: Account): Observable<Account> {
    return this.http.put<Account>(this.url + `/${account.id}`, account);
  }

  updateCurrentAccountBalanceAdd(
    account: Account,
    amount: number
  ): Observable<Account> {
    return this.http.put<Account>(
      this.url + `/update-balance-add/${account.id}?amount=${amount}`,
      account
    );
  }

  updateCurrentAccountBalanceSubtract(
    account: Account,
    amount: number
  ): Observable<Account> {
    return this.http.put<Account>(
      this.url + `/update-balance-subtract/${account.id}?amount=${amount}`,
      account
    );
  }
}
