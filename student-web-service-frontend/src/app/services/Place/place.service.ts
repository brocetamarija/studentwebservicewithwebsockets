import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Place } from "src/app/shared/model/Place";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class PlaceService {
  private readonly url = "api/places";

  constructor(private http: HttpClient) {}

  getAll(): Observable<Place[]> {
    return this.http.get<Place[]>(this.url);
  }
}
