import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Title } from "src/app/shared/model/Title";

@Injectable({
  providedIn: "root"
})
export class TitleService {
  private readonly url = "api/titles";

  constructor(private http: HttpClient) {}

  getTitles(): Observable<Title[]> {
    return this.http.get<Title[]>(this.url);
  }
}
