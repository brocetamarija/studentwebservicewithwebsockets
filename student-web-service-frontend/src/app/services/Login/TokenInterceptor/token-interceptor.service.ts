import { Injectable, Injector } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpHeaders
} from "@angular/common/http";
import { Observable } from "rxjs";
import { AuthenticationService } from "../Authentication/authentication.service";

@Injectable({
  providedIn: "root"
})
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private inj: Injector) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let authenticationService: AuthenticationService = this.inj.get(
      AuthenticationService
    );
    const headers = new HttpHeaders({
      Authorization: `Bearer  ${authenticationService.getToken()}`
    });

    request = request.clone({
      headers: headers
    });
    return next.handle(request);
  }
}
