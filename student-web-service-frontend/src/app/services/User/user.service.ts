import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "src/app/shared/model/User";
import { Observable } from "rxjs";
import PasswordChanger from "src/app/shared/model/PasswordChanger";

@Injectable({
  providedIn: "root"
})
export class UserService {
  private readonly url = "api/users";

  constructor(private http: HttpClient) {}

  getByUsername(username: string): Observable<User> {
    return this.http.get<User>(this.url + `/by-username/${username}`);
  }

  getAdmins(pageNumber: number, size: number): any {
    return this.http.get(this.url + `/admins?page=${pageNumber}&size=${size}`, {
      observe: "response"
    });
  }

  create(user: User): Observable<User> {
    return this.http.post<User>(this.url, user);
  }

  update(user: User): Observable<User> {
    return this.http.put<User>(this.url + `/${user.id}`, user);
  }

  delete(id: number) {
    return this.http.delete(this.url + `/${id}`);
  }

  changePassword(password: any) {
    return this.http.post(this.url + "/change-password", password);
  }
}
