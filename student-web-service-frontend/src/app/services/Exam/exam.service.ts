import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Exam } from "src/app/shared/model/Exam";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ExamService {
  private readonly url = "api/exams";
  constructor(private http: HttpClient) {}

  getByID(id: number): Observable<Exam> {
    return this.http.get<Exam>(this.url + `/${id}`);
  }

  getPassedExams(id: number): Observable<Exam[]> {
    return this.http.get<Exam[]>(this.url + `/passed-exams/${id}`);
  }

  getTotalEctsPoints(id: number): any {
    return this.http.get(this.url + `/total-ects-points/${id}`);
  }

  getAverageGrade(id: number): any {
    return this.http.get(this.url + `/average-grade/${id}`);
  }


  getExamResults(studentID: number, examPeriodID): Observable<Exam[]> {
    return this.http.get<Exam[]>(
      this.url + `/student-exam-results/${studentID}/${examPeriodID}`
    );
  }

  getRegisteredExams(
    studentID: number,
    examPeriodID: number
  ): Observable<Exam[]> {
    return this.http.get<Exam[]>(
      this.url +
        `/registered-exams/student/${studentID}/exam-period/${examPeriodID}`
    );
  }

  getExamsForEnteringResults(id: number): Observable<Exam[]> {
    return this.http.get<Exam[]>(this.url + `/exam-enter-results/${id}`);
  }

  countTotalPoints(labPoints: number, examPoints: number): any {
    return this.http.get(
      this.url +
        `/count-total-points?lab-points=${labPoints}&exam-points=${examPoints}`
    );
  }

  countGrade(totalPoints: number): any {
    return this.http.get(this.url + `/count-grade?total-points=${totalPoints}`);
  }

  getExamResultsForTeacher(id: number): Observable<Exam[]> {
    return this.http.get<Exam[]>(this.url + `/teacher-exam-results/${id}`);
  }

  examRegistration(exam: Exam): Observable<Exam> {
    return this.http.post<Exam>(this.url + `/exam-registration`, exam);
  }
  
  update(exam: Exam): Observable<Exam> {
    return this.http.put<Exam>(this.url + `/${exam.id}`, exam);
  }

  delete(id: number) {
    return this.http.delete(this.url + `/${id}`);
  }

  cancelExamRegistration(id: number): Observable<Exam> {
    return this.http.delete<Exam>(this.url + `/cancel-exam-registration/${id}`);
  }
}
