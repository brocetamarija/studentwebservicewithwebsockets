import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { UploadModel } from "src/app/shared/model/UploadModel";

@Injectable({
  providedIn: "root"
})
export class FileService {
  private readonly url = "api/files";

  constructor(private http: HttpClient) {}

  download(filename: string) {
    let headers = new HttpHeaders();
    headers = headers.append("Accept", "application/pdf");
    return this.http.get(this.url + `/download/${filename}`, {
      observe: "response",
      headers: headers,
      responseType: "blob"
    });
  }

  uploadDocument(file: File): any {
    let formdata = new FormData();
    formdata.append("file", file);
    return this.http.post(this.url + `/upload`, formdata);
  }

  delete(filename: string) {
    return this.http.delete(this.url + `/${filename}`, {
      observe: "response",
      responseType: "text"
    });
  }
}
