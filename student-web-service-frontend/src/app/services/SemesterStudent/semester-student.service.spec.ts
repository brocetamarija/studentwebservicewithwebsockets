import { TestBed } from '@angular/core/testing';

import { SemesterStudentService } from './semester-student.service';

describe('SemesterStudentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SemesterStudentService = TestBed.get(SemesterStudentService);
    expect(service).toBeTruthy();
  });
});
