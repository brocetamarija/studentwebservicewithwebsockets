import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { SemesterStudent } from "src/app/shared/model/SemesterStudent";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class SemesterStudentService {
  private readonly url = "api/semester-student";
  constructor(private http: HttpClient) {}

  checkIfSemesterAdded(id: number): any {
    return this.http.get(this.url + `/check/${id}`);
  }

  addSemesterToStudent(
    semesterStudent: SemesterStudent
  ): Observable<SemesterStudent> {
    return this.http.post<SemesterStudent>(this.url, semesterStudent);
  }

  semesterCertification(
    semesterStudent: SemesterStudent
  ): Observable<SemesterStudent> {
    return this.http.put<SemesterStudent>(
      this.url + `/semester-certification/${semesterStudent.id}`,
      semesterStudent
    );
  }

  getSemesterStudent(id: number): Observable<SemesterStudent> {
    return this.http.get<SemesterStudent>(this.url + `/is-certified/${id}`);
  }
}
