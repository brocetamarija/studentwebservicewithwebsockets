import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { TeacherTitle } from "src/app/shared/model/TeacherTitle";

@Injectable({
  providedIn: "root"
})
export class TeacherTitleService {
  private readonly url = "api/teacher-titles";

  constructor(private http: HttpClient) {}

  getTitlesByTeacher(id: number): Observable<TeacherTitle[]> {
    return this.http.get<TeacherTitle[]>(this.url + `/teacher/${id}`);
  }

  create(teacherTitle: TeacherTitle): Observable<TeacherTitle> {
    return this.http.post<TeacherTitle>(this.url, teacherTitle);
  }

  update(teacherTitle: TeacherTitle): Observable<TeacherTitle> {
    return this.http.put<TeacherTitle>(
      this.url + `/${teacherTitle.id}`,
      teacherTitle
    );
  }
}
