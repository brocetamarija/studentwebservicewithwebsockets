import { TestBed } from '@angular/core/testing';

import { TeacherTitleService } from './teacher-title.service';

describe('TeacherTitleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TeacherTitleService = TestBed.get(TeacherTitleService);
    expect(service).toBeTruthy();
  });
});
