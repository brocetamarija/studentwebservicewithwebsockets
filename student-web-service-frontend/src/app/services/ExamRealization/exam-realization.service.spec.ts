import { TestBed } from '@angular/core/testing';

import { ExamRealizationService } from './exam-realization.service';

describe('ExamRealizationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExamRealizationService = TestBed.get(ExamRealizationService);
    expect(service).toBeTruthy();
  });
});
