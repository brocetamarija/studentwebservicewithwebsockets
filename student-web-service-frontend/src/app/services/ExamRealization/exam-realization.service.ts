import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ExamRealization } from "src/app/shared/model/ExamRealization";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ExamRealizationService {
  private readonly url = "api/exam-realizations";
  constructor(private http: HttpClient) {}

  getByID(examRealizationID: number): Observable<ExamRealization> {
    return this.http.get<ExamRealization>(this.url + `/${examRealizationID}`);
  }

  getByStudyProgramAndExamPeriod(
    studyProgramID: number,
    examPeriodID: number,
    pageNumber: number,
    size: number
  ): any {
    return this.http.get(
      this.url +
        `/study-program/${studyProgramID}/exam-period/${examPeriodID}?page=${pageNumber}&size=${size}`,
      { observe: "response" }
    );
  }

  getExamRealizationsForRegistration(
    studentID: number,
    examPeriodID: number
  ): Observable<ExamRealization[]> {
    return this.http.get<ExamRealization[]>(
      this.url + `/student/${studentID}/exam-period/${examPeriodID}`
    );
  }

  

  getExamRealizationsForTeacher(
    teacherID: number,
    examPeriodID: number
  ): Observable<ExamRealization[]> {
    return this.http.get<ExamRealization[]>(
      this.url + `/teacher/${teacherID}/exam-period/${examPeriodID}`
    );
  }

  create(examRealization: ExamRealization): Observable<ExamRealization> {
    return this.http.post<ExamRealization>(this.url, examRealization);
  }

  update(examRealization: ExamRealization): Observable<ExamRealization> {
    return this.http.put<ExamRealization>(
      this.url + `/${examRealization.id}`,
      examRealization
    );
  }

  delete(id: number) {
    return this.http.delete(this.url + `/${id}`);
  }
}
