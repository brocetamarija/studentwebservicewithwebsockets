import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { StudyProgram } from "src/app/shared/model/StudyProgram";

@Injectable({
  providedIn: "root"
})
export class StudyProgramService {
  private readonly url = "api/study-programs";

  constructor(private http: HttpClient) {}

  getAll(): Observable<StudyProgram[]> {
    return this.http.get<StudyProgram[]>(this.url);
  }

  getAllPageable(pageNumber: number, size: number): any {
    return this.http.get(
      this.url + `/pageable?page=${pageNumber}&size=${size}`,
      {
        observe: "response"
      }
    );
  }

  getByStudyTypePageable(id: number, pageNumber: number, size: number): any {
    return this.http.get(
      this.url + `/study-type-pageable/${id}?page=${pageNumber}&size=${size}`,
      { observe: "response" }
    );
  }

  getByStudyType(id: number):Observable<StudyProgram[]>{
    return this.http.get<StudyProgram[]>(this.url + `/study-type/${id}`);
  }

  getOne(id: number): Observable<StudyProgram> {
    return this.http.get<StudyProgram>(this.url + `/${id}`);
  }

  create(studyProgram: StudyProgram): Observable<StudyProgram> {
    return this.http.post<StudyProgram>(this.url, studyProgram);
  }

  update(studyProgram: StudyProgram): Observable<StudyProgram> {
    return this.http.put<StudyProgram>(
      this.url + `/${studyProgram.id}`,
      studyProgram
    );
  }

  delete(id: number) {
    return this.http.delete(this.url + `${id}`);
  }
}
