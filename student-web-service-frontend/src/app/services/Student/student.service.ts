import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Student } from "src/app/shared/model/Student";

@Injectable({
  providedIn: "root"
})
export class StudentService {
  private readonly url = "api/students";

  constructor(private http: HttpClient) {}

  getStudents(pageNumber: number, size: number): any {
    return this.http.get(this.url + `?page=${pageNumber}&size=${size}`, {
      observe: "response"
    });
  }

  getAll(): Observable<Student[]> {
    return this.http.get<Student[]>(this.url + `/all`);
  }

  getByID(id: number): Observable<Student> {
    return this.http.get<Student>(this.url + `/${id}`);
  }

  getByUsername(username: string): Observable<Student> {
    return this.http.get<Student>(this.url + `/by-username/${username}`);
  }

  getByStudyProgram(id: number, pageNumber: number, size: number): any {
    return this.http.get(
      this.url + `/study-program/${id}?page=${pageNumber}&size=${size}`,
      {
        observe: "response"
      }
    );
  }

  getByStudyProgramAndStudyType(studyProgramID: number, studyTypeID: number): Observable<Student[]>{
    return this.http.get<Student[]>(this.url + `/study-program/${studyProgramID}/study-type/${studyTypeID}`);
  }

  getStudentWhoRegisterExam(examRealizationID: number): Observable<Student[]> {
    return this.http.get<Student[]>(
      this.url + `/by-registered-exam/${examRealizationID}`
    );
  }

  create(student: Student): Observable<Student> {
    return this.http.post<Student>(this.url, student);
  }

  update(student: Student): Observable<Student> {
    return this.http.put<Student>(this.url + `/${student.id}`, student);
  }

  delete(id: number) {
    return this.http.delete(this.url + `/${id}`);
  }
}
