import { TestBed } from '@angular/core/testing';

import { TeachingService } from './teaching.service';

describe('TeachingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TeachingService = TestBed.get(TeachingService);
    expect(service).toBeTruthy();
  });
});
