import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Teaching } from "src/app/shared/model/Teaching";

@Injectable({
  providedIn: "root"
})
export class TeachingService {
  private readonly url = "api/teachings";
  constructor(private http: HttpClient) {}

  getTeachingsByTeacher(id: number, pageNumber: number, size: number): any {
    return this.http.get(
      this.url + `/teacher/${id}?page=${pageNumber}&size=${size}`,
      {
        observe: "response"
      }
    );
  }

  create(teaching: Teaching): Observable<Teaching> {
    return this.http.post<Teaching>(this.url, teaching);
  }

  delete(id: number) {
    return this.http.delete(this.url + `/${id}`);
  }
}
