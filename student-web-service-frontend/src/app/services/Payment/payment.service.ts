import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Payment } from "src/app/shared/model/Payment";

@Injectable({
  providedIn: "root"
})
export class PaymentService {
  private readonly url = "api/payments";

  constructor(private http: HttpClient) {}

  getPaymentsByStudent(id: number, pageNumber: number, size: number): any {
    return this.http.get(
      this.url + `/student/${id}?page=${pageNumber}&size=${size}`,
      { observe: "response" }
    );
  }

  create(payment: Payment): Observable<Payment> {
    return this.http.post<Payment>(this.url, payment);
  }

  addPaymentForExamRegistration(payment: Payment): Observable<Payment> {
    return this.http.post<Payment>(
      this.url + `/exam-registration-payment`,
      payment
    );
  }
}
