import { Injectable } from "@angular/core";
import * as Stomp from "stompjs";
import * as SockJS from "sockjs-client";

@Injectable({
  providedIn: "root"
})
export class WebSocketApiService {
  private readonly wsEndpoint = "http://localhost:8080/ws";
  stompClient: any;

  constructor() {}

  public connect() {
    let socket = new SockJS(this.wsEndpoint);

    let stompClient = Stomp.over(socket);

    return stompClient;
  }

}
