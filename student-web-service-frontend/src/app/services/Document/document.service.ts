import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Document } from "src/app/shared/model/Document";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class DocumentService {
  private readonly url = "api/documents";
  constructor(private http: HttpClient) {}

  getAll(pageNumber: number, size: number): any {
    return this.http.get<Document[]>(
      this.url + `?page=${pageNumber}&size=${size}`,
      { observe: "response" }
    );
  }

  getByStudent(id: number, pageNumber: number, size: number): any {
    return this.http.get(
      this.url + `/student/${id}?page=${pageNumber}&size=${size}`,
      { observe: "response" }
    );
  }

  create(document: Document): Observable<Document> {
    return this.http.post<Document>(this.url, document);
  }

  update(document: Document): Observable<Document> {
    return this.http.put<Document>(this.url + `/${document.id}`, document);
  }

  delete(id: number): Observable<Document> {
    return this.http.delete<Document>(this.url + `/${id}`);
  }
}
