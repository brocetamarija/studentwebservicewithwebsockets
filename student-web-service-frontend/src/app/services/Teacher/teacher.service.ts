import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Student } from "src/app/shared/model/Student";
import { Teacher } from "src/app/shared/model/Teacher";

@Injectable({
  providedIn: "root"
})
export class TeacherService {
  private readonly url = "api/teachers";

  constructor(private http: HttpClient) {}

  getTeachersPageable(pageNumber: number, size: number): any {
    return this.http.get(
      this.url + `/pageable?page=${pageNumber}&size=${size}`,
      {
        observe: "response"
      }
    );
  }

  getTeachers(): Observable<Teacher[]> {
    return this.http.get<Teacher[]>(this.url);
  }

  getByID(id: number): Observable<Teacher> {
    return this.http.get<Teacher>(this.url + `/${id}`);
  }

  getByUsername(username: string): Observable<Teacher> {
    return this.http.get<Teacher>(this.url + `/by-username/${username}`);
  }

  create(teacher: Teacher): Observable<Teacher> {
    return this.http.post<Teacher>(this.url, teacher);
  }

  update(teacher: Teacher): Observable<Teacher> {
    return this.http.put<Teacher>(this.url + `/${teacher.id}`, teacher);
  }

  delete(id: number) {
    return this.http.delete(this.url + `/${id}`);
  }
}
