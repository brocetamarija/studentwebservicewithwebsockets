import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Address } from "src/app/shared/model/Address";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class AddressService {
  private readonly url = "api/addresses";

  constructor(private http: HttpClient) {}

  create(address: Address): Observable<Address> {
    return this.http.post<Address>(this.url, address);
  }
}
