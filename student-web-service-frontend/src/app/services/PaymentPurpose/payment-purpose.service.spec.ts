import { TestBed } from '@angular/core/testing';

import { PaymentPurposeService } from './payment-purpose.service';

describe('PaymentPurposeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentPurposeService = TestBed.get(PaymentPurposeService);
    expect(service).toBeTruthy();
  });
});
