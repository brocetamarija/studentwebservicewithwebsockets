import { Injectable } from "@angular/core";
import { PaymentPurpose } from "src/app/shared/model/PaymentPurpose";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class PaymentPurposeService {
  private readonly url = "api/payment-purposes";
  constructor(private http: HttpClient) {}

  getPaymentPurposes(): Observable<PaymentPurpose[]> {
    return this.http.get<PaymentPurpose[]>(this.url);
  }

  getPaymentPurposeByName(name: string): Observable<PaymentPurpose> {
    return this.http.get<PaymentPurpose>(this.url + `/${name}`);
  }
}
