import { Component, OnInit } from "@angular/core";
import { TeacherService } from "../services/Teacher/teacher.service";
import { Teacher } from "../shared/model/Teacher";
import { User } from "../shared/model/User";
import { Address } from "../shared/model/Address";
import { Place } from "../shared/model/Place";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-teachers",
  templateUrl: "./teachers.component.html",
  styleUrls: ["./teachers.component.css"]
})
export class TeachersComponent implements OnInit {
  teachers: Teacher[];
  totalPages: number[];
  activePage: number = 0;
  teacherID: number;
  teacher: Teacher = {} as Teacher;
  addForm: boolean = false;
  addTeacher: boolean = true;
  pageNum: number = 0;
  size: number = 3;

  constructor(
    private teacherService: TeacherService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getTeachers(this.pageNum, this.size);
  }

  getTeachers(pageNum: number, size: number) {
    this.teacherService.getTeachersPageable(pageNum, size).subscribe(data => {
      this.teachers = data.body;
      this.totalPages = Array(Number.parseInt(data.headers.get("totalPages")))
        .fill(0)
        .map((x, i) => i);
    });
  }

  nextPage(event) {
    let pageNum = event.target.name;
    document
      .getElementById(this.activePage.toString())
      .classList.remove("active");
    this.activePage = pageNum;
    this.getTeachers(pageNum, this.size);
    document.getElementById(this.activePage.toString()).classList.add("active");
  }

  openAddForm() {
    this.addTeacher = true;
    this.addForm = true;
    this.teacher.userDTO = {} as User;
    this.teacher.userDTO.addressDTO = {} as Address;
    this.teacher.userDTO.addressDTO.placeDTO = {} as Place;
  }

  closeAddForm(event) {
    this.addForm = event;
    this.teacher = {} as Teacher;
  }

  addNewRow(event) {
    if (this.teachers.length < 3) {
      this.teachers.push(event);
    } else {
      this.getTeachers(this.pageNum, this.size);
    }
  }

  /*onEditRow(event) {
    this.teacher = event;
    let index = this.teachers.findIndex(item => item.id == this.teacher.id);
    this.teachers[index] = this.teacher;
  }*/

  onDeleteRow(event) {
    this.teacherID = event;
    this.teacherService.delete(this.teacherID).subscribe(data => {
      this.teachers = this.teachers.filter(x => x.id !== this.teacherID);
      this.getTeachers(this.pageNum, this.size);
    });
  }

  /*openEdit(event) {
    this.teacherID = event;
    let clone = this.teachers.find(x => x.id === this.teacherID);
    this.teacher = JSON.parse(JSON.stringify(clone));
    this.addForm = true;
    this.addTeacher = false;
  }*/
}
