import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { Teacher } from "src/app/shared/model/Teacher";
import { Place } from "src/app/shared/model/Place";
import { TeacherService } from "src/app/services/Teacher/teacher.service";
import { PlaceService } from "src/app/services/Place/place.service";

@Component({
  selector: "app-teachers-add-edit",
  templateUrl: "./teachers-add-edit.component.html",
  styleUrls: ["./teachers-add-edit.component.css"]
})
export class TeachersAddEditComponent implements OnInit {
  @Output() close = new EventEmitter();
  @Output() newTeacher = new EventEmitter<Teacher>();
  @Output() editTeacher = new EventEmitter<Teacher>();
  @Input() teacher: Teacher = {} as Teacher;
  @Input() addTeacher: boolean = true;
  places: Place[];
  addForm: boolean;

  constructor(
    private teacherService: TeacherService,
    private placeService: PlaceService
  ) {}

  ngOnInit() {
    this.placeService.getAll().subscribe(data => {
      this.places = data;
    });
  }

  closeAddForm() {
    this.addForm = false;
    this.close.emit(this.addForm);
  }

  submit() {
    if (this.addTeacher) {
      this.teacher.userDTO.addressDTO.placeDTO = this.places.find(
        x => x.id == this.teacher.userDTO.addressDTO.placeDTO.id
      );
      this.teacherService.create(this.teacher).subscribe(data => {
        this.newTeacher.emit(data);
        this.closeAddForm();
      });
    } else {
      this.teacherService.update(this.teacher).subscribe(data => {
        this.editTeacher.emit(data);
        this.closeAddForm();
      });
    }
  }
}
