import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeachersAddEditComponent } from './teachers-add-edit.component';

describe('TeachersAddEditComponent', () => {
  let component: TeachersAddEditComponent;
  let fixture: ComponentFixture<TeachersAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeachersAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeachersAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
