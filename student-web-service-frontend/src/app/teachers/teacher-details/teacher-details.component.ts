import { Component, OnInit, Input } from '@angular/core';
import { TeacherService } from 'src/app/services/Teacher/teacher.service';
import { Teacher } from 'src/app/shared/model/Teacher';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-teacher-details',
  templateUrl: './teacher-details.component.html',
  styleUrls: ['./teacher-details.component.css']
})
export class TeacherDetailsComponent implements OnInit {
  @Input() addTeacher: boolean = true;
  teacher: Teacher = {} as Teacher;
  teacherID: number;
  addForm:boolean;
  constructor(private teacherService: TeacherService,private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.teacherID = params["id"];
    });

    this.getTeacherByID(this.teacherID);
  }

  getTeacherByID(id: number){
    this.teacherService.getByID(id).subscribe(data => {
      this.teacher = data;
      console.log(data);
      console.log(this.teacher);
    });
  }

  edit() {
    let clone = this.teacher;
    this.teacher = JSON.parse(JSON.stringify(clone));
    this.addForm = true;
    this.addTeacher = false;
  }

  closeAddForm(event) {
    this.addForm = event;
  }

}
