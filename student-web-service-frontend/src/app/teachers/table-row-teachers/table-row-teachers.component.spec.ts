import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowTeachersComponent } from './table-row-teachers.component';

describe('TableRowTeachersComponent', () => {
  let component: TableRowTeachersComponent;
  let fixture: ComponentFixture<TableRowTeachersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowTeachersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowTeachersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
