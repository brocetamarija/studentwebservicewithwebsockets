import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Teacher } from "src/app/shared/model/Teacher";
import { Router } from "@angular/router";

@Component({
  selector: "tr[app-table-row-teachers]",
  templateUrl: "./table-row-teachers.component.html",
  styleUrls: ["./table-row-teachers.component.css"]
})
export class TableRowTeachersComponent implements OnInit {
  @Input() teacher: Teacher = {} as Teacher;
  @Input() i: number;
  id: number;

  constructor(private router: Router) {}

  ngOnInit() {}

  details(event) {
    this.id = +event.target.value;
    this.router.navigate(["/teacher-details", this.id]);
  }

  getTitles(event) {
    this.id = +event.target.value;
    this.router.navigate(["/titles/edit", this.id]);
  }

  getCourses(event) {
    this.id = +event.target.value;
    this.router.navigate(["/teachings/edit", this.id]);
  }
}
