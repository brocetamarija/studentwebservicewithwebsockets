import { Component, OnInit } from "@angular/core";
import { ExamPeriod } from "../shared/model/ExamPeriod";
import { Exam } from "../shared/model/Exam";
import { ExamPeriodService } from "../services/ExamPeriod/exam-period.service";
import { ExamService } from "../services/Exam/exam.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-student-exam-results",
  templateUrl: "./student-exam-results.component.html",
  styleUrls: ["./student-exam-results.component.css"]
})
export class StudentExamResultsComponent implements OnInit {
  examPeriods: ExamPeriod[];
  exams: Exam[];
  examPeriodID: number = 1;
  studentID: number;

  constructor(
    private examPeriodService: ExamPeriodService,
    private examService: ExamService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.studentID = +params["id"];
    });

    this.examPeriodService.getExamPeriods().subscribe(data => {
      this.examPeriods = data;
    });

    this.getExamResults(this.studentID, this.examPeriodID);
  }

  getExamResults(studentID: number, examPeriodID: number) {
    this.examService.getExamResults(studentID, examPeriodID).subscribe(data => {
      this.exams = data;
    });
  }

  onChange(event) {
    this.examPeriodID = event;
    this.getExamResults(this.studentID, this.examPeriodID);
  }
}
