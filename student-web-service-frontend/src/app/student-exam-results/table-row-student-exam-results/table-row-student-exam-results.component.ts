import { Component, OnInit, Input } from "@angular/core";
import { Exam } from "src/app/shared/model/Exam";

@Component({
  selector: "tr[app-table-row-student-exam-results]",
  templateUrl: "./table-row-student-exam-results.component.html",
  styleUrls: ["./table-row-student-exam-results.component.css"]
})
export class TableRowStudentExamResultsComponent implements OnInit {
  @Input() exam: Exam = {} as Exam;
  @Input() i: number;
  constructor() {}

  ngOnInit() {}
}
