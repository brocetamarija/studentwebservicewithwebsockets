import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowStudentExamResultsComponent } from './table-row-student-exam-results.component';

describe('TableRowStudentExamResultsComponent', () => {
  let component: TableRowStudentExamResultsComponent;
  let fixture: ComponentFixture<TableRowStudentExamResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowStudentExamResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowStudentExamResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
