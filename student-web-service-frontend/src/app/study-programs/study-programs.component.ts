import { Component, OnInit } from "@angular/core";
import { StudyProgramService } from "../services/StudyProgram/study-program.service";
import { StudyProgram } from "../shared/model/StudyProgram";
import { ActivatedRoute } from "@angular/router";
import { TypeOfStudyService } from "../services/TypeOfStudy/type-of-study.service";
import { TypeOfStudy } from "../shared/model/TypeOfStudy";

@Component({
  selector: "app-study-programs",
  templateUrl: "./study-programs.component.html",
  styleUrls: ["./study-programs.component.css"]
})
export class StudyProgramsComponent implements OnInit {
  studyPrograms: StudyProgram[];
  totalPages: number[];
  activePage: number = 0;
  addStudyProgram: boolean = true;
  addForm: boolean = false;
  studyProgram: StudyProgram = {} as StudyProgram;
  id: number;
  studyTypes: TypeOfStudy[];
  studyTypeID: number = 1;
  studyType: TypeOfStudy = {} as TypeOfStudy;
  pageNum: number = 0;
  size: number = 3;

  constructor(
    private studyProgramService: StudyProgramService,
    private route: ActivatedRoute,
    private typeOfStudyService: TypeOfStudyService
  ) {}

  ngOnInit() {
    this.typeOfStudyService.getAll().subscribe(data => {
      this.studyTypes = data;
    });

    this.getByStudyTypePageable(this.studyTypeID, this.pageNum, this.size);
    this.getStudyType(this.studyTypeID);
  }

  getByStudyTypePageable(id: number, pageNum: number, size: number) {
    this.studyProgramService
      .getByStudyTypePageable(id, pageNum, size)
      .subscribe(data => {
        this.studyPrograms = data.body;
        this.totalPages = Array(Number.parseInt(data.headers.get("totalPages")))
          .fill(0)
          .map((x, i) => i);
      });
  }

  nextPage(event) {
    let pageNum = event.target.name;
    document
      .getElementById(this.activePage.toString())
      .classList.remove("active");
    this.activePage = pageNum;
    this.getByStudyTypePageable(this.studyTypeID, pageNum, this.size);
    document.getElementById(this.activePage.toString()).classList.add("active");
  }

  openAddForm() {
    this.addStudyProgram = true;
    this.addForm = true;
    this.studyProgram.typeOfStudyDTO = {} as TypeOfStudy;
  }

  addNewRow(event) {
    if (this.studyPrograms.length < 3) {
      this.studyPrograms.push(event);
    } else {
      this.getByStudyTypePageable(this.studyTypeID, this.pageNum, this.size);
    }
  }

  closeAddForm(event) {
    this.addForm = event;
    this.studyProgram = {} as StudyProgram;
  }

  onEditRow(event) {
    this.studyProgram = event;
    let index = this.studyPrograms.findIndex(
      item => item.id == this.studyProgram.id
    );
    this.studyPrograms[index] = this.studyProgram;
  }

  onDeleteRow(event) {
    this.id = event;
    this.studyProgramService.delete(this.id).subscribe(data => {
      this.studyPrograms = this.studyPrograms.filter(x => x.id !== this.id);
      this.getByStudyTypePageable(this.studyTypeID, this.pageNum, this.size);
    });
  }

  openEdit(event) {
    this.id = event;
    let clone = this.studyPrograms.find(x => x.id === this.id);
    this.studyProgram = JSON.parse(JSON.stringify(clone));
    this.addForm = true;
    this.addStudyProgram = false;
  }

  onChange(id: number) {
    this.studyTypeID = id;
    this.getByStudyTypePageable(this.studyTypeID, this.pageNum, this.size);
    this.getStudyType(this.studyTypeID);
  }

  getStudyType(id: number) {
    this.typeOfStudyService.getOne(id).subscribe(data => {
      this.studyType = data;
    });
  }
}
