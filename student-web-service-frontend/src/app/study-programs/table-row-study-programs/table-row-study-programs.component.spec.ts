import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRowStudyProgramsComponent } from './table-row-study-programs.component';

describe('TableRowStudyProgramsComponent', () => {
  let component: TableRowStudyProgramsComponent;
  let fixture: ComponentFixture<TableRowStudyProgramsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRowStudyProgramsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRowStudyProgramsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
