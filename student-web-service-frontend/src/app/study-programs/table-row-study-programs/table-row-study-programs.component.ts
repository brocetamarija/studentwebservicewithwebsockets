import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { StudyProgram } from "src/app/shared/model/StudyProgram";
import { Router } from "@angular/router";

@Component({
  selector: "tr[app-table-row-study-programs]",
  templateUrl: "./table-row-study-programs.component.html",
  styleUrls: ["./table-row-study-programs.component.css"]
})
export class TableRowStudyProgramsComponent implements OnInit {
  @Input() studyProgram: StudyProgram = {} as StudyProgram;
  @Input() i: number;
  @Input() param: string;
  @Output() deleteRow = new EventEmitter();
  @Output() editRow = new EventEmitter();
  id: number;
  constructor(private router: Router) {}

  ngOnInit() {}

  delete(event) {
    this.id = +event.target.value;
    this.deleteRow.emit(this.id);
  }

  edit(event) {
    this.id = +event.target.value;
    this.editRow.emit(this.id);
  }

  getStudents(event) {
    this.id = +event.target.value;
    this.router.navigate(["/students", this.id]);
  }


  getCourses(event) {
    this.id = +event.target.value;
    this.router.navigate(["/courses", this.id]);
  }

}
