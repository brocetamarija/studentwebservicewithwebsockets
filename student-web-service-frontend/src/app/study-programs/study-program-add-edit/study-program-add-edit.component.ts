import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { StudyProgram } from "src/app/shared/model/StudyProgram";
import { StudyProgramService } from "src/app/services/StudyProgram/study-program.service";
import { TypeOfStudyService } from "src/app/services/TypeOfStudy/type-of-study.service";
import { TypeOfStudy } from "src/app/shared/model/TypeOfStudy";

@Component({
  selector: "app-study-program-add-edit",
  templateUrl: "./study-program-add-edit.component.html",
  styleUrls: ["./study-program-add-edit.component.css"]
})
export class StudyProgramAddEditComponent implements OnInit {
  @Output() close = new EventEmitter();
  @Output() newStudyProgram = new EventEmitter<StudyProgram>();
  @Output() editStudyProgram = new EventEmitter<StudyProgram>();
  @Input() studyProgram: StudyProgram = {} as StudyProgram;
  @Input() addStudyProgram: boolean = true;
  @Input() studyType: TypeOfStudy = {} as TypeOfStudy;
  addForm: boolean;
  studyTypes: TypeOfStudy[];

  constructor(private studyProgramService: StudyProgramService) {}

  ngOnInit() {}

  closeAddForm() {
    this.addForm = false;
    this.close.emit(this.addForm);
  }

  submit() {
    if (this.addStudyProgram) {
      this.studyProgram.typeOfStudyDTO = this.studyType;
      this.studyProgramService.create(this.studyProgram).subscribe(data => {
        this.newStudyProgram.emit(data);
        this.closeAddForm();
      });
    } else {
      this.studyProgramService.update(this.studyProgram).subscribe(data => {
        this.editStudyProgram.emit(data);
        this.closeAddForm();
      });
    }
  }
}
