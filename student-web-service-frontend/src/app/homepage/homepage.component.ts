import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { User } from "../shared/model/User";
import { Student } from "../shared/model/Student";
import { Teacher } from "../shared/model/Teacher";
import { AuthenticationService } from "../services/Login/Authentication/authentication.service";
import { StudentService } from "../services/Student/student.service";
import { TeacherService } from "../services/Teacher/teacher.service";
import { UserService } from "../services/User/user.service";
import { WebSocketApiService } from "../services/WebSocket/web-socket-api.service";
import * as Stomp from "stompjs";
import * as SockJS from "sockjs-client";

@Component({
  selector: "app-homepage",
  templateUrl: "./homepage.component.html",
  styleUrls: ["./homepage.component.css"]
})
export class HomepageComponent implements OnInit {
  logged;
  loggedRole;
  isAdmin: boolean = false;
  isStudent: boolean = false;
  isTeacher: boolean = false;
  user: User = {} as User;
  student: Student = {} as Student;
  teacher: Teacher = {} as Teacher;

  constructor(
    private authenticationService: AuthenticationService,
    private studentService: StudentService,
    private teacherService: TeacherService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.logged = this.authenticationService.getCurrentUser();
    this.loggedRole = this.logged.roles[0];
    this.userService.getByUsername(this.logged.username).subscribe(data => {
      this.user = data;
      if (this.loggedRole == "ROLE_STUDENT") {
        this.studentService
          .getByUsername(this.logged.username)
          .subscribe(data => {
            this.student = data;
            this.isStudent = true;
          });
      } else if (this.loggedRole == "ROLE_TEACHER") {
        this.teacherService
          .getByUsername(this.logged.username)
          .subscribe(data => {
            this.teacher = data;
            this.isTeacher = true;
          });
      } else {
        this.isAdmin = true;
      }
    });
  }
}
