import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthenticationService } from "../services/Login/Authentication/authentication.service";
import Login from "../shared/model/Login";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  imgPath = "assets/images/logo.png";
  loginModel: Login = {} as Login;


  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}

  ngOnInit() {}

  submit() {
    this.authenticationService.login(this.loginModel).subscribe(
      data => {
        this.router.navigate(["/home"]);
      },
      error => {
        alert("Uneli ste pogresne podatke");
      }
    );
  }
}
